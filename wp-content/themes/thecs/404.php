<?php
	get_header(); 
	?>
	<div class="page-other p404">
		<div class="text">
			<?php
				$thecs_not_found_page_msg = thecs_get_theme_option('thecs_not_found_page_msg', '');
				if($thecs_not_found_page_msg == '')
				{
					?>
					<h1 class="large"><?php esc_html_e('Whoops!', 'thecs'); ?></h1>
					<h4><?php esc_html_e('We couldn&rsquo;t find the page you were looking for!', 'thecs'); ?></h4>
					<?php
				}
				else
				{
					thecs_kses_content( $thecs_not_found_page_msg );
				}
			?>
		</div>
	</div>
<?php get_footer(); ?>
