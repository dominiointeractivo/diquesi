<?php
 /**
 * Theme directory and url
 */
 
define( 'THECS_THEME_DIR', untrailingslashit( get_template_directory() ) );
define( 'THECS_THEME_URL', untrailingslashit( get_template_directory_uri() ) );
define( 'THECS_STYLE_DIR', untrailingslashit( get_stylesheet_directory() ) );
define( 'THECS_STYLE_URL', untrailingslashit( get_stylesheet_directory_uri() ) );

$thecs_upload_directory = wp_upload_dir();
define( 'THECS_UPLOAD_DIR', untrailingslashit( $thecs_upload_directory['basedir'] ) );
define( 'THECS_UPLOAD_URL', untrailingslashit( $thecs_upload_directory['baseurl'] ) );

define( 'THECS_THEME_NAME', 'thecs' );

if( !function_exists('thecs_log') )
{
	function thecs_log( $msg )
	{
		if( !defined('WP_DEBUG') || !WP_DEBUG )
		{
			return;
		}

		global $wp_filesystem;
		WP_Filesystem();

		$file_path = THECS_THEME_DIR . '/log.txt';
		$file_str = $wp_filesystem->get_contents( $file_path );
		$wp_filesystem->put_contents( $file_path, $file_str.PHP_EOL.$msg );
	}
}

require_once THECS_THEME_DIR . '/inc/template-tags.php';
require_once THECS_THEME_DIR . '/inc/aq_resizer.php';

require_once THECS_THEME_DIR . '/inc/theme/options/theme.options.php';

if( is_admin() )
{
	//--------------    plugins    ---------------
	require_once THECS_THEME_DIR . '/inc/class-tgm-plugin-activation.php';
	require_once THECS_THEME_DIR . '/inc/theme/register-plugins.php';

	require_once THECS_THEME_DIR . '/inc/theme/options/post.options.php';
	require_once THECS_THEME_DIR . '/inc/theme/options/project.options.php';
	require_once THECS_THEME_DIR . '/inc/theme/options/product.options.php';
	require_once THECS_THEME_DIR . '/inc/theme/options/page.options.php';
	require_once THECS_THEME_DIR . '/inc/theme/options/shortcode.options.php';
	require_once THECS_THEME_DIR . '/inc/theme/option.manager.php';
	require_once THECS_THEME_DIR . '/inc/theme/custom-admin-page.php';
	require_once THECS_THEME_DIR . '/inc/theme/functions/ajax-options-functions.php';
	require_once THECS_THEME_DIR . '/inc/theme/functions/shortcode-admin-functions.php';
	require_once THECS_THEME_DIR . '/inc/theme/demo-install.php';
}

require_once THECS_THEME_DIR . '/inc/theme/enqueue-scripts.php';
require_once THECS_THEME_DIR . '/inc/theme/functions/comment-functions.php';
require_once THECS_THEME_DIR . '/inc/theme/functions/content-functions.php';
require_once THECS_THEME_DIR . '/inc/theme/functions/image-functions.php';
require_once THECS_THEME_DIR . '/inc/theme/functions/parse-data-functions.php';

if( class_exists( 'WooCommerce' ) )
{
	define( 'THECS_HAS_WOOCOMMERCE', true );

	require_once THECS_THEME_DIR . '/inc/theme/functions/woocommerce-functions.php';
}
else
{
	define( 'THECS_HAS_WOOCOMMERCE', false );
}


//--------------------------------------------


if ( ! isset( $content_width ) )
{
	$content_width = 1024;
}


/**
 * Only works in WordPress 4.7 or later.
 */
if( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) )
{
	require THECS_THEME_DIR . '/inc/back-compat.php';
}


/**
 * Theme setup
 */
add_action( 'after_setup_theme', 'thecs_theme_setup' );
if(!function_exists('thecs_theme_setup'))
{
	function thecs_theme_setup()
	{
		load_theme_textdomain( 'thecs', THECS_THEME_DIR . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );
		
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu', 'thecs' )
		));

		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
		));

		add_theme_support( 'post-formats', array() );

		add_theme_support( 'custom-header', array() );

		add_theme_support( 'custom-background', array() );

		add_editor_style( array( 'css/editor-style.css', thecs_editor_fonts_url() ) );

		//------------   custom styles   -------------
		require_once THECS_THEME_DIR . '/inc/theme/styles.php';
	}
}


/**
 * Add specific CSS class by filter.
 **/
add_filter( 'body_class', 'thecs_handle_body_class');
function thecs_handle_body_class( $classes )
{
	//1: light, 2: dark
	$site_style = thecs_get_theme_option('thecs_site_style', '1');
	$site_style_class = $site_style == '1' ? 'site-light' : 'site-dark';

	$site_big_gap = thecs_get_theme_option('thecs_site_big_gap', '0');
	$site_big_gap_class = $site_big_gap == '1' ? 'site-gap-big' : '';

	$full_scroll_class = thecs_get_theme_option('thecs_full_scroll', '1') === '1' ? 'pths-bar-full' : '';

	$classes = array_merge( $classes, array( $site_style_class, $site_big_gap_class, $full_scroll_class ) );

    return $classes;
}


/**
 * sort by asc index
 */
if (!function_exists( 'thecs_sort_asc_by_index' ))
{
	function thecs_sort_asc_by_index($obj1, $obj2) 
	{
		return $obj1['index'] - $obj2['index'];
	}
}


/**
 * sort by desc index
 */
if (!function_exists( 'thecs_sort_desc_by_index' ))
{
	function thecs_sort_desc_by_index($obj1, $obj2) 
	{
		return $obj2['index'] - $obj1['index'];
	}
}


/**
 * Check to flush rewrite rules
 */
add_action( 'admin_init', 'thecs_load_plugin' );
function thecs_load_plugin() 
{
    if ( is_admin() && get_option( 'thecs_activated_plugin', '' ) === 'Thecs-Projects' )
    {
        delete_option( 'thecs_activated_plugin' );

        flush_rewrite_rules();
    }
}


/**
 * Set post type for search
 */
add_filter('pre_get_posts','thecs_exclude_page_from_search');
if(!function_exists('thecs_exclude_page_from_search'))
{
	function thecs_exclude_page_from_search($query)
	{
		if ($query->is_search) {
			$query->set('post_type', array( 'post', 'project' ));
		}
		return $query;
	}
}


/**
 * Add custom query variables
 */
add_filter( 'query_vars', 'thecs_add_query_vars_filter' );
if(!function_exists('thecs_add_query_vars_filter'))
{
	function thecs_add_query_vars_filter( $vars )
	{
		$vars[] = '_preset';
		return $vars;
	}
}


/**
 * Get google font link for editor
 */
if(!function_exists('thecs_editor_fonts_url'))
{
	function thecs_editor_fonts_url()
	{
		$fonts_url = '';
		$fonts     = array();
		$subsets   = 'latin,latin-ext';

		if ( 'off' !== esc_html_x( 'on', 'Noto Sans font: on or off', 'thecs' ) ) {
			$fonts[] = 'Noto Sans:400italic,700italic,400,700';
		}

		if ( 'off' !== esc_html_x( 'on', 'Noto Serif font: on or off', 'thecs' ) ) {
			$fonts[] = 'Noto Serif:400italic,700italic,400,700';
		}

		if ( 'off' !== esc_html_x( 'on', 'Inconsolata font: on or off', 'thecs' ) ) {
			$fonts[] = 'Inconsolata:400,700';
		}

		$subset = esc_html_x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'thecs' );

		if ( 'cyrillic' == $subset ) {
			$subsets .= ',cyrillic,cyrillic-ext';
		} elseif ( 'greek' == $subset ) {
			$subsets .= ',greek,greek-ext';
		} elseif ( 'devanagari' == $subset ) {
			$subsets .= ',devanagari';
		} elseif ( 'vietnamese' == $subset ) {
			$subsets .= ',vietnamese';
		}

		if ( $fonts ) {
			$fonts_url = add_query_arg( array(
				'family' => urlencode( implode( '|', $fonts ) ),
				'subset' => urlencode( $subsets ),
			), '//fonts.googleapis.com/css' );
		}

		return $fonts_url;
	}
}


/**
 * Initialize widgets
 */
add_action('widgets_init', 'thecs_widgets_init');
if(!function_exists('thecs_widgets_init'))
{
	function thecs_widgets_init() 
	{
		register_sidebar( array(
			'name'          => esc_html__( 'Right Side Widget Area', 'thecs' ),
			'id'            => 'sidebar-right',
			'description'   => esc_html__( 'Add widgets here to appear in your sidebar (right).', 'thecs' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<div class="widget-title">',
			'after_title'   => '</div>'
		));

		register_sidebar( array(
			'name'          => esc_html__( 'WooCommerce Widget Area', 'thecs' ),
			'id'            => 'sidebar-woocommerce',
			'description'   => esc_html__( 'Add WooCommerce widgets here.', 'thecs' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
		));
	}
}


/**
 * Parse part path
 */
if( !function_exists('thecs_parse_part_path') )
{
	function thecs_parse_part_path( $path )
	{
		if( file_exists( THECS_STYLE_DIR . $path . '.php' ) )
		{
			return THECS_STYLE_DIR . $path . '.php';
		}
		else
		{
			return THECS_THEME_DIR . $path . '.php';
		}
	}
}