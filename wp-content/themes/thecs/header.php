<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php
		if ( is_singular() && pings_open( get_queried_object() ) )
		{
			?><link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"><?php
		}

		wp_head();
	?>
</head>
<?php
	$thecs_site_bg_url = '';
	global $thecs_g_site_bg;

	if( isset( $thecs_g_site_bg ) && $thecs_g_site_bg )
	{
		$thecs_img_obj = thecs_get_image(array(
			'rid' => $thecs_g_site_bg,
			'max_width' => 2200,
		));
		
		if( $thecs_img_obj )
		{
			$thecs_site_bg_url = $thecs_img_obj['thumb'];
		}
	}

	$thecs_main_color = thecs_get_theme_option('thecs_main_color', '#ff0000');

	$thecs_site_style = thecs_get_theme_option('thecs_site_style', '1') == '1' ? 'light' : 'dark';

?>
<body <?php body_class(); ?> data-color="<?php echo esc_attr( $thecs_main_color ); ?>">
	<div class="loader-layer"><div></div></div>
	<div class="close-layer"></div>
	<?php
		if($thecs_site_bg_url)
		{
			?><div class="site-bg bg-full" data-bg="<?php echo esc_url( $thecs_site_bg_url ); ?>"></div><?php
		}
		
		$thecs_logo = THECS_THEME_URL.'/data/images/logo-'.$thecs_site_style.'.png';
		$thecs_logo_retina = THECS_THEME_URL.'/data/images/logo-'.$thecs_site_style.'-retina.png';
		$thecs_logo_w = 50;
		$thecs_logo_h = 50;

		$thecs_logo_id = thecs_get_theme_option('thecs_logo', '');
		$thecs_img_obj = thecs_get_image(array(
			'rid' => $thecs_logo_id,
			'max_height' => 240,
		));
		if( $thecs_img_obj )
		{
			$thecs_logo = $thecs_img_obj['thumb'];
			$thecs_logo_w = $thecs_img_obj['thumb_width'];
			$thecs_logo_h = $thecs_img_obj['thumb_height'];

			$thecs_logo_retina = '';//if user upload logo, then remove the default retina logo.
		}

		$thecs_logo_retina_id = thecs_get_theme_option('thecs_logo_retina', '');
		$thecs_img_obj = thecs_get_image(array(
			'rid' => $thecs_logo_retina_id
		));
		if( $thecs_img_obj )
		{
			$thecs_logo_retina = $thecs_img_obj['thumb'];
		}

		$thecs_header_type = thecs_get_theme_option('thecs_header_style', '4');
		$thecs_header_class = 'style-0'.$thecs_header_type;

		$thecs_header_bg_color = '';

		if( $thecs_header_type == '1' )
		{
			$thecs_show_menu_bg = thecs_get_theme_option('thecs_show_menu_bg', '0');
			if( $thecs_show_menu_bg == '1' )
			{
				$thecs_header_class .= ' style-01-02';

				if( thecs_get_theme_option('thecs_header_dark', '0') === '1' )
				{
					$thecs_header_class .= ' dark';
				}

				$thecs_header_bg_color = thecs_get_theme_option('thecs_header_bg_color', '');
			}
		}

		$thecs_hidden_menu_btn_type = thecs_get_theme_option('thecs_hidden_menu_btn_type', '1');
		$thecs_enable_hidden_menu = thecs_get_theme_option('thecs_enable_hidden_menu', '1');
		
	?>
	<header class="<?php echo esc_attr( $thecs_header_class ); ?>" data-color="<?php echo esc_attr( $thecs_header_bg_color ); ?>">
		<div class="wrap">
			<div class="logo">
				<a href="<?php echo esc_url( home_url('/') );?>">
					<img alt="logo" src="<?php echo esc_url($thecs_logo); ?>" data-retina="<?php echo esc_url($thecs_logo_retina); ?>" width="<?php echo esc_attr($thecs_logo_w); ?>" height="<?php echo esc_attr($thecs_logo_h); ?>">
				</a>
			</div>
			<?php
				thecs_output_menu( 'primary', 'nav', 'main-menu-list', 'menu-list', 'main-menu', 3, true );
			?>
			<div class="header-right">
				<?php
					if( is_active_sidebar('sidebar-right') && $thecs_enable_hidden_menu === '1' )
					{
						?><i class="call-hidden-menu btn style-0<?php echo esc_attr( $thecs_hidden_menu_btn_type ); ?>"></i><?php
					}

					if( THECS_HAS_WOOCOMMERCE )
					{
						?><div class="widget_shopping_cart_content"></div><?php
					}
				?>
				<?php
					if( thecs_get_theme_option('thecs_show_search_button', '1') == '1' )
					{
						global $thecs_g_searth_form_type;
						$thecs_g_searth_form_type = '1';
						
						get_search_form();
					}
				?>
			</div>
		</div>
	</header>
	<div class="m-header">
		<div class="m-logo">
			<img alt="logo" src="<?php echo esc_url($thecs_logo); ?>" data-retina="<?php echo esc_url($thecs_logo_retina); ?>" width="<?php echo esc_attr($thecs_logo_w); ?>" height="<?php echo esc_attr($thecs_logo_h); ?>">
		</div>

		<i class="call-m-left btn"></i>
		<?php
			if( is_active_sidebar('sidebar-right') && $thecs_enable_hidden_menu === '1' )
			{
				?><i class="call-m-right btn"></i><?php
			}

			if( THECS_HAS_WOOCOMMERCE )
			{
				?><i class="call-cart btn"><span class="cart-num">0</span></i><?php
			}
		?>
		<div class="m-side-group">

			<i class="close-m-left btn"></i>
			<i class="close-m-right btn"></i>
			<i class="close-m-cart btn"></i>

			<div class="m-left m-side">
				<?php
					if( thecs_get_theme_option('thecs_show_search_button', '1') == '1' )
					{
						global $thecs_g_searth_form_type;
						$thecs_g_searth_form_type = '2';
						
						get_search_form();
					}

					thecs_output_menu( 'primary', 'nav', 'm-main-menu-list', 'm-menu-list', 'm-main-menu', 3, false );
				?>
			</div>
		</div>
	</div>
	<div class="main-content">
	<?php do_action( 'thecs_action_header_ready' ); ?>