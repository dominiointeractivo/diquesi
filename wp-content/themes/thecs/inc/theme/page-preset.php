<?php

	$thecs_page_preset = array(
		'shop' => array(
			'101' => array(
				'type' => '1', //1, 2
				'list_size' => '1', //1, 2
				'disperse' => '0', //0, 1
				'thumb_size' => '1', //1, 2, 3, 4
				'row' => '1', //1, 1-mini, 2, 3
				'boxed' => '1', //0, 1
				'item_gap' => '1', //0, 1
				'num' => -1, 
			),
			'102' => array(
				'type' => '2', //1, 2
				'list_size' => '1', //1, 2
				'disperse' => '0', //0, 1
				'thumb_size' => '1', //1, 2, 3, 4
				'row' => '1', //1, 1-mini, 2, 3
				'boxed' => '1', //0, 1
				'item_gap' => '1', //0, 1
				'num' => -1, 
			),
		),
	);

?>