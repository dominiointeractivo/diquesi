<?php
	/**
	 * Use the TGM Plugin Activator class to notify the user to install the required/recommended Plugin
	 */
	add_action( 'tgmpa_register', 'thecs_register_required_plugins' );
	function thecs_register_required_plugins() {
	    $plugins = array(
	        array(
	            'name'         => esc_html__('Thecs Projects', 'thecs'),
	            'slug'         => 'thecs-projects',
	            'source'       => THECS_THEME_DIR . '/inc/plugins/thecs-projects.zip',
	            'external_url' => THECS_THEME_DIR . '/inc/plugins/thecs-projects.zip',
	            'required'     => true,
	            'version'      => '1.0.0'
	        ),

	        array(
	            'name'         => esc_html__('Thecs Shortcodes', 'thecs'),
	            'slug'         => 'thecs-shortcodes',
	            'source'       => THECS_THEME_DIR . '/inc/plugins/thecs-shortcodes.zip',
	            'external_url' => THECS_THEME_DIR . '/inc/plugins/thecs-shortcodes.zip',
	            'required'     => true,
	            'version'      => '1.0.7'
	        ),

	        array(
	            'name'         => esc_html__('Thecs Menu Image', 'thecs'),
	            'slug'         => 'thecs-menu-image',
	            'source'       => THECS_THEME_DIR . '/inc/plugins/thecs-menu-image.zip',
	            'external_url' => THECS_THEME_DIR . '/inc/plugins/thecs-menu-image.zip',
	            'required'     => true,
	            'version'      => '1.0.0'
	        ),

	        array(
	            'name'         => esc_html__('Thecs Items Order', 'thecs'),
	            'slug'         => 'thecs-items-order',
	            'source'       => THECS_THEME_DIR . '/inc/plugins/thecs-items-order.zip',
	            'external_url' => THECS_THEME_DIR . '/inc/plugins/thecs-items-order.zip',
	            'required'     => false,
	            'version'      => '1.0.0'
	        ),

	        array(
	            'name'         => esc_html__('Thecs Custom Font', 'thecs'),
	            'slug'         => 'thecs-custom-font',
	            'source'       => THECS_THEME_DIR . '/inc/plugins/thecs-custom-font.zip',
	            'external_url' => THECS_THEME_DIR . '/inc/plugins/thecs-custom-font.zip',
	            'required'     => false,
	            'version'      => '1.0.0'
	        ),

	        array(
	            'name'         => esc_html__('Contact Form 7', 'thecs'),
	            'slug'         => 'contact-form-7',
	            'required'     => true,
	            'version'      => '4.7'
	        ),

	        array(
	            'name'         => esc_html__('Thecs Custom Script', 'thecs'),
	            'slug'         => 'thecs-custom-script',
	            'source'       => THECS_THEME_DIR . '/inc/plugins/thecs-custom-script.zip',
	            'external_url' => THECS_THEME_DIR . '/inc/plugins/thecs-custom-script.zip',
	            'required'     => false,
	            'version'      => '1.0.0'
	        ),

	        array(
	            'name'         => esc_html__('One Click Demo Import', 'thecs'),
	            'slug'         => 'one-click-demo-import',
	            'required'     => false,
	            'version'      => '2.4.0'
	        ),
	    );

		$config = array(
	        'id'           => 'thecs-tgmpa',
	        'default_path' => '',
	        'menu'         => 'tgmpa-install-plugins',
	        'parent_slug'  => 'themes.php',
	        'capability'   => 'edit_theme_options',
	        'has_notices'  => true,
	        'dismissable'  => true,
	        'dismiss_msg'  => '',
	        'is_automatic' => false,
	        'message'      => ''
	    );

		tgmpa( $plugins, $config );
	}
?>