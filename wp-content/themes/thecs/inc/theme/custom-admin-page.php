<?php

/**
 * Register custom menu page
 */
add_action('admin_menu', 'thecs_register_custom_menu_page');
if(!function_exists('thecs_register_custom_menu_page'))
{
	function thecs_register_custom_menu_page()
	{
		add_action('admin_init', 'thecs_register_options');

		add_theme_page(esc_html__('Theme Options', 'thecs'), esc_html__('Theme Options', 'thecs'), 'administrator', 'theme_options', 'thecs_output_options_page');
	}
}


/**
 * Output theme-options page
 */
if(!function_exists('thecs_output_options_page'))
{
	function thecs_output_options_page()
	{
		require_once THECS_THEME_DIR . '/inc/theme/theme-options-page.php';
	}
}


/**
 * Register options
 */
function thecs_register_options()
{
	register_setting('theme_options', Thecs_Theme_Options::$options_key);
}

?>