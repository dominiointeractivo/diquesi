<?php

/**
 * Modify comment html structures
 */
if(!function_exists('thecs_comment_callback'))
{
	function thecs_comment_callback($comment, $args, $depth)
	{
		$GLOBALS['comment'] = $comment;
		extract($args, EXTR_SKIP);

		if ( 'div' == $args['style'] ) {
			$tag = 'div';
			$add_below = 'comment';
		} else {
			$tag = 'li';
			$add_below = 'div-comment';
		}

		$comment_class = empty( $args['has_children'] ) ? '' : 'parent';
		$avatar = '';
		if ( $args['avatar_size'] != 0 )
		{
			$avatar = get_avatar( $comment, $args['avatar_size'] );
		}

		if( $avatar == '' )
		{
			$comment_class .= ' no-img';
		}

		?>

		<<?php echo esc_html($tag) ?> <?php comment_class($comment_class) ?> id="comment-<?php comment_ID(); ?>">
		<?php
			if ( 'div' != $args['style'] )
			{
				?><div id="div-comment-<?php comment_ID(); ?>" class="comment-body"><?php
			}
		?>
		<div class="comment-author">
			<?php 
				if ( $args['avatar_size'] != 0 )
				{
					?><div class="img"><?php thecs_kses_content( $avatar ); ?></div><?php
				}
			?>
			<div class="text">
				<cite class="fn"><?php echo get_comment_author_link(); ?></cite> 
				<div class="comment-meta commentmetadata"><?php echo get_comment_date(); ?></div>
			</div>
		</div>
		<div class="comment-area">
			<?php comment_text(); ?>
		</div>
		<?php 
			if ($comment->comment_approved == '0')
			{
				?><em class="comment-awaiting-moderation"><?php esc_html_e('Your comment is awaiting moderation.', 'thecs') ?></em><?php
			};
		?>

		<div class="reply">
			<?php 
				comment_reply_link( array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'])) );
			?>
		</div>

		<?php 
		if ( 'div' != $args['style'] )
		{
			?></div><?php
		}
	}
}


/**
 * Custom comment form
 */
add_filter('comment_form_default_fields','thecs_custom_comment_form');
if(!function_exists('thecs_custom_comment_form'))
{
	function thecs_custom_comment_form()
	{
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? ' aria-required="true"' : '' );
		
		$fields['author'] = '<p class="comment-form-author">' . '<input id="author" name="author" type="text" value="'.
			esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' placeholder="'.($req ? '*' : ''). esc_html__('Name', 'thecs') . '"' . ' /></p>';
		
		$fields['email'] = '<p class="comment-form-email">' . '<input id="email" name="email" type="email" value="' .
			esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' placeholder="'.($req ? '*' : ''). esc_html__('Email', 'thecs') . '"' . ' /></p>';
			
		$fields['url'] = '<p class="comment-form-url">' . '<input id="url" name="url" type="text" value="' .
			esc_attr( $commenter['comment_author_url'] ) . '" size="30"'. ' placeholder="'. esc_html__('Website', 'thecs') . '"' . ' /></p>';
		return $fields;
	}
}


/**
 * Genarete comment navigation
 */
if (!function_exists( 'thecs_comment_nav' ))
{
	function thecs_comment_nav() 
	{
		if( get_comment_pages_count() > 1 && get_option( 'page_comments' ) )
		{
			?>
			<div class="navigation comment-navigation" role="navigation">
				<div class="nav-links">
					<?php
						if ( $prev_link = get_previous_comments_link( esc_html__( 'Older Comments', 'thecs' ) ) )
						{
							printf( '<div class="nav-previous">%s</div>', $prev_link );
						}

						if ( $next_link = get_next_comments_link( esc_html__( 'Newer Comments', 'thecs' ) ) )
						{
							printf( '<div class="nav-next">%s</div>', $next_link );
						}
					?>
				</div>
			</div>
			<?php
		}
	}
}

?>