<?php

/**
 * WP-AJAX parse shortcode
 */
add_action('wp_ajax_parse_shortcode', 'thecs_parse_shortcode');
function thecs_parse_shortcode()
{
	$result = array('success'=>0, 'data'=>'');

	if(is_admin())
	{
		if( isset($_POST['shortcode']) )
		{
			$text = $_POST['shortcode'];
			if( $text )
			{
				$result['success'] = 1;
				$result['data'] = do_shortcode(shortcode_unautop(wp_specialchars_decode(stripslashes($text), ENT_QUOTES)));
			}
		}
	}

	exit( json_encode($result) );
}


/**
 * WP-AJAX shortcode preview
 */
add_action('wp_ajax_shortcode_preview', 'thecs_shortcode_preview');
function thecs_shortcode_preview()
{
	if(is_admin())
	{
		require_once WP_PLUGIN_DIR . '/thecs-shortcodes/editor/shortcode_preview.php';
	}
	exit();
}


/**
 * WP-AJAX shortcode preview warnning if user no login
 */
add_action('wp_ajax_nopriv_shortcode_preview', 'thecs_shortcode_preview_nopriv');
function thecs_shortcode_preview_nopriv()
{
	esc_html_e('You don\'t have access to this page, login first!', 'thecs');
	exit();
}


/**
 * WP-AJAX page-builder
 */
add_action('wp_ajax_pagebuilder_core', 'thecs_pagebuilder_core');
function thecs_pagebuilder_core()
{
	if(is_admin())
	{
		require_once WP_PLUGIN_DIR . '/thecs-shortcodes/editor/page-builder/pb_core.php';
	}
	exit();
}


/**
 * WP-AJAX page-builder warnning if user no login
 */
add_action('wp_ajax_nopriv_pagebuilder_core', 'thecs_pagebuilder_core_nopriv');
function thecs_pagebuilder_core_nopriv()
{
	esc_html_e('You don\'t have access to this page, login first!', 'thecs');
	exit();
}


/**
 * Build shortcode
 **/
function thecs_build_shortcode( $tag, $attr, $content = '' )
{
	$shortcode = '['.$tag;
	foreach($attr as $key=>$value)
	{
		$shortcode .= ' '.$key.'="'.$value.'"';
	}
	$shortcode .= ']';
	$shortcode .= $content;
	$shortcode .= '[/'.$tag.']';

	$shortcode = htmlspecialchars($shortcode);
	return $shortcode;
}

?>