<?php

/**
 * Theme setup
 */
add_action( 'after_setup_theme', 'thecs_wc_theme_setup' );
if( !function_exists('thecs_wc_theme_setup') )
{
	function thecs_wc_theme_setup()
	{
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );

		add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
	}
}


/**
 * Enqueue scripts for woocommerce
 */
add_action( 'admin_enqueue_scripts', 'thecs_wc_admin_enqueue_scripts', 11 );
if( !function_exists('thecs_wc_admin_enqueue_scripts') )
{
	function thecs_wc_admin_enqueue_scripts()
	{
		$page_id = get_the_id();
		$page_types = array( 'shop', 'cart', 'checkout', 'myaccount', 'terms' );

		$shop_page_ids = array( 
			wc_get_page_id( $page_types[0] ),
			wc_get_page_id( $page_types[1] ),
			wc_get_page_id( $page_types[2] ),
			wc_get_page_id( $page_types[3] ),
			wc_get_page_id( $page_types[4] ),
		);

		$index = array_search( $page_id, $shop_page_ids );
		if( $index !== false )
		{
			$admin_js = '!function(){';
			$admin_js .= 'var o = {}; window.__ptWCObject = o;';
			$admin_js .= 'o.isAdminPage = true;';
			$admin_js .= 'o.pageType = "'.esc_js( $page_types[ $index ] ).'";';
			$admin_js .= 'o.pageTypesConst = '.json_encode( $page_types ).';';
			$admin_js .= '}();';

			wp_add_inline_script(
				'thecs-custom-post-page-js', 
				$admin_js, 
				'before'
			);
		}
	}
}


/**
 * Hide template selector when page belongs to woocommerce
 */
add_filter( 'theme_page_templates', 'thecs_wc_hide_cpt_archive_templates', 10, 3 );
if( !function_exists('thecs_wc_hide_cpt_archive_templates') )
{
	function thecs_wc_hide_cpt_archive_templates( $page_templates, $class, $post )
	{
		if( !$post )
		{
			return $page_templates;
		}

		$page_types = array( 'cart', 'checkout', 'myaccount', 'terms' );

		$shop_page_ids = array( 
			wc_get_page_id( $page_types[0] ),
			wc_get_page_id( $page_types[1] ),
			wc_get_page_id( $page_types[2] ),
			wc_get_page_id( $page_types[3] ),
		);

		if ( in_array( $post->ID, $shop_page_ids ) )
		{
			$page_templates = array();
		}

		return $page_templates;
	}
}


/**
 * woocommerce related products arguments
 */
add_filter( 'woocommerce_output_related_products_args', 'thecs_output_related_products_args' );
if( !function_exists('thecs_output_related_products_args') )
{
	function thecs_output_related_products_args( $args )
	{
		if( $args )
		{
			$args['posts_per_page'] = 3;
			$args['columns'] = 3;
		}

		return $args;
	}
}


/**
 * change the posts_per_page of product page
 */
add_filter( 'loop_shop_per_page', 'thecs_loop_shop_per_page' );
if( !function_exists('thecs_loop_shop_per_page') )
{
	function thecs_loop_shop_per_page( $args )
	{
		$preset_key = get_query_var( '_preset' );
		if( $preset_key )
		{
			require THECS_THEME_DIR . '/inc/theme/page-preset.php';

			if( isset( $thecs_page_preset['shop'] ) && isset( $thecs_page_preset['shop'][$preset_key] ) )
			{
				$thecs_preset_data = $thecs_page_preset['shop'][$preset_key];

				if( isset($thecs_preset_data['num']) )
				{
					return $thecs_preset_data['num'];
				}
			}
		}

		$page_id = wc_get_page_id( 'shop' );
		$setting_params = explode( '&', get_post_meta( $page_id, '_thecs_meta_post_page', true ) );

		$setting = array();
		foreach( $setting_params as $param )
		{
			$temp_array = explode( '=', $param );
			if( count($temp_array) > 1 )
			{
				$temp_key = urldecode( $temp_array[0] );
				$setting[$temp_key] = urldecode( $temp_array[1] );
			}
		}

		return isset( $setting['shop_num'] ) ? $setting['shop_num'] : -1;
	}
}