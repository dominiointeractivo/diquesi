<?php

/**
 * Page title
 */
if(!function_exists('thecs_output_page_title'))
{
	function thecs_output_page_title()
	{
		$title_bg = thecs_get_basic_setting_property( 'title_bg', '' );
		$img_obj = thecs_get_image(array(
			'rid' => $title_bg,
			'max_height' => 1200
		));

		$title_type = thecs_get_basic_setting_property( 'title_type', '2' );
		$title = thecs_get_basic_setting_property( 'title', '' );
		$title_invert_color = thecs_get_basic_setting_property( 'title_invert_color', '0' ) === '1' ? 'color-invert' : '';
		$title_description = thecs_get_basic_setting_property( 'title_description', '' );

		if( $title_type !== '1' )
		{
			$title_invert_color = '';
		}

		if( !$title && !$title_description && !$img_obj )
		{
			return;
		}

		?>
		<div class="list-title style-0<?php echo esc_attr( $title_type ); ?> <?php echo esc_attr( $title_invert_color ); ?>">
			<?php
				if( $img_obj )
				{
					?>
					<div class="img">
						<div class="bg-full" data-bg="<?php echo esc_url( $img_obj['thumb'] ); ?>"></div>
					</div>
					<?php
				}
			?>
			<div class="title-text">
				<i class="btn title-info"></i>
				<?php
					if( $title )
					{
						?><h1><?php thecs_kses_content( $title, true, null, true ); ?></h1><?php
					}

					if( $title_description )
					{
						?><div class="intro"><?php thecs_kses_content( $title_description, true, null, true ); ?></div><?php
					}
				?>
			</div>
		</div>
		<?php
	}
}


/**
 * Generate paging navigation
 */
if(!function_exists('thecs_paging_nav'))
{
	function thecs_paging_nav($query = null, $paged = null)
	{
		if( $query === null )
		{
			global $wp_query;
			$query = $wp_query;
		}
		
		if( is_numeric($query) )
		{
			$total = $query;
		}
		else
		{
			$total = $query->max_num_pages;
		}

		if( $paged === null )
		{
			if ( get_query_var('paged') ) {
			    $paged = get_query_var('paged');
			} elseif ( get_query_var('page') ) {
			    $paged = get_query_var('page');
			} else {
			    $paged = 1;
			}
		}
		
		$big = 999999999;
		$paging_nav = paginate_links(array(
			'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
			'format' => '?paged=%#%',
			'current' => max(1, intval($paged)),
			'total' => $total,
			'prev_text' => esc_html__('<', 'thecs'),
			'next_text' => esc_html__('>', 'thecs'),
			'type' => 'array'
		));
		
		$paging_link = '';
		if($paging_nav != "")
		{
			$paging_link = '<div class="pages"><ul>';
			foreach($paging_nav as $nav_str)
			{
				$paging_link .= '<li>'.$nav_str.'</li>';
			}
			$paging_link .= '</ul></div>';
		}
		return $paging_link;
	}
}


/**
 * Build menu
 */
if(!function_exists('thecs_output_menu'))
{
	function thecs_output_menu($location, $container = '', $id = '', $class = '', $container_class = null, $depth = 3, $fallback_page = false )
	{
		$container_allowed_tags = array( 'nav', 'div', 'ul' );

		if ( !in_array( $container, $container_allowed_tags ) )
		{
			$container = 'nav';
		}

		if( $container_class == null )
		{
			$container_class = $location;
		}

		if ( has_nav_menu($location) )
		{
			wp_nav_menu(array(
				'theme_location'  => $location,
				'container'       => $container,
				'container_class' => $container_class,
				'menu_id'         => $id,
				'menu_class'      => $class,
				'depth'           => $depth
			));
		}
		else if( $fallback_page )
		{
			$show_home = true;//consider to add an option in the feature
			if($show_home)
			{
				echo '<'.$container.' class="'.esc_attr( $container_class ).' default-menu-list"><ul id="'.esc_attr( $id ).'" class="'.esc_attr( $class ).'"><li><a href="'.esc_url( home_url('/') ).'">'.esc_html__('Home', 'thecs').'</a></li>';
			}
			else	
			{
				echo '<'.$container.' class="'.esc_attr( $container_class ).' default-menu-list"><ul id="'.esc_attr( $id ).'" class="'.esc_attr( $class ).'">';
			}

			wp_list_pages('depth='.$depth.'.&title_li=');

			echo '</ul></'.$container.'>';
		}
	}
}


/**
 * Generate post socials
 */
if(!function_exists('thecs_post_socials'))
{
	function thecs_post_socials($post_id = '')
	{
		$html = '';
		$permalink = get_permalink($post_id);
		if(thecs_get_theme_option('thecs_use_facebook', '') == '1')
		{
			$link = 'http://www.facebook.com/share.php?u='.esc_url_raw($permalink);
			$html .= '<a class="share-facebook" target="_blank" href="'.esc_url($link).'"><i class="fa fa-facebook"></i></a>';
		}
		
		if(thecs_get_theme_option('thecs_use_twitter', '') == '1')
		{
			$title = rawurlencode(html_entity_decode(get_the_title($post_id)));
			$link = 'http://twitter.com/home/?status='.$title.' '.esc_url_raw($permalink);
			$html .= '<a class="share-twitter" target="_blank" href="'.esc_url($link).'"><i class="fa fa-twitter"></i></a>';
		}
		
		if(thecs_get_theme_option('thecs_use_pinterest', '') == '1')
		{
			if(has_post_thumbnail($post_id))
			{
				$img_id = get_post_thumbnail_id($post_id);
				$img_obj = wp_get_attachment_image_src($img_id, 'full');
				$imageurl = $img_obj[0];
				$title = rawurlencode(html_entity_decode(get_the_title($post_id)));
				$link = 'https://www.pinterest.com/pin/create/button/?url='.esc_url_raw($permalink).'&media='.esc_url_raw($imageurl).'&description='.$title;
				$html .= '<a class="share-pinterest" target="_blank" href="'.esc_url($link).'"><i class="fa fa-pinterest-p"></i></a>';
			}
		}
		
		if(thecs_get_theme_option('thecs_use_linkedin', '') == '1')
		{
			$title = rawurlencode(html_entity_decode(get_the_title($post_id)));
			$sitename = rawurlencode(html_entity_decode(get_bloginfo('name')));
			$summary = rawurlencode(html_entity_decode(get_post($post_id)->post_excerpt));
			
			$link = 'https://www.linkedin.com/shareArticle?mini=true&url='.esc_url_raw($permalink);
			$link .= '&title='.$title;
			$link .= '&summary='.$summary;
			$link .= '&source='.$sitename;
			$html .= '<a class="share-linkedin" target="_blank" href="'.esc_url($link).'"><i class="fa fa-linkedin"></i></a>';
		}
		
		if(thecs_get_theme_option('thecs_use_tumblr', '') == '1')
		{
			$title = rawurlencode(html_entity_decode(get_the_title($post_id)));
			$summary = rawurlencode(html_entity_decode(get_post($post_id)->post_excerpt));
			
			$link = 'http://www.tumblr.com/share/link?url='.esc_url_raw($permalink);
			$link .= '&name='.$title;
			$link .= '&description='.$summary;
			$html .= '<a class="share-tumblr" target="_blank" href="'.esc_url($link).'"><i class="fa fa-tumblr"></i></a>';
		}
		
		if(thecs_get_theme_option('thecs_use_googleplus', '') == '1')
		{
			$link = 'https://plus.google.com/share?url='.esc_url_raw($permalink);
			$html .= '<a class="share-googleplus" target="_blank" href="'.esc_url($link).'"><i class="fa fa-google-plus"></i></a>';
		}
		
		return $html;
	}
}


/**
 * A function for outputing page shortcode.
 */
if(!function_exists('thecs_output_page_shortcode'))
{
	function thecs_output_page_shortcode()
	{
		$shortcode_meta_prefix = 'pt_sc_';
		$text = thecs_get_page_setting_property( $shortcode_meta_prefix. 'shortCodeText', '' );
		if($text != '')
		{
			echo do_shortcode(shortcode_unautop(wp_specialchars_decode(stripslashes($text), ENT_QUOTES)));
		}
	}
}


/**
 * Get related posts. Return a list of object contains [link, title, tags, imageurl].
 */
if(!function_exists('thecs_get_related_posts_data'))
{
	function thecs_get_related_posts_data( $post_id, $post_type = 'post', $num = 4, $post_style = 1 )
	{
		$result = array();
		$post_ids = array($post_id);

		$query_params = array(
			'post_type' => $post_type,
			'post_style' => $post_style,
		);

		$taxonomy = $post_type == 'project' ? 'project_tag' : 'post_tag';
		$terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'ids'));
		if($terms)
		{
			$args = array(
				'tax_query' => array(
			        array(
			            'taxonomy' => $taxonomy,
			            'terms'    => $terms
			        ),
			    ),
				'post__not_in' => $post_ids,
				'showposts' => $num,
				'ignore_sticky_posts' => 1,
				'meta_key' => '_thumbnail_id',
				'post_type' => array($post_type),
				'suppress_filters' => false
			);

			$query_obj = thecs_query_related_posts($args, $taxonomy, $query_params);

			$post_ids = array_merge($post_ids, $query_obj['ids']);
			$result = array_merge($result, $query_obj['result']);
		}

		$count = count($result);
		if( $count >= $num )
		{
			return $result;
		}
		
		$taxonomy = $post_type == 'project' ? 'project_cat' : 'category';
		$terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'ids'));
		if($terms)
		{
			$args = array(
				'tax_query' => array(
			        array(
			            'taxonomy' => $taxonomy,
			            'terms'    => $terms
			        ),
			    ),
				'post__not_in' => $post_ids,
				'showposts' => $num - $count,
				'ignore_sticky_posts' => 1,
				'meta_key' => '_thumbnail_id',
				'post_type' => array($post_type),
				'suppress_filters' => false
			);

			$query_obj = thecs_query_related_posts($args, $taxonomy, $query_params);

			$post_ids = array_merge($post_ids, $query_obj['ids']);
			$result = array_merge($result, $query_obj['result']);
		}

		$count = count($result);
		if( $count >= $num )
		{
			return $result;
		}

		$args = array(
			'post__not_in' => $post_ids,
			'showposts' => $num - $count,
			'ignore_sticky_posts' => 1,
			'meta_key' => '_thumbnail_id',
			'post_type' => array($post_type),
			'suppress_filters' => false
		);

		$query_obj = thecs_query_related_posts($args, $taxonomy, $query_params);
		$result = array_merge($result, $query_obj['result']);

		return $result;
	}
}


/**
 * parse related posts
 */
if(!function_exists( 'thecs_query_related_posts' ))
{
	function thecs_query_related_posts($args, $taxonomy, $params)
	{
		$post_ids = array();
		$result = array();

		$post_type = $params['post_type'];
		$post_style = $params['post_style'];

		$query = new WP_Query($args);
		if ($query->have_posts())
		{
			while ($query->have_posts())
			{
				$query->the_post();
				
				$related_post_id = apply_filters( 'wpml_object_id', get_the_ID(), $post_type );
				$post_data = thecs_get_post_setting($related_post_id);
				$image_crop_part = thecs_get_property($post_data, 'image_crop_part', 3, 'pt_'.$post_type.'_');

				$post_ids[] = $related_post_id;
				
				$img_obj = $thecs_img_obj = thecs_get_image(array(
					'rid' => get_post_thumbnail_id(),
					'max_width' => 200,
					'crop_part_vertical' => $image_crop_part,
				));

				if( !$img_obj )
				{
					continue;
				}
				
				$tags = get_the_terms( $related_post_id, $taxonomy );
				$tag_names = array();
				if($tags)
				{
					foreach($tags as $tag)
					{
						$tag_names[] = '<span>'.$tag->name.'</span>';
					}
				}

				$related_link = get_permalink($related_post_id);
				
				$result[] = array( 
					'title' => get_the_title(), 
					'link' => $related_link, 
					'tags' => $tag_names, 
					'imageurl' => $img_obj['thumb'], 
					'w' => $img_obj['thumb_width'], 
					'h' => $img_obj['thumb_height'],
				);
			}
		}
		
		wp_reset_postdata();

		return array('ids'=>$post_ids, 'result'=>$result);
	}
}


/**
 * Kses content
 */
if (!function_exists( 'thecs_kses_content' ))
{
	function thecs_kses_content( $content, $echo = true, $custom_tags = null, $decode_html = false, $support_iframe = false ) 
	{
		$tags = $custom_tags ? $custom_tags : wp_kses_allowed_html( 'post' );

		if( $support_iframe )
		{
			$tags['iframe'] = array(
				'class' => true,
		        'id' => true,
		        'style' => true,
		        'title' => true,
		        'src' => true,
		        'frameborder' => true,
		        'allow' => true,
		        'allowfullscreen' => true,
		        'width' => true,
		        'height' => true,
		        'border' => true,
			);
		}

		if( $decode_html )
		{
			if( $echo )
			{
				echo wp_specialchars_decode( stripslashes(wp_kses($content, $tags)), ENT_QUOTES );
			}
			else
			{
				return wp_specialchars_decode( stripslashes(wp_kses($content, $tags)), ENT_QUOTES );
			}
		}
		else
		{
			if( $echo )
			{
				echo wp_kses( $content, $tags );
			}
			else
			{
				return wp_kses( $content, $tags );
			}
		}
		
		return '';
	}
}


add_filter( 'thecs_menu_image_url', 'thecs_get_menu_image_url' );
if(!function_exists('thecs_get_menu_image_url'))
{
	function thecs_get_menu_image_url( $rid = '' )
	{
		if( $rid == '' )
		{
			return '';
		}

		$img_obj = thecs_get_image(array(
			'rid' => $rid,
			'max_height' => 1080,
			'max_crop_width' => 1600
		));

		if( $img_obj )
		{
			return $img_obj['thumb'];
		}

		return '';
	}
}


/**
 * Modify read-more link
 */
add_filter( 'the_content_more_link', 'thecs_modify_read_more_link' );
if(!function_exists('thecs_modify_read_more_link'))
{
	function thecs_modify_read_more_link()
	{
		return '<a class="more-link" href="' . esc_url(get_permalink()) . '">'.esc_html__('READ MORE...', 'thecs').'</a>';
	}
}

?>