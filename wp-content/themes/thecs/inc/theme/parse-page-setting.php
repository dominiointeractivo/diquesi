<?php
	if( !isset($thecs_page_id) )
	{
		$thecs_page_id = get_queried_object_id();
	}

	$thecs_page_basic_setting = get_post_meta( $thecs_page_id, '_thecs_meta_page_basic', true );
	$thecs_page_basic_data_params = explode( '&', $thecs_page_basic_setting );

	global $thecs_g_basic_setting;
	
	$thecs_g_basic_setting = array();
	foreach( $thecs_page_basic_data_params as $thecs_page_basic_param )
	{
		$thecs_page_temp_array = explode( '=', $thecs_page_basic_param );
		if( count($thecs_page_temp_array) > 1 )
		{
			$thecs_page_temp_key = urldecode( $thecs_page_temp_array[0] );
			$thecs_g_basic_setting[$thecs_page_temp_key] = urldecode( $thecs_page_temp_array[1] );
		}
	}

	$thecs_page_setting = get_post_meta( $thecs_page_id, '_thecs_meta_post_page', true );
	$thecs_page_setting_params = explode( '&', $thecs_page_setting );

	global $thecs_g_page_setting;
	$thecs_g_page_setting = array();
	foreach( $thecs_page_setting_params as $thecs_page_param )
	{
		$thecs_page_temp_array = explode( '=', $thecs_page_param );
		if( count($thecs_page_temp_array) > 1 )
		{
			$thecs_page_temp_key = urldecode( $thecs_page_temp_array[0] );
			$thecs_g_page_setting[$thecs_page_temp_key] = urldecode( $thecs_page_temp_array[1] );
		}
	}
?>