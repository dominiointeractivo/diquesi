<?php


/**
 * Parse js path
 */
if( !function_exists('thecs_parse_js_path') )
{
	function thecs_parse_js_path( $file_name, $extend_holder = '' )
	{
		$new_path = THECS_THEME_URL.'/js/'.$extend_holder;
		if( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG )
		{
			$new_path .= 'uncompressed/'.$file_name.'.js';
		}
		else
		{
			$new_path .= $file_name.'.min.js';
		}
		return $new_path;
	}
}


/**
 * Enqueue theme scripts/styles
 */
add_action( 'wp_enqueue_scripts', 'thecs_theme_scripts' );
if( !function_exists('thecs_theme_scripts') )
{
	function thecs_theme_scripts()
	{
		wp_enqueue_style( 'thecs-fonts', esc_url(THECS_THEME_URL . '/css/fonts.css') );
		wp_enqueue_style( 'thecs-font-awesome', esc_url(THECS_THEME_URL . '/css/font-awesome/font-awesome.css') );
		wp_enqueue_style( 'thecs-pe-icon-7', esc_url(THECS_THEME_URL . '/css/pe-icon-7-stroke.css') );

		wp_enqueue_style( 'thecs-other', esc_url(THECS_THEME_URL . '/css/other.css') );
		wp_enqueue_style( 'thecs-style', get_stylesheet_uri() );

		//third plugins ------------------
		wp_enqueue_script('easing', esc_url(THECS_THEME_URL . '/js/theme/plugins/jquery.easing.min.js'), array('jquery', 'imagesloaded', 'hoverIntent'), false, true);
		wp_enqueue_script('tween-js', esc_url(THECS_THEME_URL . '/js/theme/plugins/tweenjs-0.6.2.min.js'), array(), false, true);
		wp_enqueue_script('viewportchecker', esc_url(THECS_THEME_URL . '/js/theme/plugins/viewportchecker.min.js'), array(), false, true);
		wp_enqueue_script('owl', esc_url(THECS_THEME_URL . '/js/theme/plugins/owl.carousel.min.js'), array(), false, true);
		//third plugins ------------------

		wp_enqueue_script('thecs-pt-plugins', thecs_parse_js_path('pt-plugins', 'theme/'), array(), false, true);

		wp_enqueue_style( 'thecs-widget', esc_url(THECS_THEME_URL . '/css/widget.css') );

		$custom_script = get_option('thecs_custom_script', '');
		if($custom_script != '')
	    {
	    	$custom_script = '!(function($){' . PHP_EOL . $custom_script . PHP_EOL . '})(jQuery);';
			wp_add_inline_script( 'thecs-pt-plugins', $custom_script, 'before' );
		}

		$custom_js = '!(function(win){
			win.__pt_theme_root_url = "'.esc_url(THECS_THEME_URL).'";
		})(window);';
		wp_add_inline_script( 'thecs-pt-plugins', $custom_js, 'before' );

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		wp_localize_script( 'thecs-script', 'screenReaderText', array(
			'expand'   => '<span class="screen-reader-text">' . esc_html__( 'expand child menu', 'thecs' ) . '</span>',
			'collapse' => '<span class="screen-reader-text">' . esc_html__( 'collapse child menu', 'thecs' ) . '</span>',
		) );

		thecs_add_font_family();

		$main_js_path = thecs_parse_js_path('main', 'theme/');
		wp_enqueue_script('thecs-main', esc_url($main_js_path), array(), false, true);
	}
}


/**
 * Add font family
 */
if( !function_exists('thecs_add_font_family') )
{
	function thecs_add_font_family()
	{
		$fonts = array('body'=>'body_font_family', 'title'=>'title_font_family', 'menu'=>'menu_font_family', 'custom1'=>'custom_font_family1', 'custom2'=>'custom_font_family2');
		$font_temp = array();
		foreach($fonts as $key => $value)
		{
			$font_name = thecs_get_theme_option('thecs_'.$value, '');
			if($font_name == '') continue;

			$pos = strpos($font_name, ' - ');
			if($pos === false)
			{
				$pos = strrpos($font_name, '|');
				if($pos !== false)
				{
					$font_name = substr($font_name, 0, $pos);
				}

				if( $font_name != '' && !in_array($font_name, $font_temp) )
				{
					$font_temp[] = $font_name;
					wp_enqueue_style( 'thecs-'.$key.'-fonts', '//fonts.googleapis.com/css?family='.$font_name );
				}
			}
		}
	}
}


/**
 * Enqueue google map
 */
if( !function_exists('thecs_enqueue_map') )
{
	function thecs_enqueue_map()
	{
		$map_API_key = thecs_get_theme_option('thecs_map_api_key', '');
		$map_API_key_string = '';
		if($map_API_key != '')
		{
			$map_API_key_string = '&key='.$map_API_key;
		}
		
		wp_enqueue_script('thecs-map', thecs_parse_js_path('map', 'theme/'), array(), false, true);
		wp_enqueue_script('thecs-google-map', 'https://maps.googleapis.com/maps/api/js?callback=pt_gmap.initMap'.$map_API_key_string, array(), false, true);
	}
}


/**
 * Enqueue admin custom scripts/styles
 */
add_action( 'admin_enqueue_scripts', 'thecs_admin_custom', 10 );
if(!function_exists('thecs_admin_custom'))
{
	function thecs_admin_custom()
	{
		wp_enqueue_style( 'wp-color-picker' ); 
		
		wp_enqueue_script( 'thecs-wp-color-picker-alpha', esc_url(THECS_THEME_URL . '/js/wp-color-picker-alpha.min.js'), array('wp-color-picker'), false, true );

		wp_enqueue_script( 'thecs-custom-post-page-js', thecs_parse_js_path('custom-post-page'), array(), '', 'all' );

		wp_enqueue_script( 'thickbox' );
		wp_enqueue_style( 'thickbox' );

		wp_enqueue_script( 'thecs-imagegroup-js', thecs_parse_js_path('pt-imagegroup') );
		wp_enqueue_style( 'thecs-imagegroup-style', esc_url(THECS_THEME_URL . '/css/pt-imagegroup.css') );

		$custom_js = '!(function(win){
			win.__pt_root_url = "'.esc_url(THECS_THEME_URL).'";
			win.__pt_upload_url = "'.esc_url(THECS_UPLOAD_URL).'";
			win.__pt_theme_name = "'.esc_js(THECS_THEME_NAME).'";
			win.__pt_wp_version = "'.esc_js(get_bloginfo( 'version' )).'";
		})(window);';
		wp_add_inline_script( 'thecs-imagegroup-js', $custom_js, 'before' );
		
		wp_enqueue_script( 'thecs-admin-custom-js', thecs_parse_js_path('admin-custom') );
		wp_enqueue_style( 'thecs-admin-custom-style', esc_url(THECS_THEME_URL . '/css/admin-custom.css') );

		wp_enqueue_style( 'thecs-admin-addition-style', esc_url(THECS_THEME_URL . '/css/admin-addition.css') );
				
		wp_enqueue_style( 'thecs-admin-font-awesome', esc_url(THECS_THEME_URL . '/css/font-awesome/font-awesome.css') );
		wp_enqueue_style( 'thecs-admin-pe-icon-7', esc_url(THECS_THEME_URL . '/css/pe-icon-7-stroke.css') );

		wp_enqueue_script( 'jquery-ui-sortable' );

		wp_enqueue_script( 'thecs-icons-insert-js', thecs_parse_js_path('pt-icons-insert') );
	}
}


/**
 * Enqueue scripts/styles for theme-options page
 */
add_action( 'admin_enqueue_scripts', 'thecs_theme_options_scripts' );
if(!function_exists('thecs_theme_options_scripts'))
{
	function thecs_theme_options_scripts($hook)
	{
		if($hook == 'appearance_page_theme_options')
		{
			if(function_exists( 'wp_enqueue_media' ))
			{
				wp_enqueue_media();
			}
			else
			{
				wp_enqueue_style('thickbox');
				wp_enqueue_script('media-upload');
				wp_enqueue_script('thickbox');
			}

			wp_enqueue_script('jquery-form');
			
			wp_enqueue_script( 'jquery-nice-select', esc_url(THECS_THEME_URL . '/js/jquery.nice-select.min.js') );
			
			wp_enqueue_script( 'thecs-theme-ace-js', esc_url(THECS_THEME_URL . '/js/ace/ace.js') );

			wp_enqueue_style( 'thecs-fonts', esc_url(THECS_THEME_URL . '/css/fonts.css') );
			
			wp_enqueue_style( 'thecs-font-list-style', esc_url(THECS_THEME_URL . '/css/font-list-style.css') );
			wp_enqueue_script( 'thecs-font-list-js', thecs_parse_js_path('pt-font-list') );
			
			wp_enqueue_style( 'thecs-theme-option-style', esc_url(THECS_THEME_URL . '/css/pt-theme-options.css') );
			wp_enqueue_script( 'thecs-theme-option-js', thecs_parse_js_path('pt-theme-options') );

			global $thecs_g_fonts_data;
			if(!isset($thecs_g_fonts_data) || $thecs_g_fonts_data == '')
			{
				global $wp_filesystem;
				WP_Filesystem();
				$path = THECS_THEME_DIR . '/data/fonts_enum.json';
				$fonts_string = $wp_filesystem->get_contents($path);
				$thecs_g_fonts_data = apply_filters( 'thecs_custom_font_parse_data', json_decode($fonts_string) );
			}
			$font_script = 'window.__fonts_enum = "'.addcslashes(json_encode($thecs_g_fonts_data->fonts), '"').'";';
			wp_add_inline_script( 'thecs-font-list-js', $font_script, 'before' );
		}
	}
}

?>