<?php
/**
 * foreverpinetree@gmail.com
 */

//Dynamic styles
function thecs_custom_styles($custom)
{
	$custom = apply_filters( 'thecs_custom_font_parse_fonts', $custom );
	
	//Body text start -------------------------------------------------
	$body_font_css = 'body,textarea,input,button,select';
	$font_var = '';
	$body_font_family = thecs_get_theme_option('thecs_body_font_family', '');
	if($body_font_family != '')
	{
		$pos = strpos($body_font_family, ' - ');
		if($pos !== false)
		{
			$body_font_family = substr($body_font_family, $pos + 3);
		}
		
		$pos = strrpos($body_font_family, '|');
		$fallback = '';
		if($pos !== false)
		{
			$fallback = ',' . substr($body_font_family, $pos + 1);
		}
		
		$pos = strpos($body_font_family, ':');
		$font_name = $body_font_family;
		if($pos !== false)
		{
			$font_name = substr($body_font_family, 0, $pos);
		}

		$font_var .= "font-family:" . $font_name . $fallback . ";";
	}
	
	$body_font_weight = thecs_get_theme_option('thecs_body_font_weight', '0');
	if( $body_font_weight != '0' )
	{
		$font_var .= "font-weight:" . $body_font_weight . ";";
	}

	$custom .= $body_font_css;
	$custom .= "{" . $font_var . "}"."\n";
	//Body text end ---------------------------------------------------
	
	//Title text start-------------------------------------------------
	$title_font_css = 'h1,h2,h3,h4,h5,h6,strong,b,mark,legend,blockquote';
	$font_var = '';
	$title_font_family = thecs_get_theme_option('thecs_title_font_family', '');
	if($title_font_family != '')
	{
		$pos = strpos($title_font_family, ' - ');
		if($pos !== false)
		{
			$title_font_family = substr($title_font_family, $pos + 3);
		}
		
		$pos = strrpos($title_font_family, '|');
		$fallback = '';
		if($pos !== false)
		{
			$fallback = ',' . substr($title_font_family, $pos + 1);
		}
		
		$pos = strpos($title_font_family, ':');
		$font_name = $title_font_family;
		if($pos !== false)
		{
			$font_name = substr($title_font_family, 0, $pos);
		}
		
		$font_var .= "font-family:" . $font_name . $fallback . ";";
	}
	
	$title_font_weight = thecs_get_theme_option('thecs_title_font_weight', '0');
	if( $title_font_weight != '0' )
	{
		$font_var .= "font-weight:" . $title_font_weight . ";";
	}

	$custom .= $title_font_css;
	$custom .= "{" . $font_var . "}"."\n";
	//Title text end---------------------------------------------------
	
	//Menu text start--------------------------------------------------
	$menu_font_css = '.widget.widget_nav_menu .menu > li >a,.main-menu a';
	$font_var = '';
	$menu_font_family = thecs_get_theme_option('thecs_menu_font_family', '');
	if($menu_font_family != '')
	{
		$pos = strpos($menu_font_family, ' - ');
		if($pos !== false)
		{
			$menu_font_family = substr($menu_font_family, $pos + 3);
		}
		
		$pos = strrpos($menu_font_family, '|');
		$fallback = '';
		if($pos !== false)
		{
			$fallback = ',' . substr($menu_font_family, $pos + 1);
		}
		
		$pos = strpos($menu_font_family, ':');
		$font_name = $menu_font_family;
		if($pos !== false)
		{
			$font_name = substr($menu_font_family, 0, $pos);
		}
		
		$font_var .= "font-family:" . $font_name . $fallback . ";";
	}
	
	$menu_font_weight = thecs_get_theme_option('thecs_menu_font_weight', '0');
	if( $menu_font_weight != '0' )
	{
		$font_var .= "font-weight:" . $menu_font_weight . ";";
	}

	$custom .= $menu_font_css;
	$custom .= "{" . $font_var . "}"."\n";
	//Menu text end----------------------------------------------------

	//Custom text 1 start--------------------------------------------------
	$custom_font_css = thecs_get_theme_option('thecs_custom_font_selector1', '');
	if( $custom_font_css != '' )
	{
		$font_var = '';
		$custom_font_family = thecs_get_theme_option('thecs_custom_font_family1', '');
		if($custom_font_family != '')
		{
			$pos = strpos($custom_font_family, ' - ');
			if($pos !== false)
			{
				$custom_font_family = substr($custom_font_family, $pos + 3);
			}
			
			$pos = strrpos($custom_font_family, '|');
			$fallback = '';
			if($pos !== false)
			{
				$fallback = ',' . substr($custom_font_family, $pos + 1);
			}
			
			$pos = strpos($custom_font_family, ':');
			$font_name = $custom_font_family;
			if($pos !== false)
			{
				$font_name = substr($custom_font_family, 0, $pos);
			}
			
			$font_var .= "font-family:" . $font_name . $fallback . ";";
		}
		
		$custom_font_weight = thecs_get_theme_option('thecs_custom_font_weight1', '');
		if($custom_font_weight == '')
		{
			$custom_font_weight = '700';
		}
		$font_var .= "font-weight:" . $custom_font_weight . ";";

		$custom .= $custom_font_css;
		$custom .= "{" . $font_var . "}"."\n";
	}
	//Custom text 1 end----------------------------------------------------

	//Custom text 2 start--------------------------------------------------
	$custom_font_css = thecs_get_theme_option('thecs_custom_font_selector2', '');
	if( $custom_font_css != '' )
	{
		$font_var = '';
		$custom_font_family = thecs_get_theme_option('thecs_custom_font_family2', '');
		if($custom_font_family != '')
		{
			$pos = strpos($custom_font_family, ' - ');
			if($pos !== false)
			{
				$custom_font_family = substr($custom_font_family, $pos + 3);
			}
			
			$pos = strrpos($custom_font_family, '|');
			$fallback = '';
			if($pos !== false)
			{
				$fallback = ',' . substr($custom_font_family, $pos + 1);
			}
			
			$pos = strpos($custom_font_family, ':');
			$font_name = $custom_font_family;
			if($pos !== false)
			{
				$font_name = substr($custom_font_family, 0, $pos);
			}
			
			$font_var .= "font-family:" . $font_name . $fallback . ";";
		}
		
		$custom_font_weight = thecs_get_theme_option('thecs_custom_font_weight2', '');
		if($custom_font_weight == '')
		{
			$custom_font_weight = '700';
		}
		$font_var .= "font-weight:" . $custom_font_weight . ";";

		$custom .= $custom_font_css;
		$custom .= "{" . $font_var . "}"."\n";
	}
	//Custom text 2 end----------------------------------------------------
	
	//Main color start-------------------------------------------------
	$main_color = thecs_get_theme_option('thecs_main_color', '#05b14a');

	$custom .= 'header .main-menu .sub-no-img .sub-menu:not(.flex-target),.m-search:focus,.wpcf7 form span.wpcf7-form-control-wrap input:focus,.wpcf7 form span.wpcf7-form-control-wrap textarea:focus,
i.title-info,.blog-list .list-category a:hover,.sc-btn.style-02 a:hover,.site-light .color-invert .sc-btn.style-02 a:hover,.site-dark .sc-btn.style-02 a:hover,
.pages a:hover,.page-other .search-field:focus,.pic-loader:after,.loader-layer div:after,
.comment-root textarea:focus,.comment-root p:not(.form-submit) input:focus,.wc-tabs-wrapper input:not(.submit):focus,
.widget input.search-field:focus,input.qty:focus,input#coupon_code:focus,
body:not(.woocommerce-cart) .woocommerce input.input-text:focus,
body .woocommerce textarea:focus{
		border-color: '.$main_color.';
	}'."\n";

	$custom .= '.header-search.hover form:before,header a.remove:hover,.summary > .price ins .amount span,.post-meta a:hover,.owl-nav > div:hover,
.list-widgets li.current:after,.has-sub:hover:before,header.style-02 .has-sub:hover:before,.wpcf7 > h5:before,.wc-tabs li.active a:before,.page-other a:hover em,
header.style-05 .depth-1.current_page_item > a,header.style-05 .depth-1.current-menu-ancestor > a,header .main-menu .flex-menu .current_page_item > a,body:not(.m-mode) .blog-list .item .h:hover a,
header.style-05 .wrap > i.btn:hover:before,.list-title .h:before,i.title-info:before,.default-template-page .text-area a:hover,.ptsc .intro a:hover,
.pic-list .list-category a:hover,a.more:hover:before,a.list-date,.comment-reply-title:before,.woocommerce-Reviews-title:before,.call-widget-sub:hover,
body:not(.m-mode) .post-02 .ctrl:hover span:before,.woocommerce-MyAccount-content a,.woocommerce-checkout-payment li a,#payment li.wc_payment_method > input[type="radio"]:first-child:checked + label:before,
.recentcomments span,.widget a:hover,.widget_archive li,.widget_rss cite,.widget_categories a{
		color:'.$main_color.';
	}'."\n";

	$custom .= 'footer > i.btn:hover,i.close-search:hover,.hoverLine,
.m-header li[class*="current"]:after,.title-text .h .mo-line,.blog-list .list-category a:hover,.item.sticky .img:before,.sticky.text-post:before,
.pt-mouse-scroll-boxed .list-scroller,.pt-mouse-scroll-boxed .list-scroller:after,body:not(.m-mode) .list-icon i:hover,body:not(.m-mode) .list-icon a:hover,
.pages a:hover,header .list-scroller-target,i.close-sub-menu:hover,i.close-hidden-menu:hover,.call-cart.btn:hover:after,.call-hidden-menu.btn:hover:after,.call-search.btn:hover:after,
header:not(.style-05) .depth-1.current_page_item > a:after,header:not(.style-05) .depth-1.current-menu-ancestor > a:after,.sc-btn a:hover,input.wpcf7-submit:hover,.related.products a.add_to_cart_button:hover,
body:not(.m-mode) .pt-social a:hover,.site-light .color-invert .pt-social a:hover,.site-dark .color-invert .pt-social a:hover,input[name="apply_coupon"],a.checkout-button,
.site-dark .sc-btn.style-01 a:hover,.site-light .sc-btn.style-01 a:hover,.site-light .color-invert .sc-btn.style-01 a:hover,.site-dark .color-invert .sc-btn.style-01 a:hover,
.site-dark .sc-btn.style-02 a:hover,.site-light .sc-btn.style-02 a:hover,.site-light .color-invert .sc-btn.style-02 a:hover,.site-dark .color-invert .sc-btn.style-02 a:hover,
body:not(.m-mode) .pt-area-roll-wrap .pt-area-roll-btn:hover,.woocommerce-mini-cart__buttons a:hover,.v-ctrl:not(.v-mobile-play):hover,
.pt-swipe-dots li:before,.woocommerce-Price-amount .mo-line,.call-list-widgets:after,.woocommerce-Reviews input[type="submit"],.owl-dots .active span,
.single_add_to_cart_button,.comment-root .reply a:hover,.comment-root input[type="submit"],.share a:hover,.post-tags a:hover,
input[name="login"],input[name="register"],button[name="login"],button[name="register"],body .woocommerce p button[type="submit"],
i.close-list-widgets,.product_title .onsale,.product_title .onsale:after,.related.products > h2:before,.up-sells.upsells > h2:before,.post-extend h2:before,
.pt-bar-group:hover .pt-bar,.pths-bar-full .pt-mouse-scroll-boxed .list-scroller-target,.comment-root:before,
.default-template-page .title.h:before,.woocommerce-MyAccount-navigation li.is-active a,#place_order.button,
input[name="save_account_details"]:hover,input[name="save_address"]:hover,i.close-depth-3:hover,
.tagcloud a,.widget-title:before,.widget .search-submit,.woocommerce-product-search input[type="submit"],
.widget_price_filter .ui-slider-handle,.widget_price_filter .ui-slider-range,.widget_price_filter button.button:hover{
		background-color: '.$main_color.';
	}'."\n";
	//Main color end------------------------------------------------

	//Footer bg color -------------------------------------------------
	$footer_bg_color = thecs_get_theme_option('thecs_footer_bg_color', '');
	if( $footer_bg_color )
	{
		$custom .= 'footer{
			background-color:'.$footer_bg_color.';
		}'."\n";
	}

	$custom .= '::selection{
		color:#fff;
		background:'.$main_color.';
		text-shadow:none;
	}'."\n";

	$normal_font_size = thecs_get_theme_option('thecs_normal_font_size', '14');
	$custom .= 'html{font-size:'.$normal_font_size.'px;}'."\n";

	$custom_style = thecs_get_theme_option('thecs_custom_style', '');
	if($custom_style != '')
	{
		$custom .= $custom_style."\n";
	}

	//Output all the styles
	wp_add_inline_style('thecs-style', $custom);	
}
add_action( 'wp_enqueue_scripts', 'thecs_custom_styles' );