<?php
	class Thecs_Product_Options
	{
		static public $data = null;

		static private $inited = false;

		static public function init_data()
		{
			if(self::$inited) return;
			self::$inited = true;

			self::$data = array(
				'pt_product'=>array(
					array('var'=>'caption_img', 'label'=>esc_html__('Hover image', 'thecs'), 'default'=>'', 'type'=>'image', 'des'=>''),
				)
			);
		}
	}

	Thecs_Product_Options::init_data();
?>