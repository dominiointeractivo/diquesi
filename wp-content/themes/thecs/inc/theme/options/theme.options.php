<?php 
	class Thecs_Theme_Options
	{
		static public $domain = 'Theme Options';
		static public $options_key = 'thecs_theme_options';
		static public $prefix = 'thecs_';
		static public $data = null;
		static public $wpml_options = null;

		static private $inited = false;

		static public function init_data()
		{
			if(self::$inited) return;
			self::$inited = true;

			self::$data = array(
				array(
					'title'=>esc_html__('Color', 'thecs'),
					'des'=>'',
					'data'=>array(
						array('var'=>'site_style', 'label'=>esc_html__('Site style', 'thecs'), 'default'=>'1', 'type'=>'select', 
							'options'=>array(
								'1' => esc_html__('Light', 'thecs'), 
								'2' => esc_html__('Dark', 'thecs')
							), 
							'des'=>''
						),
						array('var'=>'main_color', 'label'=>esc_html__('Main color', 'thecs'), 'default'=>'#05b14a', 'type'=>'color', 'des'=>''),
					)
				),
				array(
					'title'=>esc_html__('Hidden Menu', 'thecs'),
					'des'=>'',
					'data'=>array(
						array('var'=>'hidden_menu_btn_type', 'label'=>esc_html__('Menu button type', 'thecs'), 'default'=>'', 'type'=>'select', 
							'options'=>array(
								'1' => esc_html__('Type 1', 'thecs'), 
								'2' => esc_html__('Type 2', 'thecs'),
								'3' => esc_html__('Type 3', 'thecs'),
							), 
							'des'=>''
						),
						array('var'=>'enable_hidden_menu', 'label'=>esc_html__('Enable', 'thecs'), 'type'=>'checkbox', 'default'=>'1', 'des'=>''),
					)
				),
				array(
					'title'=>esc_html__('Logo', 'thecs'),
					'des'=>'',
					'data'=>array(
						array('var'=>'logo', 'label'=>esc_html__('Logo', 'thecs'), 'default'=>'', 'type'=>'image', 'des'=>''),
						array('var'=>'logo_retina', 'label'=>esc_html__('Rentina logo', 'thecs'), 'default'=>'', 'type'=>'image', 'des'=>esc_html__('A retina logo image is the same as the normal logo image, though twice the size.', 'thecs')),
					)
				),
				array(
					'title'=>esc_html__('Site', 'thecs'),
					'des'=>'',
					'data'=>array(
						array('var'=>'full_scroll', 'label'=>esc_html__('Use full scrollbar', 'thecs'), 'type'=>'checkbox', 'default'=>'1', 'des'=>'Whether use full scrollbar (only for hozizontal scrollbar) or not.'),

						array('var'=>'header_style', 'label'=>esc_html__('Menu style', 'thecs'), 'default'=>'4', 'type'=>'select', 'use_preview'=>true, 
							'options'=>array(
								'1' => esc_html__('Style 1', 'thecs'), 
								'2' => esc_html__('Style 2', 'thecs'),
								'3' => esc_html__('Style 3', 'thecs'),
								'4' => esc_html__('Style 4', 'thecs'),
								'5' => esc_html__('Style 5', 'thecs'),
							), 
							'only_available'=>array(
								'1' => array('show_menu_bg', 'header_dark&show_menu_bg=1', 'header_bg_color&show_menu_bg=1'),
							),
							'des'=>''
						),
						
						array('var'=>'show_menu_bg', 'label'=>esc_html__('Show menu background', 'thecs'), 'type'=>'checkbox', 'default'=>'0', 'class'=>'col-3', 
							'only_available'=>array(
								'1' => array('header_dark&header_style=1', 'header_bg_color&header_style=1'),
							),
							'des'=>''
						),
						array('var'=>'header_dark', 'label'=>esc_html__('Menu dark style', 'thecs'), 'type'=>'checkbox', 'default'=>'0', 'class'=>'col-3', 'des'=>''),
						array('var'=>'header_bg_color', 'label'=>esc_html__('Menu bg color', 'thecs'), 'default'=>'', 'type'=>'color', 'class'=>'col-3', 'des'=>''),

						array('var'=>'site_big_gap', 'label'=>esc_html__('Big gap (menu)', 'thecs'), 'type'=>'checkbox', 'default'=>'0', 'class'=>'col-2', 'des'=>esc_html('It\'s only available for Menu style 2, 3, 4.', 'thecs')),

						array('var'=>'archives_img_raw', 'label'=>esc_html__('Raw proportion for archives images', 'thecs'), 'type'=>'checkbox', 'default'=>'1', 'class'=>'col-2', 'des'=>''),
						
						array('var'=>'show_search_button', 'label'=>esc_html__('Show search button', 'thecs'), 'type'=>'checkbox', 'default'=>'1', 'class'=>'col-2', 'des'=>''),
						array('var'=>'image_quality', 'label'=>esc_html__('Image quality', 'thecs'), 'default'=>'90', 'type'=>'text', 'des'=>esc_html__('It should be 50 - 100. Note: it affects new added image only.', 'thecs'), 'placeholder'=>''),
					)
				),
				array(
					'title'=>esc_html__('Fonts', 'thecs'),
					'des'=>'',
					'data'=>array(
						array('var'=>'normal_font_size', 'label'=>esc_html__('Normal font size', 'thecs'), 'default'=>'14', 'class'=>'sub-tab-1', 'tab_name'=>esc_html__('Font Setting', 'thecs'), 'type'=>'select', 
							'options'=>array(
								'11' => esc_html__('11 Pixel', 'thecs'), 
								'12' => esc_html__('12 Pixel', 'thecs'), 
								'13' => esc_html__('13 Pixel', 'thecs'), 
								'14' => esc_html__('14 Pixel', 'thecs'), 
								'15' => esc_html__('15 Pixel', 'thecs'), 
								'16' => esc_html__('16 Pixel', 'thecs'), 
								'17' => esc_html__('17 Pixel', 'thecs'), 
								'18' => esc_html__('18 Pixel', 'thecs')
							), 
							'des'=>''
						),
						
						array('var'=>'', 'label'=>esc_html__('Font weight', 'thecs'), 'class'=>'sub-tab-1', 'tab_name'=>esc_html__('Font Setting', 'thecs'), 'type'=>'title', 'des'=>''),
						array('var'=>'body_font_weight', 'label'=>esc_html__('Body', 'thecs'), 'default'=>'0', 'class'=>'sub-tab-1 col-3', 'tab_name'=>esc_html__('Font Setting', 'thecs'), 'type'=>'select', 
							'options'=>array(
								'0'=>'Default', 
								'100' => esc_html__('100', 'thecs'), 
								'200' => esc_html__('200', 'thecs'), 
								'300' => esc_html__('300', 'thecs'), 
								'400' => esc_html__('400', 'thecs'), 
								'500' => esc_html__('500', 'thecs'), 
								'600' => esc_html__('600', 'thecs'), 
								'700' => esc_html__('700', 'thecs'), 
								'800' => esc_html__('800', 'thecs'), 
								'900' => esc_html__('900', 'thecs')
							), 
							'des'=>''
						),
						array('var'=>'title_font_weight', 'label'=>esc_html__('Title', 'thecs'), 'default'=>'0', 'class'=>'sub-tab-1 col-3', 'tab_name'=>esc_html__('Font Setting', 'thecs'), 'type'=>'select', 
							'options'=>array(
								'0'=>'Default', 
								'100' => esc_html__('100', 'thecs'), 
								'200' => esc_html__('200', 'thecs'), 
								'300' => esc_html__('300', 'thecs'), 
								'400' => esc_html__('400', 'thecs'), 
								'500' => esc_html__('500', 'thecs'), 
								'600' => esc_html__('600', 'thecs'), 
								'700' => esc_html__('700', 'thecs'), 
								'800' => esc_html__('800', 'thecs'), 
								'900' => esc_html__('900', 'thecs')
							), 
							'des'=>''
						),
						array('var'=>'menu_font_weight', 'label'=>esc_html__('Menu', 'thecs'), 'default'=>'0', 'class'=>'sub-tab-1 col-3', 'tab_name'=>esc_html__('Font Setting', 'thecs'), 'type'=>'select', 
							'options'=>array(
								'0'=>'Default', 
								'100' => esc_html__('100', 'thecs'), 
								'200' => esc_html__('200', 'thecs'), 
								'300' => esc_html__('300', 'thecs'), 
								'400' => esc_html__('400', 'thecs'), 
								'500' => esc_html__('500', 'thecs'), 
								'600' => esc_html__('600', 'thecs'), 
								'700' => esc_html__('700', 'thecs'), 
								'800' => esc_html__('800', 'thecs'), 
								'900' => esc_html__('900', 'thecs')
							), 
							'des'=>''
						),
						

						array('var'=>'body_font_family', 'label'=>esc_html__('Body Font-family', 'thecs'), 'default'=>'', 'class'=>'sub-tab-2', 'tab_name'=>esc_html__('Font Family', 'thecs'), 'type'=>'font', 'des'=>''),
						array('var'=>'title_font_family', 'label'=>esc_html__('Title Font-family', 'thecs'), 'default'=>'', 'class'=>'sub-tab-2', 'tab_name'=>esc_html__('Font Family', 'thecs'), 'type'=>'font', 'des'=>''),
						array('var'=>'menu_font_family', 'label'=>esc_html__('Menu Font-family', 'thecs'), 'default'=>'', 'class'=>'sub-tab-2', 'tab_name'=>esc_html__('Font Family', 'thecs'), 'type'=>'font', 'des'=>''),

						array('var'=>'custom_font_weight1', 'label'=>esc_html__('Custom Font Weight', 'thecs'), 'default'=>'700', 'class'=>'sub-tab-3', 'tab_name'=>esc_html__('Custom Font 1', 'thecs'), 'type'=>'select', 
							'options'=>array(
								'100' => esc_html__('100', 'thecs'), 
								'200' => esc_html__('200', 'thecs'), 
								'300' => esc_html__('300', 'thecs'), 
								'400' => esc_html__('400', 'thecs'), 
								'500' => esc_html__('500', 'thecs'), 
								'600' => esc_html__('600', 'thecs'), 
								'700' => esc_html__('700', 'thecs'), 
								'800' => esc_html__('800', 'thecs'), 
								'900' => esc_html__('900', 'thecs')
							), 
							'des'=>''
						),
						array('var'=>'custom_font_family1', 'label'=>esc_html__('Custom Font-family', 'thecs'), 'default'=>'', 'class'=>'sub-tab-3', 'tab_name'=>esc_html__('Custom Font 1', 'thecs'), 'type'=>'font', 'des'=>''),
						array('var'=>'custom_font_selector1', 'label'=>esc_html__('Custom Font Selector', 'thecs'), 'default'=>'', 'class'=>'sub-tab-3', 'tab_name'=>esc_html__('Custom Font 1', 'thecs'), 'type'=>'code', 'height'=>150, 'editor'=>'css', 'des'=>esc_html__('Enter custom font selector (class, id, etc...) here, for example: .font-title', 'thecs')),

						array('var'=>'custom_font_weight2', 'label'=>esc_html__('Custom Font Weight', 'thecs'), 'default'=>'700', 'class'=>'sub-tab-4', 'tab_name'=>esc_html__('Custom Font 2', 'thecs'), 'type'=>'select', 
							'options'=>array(
								'100' => esc_html__('100', 'thecs'), 
								'200' => esc_html__('200', 'thecs'), 
								'300' => esc_html__('300', 'thecs'), 
								'400' => esc_html__('400', 'thecs'), 
								'500' => esc_html__('500', 'thecs'), 
								'600' => esc_html__('600', 'thecs'), 
								'700' => esc_html__('700', 'thecs'), 
								'800' => esc_html__('800', 'thecs'), 
								'900' => esc_html__('900', 'thecs')
							), 
							'des'=>''
						),
						array('var'=>'custom_font_family2', 'label'=>esc_html__('Custom Font-family', 'thecs'), 'default'=>'', 'class'=>'sub-tab-4', 'tab_name'=>esc_html__('Custom Font 2', 'thecs'), 'type'=>'font', 'des'=>''),
						array('var'=>'custom_font_selector2', 'label'=>esc_html__('Custom Font Selector', 'thecs'), 'default'=>'', 'class'=>'sub-tab-4', 'tab_name'=>esc_html__('Custom Font 2', 'thecs'), 'type'=>'code', 'height'=>150, 'editor'=>'css', 'des'=>esc_html__('Enter custom font selector (class, id, etc...) here, for example: .font-title', 'thecs'))
					)
				),
				array(
					'title'=>esc_html__('Map', 'thecs'),
					'des'=>'',
					'data'=>array(
						array('var'=>'map_api_key', 'label'=>esc_html__('Google map API key', 'thecs'), 'default'=>'', 'type'=>'text', 'des'=>esc_html__('Go to https://developers.google.com/maps/documentation/javascript/ and click "GET A KEY" to get your key, then type it into this field.', 'thecs')),
						array('var'=>'map_style', 'label'=>esc_html__('Map style', 'thecs'), 'default'=>'1', 'type'=>'select', 'use_preview'=>true, 
							'options'=>array(
								'0' => esc_html__('Basic', 'thecs'), 
								'1' => esc_html__('Style 1', 'thecs'), 
								'2' => esc_html__('Style 2', 'thecs'), 
								'3' => esc_html__('Style 3', 'thecs'), 
								'4' => esc_html__('Style 4', 'thecs'), 
								'5' => esc_html__('Style 5', 'thecs')
							), 
							'des'=>''
						),
						array('var'=>'map_marker', 'label'=>esc_html__('Custom marker', 'thecs'), 'default'=>'', 'type'=>'image', 'des'=>esc_html__('Upload custom map marker icon here.', 'thecs'))
					)
				),
				array(
					'title'=>esc_html__('Post & Share', 'thecs'),
					'des'=>'',
					'data'=>array(
						array('var'=>'show_comment', 'label'=>esc_html__('Show comment', 'thecs'), 'default'=>'1', 'type'=>'checkbox', 'des'=>esc_html__('If select No, this site will not show comment (Blog Post & Page, excluded Project).', 'thecs')),
						array('var'=>'show_project_comment', 'label'=>esc_html__('Show Project comment', 'thecs'), 'type'=>'checkbox', 'des'=>esc_html__('If select No, this site will not show Project comment.', 'thecs')),

						array('var'=>'', 'label'=>esc_html__('SHARE:', 'thecs'), 'type'=>'title', 'des'=>''),
						array('var'=>'use_facebook', 'label'=>esc_html__('Enable Facebook', 'thecs'), 'type'=>'checkbox', 'class'=>'col-3', 'des'=>''),
						array('var'=>'use_twitter', 'label'=>esc_html__('Enable Twitter', 'thecs'), 'type'=>'checkbox', 'class'=>'col-3', 'des'=>''),
						array('var'=>'use_pinterest', 'label'=>esc_html__('Enable Pinterest', 'thecs'), 'type'=>'checkbox', 'class'=>'col-3', 'des'=>''),
						array('var'=>'use_tumblr', 'label'=>esc_html__('Enable Tumblr', 'thecs'), 'type'=>'checkbox', 'class'=>'col-3', 'des'=>''),
						array('var'=>'use_googleplus', 'label'=>esc_html__('Enable Google+', 'thecs'), 'type'=>'checkbox', 'class'=>'col-3', 'des'=>''),
						array('var'=>'use_linkedin', 'label'=>esc_html__('Enable Linkedin', 'thecs'), 'type'=>'checkbox', 'class'=>'col-3', 'des'=>'')
					)
				),
				array(
					'title'=>esc_html__('Project', 'thecs'),
					'des'=>'',
					'data'=>array(
						array('var'=>'project_slug', 'label'=>esc_html__('Project slug', 'thecs'), 'default'=>'project_list', 'type'=>'text', 'des'=>esc_html__('WordPress generates the permalink of archive of Project with this slug, so it should be an unique string. When finish changing you need to goto Dashboard &raquo; Settings &raquo; Permalinks and click \'Save Changes\', then refresh the browser. The same as below "Project taxonomy slug" and "Project tag slug".', 'thecs'), 'placeholder'=>''),
						array('var'=>'project_taxonomy_slug', 'label'=>esc_html__('Project taxonomy slug', 'thecs'), 'default'=>'project_cat', 'type'=>'text', 'des'=>'', 'placeholder'=>''),
						array('var'=>'project_tag_slug', 'label'=>esc_html__('Project tag slug', 'thecs'), 'default'=>'project_tag', 'type'=>'text', 'des'=>'', 'placeholder'=>'')
					)
				), 
				array(
					'title'=>esc_html__('Site Socials', 'thecs'),
					'des'=>'',
					'data'=>array(
						array('var'=>'social_facebook', 'label'=>esc_html__('Facebook', 'thecs'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thecs')),
						array('var'=>'social_twitter', 'label'=>esc_html__('Twitter', 'thecs'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thecs')),
						array('var'=>'social_pinterest', 'label'=>esc_html__('Pinterest', 'thecs'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thecs')),
						array('var'=>'social_flickr', 'label'=>esc_html__('Flickr', 'thecs'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thecs')),
						array('var'=>'social_instagram', 'label'=>esc_html__('Instagram', 'thecs'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thecs')),
						array('var'=>'social_linkedin', 'label'=>esc_html__('LinkedIn', 'thecs'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thecs')),
						array('var'=>'social_whatsapp', 'label'=>esc_html__('Whats app', 'thecs'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thecs')),
						array('var'=>'social_behance', 'label'=>esc_html__('Behance', 'thecs'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thecs')),
						array('var'=>'social_tumblr', 'label'=>esc_html__('Tumblr', 'thecs'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thecs')),
						array('var'=>'social_googleplus', 'label'=>esc_html__('Google+', 'thecs'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thecs')),
						array('var'=>'social_vimeo', 'label'=>esc_html__('Vimeo', 'thecs'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thecs')),
						array('var'=>'social_youtube', 'label'=>esc_html__('Youtube', 'thecs'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thecs')),
						array('var'=>'social_email', 'label'=>esc_html__('Email', 'thecs'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thecs'))
					)
				),
				array(
					'title'=>esc_html__('Footer', 'thecs'),
					'des'=>'',
					'data'=>array(
						array('var'=>'footer_title', 'label'=>esc_html__('Title', 'thecs'), 'default'=>'', 'wpml'=>true, 'type'=>'text', 'des'=>''),
						array('var'=>'footer_content', 'label'=>esc_html__('Description', 'thecs'), 'default'=>'', 'wpml'=>true, 'type'=>'textarea', 'rows'=>5, 'des'=>''),
						array('var'=>'footer_img', 'label'=>esc_html__('Image', 'thecs'), 'default'=>'', 'type'=>'image', 'class'=>'col-2', 'des'=>''),
						array('var'=>'footer_copyright', 'label'=>esc_html__('Copyright', 'thecs'), 'default'=>esc_html__('&copy;2018 FOREVERPINETREE', 'thecs'), 'wpml'=>true, 'type'=>'text', 'class'=>'col-2', 'des'=>''),
						array('var'=>'enable_footer', 'label'=>esc_html__('Enable', 'thecs'), 'type'=>'checkbox', 'default'=>'1', 'class'=>'col-2', 'des'=>''),
						array('var'=>'footer_bg_color', 'label'=>esc_html__('Bg color', 'thecs'), 'default'=>'', 'type'=>'color', 'class'=>'col-2', 'des'=>''),
					)
				),
				array(
					'title'=>esc_html__('404 & Search', 'thecs'),
					'des'=>'',
					'data'=>array(
						array('var'=>'not_found_page_msg', 'label'=>esc_html__('404 warnning', 'thecs'), 'default'=>'', 'wpml'=>true, 'type'=>'textarea', 'rows'=>5, 'des'=>''),
						array('var'=>'not_found_content_msg', 'label'=>esc_html__('No-Content warnning', 'thecs'), 'default'=>'', 'wpml'=>true, 'type'=>'textarea', 'rows'=>5, 'des'=>'')
					)
				),
				array(
					'title'=>esc_html__('Import & Export', 'thecs'),
					'des'=>'',
					'data'=>array(
						array('var'=>'', 'label'=>esc_html__('Import options', 'thecs'), 'type'=>'title', 'des'=>esc_html__('Import options from *.json file. (Note: before importing you\'d better backup your options via Export options.)', 'thecs')),
						array('var'=>'', 'class'=>'importUpdateOptions', 'label'=>esc_html__('Import', 'thecs'), 'default'=>'', 'type'=>'button', 'des'=>''),

						array('var'=>'', 'label'=>esc_html__('Export options', 'thecs'), 'type'=>'title', 'des'=>esc_html__('Export options to *.json file. If your browser blocks the download, you should set the browser to allow it. If your browser opens the *.json file directly, you should right-click the mouse then save it as *.json file. Also, you can get the *.json file from {theme}/export-options/ folder.', 'thecs')),
						array('var'=>'', 'class'=>'exportOptions', 'label'=>esc_html__('Export', 'thecs'), 'default'=>'', 'type'=>'button')
					)
				),
			);

			// ----------------- start ---------------------
			self::_start();
		}

		static private function _start()
		{
			if( !self::$wpml_options )
			{
				self::$wpml_options = self::_get_register_wpml_string_data();
			}

			global $thecs_g_theme_options;

			if( !isset( $thecs_g_theme_options ) )
			{
				$options_key = self::$options_key;
				$options = get_option( $options_key, '' );

				if( $options === '' )
				{
					$options = self::_get_data_from_db();
					if( get_option( $options_key ) === false )
					{
						add_option( $options_key, $options, '', 'yes' );
						self::register_wpml_theme_options( $options );
					}
					else
					{
						update_option( $options_key, $options );
					}
				}
				
				$thecs_g_theme_options = json_decode( $options, true );
			}
		}

		static private function _get_data_from_db()
		{
			$data = self::$data;
			$len = count($data);
			$result = array();
			$prefix = self::$prefix;
			for($i = 0; $i < $len; $i ++)
			{
				$obj = $data[$i];
				$arr = $obj['data'];
				foreach($arr as $value)
				{
					if( $value['var'] )
					{
						$key = $prefix.$value['var'];
						$result[$key] = get_option($key, isset($value['default']) ? $value['default'] : '');
					}
				}
			}

			return json_encode( $result );
		}

		static private function _get_register_wpml_string_data()
		{
			$data = self::$data;
			$len = count($data);
			$result = array();
			$prefix = self::$prefix;
			for($i = 0; $i < $len; $i ++)
			{
				$obj = $data[$i];
				$arr = $obj['data'];
				foreach($arr as $value)
				{
					if( $value['var'] && isset($value['wpml']) && $value['wpml'] )
					{
						$id = $prefix.$value['var'];
						$result[$id] = array(
							'id' => $id,
							'title' => $obj['title'].' - '.$value['label'],
						);
					}
				}
			}

			return $result;
		}

		static public function check_wpml( $key )
		{
			if( isset( self::$wpml_options[$key] ) )
			{
				return self::$wpml_options[$key];
			}
			return null;
		}

		static public function register_wpml_theme_options( $json_str )
		{
			if ( !function_exists( 'icl_register_string' ) )
			{
				return;
			}
	
			$json = json_decode( $json_str, true );
			
			$wpml_options = self::$wpml_options;
			if( is_array($wpml_options) )
			{
				foreach( $wpml_options as $option )
				{
					$id = $option['id'];
	
					if( !isset( $json[$id]) )
					{
						continue;
					}

					$value = $json[$id];
					icl_register_string( self::$domain, $option['title'], $value );
				}
			}
		}

		static public function get_keys()
		{
			$data = self::$data;
			$len = count($data);
			$json = array();
			$prefix = self::$prefix;
			$keys = array();
			for($i = 0; $i < $len; $i ++)
			{			
				$obj = $data[$i];
				$arr = $obj['data'];
				foreach($arr as $value)
				{
					if($value['var'] != '')
					{
						$keys[] = $prefix.$value['var'];
					}
				}
			}
			return $keys;
		}
	}

	Thecs_Theme_Options::init_data();

	add_action( 'update_option_'.Thecs_Theme_Options::$options_key, 'thecs_update_theme_options_hook', 10, 2 );
	if( !function_exists('thecs_update_theme_options_hook') )
	{
		function thecs_update_theme_options_hook( $old_value, $value )
		{
			Thecs_Theme_Options::register_wpml_theme_options( $value );
		}
	}
?>