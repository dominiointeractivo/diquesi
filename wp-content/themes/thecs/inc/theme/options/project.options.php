<?php
	class Thecs_Project_Options
	{
		static public $data = null;

		static private $inited = false;

		static public function init_data()
		{
			if(self::$inited) return;
			self::$inited = true;

			self::$data = array(
				'pt_project'=>array(
					//----------------- detail page setting -----------------
					array('var'=>'setting_box_title', 'default'=>'', 'type'=>'text', 'des'=>esc_html__('Detail page setting', 'thecs')),

					array('var'=>'type', 'label'=>esc_html__('Detail page type', 'thecs'), 'default'=>'1', 'type'=>'image_select', 'remove_path_prefix'=>true, 
						'options'=>array('1', '2'),
						'only_available'=>array(
							'1' => array('parallax'),
						),
						'body_class_prefix'=>'project-type-',
						'des'=>''
					),

					array('var'=>'banner_type', 'label'=>esc_html__('Banner type', 'thecs'), 'default'=>'1', 'type'=>'select', 
						'options'=>array(
							'1' => esc_html__('Image', 'thecs'), 
							'2' => esc_html__('Video', 'thecs'), 
						), 
						'only_available' => array(
							'1' => array('image_list', 'slides_to_show&rawsize=1'),
							'2' => array('video_type', 'video_link', 'video_volume', 'video_bg_img')
						),
						'des'=>''
					),

					array('var'=>'video_type', 'label'=>esc_html__('Video type', 'thecs'), 'default'=>'1', 'type'=>'select', 
						'options'=>array(
							'1' => esc_html__('*.mp4', 'thecs'), 
							'2' => esc_html__('Youtube', 'thecs'), 
							'3' => esc_html__('Vimeo', 'thecs')
						), 
						'des'=>''
					),
					array('var'=>'video_link', 'label'=>esc_html__('Video link', 'thecs'), 'default'=>'', 'type'=>'text', 'help'=>'video_link?w=600&h=400&ox=0&oy=-180&bc=#eeeeee', 'des'=>''),
					array('var'=>'video_volume', 'label'=>esc_html__('Video volume', 'thecs'), 'default'=>'1', 'type'=>'text', 'des'=>esc_html__('From 0 to 1.', 'thecs')),
					array('var'=>'video_autoplay', 'label'=>esc_html__('Video autoplay', 'thecs'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),
					array('var'=>'video_bg_img', 'label'=>esc_html__('Video bg image', 'thecs'), 'default'=>'', 'type'=>'image', 'des'=>''),

					array('var'=>'image_list', 'label'=>esc_html__('Slider images', 'thecs'), 'default'=>'', 'class'=>'pt-theme-option-extend-images', 'type'=>'imagelist', 'des'=>esc_html__('Add more images here.', 'thecs')),
					array('var'=>'caption_img', 'label'=>esc_html__('Banner caption', 'thecs'), 'default'=>'', 'type'=>'image', 'des'=>''),
					array('var'=>'hover_img', 'label'=>esc_html__('Hover image', 'thecs'), 'default'=>'', 'type'=>'image', 'des'=>''),

					// -- hr --
					array('var'=>'delimiter', 'default'=>'', 'type'=>'text', 'des'=>''),

					array('var'=>'height_type', 'label'=>esc_html__('Banner height', 'thecs'), 'default'=>'h-normal', 'type'=>'select',
						'options'=>array(
							'h-full' => esc_html__('Full', 'thecs'),
							'h-normal' => esc_html__('Normal', 'thecs'),
							'h-short' => esc_html__('Short', 'thecs'),
						), 
						'des'=>''
					),
					array('var'=>'bg_color', 'label'=>esc_html__('Banner bg color', 'thecs'), 'default'=>'', 'type'=>'color', 'des'=>''),
					array('var'=>'parallax', 'label'=>esc_html__('Parallax effect', 'thecs'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),
					array('var'=>'rawsize', 'label'=>esc_html__('Raw proportion', 'thecs'), 'default'=>'1', 'type'=>'checkbox', 
						'only_available' => array(
							'1' => array('slides_to_show&banner_type=1'),
						),
						'des'=>''
					),
					array('var'=>'slides_to_show', 'label'=>esc_html__('Slides to show', 'thecs'), 'default'=>'1', 'type'=>'select',
						'options'=>array(
							'1' => esc_html__('Multiple', 'thecs'),
							'2' => esc_html__('Single', 'thecs'),
						), 
						'des'=>esc_html__('It determines whether or not show multiple slides on each scroll.', 'thecs'),
					),

					array('var'=>'use_original_img', 'label'=>esc_html__('Use original image', 'thecs'), 'default'=>'0', 'type'=>'checkbox', 'des'=>esc_html__('By default the max-width of image is set to 1920 pixels, you can select "Yes" if you prefer original size.', 'thecs')),

					// -- hr --
					array('var'=>'delimiter', 'default'=>'', 'type'=>'text', 'des'=>''),

					array('var'=>'text_typeset', 'label'=>esc_html__('Text typeset', 'thecs'), 'default'=>'2', 'type'=>'image_select', 'remove_path_prefix'=>true, 
						'options'=>array('1', '2'), 
						'des'=>''
					),

					// -- hr --
					array('var'=>'delimiter', 'default'=>'', 'type'=>'text', 'des'=>''),

					array('var'=>'image_crop_part', 'label'=>esc_html__('Image crop part', 'thecs'), 'default'=>'3', 'type'=>'select', 'help'=>'imageCropPart?w=350&h=300&ox=0&oy=-130&bc=#eeeeee', 
						'options'=>array(
							'1' => esc_html__('1/5', 'thecs'),
							'2' => esc_html__('2/5', 'thecs'),
							'3' => esc_html__('3/5', 'thecs'),
							'4' => esc_html__('4/5', 'thecs'),
							'5' => esc_html__('5/5', 'thecs')
						), 
						'des'=>esc_html__('Set crop part of Featured Image, it\'s useful when the Featured Image is vertical (height larger than width).', 'thecs')
					),

					//----------------- Extra gallery option -----------------
					array('var'=>'setting_box_title', 'default'=>'', 'type'=>'text', 'des'=>esc_html__('Extra gallery option', 'thecs')),
					
					array('var'=>'post_gallery', 'label'=>esc_html__('Extra image', 'thecs'), 'default'=>'', 'type'=>'image-group',
						'extend' => array(
							'gap' => array('type'=>'checkbox', 'label'=>esc_html__('Gap', 'thecs'), 'default'=>'1'),
							'display' => array('type'=>'select', 'label'=>esc_html__('Display', 'thecs'), 'options'=>array(
								'boxed' => esc_html__('Boxed', 'thecs'),
								'fullwidth' => esc_html__('Fullwidth', 'thecs')
							), 'default'=>'boxed'),
							'text_type' => array('type'=>'select', 'label'=>esc_html__('Text type', 'thecs'), 'options'=>array(
								'1' => esc_html__('Type 1', 'thecs'),
								'2' => esc_html__('Type 2', 'thecs'),
								'3' => esc_html__('Type 3', 'thecs'),
							), 'help'=>'text_type?w=600&h=400&ox=0&oy=-180&bc=#eeeeee', 'default'=>'1'),
							'title' => array('type'=>'text', 'label'=>esc_html__('Title', 'thecs'), 'default'=>''),
							'des' => array('type'=>'textarea', 'label'=>esc_html__('Description', 'thecs'), 'default'=>''),
							'video_type' => array('type'=>'select', 'label'=>esc_html__('Video type', 'thecs'), 'options'=>array(
								'0' => esc_html__('None', 'thecs'),
								'1' => esc_html__('*.mp4', 'thecs'), 
								'2' => esc_html__('Youtube', 'thecs'), 
								'3' => esc_html__('Vimeo', 'thecs')
							), 'default'=>'0'),
							'video_link' => array('type'=>'text', 'label'=>esc_html__('Video link', 'thecs'), 'default'=>''),
							'video_volume' => array('type'=>'text', 'label'=>esc_html__('Video volume', 'thecs'), 'default'=>''),
						),
						'hide_cat'=>true,
					 'des'=>esc_html__('Detail page gallery.', 'thecs')),
				)
			);
		}
	}

	Thecs_Project_Options::init_data();
?>