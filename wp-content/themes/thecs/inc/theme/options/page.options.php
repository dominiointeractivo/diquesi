<?php
	class Thecs_Page_Options
	{
		static public $basic_data = null;
		static public $data = null;

		static private $inited = false;

		static public function init_data()
		{
			if(self::$inited) return;
			self::$inited = true;

			self::$basic_data = array(
				'pt_basic'=>array(
					array('var'=>'title_type', 'label'=>esc_html__('Type', 'thecs'), 'default'=>'2', 'type'=>'image_select',
						'options'=>array('2', '1'), 
						'only_available'=>array(
							'1' => array('title_invert_color')
						),
						'des'=>''
					),
					array('var'=>'title_bg', 'label'=>esc_html__('Image', 'thecs'), 'type'=>'image', 'des'=>''),
					array('var'=>'title', 'label'=>esc_html__('Title', 'thecs'), 'default'=>'', 'type'=>'text', 'des'=>''),
					array('var'=>'title_description', 'label'=>esc_html__('Description', 'thecs'), 'default'=>'', 'type'=>'textarea', 'rows'=>3, 'des'=>''),
					array('var'=>'title_invert_color', 'label'=>esc_html__('Invert text-color', 'thecs'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),
				)
			);

			self::$data = array(
				'default'=>array(
					array('var'=>'site_bg', 'label'=>esc_html__('Site background image', 'thecs'), 'default'=>'', 'type'=>'image', 'des'=>''),
				),

				'shortcode'=>array(
					array('var'=>'site_bg', 'label'=>esc_html__('Site background image', 'thecs'), 'default'=>'', 'type'=>'image', 'des'=>''),
				),

				'blog'=>array(
					array('var'=>'type', 'label'=>esc_html__('Blog type', 'thecs'), 'default'=>'2', 'type'=>'image_select',
						'options'=>array('1', '2'),
						'only_available'=>array(
							'2' => array('rawsize'),
						),
						'body_class_prefix'=>'blog-type-',
						'des'=>esc_html__('Select blog type.', 'thecs')
					),
					array('var'=>'category', 'label'=>esc_html__('Category', 'thecs'), 'default'=>'', 'type'=>'category', 'data'=>'post', 'des'=>''),
					array('var'=>'num', 'label'=>esc_html__('Item count', 'thecs'), 'default'=>'12', 'type'=>'text', 'class'=>'mini-width', 'des'=>esc_html__('Show how many items on a page. It means all when equal -1.', 'thecs')),
					array('var'=>'large', 'label'=>esc_html__('Large', 'thecs'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),

					array('var'=>'rawsize', 'label'=>esc_html__('Raw proportion', 'thecs'), 'default'=>'1', 'type'=>'checkbox', 'des'=>''),
					
					array('var'=>'site_bg', 'label'=>esc_html__('Site background image', 'thecs'), 'default'=>'', 'type'=>'image', 'des'=>''),
				),

				'portfolio'=>array(
					array('var'=>'type', 'label'=>esc_html__('Portfolio type', 'thecs'), 'default'=>'1', 'type'=>'image_select',
						'options'=>array('1', '2'),
						'only_available'=>array(
							'1' => array('list_size', 'disperse&list_size=2'),
							'2' => array('row', 'use_boxed', 'item_gap')
						),
						'body_class_prefix'=>'portfolio-type-',
						'des'=>esc_html__('Select portfolio type.', 'thecs')
					),
					array('var'=>'list_size', 'label'=>esc_html__('List size', 'thecs'), 'default'=>'1', 'type'=>'select', 
						'options'=>array(
							'1' => esc_html__('Normal', 'thecs'),
							'2' => esc_html__('Small', 'thecs')
						), 
						'only_available'=>array(
							'2' => array('disperse&type=1')
						),
						'des'=>''
					),
					array('var'=>'disperse', 'label'=>esc_html__('Disperse', 'thecs'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),
					array('var'=>'row', 'label'=>esc_html__('Row', 'thecs'), 'default'=>'1', 'type'=>'image_select', 
						'options'=>array( '1', '1-mini', '2', '3' ), 
						'des'=>''
					),
					array('var'=>'use_boxed', 'label'=>esc_html__('Boxed', 'thecs'), 'default'=>'1', 'type'=>'checkbox', 'des'=>''),
					array('var'=>'item_gap', 'label'=>esc_html__('Item gap', 'thecs'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),
					array('var'=>'thumb_size', 'label'=>esc_html__('Thumbnail size', 'thecs'), 'default'=>'1', 'type'=>'image_select',
						'options'=>array( '1', '2', '3', '4' ),
						'des'=>''
					),
					array('var'=>'category', 'label'=>esc_html__('Category', 'thecs'), 'default'=>'', 'type'=>'category', 'data'=>'project', 'des'=>''),
					array('var'=>'num', 'label'=>esc_html__('Item count', 'thecs'), 'default'=>'12', 'type'=>'text', 'class'=>'mini-width', 'des'=>esc_html__('Show how many items on a page. It means all when equal -1.', 'thecs')),
					
					array('var'=>'site_bg', 'label'=>esc_html__('Site background image', 'thecs'), 'default'=>'', 'type'=>'image', 'des'=>''),
				),

				'gallery'=>array(
					array('var'=>'type', 'label'=>esc_html__('Gallery type', 'thecs'), 'default'=>'1', 'type'=>'image_select',
						'options'=>array('1', '2'),
						'only_available'=>array(
							'1' => array('list_size', 'disperse&list_size=2'),
							'2' => array('row', 'use_boxed', 'item_gap')
						),
						'body_class_prefix'=>'gallery-type-',
						'des'=>esc_html__('Select gallery type.', 'thecs')
					),
					array('var'=>'data', 'label'=>esc_html__('Gallery data', 'thecs'), 'default'=>'', 'type'=>'image-group',
						'extend' => array(
							'title' => array('type'=>'text', 'label'=>esc_html__('Title', 'thecs'), 'default'=>''),
						),
						'hide_cat' => false,
						'des'=>esc_html__('Upload or update your images here.', 'thecs')
					),
					array('var'=>'list_size', 'label'=>esc_html__('List size', 'thecs'), 'default'=>'1', 'type'=>'select', 
						'options'=>array(
							'1' => esc_html__('Normal', 'thecs'),
							'2' => esc_html__('Small', 'thecs')
						), 
						'only_available'=>array(
							'2' => array('disperse&type=1')
						),
						'des'=>''
					),
					array('var'=>'disperse', 'label'=>esc_html__('Disperse', 'thecs'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),
					array('var'=>'row', 'label'=>esc_html__('Row', 'thecs'), 'default'=>'1', 'type'=>'image_select', 
						'options'=>array( '1', '1-mini', '2', '3' ), 
						'des'=>''
					),
					array('var'=>'use_boxed', 'label'=>esc_html__('Boxed', 'thecs'), 'default'=>'1', 'type'=>'checkbox', 'des'=>''),
					array('var'=>'item_gap', 'label'=>esc_html__('Item gap', 'thecs'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),
					array('var'=>'thumb_size', 'label'=>esc_html__('Thumbnail size', 'thecs'), 'default'=>'1', 'type'=>'image_select',
						'options'=>array( '1', '2', '3', '4' ),
						'des'=>''
					),
					array('var'=>'num', 'label'=>esc_html__('Item count', 'thecs'), 'default'=>'-1', 'type'=>'text', 'class'=>'mini-width', 'des'=>esc_html__('Show how many items on a page. It means all when equal -1.', 'thecs')),

					array('var'=>'site_bg', 'label'=>esc_html__('Site background image', 'thecs'), 'default'=>'', 'type'=>'image', 'des'=>''),
				),

				'shop'=>array(
					array('var'=>'type', 'label'=>esc_html__('Shop type', 'thecs'), 'default'=>'1', 'type'=>'image_select',
						'options'=>array('1', '2'),
						'only_available'=>array(
							'1' => array('list_size', 'disperse&list_size=2'),
							'2' => array('row', 'use_boxed', 'item_gap')
						),
						'body_class_prefix'=>'shop-type-',
						'des'=>esc_html__('Select shop type.', 'thecs')
					),
					array('var'=>'list_size', 'label'=>esc_html__('List size', 'thecs'), 'default'=>'1', 'type'=>'select', 
						'options'=>array(
							'1' => esc_html__('Normal', 'thecs'),
							'2' => esc_html__('Small', 'thecs')
						), 
						'only_available'=>array(
							'2' => array('disperse&type=1')
						),
						'des'=>''
					),
					array('var'=>'disperse', 'label'=>esc_html__('Disperse', 'thecs'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),
					array('var'=>'row', 'label'=>esc_html__('Row', 'thecs'), 'default'=>'1', 'type'=>'image_select', 
						'options'=>array( '1', '1-mini', '2', '3' ), 
						'des'=>''
					),
					array('var'=>'use_boxed', 'label'=>esc_html__('Boxed', 'thecs'), 'default'=>'1', 'type'=>'checkbox', 'des'=>''),
					array('var'=>'item_gap', 'label'=>esc_html__('Item gap', 'thecs'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),
					array('var'=>'thumb_size', 'label'=>esc_html__('Thumbnail size', 'thecs'), 'default'=>'1', 'type'=>'image_select',
						'options'=>array( '1', '2', '3', '4' ),
						'des'=>''
					),
					array('var'=>'num', 'label'=>esc_html__('Item count', 'thecs'), 'default'=>'-1', 'type'=>'text', 'class'=>'mini-width', 'des'=>esc_html__('Show how many items on a page. It means all when equal -1.', 'thecs')),

					array('var'=>'site_bg', 'label'=>esc_html__('Site background image', 'thecs'), 'default'=>'', 'type'=>'image', 'des'=>''),
				),
			);
		}
	}

	Thecs_Page_Options::init_data();
?>