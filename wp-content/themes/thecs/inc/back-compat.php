<?php
function thecs_switch_theme() {
	switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );

	unset( $_GET['activated'] );

	add_action( 'admin_notices', 'thecs_upgrade_notice' );
}
add_action( 'after_switch_theme', 'thecs_switch_theme' );

function thecs_upgrade_notice() {
	$message = sprintf( esc_html__( 'thecs Theme requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'thecs' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

function thecs_customize() {
	wp_die( sprintf( esc_html__( 'thecs Theme requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'thecs' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'thecs_customize' );

function thecs_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( esc_html__( 'thecs Theme requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'thecs' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'thecs_preview' );
