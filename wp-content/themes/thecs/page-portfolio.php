<?php
/*
 * Template Name: Portfolio
 */
	require THECS_THEME_DIR . '/inc/theme/parse-page-setting.php';

	global $thecs_g_site_bg;
	$thecs_g_site_bg = thecs_get_page_setting_property( 'portfolio_site_bg', '' );

	get_header();

	$thecs_portfolio_type = thecs_get_page_setting_property( 'portfolio_type', '1' );
	require thecs_parse_part_path( '/template-parts/portfolio/content-'.$thecs_portfolio_type );
	
	get_footer(); 
?>