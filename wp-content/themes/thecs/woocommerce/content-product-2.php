<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
global $thecs_row;
global $thecs_use_boxed;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

$thecs_thumburl = '';
$thecs_thumb_width = 0;
$thecs_thumb_height = 0;

if( $thecs_row == '1' )
{
	$thecs_max_size = $thecs_use_boxed === 'boxed' ? 1200 : 1920;
}
elseif( $thecs_row == '2' )
{
	$thecs_max_size = 720;
}
elseif( $thecs_row == '3' )
{
	$thecs_max_size = 720;
}

if( has_post_thumbnail() )
{
	$thecs_img_obj = thecs_get_image(array(
		'rid' => get_post_thumbnail_id(),
		'max_width' => $thecs_max_size,
		'max_height' => $thecs_max_size,
	));

	if( $thecs_img_obj )
	{
		$thecs_thumburl = $thecs_img_obj['thumb'];

		$thecs_thumb_width = $thecs_img_obj['thumb_width'];
		$thecs_thumb_height = $thecs_img_obj['thumb_height'];
	}
}

$thecs_post_data = thecs_get_post_setting( get_the_ID() );

$thecs_caption_rid = thecs_get_property( $thecs_post_data, 'caption_img', '', 'pt_product_' );
$thecs_caption_url = '';

$thecs_img_obj = thecs_get_image(array(
	'rid' => $thecs_caption_rid,
	'width' => $thecs_thumb_width,
	'height' => $thecs_thumb_height,
));

if( $thecs_img_obj )
{
	$thecs_caption_url = $thecs_img_obj['thumb'];
}

?>
<div <?php post_class('item product-item'); ?> data-w="<?php echo esc_attr( $thecs_thumb_width ); ?>" data-h="<?php echo esc_attr( $thecs_thumb_height ); ?>">
	<div class="img">
		<div class="bg-full" data-bg="<?php echo esc_url( $thecs_thumburl ); ?>"></div>
		<div class="hover-img" data-bg="<?php echo esc_url( $thecs_caption_url ); ?>"></div>
		<a class="full" data-href="<?php the_permalink(); ?>"></a>
	</div>

	<div class="text">
		<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
		<h6><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
	</div>
	<div class="mask"></div>
	<div class="list-icon">
		<?php 
			woocommerce_template_loop_add_to_cart();
		?>
	</div>
</div>
