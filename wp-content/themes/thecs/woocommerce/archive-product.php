<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

$thecs_page_id = wc_get_page_id( 'shop' );

require THECS_THEME_DIR . '/inc/theme/parse-page-setting.php';

global $thecs_g_site_bg;
$thecs_g_site_bg = thecs_get_page_setting_property( 'shop_site_bg', '' );

get_header( 'shop' ); ?>

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 * @hooked WC_Structured_Data::generate_website_data() - 30
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

	<?php
		global $thecs_row;
		global $thecs_use_boxed;

		$thecs_is_preset = false;

		$thecs_preset_key = get_query_var( '_preset' );
		if( $thecs_preset_key )
		{
			require THECS_THEME_DIR . '/inc/theme/page-preset.php';

			if( isset( $thecs_page_preset['shop'] ) && isset( $thecs_page_preset['shop'][$thecs_preset_key] ) )
			{
				$thecs_is_preset = true;

				$thecs_preset_data = $thecs_page_preset['shop'][$thecs_preset_key];
				$thecs_shop_type = $thecs_preset_data['type'];
				
				if( $thecs_shop_type === '1' )
				{
					$thecs_list_size = $thecs_preset_data['list_size'] === '1' ? 'size-normal' : 'size-small';
					$thecs_disperse = $thecs_preset_data['disperse'] === '1' ? 'disperse' : '';
					$thecs_thumb_size = $thecs_preset_data['thumb_size'];
				}
				elseif( $thecs_shop_type === '2' )
				{
					$thecs_row = $thecs_preset_data['row'];
					$thecs_boxed = $thecs_preset_data['boxed'] === '1' ? 'boxed' : '';
					$thecs_item_gap = $thecs_preset_data['item_gap'] === '1' ? 'item-gap' : '';
					$thecs_thumb_size = $thecs_preset_data['thumb_size'];
				}
			}
		}
		
		if( !$thecs_is_preset )
		{
			$thecs_shop_type = thecs_get_page_setting_property( 'shop_type', '1' );

			if( $thecs_shop_type === '1' )
			{
				$thecs_list_size = thecs_get_page_setting_property( 'shop_list_size', '1' ) === '1' ? 'size-normal' : 'size-small';
				$thecs_disperse = thecs_get_page_setting_property( 'shop_disperse', '0' ) === '1' ? 'disperse' : '';
				$thecs_thumb_size = thecs_get_page_setting_property( 'shop_thumb_size', '1' );
			}
			elseif( $thecs_shop_type === '2' )
			{
				$thecs_row = thecs_get_page_setting_property( 'shop_row', '1' );
				$thecs_boxed = thecs_get_page_setting_property( 'shop_use_boxed', '1' ) === '1' ? 'boxed' : '';
				$thecs_item_gap = thecs_get_page_setting_property( 'shop_item_gap', '0' ) === '1' ? 'item-gap' : '';
				$thecs_thumb_size = thecs_get_page_setting_property( 'shop_thumb_size', '1' );
			}
		}

		if( $thecs_shop_type === '1' )
		{
			$thecs_fixed_classes = array('', 'fixed fixed-h', 'fixed fixed-v', 'fixed fixed-s');
			$thecs_fixed_class = $thecs_fixed_classes[intval($thecs_thumb_size) - 1];
			
			$thecs_list_class = 'pic-list product-list style-01 '.$thecs_list_size.' '.$thecs_disperse.' '.$thecs_fixed_class;
		}
		elseif( $thecs_shop_type === '2' )
		{
			$thecs_use_boxed = $thecs_boxed;
			
			$thecs_fixed_classes = array('', 'fixed fixed-h', 'fixed fixed-v', 'fixed fixed-s');
			$thecs_fixed_class = $thecs_fixed_classes[intval($thecs_thumb_size) - 1];

			$thecs_list_class = 'pic-list product-list style-02 row-'.$thecs_row.' '.$thecs_boxed.' '.$thecs_fixed_class.' '.$thecs_item_gap;
		}
	?>

    <div class="<?php echo esc_attr( $thecs_list_class ); ?>">

    	<div class="list-widgets">

			<i class="call-list-widgets btn"></i>

			<div class="list-widgets-wrap">

				 <?php woocommerce_catalog_ordering(); ?>

				 <?php get_sidebar('woocommerce'); ?>

			</div><!-- wrap end -->

		</div>


	<?php
		if ( woocommerce_product_loop() ) {

			/**
			 * Hook: woocommerce_before_shop_loop.
			 *
			 * @hooked wc_print_notices - 10
			 * @hooked woocommerce_result_count - 20
			 * @hooked woocommerce_catalog_ordering - 30
			 */
			//do_action( 'woocommerce_before_shop_loop' );

			?><div class="wrap products"><?php

			thecs_output_page_title();

			woocommerce_product_subcategories();

			if ( wc_get_loop_prop( 'total' ) ) {
				while ( have_posts() ) {
					the_post();

					/**
					 * Hook: woocommerce_shop_loop.
					 *
					 * @hooked WC_Structured_Data::generate_product_data() - 10
					 */
					do_action( 'woocommerce_shop_loop' );

					wc_get_template_part( 'content', 'product-'.$thecs_shop_type );
				}
			}

			?></div><?php

			/**
			 * Hook: woocommerce_after_shop_loop.
			 *
			 * @hooked woocommerce_pagination - 10
			 */
			do_action( 'woocommerce_after_shop_loop' );
		} else {
			/**
			 * Hook: woocommerce_no_products_found.
			 *
			 * @hooked wc_no_products_found - 10
			 */
			do_action( 'woocommerce_no_products_found' );
		}
	
		?></div><?php

		/**
		 * Hook: woocommerce_after_main_content.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );

		get_footer( 'shop' );
	?>