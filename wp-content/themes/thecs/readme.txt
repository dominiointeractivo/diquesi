Theme Name: thecs
Theme URI: http://3theme.com/tf004/
Description: Thecs - Portfolio WordPress Theme
Version: 1.1.7
Author: foreverpinetree@gmail.com
Author URI: http://foreverpinetree.com/
Text Domain: thecs
Tags: custom-menu, featured-images, translation-ready
License: GNU General Public License version 3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html