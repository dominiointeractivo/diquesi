<?php
/*
 * Template Name: Gallery
 */
	require THECS_THEME_DIR . '/inc/theme/parse-page-setting.php';

	global $thecs_g_site_bg;
	$thecs_g_site_bg = thecs_get_page_setting_property( 'gallery_site_bg', '' );

	get_header();

	$thecs_gallery_type = thecs_get_page_setting_property( 'gallery_type', '1' );
	require thecs_parse_part_path( '/template-parts/gallery/content-'.$thecs_gallery_type );

	get_footer(); 
?>