<?php

	require THECS_THEME_DIR . '/inc/theme/parse-page-setting.php';
	
	global $thecs_g_site_bg;
	$thecs_g_site_bg = thecs_get_page_setting_property( 'default_site_bg', '' );

	get_header();

	?>
	<div class="default-template-page">
		<div class="wrap">
		<?php
			while (have_posts())
			{
				the_post();

				?><h1 class="title"><?php the_title(); ?></h1><?php
				?><div class="text-area cf"><?php the_content(); ?></div><?php

				wp_link_pages('before=<div class="page-links">&after=</div>&next_or_number=next&previouspagelink='.esc_html__('Prev Page', 'thecs').'&nextpagelink='.esc_html__('Next Page', 'thecs'));
				
				$thecs_show_comment = thecs_get_theme_option('thecs_show_comment', '1');
				if ($thecs_show_comment != '0' && (comments_open() || get_comments_number())) {
					comments_template();
				}
			}
		?>
		</div>
	</div>
	<?php
	
	get_footer(); 
?>