<?php
/*
 * Template Name: Blog
 */
	require THECS_THEME_DIR . '/inc/theme/parse-page-setting.php';

	global $thecs_g_site_bg;
	$thecs_g_site_bg = thecs_get_page_setting_property( 'blog_site_bg', '' );

	get_header();

	$thecs_blog_type = thecs_get_page_setting_property( 'blog_type', '2' );
	$thecs_blog_large = thecs_get_page_setting_property( 'blog_large', '0' );

	if( $thecs_blog_type === '2' )
	{
		$thecs_raw_proportion = thecs_get_page_setting_property( 'blog_rawsize', '1' ) === '1';
		$thecs_raw_class = $thecs_raw_proportion ? ' raw-proportion' : '';
	}
	else
	{
		$thecs_raw_proportion = false;
		$thecs_raw_class = '';
	}

	$thecs_list_class = 'blog-list style-0'.$thecs_blog_type.$thecs_raw_class;
	$thecs_list_class .= $thecs_blog_large === '1' ? ' large' : '';
	?>
	<div class="<?php echo esc_attr( $thecs_list_class ); ?>">
	<?php
		require thecs_parse_part_path( '/template-parts/blog/content' );
	?>
	</div>
	<?php
	
	get_footer(); 
?>