/**
 * @author   foreverpinetree@gmail.com
 * @profile  www.3theme.com
 */

//checkbox plugin
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof exports?module.exports=a(require("jquery")):a(jQuery)}(function(a){function b(b,c){this.element=b,this.$element=a(b);var d=this.$element.data();return""===d.reverse&&(d.reverse=!0),""===d.switchAlways&&(d.switchAlways=!0),""===d.html&&(d.html=!0),this.options=a.extend({},a.fn.checkboxpicker.defaults,c,d),this.$element.closest("label").length?void console.warn(this.options.warningMessage):(this.$group=a.create("div"),this.$buttons=a.create("a","a"),this.$off=this.$buttons.eq(this.options.reverse?1:0),this.$on=this.$buttons.eq(this.options.reverse?0:1),void this.init())}a.create=function(){return a(a.map(arguments,a.proxy(document,"createElement")))},b.prototype={init:function(){var b=this.options.html?"html":"text";this.$element.addClass("hidden"),this.$group.addClass(this.options.baseGroupCls).addClass(this.options.groupCls),this.$buttons.addClass(this.options.baseCls).addClass(this.options.cls),this.options.offLabel&&this.$off[b](this.options.offLabel),this.options.onLabel&&this.$on[b](this.options.onLabel),this.options.offIconCls&&(this.options.offLabel&&this.$off.prepend("&nbsp;"),a.create("span").addClass(this.options.iconCls).addClass(this.options.offIconCls).prependTo(this.$off)),this.options.onIconCls&&(this.options.onLabel&&this.$on.prepend("&nbsp;"),a.create("span").addClass(this.options.iconCls).addClass(this.options.onIconCls).prependTo(this.$on)),this.element.checked?(this.$on.addClass("active"),this.$on.addClass(this.options.onActiveCls),this.$off.addClass(this.options.offCls)):(this.$off.addClass("active"),this.$off.addClass(this.options.offActiveCls),this.$on.addClass(this.options.onCls)),this.element.title?this.$group.attr("title",this.element.title):(this.options.offTitle&&this.$off.attr("title",this.options.offTitle),this.options.onTitle&&this.$on.attr("title",this.options.onTitle)),this.$group.on("keydown",a.proxy(this,"keydown")),this.$buttons.on("click",a.proxy(this,"click")),this.$element.on("change",a.proxy(this,"toggleChecked")),a(this.element.labels).on("click",a.proxy(this,"focus")),a(this.element.form).on("reset",a.proxy(this,"reset")),this.$group.append(this.$buttons).insertAfter(this.element),this.element.disabled?(this.$buttons.addClass("disabled"),this.options.disabledCursor&&this.$group.css("cursor",this.options.disabledCursor)):(this.$group.attr("tabindex",this.element.tabIndex),this.element.autofocus&&this.focus())},toggleChecked:function(){this.$buttons.toggleClass("active"),this.$off.toggleClass(this.options.offCls),this.$off.toggleClass(this.options.offActiveCls),this.$on.toggleClass(this.options.onCls),this.$on.toggleClass(this.options.onActiveCls)},toggleDisabled:function(){this.$buttons.toggleClass("disabled"),this.element.disabled?(this.$group.attr("tabindex",this.element.tabIndex),this.$group.css("cursor","")):(this.$group.removeAttr("tabindex"),this.options.disabledCursor&&this.$group.css("cursor",this.options.disabledCursor))},focus:function(){this.$group.trigger("focus")},click:function(b){var c=a(b.currentTarget);c.hasClass("active")&&!this.options.switchAlways||this.change()},change:function(){this.set(!this.element.checked)},set:function(a){this.element.checked=a,this.$element.trigger("change")},keydown:function(b){-1!=a.inArray(b.keyCode,this.options.toggleKeyCodes)?(b.preventDefault(),this.change()):13==b.keyCode&&a(this.element.form).trigger("submit")},reset:function(){(this.element.defaultChecked&&this.$off.hasClass("active")||!this.element.defaultChecked&&this.$on.hasClass("active"))&&this.set(this.element.defaultChecked)}};var c=a.extend({},a.propHooks);a.extend(a.propHooks,{checked:{set:function(b,d){var e=a.data(b,"bs.checkbox");e&&b.checked!=d&&e.change(d),c.checked&&c.checked.set&&c.checked.set(b,d)}},disabled:{set:function(b,d){var e=a.data(b,"bs.checkbox");e&&b.disabled!=d&&e.toggleDisabled(),c.disabled&&c.disabled.set&&c.disabled.set(b,d)}}});var d=a.fn.checkboxpicker;return a.fn.checkboxpicker=function(c,d){var e;return e=this instanceof a?this:a("string"==typeof c?c:d),e.each(function(){var d=a.data(this,"bs.checkbox");d||(d=new b(this,c),a.data(this,"bs.checkbox",d))})},a.fn.checkboxpicker.defaults={baseGroupCls:"btn-group",baseCls:"btn",groupCls:null,cls:null,offCls:"btn-default",onCls:"btn-default",offActiveCls:"btn-danger",onActiveCls:"btn-success",offLabel:"No",onLabel:"Yes",offTitle:!1,onTitle:!1,iconCls:"glyphicon",disabledCursor:"not-allowed",toggleKeyCodes:[13,32],warningMessage:"Please do not use Bootstrap-checkbox element in label element."},a.fn.checkboxpicker.Constructor=b,a.fn.checkboxpicker.noConflict=function(){return a.fn.checkboxpicker=d,this},a.fn.checkboxpicker});

if(!window.pt_admin)
{
	window.pt_admin = {};
}

/**
 * pt-group--------------------start
 */
!(function($)
{
	"use strict";
	if(!window.pt_group)
	{
		window.pt_group = {};
	}

	pt_group._listeners = {};
	pt_group._setImageByAttachmentId = null;

	pt_group.init = function(params)
	{
		if(params)
		{
			pt_group._setImageByAttachmentId = params.setImageByAttachmentId;
		}
		$('.pt-group').each(function(){
			var $group = $(this);
			var $input = $group.find('.pt-group-value');
			$group.closest('form').submit(function () {
				var dataArr = [];
				$group.find('.pt-group-item:not(.hidden)').each(function(){
					var $item = $(this);
					var arr = [];
					$item.find('.pt-group-option-data').each(function(){
						var $self = $(this);
						var data = $self.data('data');
						if(data == undefined)
						{
							data = $self.val();
						}
						var name = $self.data('name');
						if(name == undefined)
						{
							name = $self.attr('name');
						}
						arr.push(encodeURIComponent(name) + "=" + encodeURIComponent(data));
					});
					dataArr.push(arr.join('&'));
				});
				$input.val(dataArr.join(','));
			});
			pt_group.generateByData($group, $input.val());
		});
		$('div.pt-group>.pt-option-add-button').on('click', pt_group.clone);
		$('div.pt-group .pt-group-item-delete').one('click', function(evt){
			pt_group.dispatchEvent('delete-item', evt.currentTarget.parentNode);
			$(evt.currentTarget.parentNode).remove();
		});
		$(".pt-group-container").sortable({
			items: '.pt-group-item:not(".hidden")',
			distance : 30
		});
	}

	pt_group.clone = function(evt)
	{
		var $group = $(evt.currentTarget.parentNode);
		var $cloneSource = $group.find('.pt-group-item.hidden').clone().removeAttr('style').removeClass('hidden');
		$group.find('.pt-group-container').append($cloneSource);
		$cloneSource.find('.pt-group-item-delete').one('click', function(evt){
			pt_group.dispatchEvent('delete-item', evt.currentTarget.parentNode);
			$(evt.currentTarget.parentNode).remove();
		});
		pt_group.dispatchEvent('clone-item', $cloneSource.get(0));
	}

	pt_group.generateByData = function($group, dataStr)
	{
		if(!dataStr)
		{
			return;
		}
		var data = dataStr.split(',');
		var $cloneSource  = null;
		var len = data.length, arr, i, j, str, cArr, cLen, cStr, kArr, obj;
		var $target, bindto, bindtoTarget, paramArr;
		for(i = 0; i < len; i ++)
		{
			$cloneSource = $group.find('.pt-group-item.hidden').clone().removeAttr('style').removeClass('hidden');
			$group.find('.pt-group-container').append($cloneSource);
			obj = {};
			str = data[i];
			arr = str.split('&');
			cLen = arr.length;
			for(j = 0; j < cLen; j ++)
			{
				cStr = arr[j];
				if(cStr)
				{
					kArr = cStr.split('=');
					if(kArr.length > 1)
					{
						obj[decodeURIComponent(kArr[0])] = decodeURIComponent(kArr[1]);
					}
				}
			}
			for(var key in obj)
			{
				$target = $cloneSource.find('.pt-group-option-data[data-name="' + key + '"]');
				$target.val(obj[key]);
				bindto = $target.data('bindto');
				if(bindto)
				{
					paramArr = bindto.split('&');
					if(paramArr && paramArr.length > 0)
					{
						if(paramArr[0] == 'image')
						{
							bindtoTarget = $target.parent().find('.' + paramArr[1]);
							if(bindtoTarget.get(0))
							{
								if(pt_group._setImageByAttachmentId)
								{
									if(obj[key])
									{
										pt_group._setImageByAttachmentId(bindtoTarget, obj[key], 'thumbnail', true);
										$(bindtoTarget).parent().removeClass('empty');
									}
									else
									{
										//
									}
								}
								else
								{
									bindtoTarget.attr(paramArr[2], obj[key]);
								}
								if(obj[key])
								{
									bindtoTarget.parent().show();
								}
							}
						}
					}
				}
			}
			pt_group.dispatchEvent('generate-item', $cloneSource.get(0));
		}
	}

	pt_group.addListener = function(type, listener, scope, params)
	{
		if(!type || !listener)
		{
			return;
		}
		var arr = this._listeners[type];
		if(!arr)
		{
			arr = [];
			this._listeners[type] = arr;
		}
		arr.push({listener:listener, scope:scope, params:params});
	}

	pt_group.removeListener = function(type, listener, scope)
	{
		if(!type || !listener)
		{
			return;
		}
		var arr = this._listeners[type];
		if(!arr)
		{
			return;
		}
		var len = arr.length, obj;
		for(var i = len - 1; i >= 0; i --)
		{
			obj = arr[i];
			if(obj && obj.listener === listener && obj.scope === scope)
			{
				arr.splice(i, 1);
				delete obj.listener;
				delete obj.scope;
			}
		}
	}

	pt_group.dispatchEvent = function(type, paramObj)
	{
		if(!type)
		{
			return;
		}
		var arr = this._listeners[type];
		if(!arr)
		{
			return;
		}
		var len = arr.length, obj, params;
		for(var i = len - 1; i >= 0; i --)
		{
			obj = arr[i];
			if(obj && obj.listener)
			{
				params = obj.params || [];
				params.unshift(paramObj);
				params.unshift(type);
				obj.listener.apply(obj.scope || null, params);
			}
		}
	}
}(jQuery));
/**
 * pt-group--------------------end
 */

/**
 * pt-select--------------------begin
 */
!(function($)
{
	"use strict";

	var Select = function($target, obj)
	{
		if(!$target || !$target.get(0))
		{
			return;
		}

		this._allowCalls = ['setValue'];

		var extendClass = obj.className || '';
		var extendClassName = extendClass ? ' ' + extendClass : '';

		var originSelect = $target;
		var liHtml = '<ul class="pt-select-list">';
		var self = this;
		originSelect.find('option').each(function(){
			var selected = $(this).attr('selected');
			var value = $(this).val() || '';
			var key;
			var dataset = this.dataset, dataArr = [], dataValue;
			if(dataset)
			{
				for(key in dataset)
				{
					dataValue = dataset[key];
					if(key != undefined && dataValue != undefined)
					{
						dataArr.push('data-' + key + '="' + dataValue + '"');
					}
				}
			}
			else
			{
				var insterestedData = ['previewid'];
				var i = insterestedData.length;
				while(i --)
				{
					key = insterestedData[i];
					dataValue = self.getDataSet(this, key);
					dataArr.push('data-' + key + '="' + dataValue + '"');
				}
			}
			var dataStr = dataArr.join(' ');
			liHtml += '<li class="' + (selected ? 'selected list-item' : 'list-item') + '" data-value="' + value + '" ' + dataStr + '>'
				+ this.innerHTML + '</li>';
		});
		liHtml += '</ul>';
		var ele = originSelect.get(0);
		originSelect.addClass('is-pt-select');
		var newHtml = '<div class="' + ele.className + extendClassName + '">' + '<span class="pt-select-label">&nbsp;</span>' + liHtml + '</div>';
		var newEle = $(newHtml).get(0);
		originSelect.before(newEle);
		newEle.appendChild(ele);
		this._originSelect = originSelect;
		this._newElement = newEle;

		this.initElement(this._newElement);
		this._onClickWindowHandler = this._onClickWindow.bind(this);
		window.addEventListener('click', this._onClickWindowHandler, false);
	}

	var p = Select.prototype;

	p.initElement = function(selectDivNode)
	{
		var sel = selectDivNode;
		if(!$(sel).hasClass('pt-select'))
		{
			$(sel).addClass('pt-select');
		}
		var item = sel.querySelector("ul>li.selected");
		if(!item) item = sel.querySelector("ul>li:first-child");
		if(!item) return;
		var value = $(item).data('value');
		sel.querySelector('select').value = value;
		var text = item.innerHTML;
		var labelEle = sel.querySelector('.pt-select-label');
		if(labelEle) labelEle.innerHTML = text;
		this.initElementEvent(sel);
	}

	p.initElementEvent = function(selectDivNode)
	{
		var sel = selectDivNode;
		var labelEle = sel.querySelector('.pt-select-label');
		$(labelEle).off("click");
		$(labelEle).on("click", function(evt){
			var list = $(this.parentNode).find('.pt-select-list');
			if(list.hasClass("open"))
			{
				list.removeClass("open");
			}
			else
			{
				list.addClass("open");
			}
		});
		var li = $(sel).find('ul>li');
		li.on("click", function(evt){
			var index = $(this).index();
			var list = $(this.parentNode);
			list.removeClass("open");
			var root = list.parent();
			root.find('select>option').removeAttr('selected');
			var selectItem = root.find('select>option:eq(' + index + ')');
			selectItem.attr("selected", "selected");
			root.find('select').val(selectItem.val());
			$(root.find('select')).trigger("change");
			root.find('.pt-select-label').text($(this).text());
		});
	}

	p.setValue = function(value)
	{
		var $list = $(this._newElement).find('ul');
		var $target = $list.children('li[data-value="' + value + '"]');
		if($target.length > 0)
		{
			var index = $target.index();
			$list.removeClass("open");
			var $root = $list.parent();
			$root.find('select>option').removeAttr('selected');
			var $selectItem = $root.find('select>option:eq(' + index + ')');
			$selectItem.attr("selected", "selected");
			$root.find('select').val($selectItem.val());
			$($root.find('select')).trigger("change");
			$root.find('.pt-select-label').text($target.text());
		}
	}

	p._onClickWindow = function(evt)
	{
		var list = $(this._newElement).find('ul');
		if(!list.hasClass("open"))
		{
			return;
		}
		if(!$.contains(this._newElement, evt.target))
		{
			list.removeClass("open");
		}
	}

	p.getDataSet = function(element, key)
	{
		if(!element || !element.getAttribute) return null;
		return element.dataset ? element.dataset[key] : element.getAttribute("data-" + key);
	}

	p.reset = function()
	{
		this._originSelect.selectedIndex = 0;
		var list = $(this._newElement).find('ul');
		list.removeClass("open");
		var li0 = list.find('li:first');
		var root = list.parent();
		root.find('.pt-select-label').text(li0.text());
	}

	p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(!Array.isArray(funcParams))
        {
        	funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) == 'function')
        {
            f.apply(this, funcParams || []);
        }
    }

	p.dispose = function()
	{
		if(!this._newElement) return;
		$(this._newElement).find('.pt-select-label').off("click");
		$(this._newElement).find('ul>li').off("click");
		window.removeEventListener('click', this._onClickWindowHandler, false);
		this._onClickWindowHandler = null;
	}

	$.fn.extend({
		ptSelect:function(obj) {
			this.each(function(){
				if($(this).hasClass('is-pt-select'))
					return;
				if(!window.__pt_selects)
					window.__pt_selects = [];
				var s = new Select($(this), obj || {});
				window.__pt_selects.push({element:this, select:s});
			});
		},
		ptSelectCall:function(funcName, funcParams) {
			this.each(function(){
				if(!window.__pt_selects)
					return;
				var i, len = window.__pt_selects.length, obj, s;
				for(i = 0; i < len; i ++)
				{
					obj = window.__pt_selects[i];
					if(obj && obj.element == this)
					{
						s = obj.select;
						
						if(s)
						{
							s.execute(funcName, funcParams);
							break;
						}
					}
				}
			});
		},
		resetPtSelect:function() {
			this.each(function(){
				if(!window.__pt_selects)
					return;
				var i, len = window.__pt_selects.length, obj, s;
				for(i = 0; i < len; i ++)
				{
					obj = window.__pt_selects[i];
					if(obj && obj.element == this)
					{
						s = obj.select;
						if(s)
						{
							s.reset();
							break;
						}
					}
				}
			});
		},
		disposePtSelect:function() {
			this.each(function(){
				if(!window.__pt_selects)
					return;
				var i, len = window.__pt_selects.length, obj, s;
				for(i = 0; i < len; i ++)
				{
					obj = window.__pt_selects[i];
					if(obj && obj.element == this)
					{
						s = obj.select;
						if(s)
						{
							s.dispose();
							window.__pt_selects.splice(i, 1);
							break;
						}
					}
				}
			});
		}
	});
})(jQuery);
/**
 * pt-select--------------------end
 */

 /**
 * pt-image-select--------------------begin
 */
!(function($, win)
{
	"use strict";
	var ImageSelect = function(element, obj)
	{
		element.__ptImageSelect = this;
		this._element = element;
		this.$ele = $(element);

		var extendClass = obj.className || '';
		if(extendClass)
		{
			this.$ele.addClass(extendClass);
		}

		this._options = [];

		this._allowCalls = ['dispose', 'reset', 'getOptions'];

		this.$input = this.$ele.find('input[type="hidden"]');
		this.$list = this.$ele.find('ul');
		this.$items = this.$list.children('li');

		this._defaultValue = this.$input.val();

		this._imgType = this.$ele.data('imgType') || 'png';

		this.init();
	}

	var p = ImageSelect.prototype;

	p.init = function()
	{
		this.$items.removeClass('selected');

		var url, imgType = this._imgType, self = this, value = this._defaultValue;

		this.$items.each(function(){
			var $this = $(this);

			url = win.__pt_root_url + '/data/images/image_select/'+ $this.data('imgName') + '.' + imgType;
			$this.empty().append('<img src="' + url + '" alt=""/>');

			self._options.push($this.data('value'));

			if($this.data('value') == value)
			{
				$this.addClass('selected');
			}
		});

		this.$items.on('click', function(){
			self.$input.val($(this).data('value'));
			self.$input.trigger('change');
		});

		this.$input.on('change', function(){
			var value = this.value;

			self.$items.removeClass('selected');
			self.$items.each(function(){
				var $this = $(this);

				if($this.data('value') == value)
				{
					$this.addClass('selected');
				}
			});
		});

		this.$list.addClass('ready');
	}

	p.reset = function()
	{
		this.$input.val(this._defaultValue);
		this.$input.trigger('change');
	}

	p.getOptions = function()
	{
		return this._options;
	}

	p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(funcParams && !Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName], result;
        if(f && typeof(f) == 'function')
        {
            result = f.apply(this, funcParams || []);
        }

        return result;
    };
	
	p.dispose = function()
	{
		 this._element.__ptImageSelect = null;
	}

	$.fn.extend({
		ptImageSelect:function(obj) {
			this.each(function(){
                var imageSelect = this.__ptImageSelect;
            	imageSelect && imageSelect.dispose();

                new ImageSelect(this, obj || {});
            });
            return this;
		},
		ptImageSelectCall:function(funcName, funcParams) {
            var ele = this.get(0), result = [];
            if(ele && ele.__ptImageSelect)
            {
            	result = ele.__ptImageSelect.execute(funcName, funcParams);
            }
            return result;
		}
	});
})(jQuery, window);
/**
 * pt-image-select--------------------end
 */

/**
 * PreviewImage--------------------begin
 */
!(function($)
{
	"use strict";
	var PreviewImage = function()
	{
		this._loadingImage = new Image();
		this._loadingImage.style.zIndex = 0;
		this._loadingImage.className = 'pt-admin-preview-loading';
		if(window.__pt_root_url)
		{
			this._loadingImage.src = window.__pt_root_url + '/data/images/loading.gif';
		}
		this._previewImage = new Image();
		this._previewImage.style.zIndex = 1;
		this._previewImage.style.position = 'absolute';
		this._previewImage.addEventListener("load", function(evt){
			this.style.display = 'block';
			$(this.parentNode).removeClass('loading');
		}, false);
		this._htmlElement = document.createElement("div");
		this._htmlElement.className = 'pt-admin-preview-image';
		this._htmlElement.appendChild(this._loadingImage);
		this._htmlElement.appendChild(this._previewImage);
		this._currentPreviewUrl = '';
		this._htmlElement.addEventListener("click", function(evt){
			pt_admin.hidePreviewImage();
		}, false);
	}

	var p = PreviewImage.prototype;

	p.show = function(url, x, y, w, h, borderColor)
	{
		if(this._currentPreviewUrl == url) return;
		this._currentPreviewUrl = url;
		this._previewImage.style.display = 'none';
		this._previewImage.src = url;
		$(this._htmlElement).addClass('loading');
		this._htmlElement.style.left = x + 'px';
		this._htmlElement.style.top = y + 'px';
		this._htmlElement.style.width = w ? w + 'px' : 'auto';
		this._htmlElement.style.height = h ? h + 'px' : 'auto';
		if(borderColor)
		{
			this._htmlElement.style.border = "1px solid " + borderColor;
		}
		document.body.appendChild(this._htmlElement);
	}

	p.hide = function()
	{
		this._currentPreviewUrl = '';
		this._previewImage.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
		if(this._htmlElement.parentNode)
		{
			this._htmlElement.parentNode.removeChild(this._htmlElement);
		}
	}

	pt_admin.PreviewImage = PreviewImage;

}(jQuery));
/**
 * PreviewImage--------------------end
 */

/**
 * admin-custom--------------------begin
 */
!(function($)
{
	"use strict";

	var p = window.pt_admin;
	
	p._attachments = {};
	p.targetfield = null;
	p._previewImg = new p.PreviewImage();

	p.setImageByAttachmentId = function($target, rid, sizeType, isImg)
	{
		if(!$target || !rid) return;

		var sizes = ['thumbnail', 'medium', 'large', 'full'];
		if(sizes.indexOf(sizeType) == -1)
		{
			sizeType = 'full';
		}
		var $classTarget = isImg ? $target.parent() : $target;
		$classTarget.addClass('is-loading');
		var attachment = wp.media.attachment(rid);
		var index = sizes.indexOf(sizeType);
		var imgLoader;
		if(attachment.get('url'))
		{
			var sizeObj = attachment.get('sizes');
			if(sizeObj)
			{
				var obj;
				while( !(obj = sizeObj[sizeType]) )
				{
					index ++;
					if(index >= sizes.length)
					{
						break;
					}
					sizeType = sizes[index];
				}
				if(obj && obj.url)
				{
					if(isImg)
					{
						$target.attr('src', obj.url);

						$target.on('load', function(evt){
							$classTarget.removeClass('is-loading');
						});
					}
					else
					{
						$target.css('background-image', 'url(' + obj.url + ')');

						imgLoader = new Image();
						imgLoader.onload = function(){
							$classTarget.removeClass('is-loading');
						};
						imgLoader.src = obj.url;
					}
					return;
				}
			}
			if(isImg)
			{
				$target.attr('src', attachment.get('url'));

				$target.on('load', function(evt){
					$classTarget.removeClass('is-loading');
				});
			}
			else
			{
				$target.css('background-image', 'url(' + attachment.get('url') + ')');

				imgLoader = new Image();
				imgLoader.onload = function(){
					$classTarget.removeClass('is-loading');
				};
				imgLoader.src = attachment.get('url');
			}
			return;
		}
		attachment.fetch({
			success:function(attachment){
				var sizeObj = attachment.get('sizes');
				if(sizeObj)
				{
					var obj;
					while( !(obj = sizeObj[sizeType]) )
					{
						index ++;
						if(index >= sizes.length)
						{
							break;
						}
						sizeType = sizes[index];
					}
					if(obj && obj.url)
					{
						if(isImg)
						{
							$target.attr('src', obj.url);

							$target.on('load', function(evt){
								$classTarget.removeClass('is-loading');
							});
						}
						else
						{
							$target.css('background-image', 'url(' + obj.url + ')');
							
							imgLoader = new Image();
							imgLoader.onload = function(){
								$classTarget.removeClass('is-loading');
							};
							imgLoader.src = obj.url;
							
						}
						return;
					}
				}
				if(isImg)
				{
					$target.attr('src', attachment.get('url'));

					$target.on('load', function(evt){
						$classTarget.removeClass('is-loading');
					});
				}
				else
				{
					$target.css('background-image', 'url(' + attachment.get('url') + ')');

					imgLoader = new Image();
					imgLoader.onload = function(){
						$classTarget.removeClass('is-loading');
					};
					imgLoader.src = attachment.get('url');
				}
			}
		});
	};

	p.showPreviewImage = function(url, x, y, w, h, borderColor)
	{
		p._previewImg.show(url, x, y, w, h, borderColor);
	}

	p.hidePreviewImage = function()
	{
		p._previewImg.hide();
	}

	p.parseUrlParams = function(source, delimiter)
    {
        if(!source)
		{
			return null;
		}
		source = source.toString();
		var index = source.indexOf('?');
		if(index == -1)
		{
			return null;
		}
		if(delimiter == undefined)
		{
			delimiter = '&';
		}
		var dataStr = source.substring(index + 1);
        var arr = dataStr.split(delimiter), temp;
        var len = arr.length, obj = {};
        for(var i = 0; i < len; i ++)
        {
            temp = arr[i].split("=");
            if(temp.length > 1)
            {
                obj[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
            }
        }
        return obj;
    }

    p.getFileExtension = function(fileName)
    {
        if(!fileName) return "";
        var index = fileName.lastIndexOf(".");
        if(index > 0)
        {
            return fileName.substring(index + 1).toLowerCase();
        }
        return "";
    }

    p.requestAnimationFrame = function(handler)
	{
		if(window.requestAnimationFrame)
        {
            window.requestAnimationFrame(handler);
        }
        else
        {
            setTimeout(handler, 17);
        }
	}

	p.addEscapeListener = function()
	{
		if(!window.wp) return;
		pt_admin.requestAnimationFrame(function(){
            var id = window.wp.media.editor.id();
			var editor = window.wp.media.editor.get(id);
			if('undefined' != typeof(editor)) editor = window.wp.media.editor.add( id );
			if (editor) {
				editor.off('escape', pt_admin.resetTBRemoveHandler);
				editor.on('escape', pt_admin.resetTBRemoveHandler);
			}
        });
	}

	p.resetTBRemoveHandler = function()
	{
		window.tb_remove = pt_admin.tb_remove;
		pt_admin.removeEscapeListener();
	}

	p.removeEscapeListener = function()
	{
		if(!window.wp) return;
		var id = window.wp.media.editor.id();
		var editor = window.wp.media.editor.get(id);
		if('undefined' != typeof(editor)) editor = window.wp.media.editor.add( id );
		if (editor) {
			editor.off('escape', pt_admin.resetTBRemoveHandler);
		}
		window.send_to_editor = pt_admin.orig_send_to_editor;
	}

	p.bindUploadEvent = function(evt)
	{
		var target = evt.currentTarget;
		pt_admin.targetfield = $(target).prev();
		var send_attachment_bkp = window.wp.media.editor.send.attachment;
		window.wp.media.editor.send.attachment = function(props, attachment) {
			var rid = attachment.id;
			$(pt_admin.targetfield).val(rid);
			var $imgContainer = $(pt_admin.targetfield).parent().children('.uploader-image-container');
			var img = $imgContainer.children('img').get(0);
			if(img)
			{
				pt_admin.setImageByAttachmentId($(img), rid, 'thumbnail', true);
				$imgContainer.css('display', 'block');
				$imgContainer.removeClass('empty');
			}
			var winTop = $(window).scrollTop();
			setTimeout(function(){
				$(window).scrollTop(winTop);
			}, 0);
			window.wp.media.editor.send.attachment = send_attachment_bkp;
		}
		window.wp.media.editor.open(null, {"multiple":false});
		return false;
	}

	p.bindRemoveImgEvent = function(evt)
	{
		var currentTarget = evt.currentTarget, target;
		if($(currentTarget).hasClass('single-image-upload-delete-button'))
		{
			target = currentTarget.parentNode;
		}
		else
		{
			target = currentTarget;
		}

		var img = target.querySelector('img');
		$(img).parent().removeClass('is-loading');
		if(img)
		{
			img.src = $(img).data('empty');
			$(img).parent().addClass('empty');
		}
		else
		{
			return;
		}
		target.style.display = "none";

		var $targetfield = $(target).closest('.uploader-image-container').parent().children('input[type="hidden"]');
		$targetfield.val('');
	}

	p.bindSCImageEvent = function(evt)
	{
		var target = evt.currentTarget;
		pt_admin.targetfield = $(target).prev();
		var send_attachment_bkp = window.wp.media.editor.send.attachment;
		window.tb_remove = null;
		pt_admin.addEscapeListener();
		window.wp.media.editor.send.attachment = function(props, attachment) {
			var rid = attachment.id;
			$(pt_admin.targetfield).val(rid);
			var imgContainer = $(pt_admin.targetfield).next().next()[0];
			var img = imgContainer.querySelector('img');
			if(img)
			{
				pt_admin.setImageByAttachmentId($(img), rid, 'thumbnail', true);
				imgContainer.style.display = "block";
				$(imgContainer).removeClass('empty');
			}
			var winTop = $(window).scrollTop();
			setTimeout(function(){
				$(window).scrollTop(winTop);
			}, 0);
			window.wp.media.editor.send.attachment = send_attachment_bkp;
			if(ptShortcodeMgr && ptShortcodeMgr.isOpen)
			{
				$( 'body' ).addClass( 'modal-open' );
				window.send_to_editor = function(){};
			}
			setTimeout(function(){
				window.tb_remove = pt_admin.tb_remove;
				pt_admin.removeEscapeListener();
			}, 17);
		}
		window.wp.media.editor.open(null, {"multiple":false});
		return false;
	}

	p.bindSCRemoveImgEvent = function(evt)
	{
		var currentTarget = evt.currentTarget, target;
		if($(currentTarget).hasClass('ptsc-image-upload-delete-button'))
		{
			target = currentTarget.parentNode;
		}
		else
		{
			target = currentTarget;
		}

		var img = target.querySelector('img');
		$(img).parent().removeClass('is-loading');
		if(img)
		{
			img.src = $(img).data('empty');
			$(img).parent().addClass('empty');
		}
		else
		{
			return;
		}
		target.style.display = "none";
		var targetfield = $(target).prev().prev();
		$(targetfield).val('');
	}

	p.bindUploadImgListEvent = function(evt)
	{
		var target = evt.currentTarget;
		pt_admin.targetfield = $(target).parent().children('.image-list-target-input').get(0);
		if(!pt_admin.imgListMedia)
		{
			pt_admin.imgListMedia = window.wp.media( {multiple:true} );
			pt_admin.imgListMedia.on('select', function(){
				var state = pt_admin.imgListMedia.state();
			    var selection = state.get('selection');
			    if ( ! selection ) return;
			    var rids = '', arr = [];
			    var $imglist = $(pt_admin.targetfield).parent().children('.image-list-container');
			    selection.each(function(attachment) {
			    	var rid = attachment.attributes.id || '';
			        arr.push(rid);
					var $html = $('<div class="uploader-image-container image-list imagelist-upload-delete" data-rid="' + rid + '">' +
						'<span class="imagelist-upload-delete-button upload-delete-button"></span>' + 
						'<img class="uploader-image-preview uploader-image-list" alt=""/></div>');
					pt_admin.setImageByAttachmentId($html.find('img'), rid, 'thumbnail', true);
					$imglist.append($html);
			    });
			    rids = arr.join(',');
			    var value = $(pt_admin.targetfield).val();
				if(value == '')
				{
					value = rids;
				}
				else
				{
					value += ',' + rids;
				}
				$(pt_admin.targetfield).val(value);
				var winTop = $(window).scrollTop();
				setTimeout(function(){
					$(window).scrollTop(winTop);
				}, 0);
			});
		}
		pt_admin.imgListMedia.open();
		return false;
	}

	p.removeImgListItem = function(itemImgContainer)
	{
		var img = itemImgContainer.querySelector('img');
		$(img).parent().removeClass('is-loading');
		if(img)
		{
			var imglist = itemImgContainer.parentNode;
			if(!imglist) return;
			var targetfield = $(imglist).parent().children('.image-list-target-input');
			var value = $(targetfield).val();
			if(value != '')
			{
				var rids = value.split(',');
				var children = $(imglist).find(".uploader-image-container");
				var len = children.length, child, index;
				for(var i = 0; i < len; i ++)
				{
					child = children[i];
					if(child == itemImgContainer)
					{
						index = i;
						break;
					}
				}
				if(index < rids.length)
				{
					rids.splice(index, 1);
					$(targetfield).val(rids.join(','));
					$(itemImgContainer).remove();
				}
			}
		}
	}

	p.onOverPreview = function(target)
	{
		var id = $(target).data('previewid').toString();
		if(!id) return;
		var params = pt_admin.parseUrlParams(id);
		var w = 0, h = 0, borderColor = '';
		if(params)
		{
			w = params['w'] || 0;
			h = params['h'] || 0;
			borderColor = params['bc'] || '';
		}
		var index = id.indexOf('?');
		if(index > -1)
		{
			id = id.substring(0, index);
		}
		var url = window.__pt_root_url + '/data/images/select_preview/'+ id + '.png';
		var x = Math.floor($(target).offset().left - $(window).scrollLeft());
		var y = Math.floor($(target).offset().top - $(window).scrollTop());
		if(pt_admin)
		{
			if(pt_admin.isRTL)
			{
				x = x - $(target).closest('.pt-select-list').width() + 155;
			}
			else
			{
				x = x + 225;
			}

			pt_admin.showPreviewImage(url, x, y, w, h, borderColor);
		}
	}

	p.onOverHelpIcon = function(target)
	{
		var id = $(target).data('id').toString();
		if(!id) return;
		var params = pt_admin.parseUrlParams(id);
		var w = 0, h = 0, offsetX = 0, offsetY = 0, borderColor = '';
		if(params)
		{
			w = parseFloat(params['w'] || 0);
			h = parseFloat(params['h'] || 0);
			offsetX = parseFloat(params['ox'] || 0);
			offsetY = parseFloat(params['oy'] || 0);
			borderColor = params['bc'] || '';
		}
		var index = id.indexOf('?');
		if(index > -1)
		{
			id = id.substring(0, index);
		}
		var url = window.__pt_root_url + '/data/images/help/'+ id + '.png';
		var x = Math.floor($(target).offset().left - $(window).scrollLeft());
		var y = Math.floor($(target).offset().top - $(window).scrollTop());
		var realX = x + 38 + offsetX, realY = y - 10 + offsetY;
		if(realX + w > $(window).width() - 10)
		{
			realX = $(window).width() - w - 10;
			realY = y - 10 - h;
		}
		if(pt_admin)
		{
			if(pt_admin.isRTL)
			{
				if(w == 0)
				{
					w = 60;
				}

				realX = realX - w - 38 - 10;
			}
			pt_admin.showPreviewImage(url, realX, realY, w, h, borderColor);
		}
	}

	p.setOptionAvailable = function(moduleName, data, value, $groupItem, isThemeOption)
	{
		var listenOptions = [], arr, i, len, str, paramIndex, param, optionIndex, paramArr, paramValues;
		for(var key in data)
		{
			arr = data[key];
			if(arr)
			{
				len = arr.length;
				for(i = 0; i < len; i ++)
				{
					str = arr[i];
					paramIndex = str.indexOf('&');
					if(paramIndex > -1)
					{
						str = str.substring(0, paramIndex);
					}
					if(listenOptions.indexOf(str) == -1)
					{
						listenOptions.push(str);
					}
				}
			}
		}
		var currentArr = data[value.toString()];
		if(currentArr)
		{
			len = currentArr.length;
			for(i = 0; i < len; i ++)
			{
				str = currentArr[i];
				paramIndex = str.indexOf('&');//param only supports for Select and Checkbox, and only supports one param.
				if(paramIndex > -1)
				{
					param = str.substring(paramIndex + 1);
					str = str.substring(0, paramIndex);
				}
				optionIndex = listenOptions.indexOf(str)
				if(optionIndex >= 0)
				{
					if(paramIndex > -1)
					{
						paramArr = param.split('=');
						if(paramArr && paramArr.length > 1)
						{
							paramValues = paramArr[1].split('|');
							paramValues.forEach(function(value, i){
								if(value === '')
								{
									paramValues.splice(i, 1);
								}
							});

							if($groupItem)
							{
								if(paramValues.indexOf($groupItem.find('[data-name="' + paramArr[0] + '"]').val()) > -1)
								{
									listenOptions.splice(optionIndex, 1);
									$groupItem.find('[data-name="' + str + '"]').closest('.pt-group-option-item').removeClass('option-item-hide');
								}
							}
							else if(isThemeOption)
							{
								if(paramValues.indexOf($('.pt-theme-option-item [name="' + window.__pt_theme_name + '_' + paramArr[0] + '"]').val()) > -1)
								{
									listenOptions.splice(optionIndex, 1);
									$('.pt-theme-option-item.' + window.__pt_theme_name + '_' + str).removeClass('option-item-hide');
								}
							}
							else
							{
								if(paramValues.indexOf($('.option-item [name="' + moduleName + '_' + paramArr[0] + '"]').val()) > -1)
								{
									listenOptions.splice(optionIndex, 1);
									$('.option-item.' + moduleName + '_' + str).removeClass('option-item-hide');
								}
							}
						}
					}
					else
					{
						listenOptions.splice(optionIndex, 1);
						if($groupItem)
						{
							$groupItem.find('[data-name="' + str + '"]').closest('.pt-group-option-item').removeClass('option-item-hide');
						}
						else if(isThemeOption)
						{
							$('.pt-theme-option-item.' + window.__pt_theme_name + '_' + str).removeClass('option-item-hide');
						}
						else
						{
							$('.option-item.' + moduleName + '_' + str).removeClass('option-item-hide');
						}
					}
				}
			}
		}
		len = listenOptions.length;
		for(i = 0; i < len; i ++)
		{
			if($groupItem)
			{
				$groupItem.find('[data-name="' + listenOptions[i] + '"]').closest('.pt-group-option-item').addClass('option-item-hide');
			}
			else if(isThemeOption)
			{
				$('.pt-theme-option-item.' + window.__pt_theme_name + '_' + listenOptions[i]).addClass('option-item-hide');
			}
			else
			{
				$('.option-item.' + moduleName + '_' + listenOptions[i]).addClass('option-item-hide');
			}
		}
	}

	$(function()
	{
		// -------------------------------------------------------
		// ------------- admin-custom ready start ----------------

		pt_admin.isRTL = ($('body').css('direction') || '').toLowerCase() === 'rtl';

		pt_admin.orig_send_to_editor = window.send_to_editor;
		pt_admin.tb_remove = window.tb_remove;

		$('.single-image-upload').on('click', pt_admin.bindUploadEvent);
		$('.single-image-upload-delete-button').on('click', pt_admin.bindRemoveImgEvent);
		$('.imagelist-upload').on('click', pt_admin.bindUploadImgListEvent);
		$('.ptsc-image-upload').on('click', pt_admin.bindSCImageEvent);
		$('.ptsc-image-upload-delete-button').on('click', pt_admin.bindSCRemoveImgEvent);

		$(".image-list-container.sortable").sortable();
		$('.image-list-container.sortable').on('sortupdate', function(evt, ui){
			var $imglist = $(evt.currentTarget);
			var $targetfield = $imglist.parent().children('.image-list-target-input');
			var rids = [];
			$imglist.find('.uploader-image-container').each(function(){
				rids.push($(this).data('rid'));
			});
			$targetfield.val(rids.join(","));
		});

		$('.image-list-container').on('click', function(evt){
			var target = evt.target;
			if($(target).hasClass('imagelist-upload-delete-button'))
			{
				evt.stopPropagation();
				pt_admin.removeImgListItem(target.parentNode);
			}
		});

		$('.option-item').find('input.pt-checkbox').checkboxpicker();
		$('table[data-type="main_shortcode"] input.thecs-sc-form-checkbox').checkboxpicker();

		pt_group.addListener('generate-item', function(type, target){
			var $target = $(target);

			$target.find('.pt-group-select').ptSelect();
			$target.find('.pt-group-image-select').ptImageSelect();

			$target.find('.group-color-field').wpColorPicker();
			$target.find('.single-image-upload').on('click', pt_admin.bindUploadEvent);
			$target.find('.single-image-upload-delete-button').on('click', pt_admin.bindRemoveImgEvent);

			$target.find('input[type="checkbox"]').on('change', function(evt){
				var $inp = $(this).siblings('input[type="hidden"]');
				$inp.val(this.checked ? '1' : '0');
				$inp.trigger('change');
			});

			$target.find('.pt-group-checkbox').checkboxpicker();
		}, null, null);

		pt_group.addListener('clone-item', function(type, target){
			var $target = $(target);
			$target.find('.group-color-field').wpColorPicker();
			$target.find('.single-image-upload').on('click', pt_admin.bindUploadEvent);
			$target.find('.single-image-upload-delete-button').on('click', pt_admin.bindRemoveImgEvent);
			window.IconClassInsertMgr.listen(target);
			
			$target.find('.pt-group-select').ptSelect();

			$target.find('.list-item[data-previewid]').hover(
				function(evt){
					pt_admin && pt_admin.onOverPreview(evt.target);
				},
				function(evt){
					pt_admin && pt_admin.hidePreviewImage();
				}
			);

			$target.find('.pt-group-image-select').ptImageSelect();

			$target.find('input[type="checkbox"]').on('change', function(evt){
				var $inp = $(this).siblings('input[type="hidden"]');
				$inp.val(this.checked ? '1' : '0');
				$inp.trigger('change');
			});

			$target.find('.pt-group-checkbox').checkboxpicker();

        	$target.find('select,.checkbox-item input[type="hidden"],.pt-image-select input[type="hidden"]').each(function(){
        		var value = $(this).val();
	        	var availableObj = $(this).data('available');
	        	if(availableObj)
	        	{
	        		pt_admin.setOptionAvailable('', availableObj, value, $target, window.__isThemeOpion);
	        	}
        	});
        	$target.find('select,.checkbox-item input[type="hidden"],.pt-image-select input[type="hidden"]').on('change', function(evt){
	        	var value = $(this).val();
	        	var availableObj = $(this).data('available');
	        	if(availableObj)
	        	{
	        		pt_admin.setOptionAvailable('', availableObj, value, $target, window.__isThemeOpion);
	        	}
	        });
		}, null, null);

		pt_group.addListener('delete-item', function(type, target){
			var $target = $(target);
			$target.find('.pt-select').disposePtSelect();
			$target.find('.pt-image-select').ptImageSelectCall('dispose');
			window.IconClassInsertMgr.cancle(target);
		}, null, null);

		pt_group.init({
			setImageByAttachmentId: pt_admin.setImageByAttachmentId
		});

		$('.option-item.checkbox-item,.pt-group-option-item.checkbox-item').each(function(){
			$(this).find('input[type="checkbox"]').on('change', function(evt){
				var $inp = $(this).siblings('input[type="hidden"]');
				$inp.val(this.checked ? '1' : '0');
				$inp.trigger('change');
			});
		});

		$('.pt-group-option-item.checkbox-item').each(function(){
			var value = $(this).find('input[type="hidden"]').val();
			if(value == '1')
			{
				$(this).find('input[type="checkbox"]').prop('checked', true);
			}
		});

		$('.pt-theme-option-item.checkbox-item input[type="checkbox"]').on('change',function(){
			var $inp = $(this).siblings('input[type="hidden"]');
			$inp.val(this.checked ? '1' : '0');
			$inp.trigger('change');
		});

		$('.pt-select:not(.pt-group-select):not(.pt-select-sc)').ptSelect();
		$('.pt-select-list>li.list-item[data-previewid]').hover(
			function(evt){
				pt_admin && pt_admin.onOverPreview(evt.target);
			},
			function(evt){
				pt_admin && pt_admin.hidePreviewImage();
			}
		);

		$('.pt-image-select:not(.pt-group-image-select):not(.pt-sc-image-select)').ptImageSelect();

		$('.option-item.bind-body-class select').each(function(){
			$(this).on('change', function(evt){
				var prefix = $(this).data('bodyclassPrefix') || '', className, curValue = this.value, $body = $('body');
				$(this).children('option').each(function(){
					className = prefix + this.value;
					if(this.value != curValue)
					{
						$body.removeClass(className);
					}
					else
					{
						$body.addClass(className);
					}
				});
			});
		});

		$('.option-item.pt-image-select.bind-body-class input[type="hidden"]').each(function(){
			$(this).on('change', function(evt){
				var $this = $(this);
				var $optionItem = $this.closest('.option-item.pt-image-select');
				var prefix = $this.data('bodyclassPrefix') || '', className, curValue = this.value, $body = $('body');
				var options = $optionItem.ptImageSelectCall('getOptions');
				options.forEach(function(value){
					className = prefix + value;
					if(value != curValue)
					{
						$body.removeClass(className);
					}
					else
					{
						$body.addClass(className);
					}
				});
			});
		});

		$('.option-item.checkbox-item.bind-body-class input[type="checkbox"]').each(function(){
			$(this).on('change', function(evt){
				var $valueInput = $(this).closest('.option-item').find('input[type="hidden"]');
				var prefix = $valueInput.data('bodyclassPrefix') || '';
				var value = $valueInput.val() == '1' ? '1' : '0';
				$('body').removeClass(prefix + '1' + ' ' + prefix + '0').addClass(prefix + value);
			});
		});

		$('.pt-admin-help-icon').hover(
			function(evt){
				pt_admin && pt_admin.onOverHelpIcon(evt.currentTarget);
			},
			function(evt){
				pt_admin && pt_admin.hidePreviewImage();
			}
		);

		$('.thickbox.button.ptsc-thicbox').on('click', function(){
				setTimeout(function(){
					$('#ptsc-shortcode-wrap').closest('#TB_window').css('overflow', 'hidden');
				}, 1);
			}
		);

        $('input.theme-option-code[data-editor]').each(function () {
            var input = $(this);
            var h = input.data('height') || 550;
            var mode = input.data('editor');
            var editDiv = $('<div>', {
                position: 'absolute',
                width: '100%',
                height: h + 'px',
                'class': input.attr('class')
            }).insertBefore(input);
            input.css('visibility', 'hidden');
            var editor = ace.edit(editDiv[0]);
            editor.renderer.setShowGutter(false);
			editor.setShowPrintMargin(false);
            editor.getSession().setValue(input.val());
            editor.getSession().setMode("ace/mode/" + mode);
            editor.setTheme("ace/theme/monokai");
            input.closest('form').submit(function () {
                input.val(editor.getSession().getValue());
            });
        });

        $('.color-field').wpColorPicker();

        //when checkbox value change
        $('.checkbox-group>.mc-option-item>input').on('change', function(evt){
        	var $group = $(this).parent().parent();
        	var $inp = $group.siblings('input');
        	var isSelect = $(this).prop('checked');
        	var firstElement = $group.children().first().get(0);
        	var result = [];
        	if(firstElement && firstElement == $(this).parent().get(0))
        	{
        		if(isSelect)
        		{
        			$group.children().each(function(){
        				if(this != firstElement)
        				{
        					$(this).find('input').prop('checked', false);
        				}
        			});
        		}
        		result.length = 0;
        	}
        	else
        	{
        		$(firstElement).find('input').prop('checked', false);
        		$group.children().each(function(){
    				if(this != firstElement)
    				{
    					var $input = $(this).find('input');
    					if($input.prop('checked'))
    					{
							result.push( $input.data('value') );
    					}
    				}
    			});
        	}
        	$inp.val( result.join(',') );
        });

        //init category checkbox-group
        $('.checkbox-group').siblings('input').each(function(){
        	var value = this.value;
        	var $group = $(this).siblings('.checkbox-group');
        	var firstElement = $group.children().first().get(0);
        	if($.trim(value) == '')
        	{
        		$(firstElement).find('input').prop('checked', true);
        		$group.children().each(function(){
    				if(this != firstElement)
    				{
    					$(this).find('input').prop('checked', false);
    				}
    			});
        	}
        	else
        	{
        		var arr = value.split(',');
        		$group.children().each(function(){
    				if(this != firstElement)
    				{
    					var $input = $(this).find('input');
    					var value = $input.data('value');
    					if(arr.indexOf(value) > -1)
    					{
    						$input.prop('checked', true);
    					}
    				}
    			});
        	}
        });

        //init image-group
        $('.image-group').ptImageGroup();
        $('.popup-textarea-open-btn').on('click', function(evt){
        	$('body').addClass('body-modal');
        	$(this).siblings('.popup-textarea-container').css('display', 'block');
        });

        $('.popup-textarea-container').on('click', function(evt){
        	if(evt.target == this)
        	{
        		$(this).hide();
        		$('body').removeClass('body-modal');
        	}
        });
        $('.popup-textarea-close-btn').on('click', function(evt){
        	$(this).closest('.popup-textarea-container').hide();
        	$('body').removeClass('body-modal');
        });

        $('select#select-menu-to-edit').on('change', function(evt){
        	$(this).closest('form').submit();
        });

        //init only-available
        !(function()
        {
        	$('.option-item select,.option-item.checkbox-item input[type="hidden"],' +
        		'.pt-group-option-item.checkbox-item input[type="hidden"],' + 
        		'.pt-theme-option-item.checkbox-item input[type="hidden"], .pt-theme-option-item select, .pt-theme-option-item.pt-image-select input[type="hidden"],' + 
        		'.option-item.pt-image-select input[type="hidden"],' + 
        		'.pt-group-option-item.pt-image-select input[type="hidden"]').each(function(){
        		var value = $(this).val();
        		var type = $(this).data('type');
        		var $groupItem = null;
        		if(type === 'group')
        		{
        			$groupItem = $(this).closest('.pt-group-item');
        		}
	        	var availableObj = $(this).data('available');
	        	if(availableObj)
	        	{
	        		pt_admin.setOptionAvailable($(this).data('prefix'), availableObj, value, $groupItem, window.__isThemeOpion);
	        	}
        	});

        	$('.option-item select,.option-item.checkbox-item input[type="hidden"],' + 
        		'.pt-group-option-item.checkbox-item input[type="hidden"],' + 
        		'.pt-theme-option-item.checkbox-item input[type="hidden"], .pt-theme-option-item select, .pt-theme-option-item.pt-image-select input[type="hidden"],' + 
        		'.option-item.pt-image-select input[type="hidden"],' + 
        		'.pt-group-option-item.pt-image-select input[type="hidden"]').on('change', function(evt){
	        	var value = $(this).val();
	        	var type = $(this).data('type');
        		var $groupItem = null;
        		if(type === 'group')
        		{
        			$groupItem = $(this).closest('.pt-group-item');
        		}
	        	var availableObj = $(this).data('available');
	        	if(availableObj)
	        	{
	        		pt_admin.setOptionAvailable($(this).data('prefix'), availableObj, value, $groupItem, window.__isThemeOpion);
	        	}
	        });
        })();

        !(function()
        {
        	var onCheck = function()
        	{
        		var value = $(this).val();
	        	if(value == '0')
	        	{
	        		value = 'standard';
	        	}
	        	$('.option-item.post-format-filter').each(function(){
	        		var formats = $(this).data('format');
	        		if(formats)
	        		{
	        			var arr = formats.split(',');
	        			if(arr.indexOf(value) > -1)
	        			{
	        				$(this).removeClass('option-item-hide');
	        			}
	        			else
	        			{
	        				$(this).addClass('option-item-hide');
	        			}
	        		}
	        	});
        	};
        	$('input[name="post_format"][checked]').each(onCheck);
        	$('input[name="post_format"]').on('change', onCheck);
        }());

        $('body').append('<a href="#" class="go_top"></a>');
		var go_top_h = $(window).height()*0.6;
		$(document).on("scroll",function(){
			if ( $(document).scrollTop() > go_top_h ){
				$(".go_top").fadeIn(100)
			}else{
				$(".go_top").fadeOut(100)
			};
		});

		// ------------- admin-custom ready end ----------------
		// -----------------------------------------------------
	});
}(jQuery));
/**
 * admin-custom--------------------end
 */

/**
 * admin-adjust--------------------begin
 */
!(function($, win)
{
	"use strict";

	win.__pt_admin_ready = function(){
		if(win.pagenow == 'post' || win.pagenow == 'project')
		{
			win.__admin_refresh_after && win.__admin_refresh_after();
		}

		$('#thecs_post_setting, #thecs_project_setting').addClass('pt-post-setting-root');

		$('body').addClass('pt-admin-ready');

		setTimeout(function compatibleWithYoastSEO ()
		{
			if (!window.YoastSEO || !window.tinyMCE || !window.ajaxurl)
			{
				return;
			}

			function queryShortCode(str, callback) {
				$.ajax({
					url: ajaxurl,
					type: "post",
					dataType: 'json',
					data: {
						action: 'parse_shortcode',
						shortcode: str
					},
					success: function (data) {
						var content = data.success ? data.data : '';
						callback && callback(content);
					},
					error: function(err) {
						callback && callback('');
					}
				});
			}

			var appendString = '';
			var YoastSEOAppendTextPlugin = function () {
				YoastSEO.app.registerPlugin('yoastSEOAppendTextPlugin', { status: 'ready' });
				YoastSEO.app.registerModification('content', this.contentModification, 'yoastSEOAppendTextPlugin', 5);
			}

			YoastSEOAppendTextPlugin.prototype.contentModification = function (data) {
				return data + appendString;
			};

			new YoastSEOAppendTextPlugin();

			var scEditor = window.tinyMCE.get('pt_sc_shortCodeText');
			if (!scEditor)
			{
				return;
			}

			scEditor.on('Change', function (e) {
				appendString = '';
				queryShortCode(scEditor.getContent(), function (data) {
					appendString = data;
					YoastSEO.app.pluginReloaded('yoastSEOAppendTextPlugin');
				});
			});

			setTimeout(function(){
				scEditor.fire('Change');
			}, 1000);

		}, 0);
		
	}

	win.__admin_refresh_before = function(type){

	}

	win.__admin_refresh_after = function(type){
		$('.option-item.bind-body-class select').each(function(){

			var prefix = $(this).data('bodyclassPrefix') || '', className, curValue = this.value, $body = $('body');
			
			if($(this).attr('disabled') != 'disabled')
			{
				$(this).children('option').each(function(){
					className = prefix + this.value;
					if(this.value != curValue)
					{
						$body.removeClass(className);
					}
					else
					{
						$body.addClass(className);
					}
				});
			}
			else
			{
				$(this).children('option').each(function(){
					className = prefix + this.value;
					$body.removeClass(className);
				});
			}
		});

		$('.option-item.pt-image-select.bind-body-class input[type="hidden"]').each(function(){
			var $this = $(this);
			var $optionItem = $this.closest('.option-item.pt-image-select');
			var prefix = $this.data('bodyclassPrefix') || '', className, curValue = this.value, $body = $('body');
			var options = $optionItem.ptImageSelectCall('getOptions');

			if($this.attr('disabled') != 'disabled')
			{
				options.forEach(function(value){
					className = prefix + value;
					if(value != curValue)
					{
						$body.removeClass(className);
					}
					else
					{
						$body.addClass(className);
					}
				});
			}
			else
			{
				options.forEach(function(value){
					className = prefix + value;
					$body.removeClass(className);
				});
			}
		});

		$('.option-item.checkbox-item.bind-body-class input[type="hidden"]').each(function(){
			var prefix = $(this).data('bodyclassPrefix') || '';
			$('body').removeClass(prefix + '1' + ' ' + prefix + '0').addClass(prefix + (this.value == '1' ? '1' : '0'));
		});
	}
}(jQuery, window));
/**
 * admin-adjust--------------------end
 */