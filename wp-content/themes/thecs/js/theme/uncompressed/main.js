
(function($){

	'use strict';

	var isRTL = ($('body').css('direction') || '').toLowerCase() == 'rtl';

	window.onpageshow = function(event){

		if (event && event.persisted){
			$('.loader-layer').addClass('load').fadeOut(200);
			$(window).trigger('resize');
		};

	};

	window.__ptMenu02Edit = function($lists, maxNumber){
		var allClass = [];
		var i = maxNumber;
		while(i --){ allClass.push('nth-' + (i + 1)) };
		var allClassStr = allClass.join(' ');
		$lists.removeClass(allClassStr);
		
		var arr = [];
		$lists.each(function(){
			if(this.style.display != 'none'){ arr.push(this) }
		});
		
		var index = 0;
		var len = arr.length, item;
		for(i = 0; i < len; i ++)
		{
			item = arr[i];

			if ( index == 0) {
				$('table.menu-02 tbody').append('<tr class="temp-target"></tr>');
			};

			var li_html = $(item).html();
			var li_class = $(item).attr('class');
			$('table.menu-02 tr.temp-target').append('<td class="'+li_class+'">'+li_html+'</td>');

			if ( index == maxNumber -1) {
				$('table.menu-02 tbody tr').removeClass('temp-target');
			};

			if(maxNumber == 1){
				break;
			}

			index ++;
			if(index >= maxNumber){ index = 0 ;}

			// is over
			if ( i == len - 1) {
				$('header nav.main-menu').remove();
				$('table.menu-02 tbody tr').removeClass('temp-target');
			}
			
		}
	};

	

	//func for data-bg-image
	$.fn.extend({
		PtBgTrans:function (obj) {
			this.each(function(){

				if ( $(this).data('bg') ) {
					var dataBgUrl = $(this).data('bg');
					$(this).css("backgroundImage",'url('+ dataBgUrl +')').imagesLoaded( { background: true }, function(instance) {
						var elements = instance.elements;
						$(elements).closest('.bg-full').removeAttr('data-bg');

						if ( $(elements).closest('.hover-img').length == 0 ) {
							$(elements).closest('.item').addClass('show')
						}else{
							$(elements).closest('.item').find('.hover-img').addClass('show')
						};
						
						if ( $(elements).closest('.sc-mixbox').length > 0 ) {
							$(elements).closest('.item').addClass('bg-loaded');
						};
						if ( $(elements).parent().parent('.list-title').length > 0 ) {
							$(elements).parent().addClass('show');
						}
					})
				}

			});
			return this;
		}
	});


		//func for data-bg-image
	$.fn.extend({
		PtListBgTrans:function (obj) {
			this.each(function(){

				if ( $(this).data('bg') ) {

					var dataBgUrl = $(this).data('bg');
					$(this).css("backgroundImage",'url('+ dataBgUrl +')').imagesLoaded( { background: true }, function(instance) {
						var elements = instance.elements;
						$(elements).closest('.bg-full').removeAttr('data-bg');

						var $item = $(elements).closest('.item');
						$item.addClass('show').find('.pic-loader').remove();

						if ( $(elements).closest('.pic-list.style-01').length > 0 ) {
							$item.find('.h').MotionText();
						};
						if ( $(elements).closest('.pic-list.style-01.product-list').length > 0 ) {
							// $item.find('.woocommerce-Price-amount').MotionText();
							$item.find('span.price').addClass('show');
						}
						
					})
				}
			});
			return this;
		}
	});



	$.fn.extend({
		PtImgLoaded:function (obj) {
			this.each(function(){

				$(this).imagesLoaded( function(instance) {
					var elements = instance.elements;
					$(elements).closest('.img').addClass('img-loaded').find('.pic-loader').remove();
					if ( $(elements).closest('.img').hasClass('inviewport') ) {
						$(elements).closest('.img').addClass('show');
					}
				})

			});
			return this;
		}
	});



	function responsive(){

		var wp_bar_h;

		if ( $('#wpadminbar').length==1 ) {
			if (  window.innerWidth > 780 ) {
				wp_bar_h = 32
			}else{
				wp_bar_h = 46;
			}
		}else{
			wp_bar_h = 0;
		};


		window.win_h	 = $(window).height() - wp_bar_h;
		window.win_w	 = window.innerWidth;



		//mobile-mode

		var isMobile = /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);

		if (  window.innerWidth <1024 || isMobile ) {
			$('body').addClass('m-mode').removeClass('pc-mode');
		}else{
			$('body').addClass('pc-mode').removeClass('m-mode')
		};

		$('.m-side,.hidden-menu').css('min-height',win_h - 1);



		if ( window.innerWidth > 1440 ) {

			if ( $('.site-gap-big').length > 0 ) {
				window.siteGap = 40;
			}else{
				window.siteGap = 28;
			}
			
		}else{

			if ( $('.site-gap-big').length > 0 ) {
				window.siteGap = 32;
			}else{
				window.siteGap = 18;
			}

		};


		var $list_widget = $('.list-widgets-wrap');

		setTimeout(function(){
			if ( $list_widget.outerHeight() % 2 != 0 ) {
				$list_widget.addClass('odd-hack')
			}else{
				$list_widget.removeClass('odd-hack')
			}
		},800);


		$list_widget.css({
			'max-height':win_h - 200
		});

		if ( $list_widget.outerHeight() >= win_h ) {
			$('.list-widgets').addClass('over')
		}else{
			$('.list-widgets').removeClass('over')
		};


		$('body > header:not(.style-05)').css('padding',siteGap).css('padding-top',siteGap );

		var $nav = $('.main-menu');
		var $logo = $('.logo');
		var $headerRight = $('.header-right');
		var header234 = $('header.style-02,header.style-03,header.style-04').length;
		var mHeaderH = $('.m-header').height();
		var navLi_len = $('.main-menu>ul>li').length;
		var logoRawH = $('.logo img').attr('height');
		var logoRaww = $('.logo img').attr('width');
		var logoNewH;
		var headerRightH = 40;
		var header05_w;
		var wc_topGap;


		if ( $('header.style-05').length > 0 ) {
			header05_w = $('header.style-05').outerWidth();
			logoNewH = Math.ceil( (header05_w * logoRawH)/logoRaww );
			wc_topGap = 100;
		}else{

			if ( logoRawH < 60 ) {
				logoNewH = logoRawH;
			}else{
				logoNewH = 60;
			};
			
			header05_w = 0;
			wc_topGap = 0;
		};


		$('body > header .widget_shopping_cart_content').css('max-height',win_h * 0.8);


		if ( $('header.style-02').length > 0 ) {
			var $navUl = $('header.style-02 .main-menu>ul');
			var navCol = Math.ceil(navLi_len / 3);
			$navUl.css('column-count',navCol);
		};


		var gapTop = 0;
		if($logo.get(0)){
			gapTop = $logo.offset().top - $(document).scrollTop() + logoNewH*0.5
		};

		

		if ( $('header.style-04').length > 0 ) {
			var navLeft = $nav.width()*0.5;
			$nav.css({'margin-left':-navLeft,'top':gapTop - 20})
		};


		if ( $('header.style-05').length == 0 ) {

			if(isRTL)
			{
				$headerRight.css({'top':gapTop,'left':gapTop - wp_bar_h - 20});

				setTimeout(function(){
					$headerRight.css({'top':gapTop,'left':gapTop - wp_bar_h - 20});
				},200);
			}
			else
			{
				$headerRight.css({'top':gapTop,'right':gapTop - wp_bar_h - 20});

				setTimeout(function(){
					$headerRight.css({'top':gapTop,'right':gapTop - wp_bar_h - 20});
				},200);
			}
		};



		// header height
		var headerH;
		if ( $('header.style-05,header.style-01').length == 0 ) {
			headerH = $('body > header').outerHeight();
		}else{
			headerH = 0;
		};

		$('.header_bg').css('height',headerH);

		// site maincontent height
		var mainContentH = win_h - headerH;
		window.mainContentW = win_w - header05_w;

		var title01_w = win_w * 0.32;
		if ( $('.pic-list.style-01 .list-title.style-01.no-img').length > 0 ) {
			title01_w = win_w * 0.26;
		};

		$('.list-title.style-01 .title-text').css({
			'width':title01_w
		});



		

		// post detail

		var postBannerH;
		var raw_list_W = 0;
		window.headerGap = 0;

		if ( headerH == 0 ) {
			if($headerRight.length > 0)
			{
				headerGap = $headerRight.height() + $headerRight.offset().top * 2;
			}
		}else{
			headerGap = headerH;
		};

		if ( $('#post-main.post-01 .post-banner.h-full:not(.raw-proportion)').length > 0 ) { postBannerH = mainContentH };
		if ( $('#post-main.post-01 .post-banner.h-normal').length > 0 ) { postBannerH = mainContentH * 0.7 };
		if ( $('#post-main.post-01 .post-banner.h-short').length > 0 ) { postBannerH = mainContentH * 0.5 };

		if ( $('#post-main.post-01 .post-banner.h-full.raw-proportion').length > 0 ) {
			if ( headerH == 0 ) {
				postBannerH = mainContentH - headerGap * 2; 
			}else{
				postBannerH = mainContentH - headerGap
			}
		};


		$('.post-02 .post-banner.h-full').css('width',mainContentW);

		if ( $('#post-main.post-02 .post-banner.h-normal').length > 0 ) {
			if ( headerH == 0 ) {
				postBannerH = mainContentH - headerGap * 2; 
			}else{
				postBannerH = mainContentH - headerGap
			}
		};

		if ( $('body.m-mode').length > 0) {

			postBannerH = $(window).width()*0.6;

			$('.post-02 .post-banner.h-full').css('height',postBannerH);

		}else{

			$('.post-02 .post-banner.h-full').css('height',mainContentH);
		};


		$('.post-02 .post-banner.h-normal').css({
			'height':postBannerH,
			'width':mainContentW - headerGap *2,
			'margin-left':headerGap
		});

		
		$('.post-02 .post-boxed-wrap').css({
			'width':mainContentW
		});
		$('.post-02 .post-boxed-wrap > .wrap').css('top',-headerH*0.5);



		if ( $('#post-main.post-01 .post-banner.raw-proportion').length > 0 ) {


			$('.post-banner > .wrap').css({
				'padding-left':headerGap,
				'padding-right':headerGap
			})


			$('.post-banner .item').each(function(){

				var box_h = postBannerH;
				var pic_w = $('.bg-full',this).attr('data-w');
				var pic_h =$('.bg-full',this).attr('data-h');
				var box_w = Math.ceil( (box_h*pic_w)/pic_h );

				$(this).css({
					'width':box_w,
					'height':box_h
				});

				raw_list_W += box_w;

			});



			$('.post-banner').css({
				'height':postBannerH,
				'margin-top': headerGap
			});

		};

		
		$('#post-main.post-01 .post-banner:not(.raw-proportion)').css({
			'height':postBannerH,
			'margin-top':headerH
		});

		if ( $('#post-main.post-02').length > 0 ) {

			$('.main-content').css({
				'height':mainContentH,
				'top':headerH
			});


			$('.post-banner.raw-proportion .item').each(function(){

				var bannerW = $('.post-banner').width();
				var box_h = $('.post-banner').height();
				var pic_w = $('.bg-full',this).attr('data-w');
				var pic_h =$('.bg-full',this).attr('data-h');
				var box_w = Math.ceil( (box_h*pic_w)/pic_h );
				var box_h2 = Math.ceil( (bannerW*pic_h)/pic_w );

				var box_radio = box_h/bannerW;


				if (pic_h/pic_w > box_radio) {
					$(this).css({
						'width':box_w,
						'height':box_h
					});
					raw_list_W += box_w;

				}else{
					$(this).css({
						'width':bannerW,
						'height':box_h2
					});
					raw_list_W += bannerW;
				};

				
			});



			$('.post-footer').css({
				'height':mainContentH - headerGap*2,
				'top':-headerGap * 0.5 * header234
			});


			$('.comment-root').css({
				'top': -headerH*0.5
			});


		};


		$('.post-banner.raw-proportion').attr('data-listW',raw_list_W);


		$('.post-01 .post-extend .item,body.m-mode .post-02 .post-extend .item:not(.text-02)').each(function(){

			var box_w = Math.ceil( $(this).width() );
			var pic_w = $(this).attr('data-w');
			var pic_h = $(this).attr('data-h');
			var box_h = Math.ceil( (box_w*pic_h)/pic_w );

			$('.img',this).css({
				'height':box_h,
				'width':'100%'
			})

		});



		$('body:not(.m-mode) .post-02 .post-extend .item.text-01').each(function(){

			var box_h = mainContentH;
			var pic_w = $(this).attr('data-w');
			var pic_h = $(this).attr('data-h');
			var box_w = Math.ceil( (box_h*pic_w)/pic_h );

			$('.img',this).css({
				'width':box_w,
				'height':'100%'
			})

		});


		$('body:not(.m-mode) .post-02 .post-extend .item.text-03').each(function(){

			var box_h = mainContentH - headerGap*2;
			var pic_w = $(this).attr('data-w');
			var pic_h = $(this).attr('data-h');
			var box_w = Math.ceil( (box_h*pic_w)/pic_h );

			$('.img',this).css({
				'width':box_w,
				'height':box_h
			});

		});


		$('.post-02 .post-extend .text-03 .img,.post-02 .post-extend .text-02,.post-02 .post-extend .text-03 .text').css('top',-headerH*0.5);



		var $post02_textarea = $('.post-02 .post-info .text-area');
		var post02_info_lineH = $post02_textarea.css('line-height');
		$post02_textarea.css('max-height',parseInt(post02_info_lineH) * 12 + 1);


		$('.post-02 .post-extend .item.text-02').each(function(){

			var tempLineH = $('p',this).css('line-height');

			$('p',this).css('height',parseInt(tempLineH) * 5);

		});

		$('.post-02 .post-extend .item.text-03').each(function(){

			var tempLineH = $('p',this).css('line-height');

			$('p',this).css('max-height',parseInt(tempLineH) * 10);


		});



		var listTitle_w;
		if ( $('.list-title').length > 0 ) {
			listTitle_w = $('.list-title').outerWidth() + parseInt( $('.list-title').css('margin-left') )*2;
		}else{
			listTitle_w = 0;
		};



		//wc detail —————————————————————————————————————————————————————————————————————————

		var des_lineNum;

		if ( window.innerWidth < 1279 ){ des_lineNum = 5 }else{des_lineNum = 7}

		var wc_des_H = parseInt( $('.woocommerce-product-details__short-description p').css('line-height') )* des_lineNum;

		$('.des-text-wrap').css('height',wc_des_H );

		var des_row = 0;

		$('.des-text-wrap p').each(function(){
			des_row += $(this).height();
		});




		if ( des_row <= wc_des_H ) {
			$('.des-text-wrap').addClass('non-roll')
		}else{
			$('.des-text-wrap').removeClass('non-roll')
		};



		$('body.single-product #primary').css({
			'height':mainContentH - headerH - wc_topGap*2,
			'top':headerH + wc_topGap
		});


		$('.wc-main-part').css('width',mainContentW);




		var wc_tabs_innerH = $('.wc-tabs-wrapper').height();
		var wc_reviewsH = $('.woocommerce-Reviews').outerHeight();
		var wc_cartH;

		if ( wc_reviewsH > wc_tabs_innerH) {
			$('.wc-reviews-root').addClass('over')
		}else{
			$('.wc-reviews-root').removeClass('over')
		};

		$('.wc-reviews-root').css('max-height',wc_tabs_innerH);

		if ( $('body.woocommerce-cart,body.woocommerce-account').length > 0) {

			wc_cartH = $('.main-content').height();
			if ( wc_cartH < win_h - headerH*2) {
				$('.main-content').css('margin-top',(win_h - wc_cartH)*0.5)
			}else{
				$('.main-content').removeAttr('style');
			}
		};







		//blog ——————————————————————————————————————————————————————————————————————————————


		$('.blog-list,.ptsc-list').css({
			'height':mainContentH,
			'top':headerH
		});



		if ( $('body.m-mode').length > 0) {

			$('.sc-slider').each(function(){
				var slider_num = $(this).data('w');
				var slider_w = $(this).closest('.ptsc-list').width();

				if ( !$(this).hasClass('mobile-fullscreen') && !$(this).hasClass('mobile-list')) {
					$(this).css({
						'height':( slider_w / slider_num ) * 0.6
					});
				}

				if ( $(this).hasClass('mobile-fullscreen') ) {



					if ( win_w > 768 ) {

						$(this).css({
							'height':win_h - mHeaderH * 2 - 50
						});


					}else{
						$(this).css({
							'height':win_h - mHeaderH
						});
					}

				}

				if ( $(this).hasClass('mobile-list')) {
					$('.item',this).css({
						'height':( slider_w / slider_num ) * 0.6
					})
				}

			});

		}else{

			$('.sc-slider').height(mainContentH);
			$('.sc-slider.mobile-list .item').css('height','100%');

		};



		var box_h;

		if ( $('.blog-list.large').length == 0 ) {
			if ( window.innerHeight > 900 ) {
				box_h = 240;
			}else{
				box_h = 180;
			}
		}else{
			box_h = win_h * 0.3
		};

		$('.blog-list.style-02.raw-proportion .item').each(function(){

			var box_mode_w = $(this).width();
			var pic_w = $(this).attr('data-w');
			var pic_h = $(this).attr('data-h');
			var box_w = Math.ceil( (box_h*pic_w)/pic_h );
			var box_mode_h = Math.ceil( (box_mode_w*pic_h)/pic_w );

			if ( window.innerWidth > 1024 ) {

				$('.img',this).css({
					'width':box_w,
					'height':box_h
				});

				$(this).width(box_w)

			}else{

				$('.img',this).css({
					'width':'100%',
					'height':box_mode_h
				})
			}

		});



		//sc ——————————————————————————————————————————————————————————————————————————————

		var scWrapW;
		
		scWrapW = win_w - header05_w - listTitle_w;


		if ( $('.ptsc').length > 0) {


			$('.ptsc:not(.sc-images)').each(function(){

				var $this = $(this);

				if ( $(this).hasClass('sc-slider')) {

					var sliderW = scWrapW * $(this).data('w');
					$(this).css('width',sliderW);

				}else{

					$('.item',this).each(function(){

						var scItemW = Math.floor(scWrapW * $(this).data('w'));
						$(this).css('width',scItemW);

						var textareaH = mainContentH - win_h * 0.22; 

						var imgH   = $('.sc-inner > .img,.sc-inner > .icon',this).outerHeight();
						var titleH = $('.text > .h',this).outerHeight();
						if ( imgH !== null ) {
							textareaH -= imgH;
						}
						if ( titleH !== null ) {
							textareaH -= titleH;
						}

						if ( $(this).hasClass('text-h-high') ){
							//used textareaH
						}else if( $(this).hasClass('text-h-normal') ){
							textareaH *= 0.6;
						}else{
							textareaH *= 0.4;
						}


						var textLineH = parseInt( $('.intro',this).css('line-height') );
						var lineNum = Math.round( textareaH / textLineH );
						$('.intro',this).css('max-height',textLineH * lineNum);

					});


					if( $this.hasClass('pricing-table') && window.innerWidth < 1440 ){

						$('.item[data-w="0.25"]',this).css('width',Math.floor(scWrapW * 0.3333));

					}

				}

			});


			$('.sc-images .item').each(function(){

				var box_h = mainContentH;
				var item_w = $(this).width();
				var pic_w = $(this).attr('data-w');
				var pic_h = $(this).attr('data-h');
				var box_w = Math.ceil( (box_h*pic_w)/pic_h );
				var m_box_h = Math.ceil( (item_w*pic_h)/pic_w );



				if ( $('body.m-mode').length ==0 ) {
					$('.img',this).css({
						'width':box_w,
						'height':box_h
					})
				}else{
					$('.img',this).css({
						'width':item_w,
						'height':m_box_h
					})

				}

			});


		};



		// portfolio ————————————————————————————————————————————————————————————————————————


		var picList_h;
		var picList02_border = 0;
		var picList02_borderTop = 0;

		if ( $('.pic-list.style-02.item-gap').length > 0 && $('.list-title.style-01').length == 0) {

			if ( $('.pic-list.row-1').length>0 || $('.pic-list.row-1-mini').length>0 ) { picList02_border = 8 };
			if ( $('.pic-list.row-2').length>0 ) { picList02_border = 16 };
			if ( $('.pic-list.row-3').length>0 ) { picList02_border = 24 };
			picList02_borderTop = 4;

			if ( $('.pic-list.style-02:not(.boxed)').length > 0 ) {
				picList02_borderTop = 4;
			}else{
				picList02_borderTop = picList02_border*0.5
			}

		};

		if ( $('.pic-list.style-01').length > 0 ) {

			if ( $('header.style-05,header.style-01').length > 0 ) {
				picList_h = Math.ceil(( win_h - headerH * 2 ) *0.7);
			}else{
				if ( $(window).height() > 700 ) {
					picList_h = Math.ceil(( win_h - headerH * 2 ) *0.7);
				}else{
					picList_h = Math.ceil(( win_h - headerH * 2 ));
				}
			}

		};


		if ( $('.pic-list.style-02').length > 0 ) {

			var pic02Top;

			if ( $('.pic-list.style-02.boxed').length > 0 ) {

				picList_h = Math.ceil(( win_h - headerH*2 )*0.8 - picList02_border);
				pic02Top = Math.ceil( (win_h - picList_h  )*0.5 );

			}else{
				picList_h = Math.ceil( mainContentH - picList02_border );
				pic02Top = headerH;
			};

			if ( $('.pic-list.style-02.row-1-mini').length > 0 ) {
				picList_h= win_h *0.5 - picList02_border;
				pic02Top = win_h *0.25;
			};

			$('.list-title-bg').css({
				'top': Math.ceil(pic02Top + wp_bar_h)
			});

			if ( picList_h % 2 != 0 ) {
				picList_h += 1
			}


		};
		


		$('.pic-list.style-01 .wrap').css({
			'height': picList_h,
			'top': Math.ceil((win_h - picList_h)*0.5)
		});


		$('.pic-list.style-02').css({
			'height': picList_h + picList02_border,
			'top': Math.ceil(pic02Top - picList02_borderTop)
		});


		if ( window.innerWidth <1024 || isMobile ){

			$('body').append($('.list-widgets:not(.moved)').addClass('moved'));

			$('.pic-list:not(.fixed) .item').each(function(){



				var box_w = Math.ceil( $('.pic-list > .wrap').width() );
				var pic_w = $(this).attr('data-w');
				var pic_h = $(this).attr('data-h');
				var box_h = Math.ceil( (box_w*pic_h)/pic_w );

				$('.img',this).css({
					'width':box_w,
					'height':box_h
				})

			});

			if ( $('.pic-list.fixed').length > 0 ) {

				var fixedBox_w = $('.pic-list.fixed .item').width();

				$('.pic-list.fixed .item').each(function(){

					var fixedBox_h;
					if ( $('.pic-list.fixed-v').length == 1 ) {
						fixedBox_h = Math.ceil( fixedBox_w/0.75 );
					}
					if ( $('.pic-list.fixed-s').length == 1 ) {
						fixedBox_h =fixedBox_w;
					}
					if ( $('.pic-list.fixed-h').length == 1 ) {
						fixedBox_h = Math.ceil( fixedBox_w*0.56 );
					};

					$('.img',this).css({
						'width':fixedBox_w,
						'height':fixedBox_h
					})
					
				})
			}

		}else{


			$('.pic-list').prepend($('.list-widgets.moved').removeClass('moved'));


			$('.pic-list.style-01 .item').each(function(){

				var pic01_h;

				if ( $(this).closest('.pic-list').hasClass('size-normal') ) {
					pic01_h = picList_h
				}
				if ( $(this).closest('.pic-list').hasClass('size-small') ) {
					pic01_h = picList_h * 0.7
				}
				
				var pic_w = $(this).attr('data-w');
				var pic_h = $(this).attr('data-h');

				var ratio = pic_w / pic_h;

				var box_h;

				if ( ratio > 1.2 ) {
					$(this).addClass('type-h');
					box_h = pic01_h * 0.7;
				}else if ( ratio <= 1.2 && ratio > 0.8 ) {
					$(this).addClass('type-s');
					box_h = pic01_h * 0.85;
				}else{
					$(this).addClass('type-v');
					box_h = pic01_h;
				};

				var box_w = Math.ceil( (box_h*pic_w)/pic_h );

				var fixedBox_w;
				var fixedBox_h = Math.ceil(pic01_h);



				if ( $('.pic-list.fixed-v').length == 1 ) {
					fixedBox_w = Math.ceil( fixedBox_h*0.75 );
				}
				if ( $('.pic-list.fixed-s').length == 1 ) {
					fixedBox_w =fixedBox_h;
				}
				if ( $('.pic-list.fixed-h').length == 1 ) {
					fixedBox_w = Math.ceil( fixedBox_h/0.6 );
				};


				if ($('.pic-list.fixed').length ==0 ) {
					$('.img',this).css({
						'width':box_w,
						'height':box_h
					})
				}else{
					$('.img',this).css({
						'width':fixedBox_w,
						'height':fixedBox_h
					});
				}

				

			});



			var pic02_h;

			if ( $('.pic-list.row-1-mini').length > 0 ) {
				pic02_h = win_h * 0.5;
			}
			if ( $('.pic-list.row-1').length > 0 ) {
				pic02_h = picList_h
			};
			if ( window.innerHeight > 700 ) {

				if ( $('.pic-list.row-2').length > 0 ) {
					pic02_h = picList_h * 0.5;
				}
				if ( $('.pic-list.row-3').length > 0 ) {
					pic02_h = picList_h * 0.333333;
				};

			}else{

				if ( $('.pic-list.row-2').length > 0 || $('.pic-list.row-3').length > 0 ) {
					pic02_h = win_h * 0.5;
				}

			};


			$('.pic-list.style-02 .item').each(function(){

				
				var pic_w = $(this).attr('data-w');
				var pic_h = $(this).attr('data-h');

				var box_h = Math.ceil(pic02_h);
				var box_w = Math.ceil( (box_h*pic_w)/pic_h );


				var fixedBox_w;
				if ( $('.pic-list.fixed-v').length == 1 ) {
					fixedBox_w = Math.ceil( box_h*0.75 );
				}
				if ( $('.pic-list.fixed-s').length == 1 ) {
					fixedBox_w =box_h;
				}
				if ( $('.pic-list.fixed-h').length == 1 ) {
					fixedBox_w = Math.ceil( box_h/0.56 );
				};

				if ( $('.pic-list.style-02.fixed').length == 0 ) {
					$('.img',this).css({
						'width':box_w,
						'height':box_h
					})
				}else{
					$('.img',this).css({
						'width':fixedBox_w,
						'height':box_h
					})
				}

			});//pic02 each end


		};


		if ( $('.pic-list.style-02.row-1-mini').length > 0 ) {

			$('body > .list-title-bg').css({
				'width':title01_w,
				'height':win_h * 0.5,
				'top':win_h  * 0.25 + wp_bar_h
			});

			var pic01MiniTop = (picList_h - win_h *0.5)*0.5;

			$('.pic-list.style-02.row-1-mini .wrap').css('top',0);

		}else{

			$('body > .list-title-bg').css({
				'width':title01_w,
				'height':picList_h
			})

		};

		if ( window.innerHeight < 700 && $('.m-mode').length == 0 && $('.pic-list.style-02 .list-title.style-01').length > 0 ) {
			var pic02ImgH = $('.pic-list.style-02 .item .img').height();
			$('.pic-list.style-02 .list-title.style-01 .img').css({
				'height':pic02ImgH,
				'margin-top':pic02ImgH * -0.5
			});
			$('.pic-list.style-02 > .wrap').css('top', headerH* -0.5);
		}else{
			$('.pic-list.style-02 .list-title.style-01 .img,.pic-list.style-02 > .wrap').removeAttr('style');
		};

		// portfolio end 




		// $('footer .wrap').css('max-height',win_h - 150);
		if ( $('body > footer .bg-full').length > 0 ) {
			var $footerImg = $('footer .img'),
				$footerBgfull = $('footer .img .bg-full'),
				footerImgWrapW = $footerImg.width(),
				footerImgW = $footerBgfull.data('w'),
				footerImgH = $footerBgfull.data('h');
			var footerimgWrapH = Math.ceil( (footerImgWrapW*footerImgH)/footerImgW );

			
			$footerImg.height(footerimgWrapH)
			
		};

		setTimeout(function(){
			var $footer = $('body > footer');
			if($footer.length > 0)
			{
				$footer.removeAttr('style');
				var footerH = $footer.outerHeight(), offset = $footer.offset();
				if(offset)
				{
					var footerOffsetTop = offset.top;
					if ( win_h > footerOffsetTop + footerH  && $('body').hasClass('m-mode')) {
						$footer.css('top',win_h - (footerOffsetTop + footerH));
					}
				}
			}
		},200)
		
		


		
		


		var blogMaxNum;
		if ( window.innerHeight >= 1080 ) {
			blogMaxNum = 4
		}else if( window.innerHeight >= 800 ){
			blogMaxNum = 3
		}else{
			blogMaxNum = 0
		};

		var blogLineH =  parseInt( $('.blog-list .item .intro').css('line-height') );
		var blogMaxLineH = blogMaxNum * blogLineH;
		$('.blog-list .item .intro').css('max-height',blogMaxLineH);


		// firefox hack

		if ( navigator.userAgent.indexOf("Firefox") > -1 && $('.post-02 .post-extend .item').length > 0 ) {

			var extendListw = 0;
			$('.post-extend .item').each(function(){
				var extendItemW =  $(this).outerWidth() + parseInt($(this).css('margin-right'));
				extendListw += extendItemW;
			});

			$('.post-extend').css('min-width',extendListw)

		};
		







	};//responsive-end


	


	// main-function  ===================================================================================================================================================================

	$(function(){


		var PtJustified = true;

		if (!!window.ActiveXObject || "ActiveXObject" in window  ){
			$('body').addClass('ie');
			PtJustified = false;
		};
		if ( navigator.userAgent.indexOf("Edge") > -1 ) {
			$('body').addClass('edge');
		};



		// if user wp-bar is true
		if ( $('#wpadminbar').length==1) {
			$('html').addClass('has-wp-bar');
		};


		//retina logo
		var $logoImg = $('.logo img,.m-logo img');
		if ( window.devicePixelRatio > 1 && !$logoImg.data('retina') =='' ) {
			$logoImg.attr('src',$logoImg.data('retina'))
		};
		var logoLink = $('.logo a').attr('href');
		$('.m-logo img').wrap('<a href="'+logoLink+'"></a>');


		// get site main color
		var siteColor = $('body').data('color');



		//all h addClass
		$("body").find("h1,h2,h3,h4,h5,h6").addClass('h');


		$('header:not(.style-05):not(.style-02) .sub-menu').find('a[title="sub-menu-hr"]').after('<hr style="margin:6px 0;border-bottom:none;opacity:0.3">').removeAttr('title');


		var hs_bar_flex;

		if($('body.pths-bar-full').length>0){
			hs_bar_flex = false;
		}else{
			hs_bar_flex = true;
		};


		$('.blog-list.style-02 .item').wrapInner('<div class="inner-wrap"></div>');
		$('.blog-list.style-02 .item').each(function(){
			$(this).append($('.list-category',this))
		});


		if ( $('.pic-list.style-02.item-gap').length > 0 && $('.list-title.style-01').length == 0) {

			$('.pic-list.style-02.item-gap').addClass('need-gap')

		};

		var related_len = $('.related-list .item').length;
		$('.related-list').addClass('for'+related_len);


		$('.sc-contactForm').each(function(){
			$('.wpcf7',this).prepend($('.sc-inner > h5',this))
		});

		$('.wpcf7-submit').closest('p').addClass('last');

		$('.ptsc .intro .fa + span:not(.fa),.ptsc .intro .fa + i:not(.fa),.ptsc .intro .fa + em:not(.fa),.ptsc .intro .fa + a:not(.fa)').addClass('ptsc-intro-icontext');


		var htmlFontSize = $('html').css('font-size')

		if ( parseInt(htmlFontSize) > 14 ) {
			$('html').addClass('fontSize-down')
		};

		if ( $('.post-banner > .wrap .item').length == 0) {
			$('.post-banner').addClass('null')
		};


		$('.ptsc-list > .wrap > p').each(function(){
			if ( $(this).html()=='&nbsp;') {
				$(this).remove()
			}
		});

		if ( $('footer .bg-full').length == 0) {
			$('footer').addClass('no-img')
		};
		if ( $('footer .text .h').text().trim() == '') {
			$('footer .text .h').remove()
		};
		if ( $('footer .pt-social li').length == 0 ) {
			$('footer .pt-social').remove()
		};
		if ( $('footer .text-area').text().trim() == '' && $('footer .pt-social li').length == 0 ) {
			$('footer .text-area').remove();
			if ( $('footer .text .h').length ==0 ) {
				$('footer.no-img .img,footer.no-img .text').remove();
				$('footer').addClass('only-copy')
			}
		};





		// woocommerce 
		$('body.single-product #primary #main div[id^="product-"]').addClass('wc-product-wrap');
		$('#primary .wc-product-wrap').prepend('<div class="wc-main-part"><div class="wrap"></div></div>').append('<div class="wc-detail-endBox"></div>');
		$('.wc-main-part .wrap').append( $('.wc-product-wrap > span.onsale,#primary .summary,.woocommerce-product-gallery'));
		$('.wc-main-part').prepend( $('.woocommerce-breadcrumb') );
		$('.woocommerce-Reviews').addClass('wc-reviews-root');
		$('.woocommerce-Reviews textarea').attr('rows','6');
		$('.woocommerce-product-details__short-description').wrapInner('<div class="des-text-wrap"><div class="content"></div></div>');

		$('.pic-list span.price').each(function(){
			if ( $('del',this).length > 0 ) {
				$(this).addClass('has-del')
			}
		});

		


		// if data-bg of bg-full not null
		$('.bg-full').each(function(){
			if ( $(this).data('bg') ) {
				$(this).addClass('has-data-bg')
			};
			if ( $(this).data('src') ) {
				$(this).addClass('has-data-video')
			}
		});



		if ( $('.list-title .bg-full').data('bg') ) {
			$('.list-title').addClass('has-img')
		}else{
			$('.list-title').addClass('no-img')
		};

		if ( $('.list-title h1').text().trim() == '' ) {
			$('.list-title').remove()
		};



		if ( $('.list-title.style-02 .intro').length > 0 ) {


			var title02Info = $('.list-title.style-02 .intro');
			title02Info.prepend($('.list-title .img'));

			$('.list-title.style-02 .title-text').hover(function(){
				title02Info.stop().fadeIn(0,function(){
					if(isRTL)
					{
						title02Info.animate({
							opacity:1,
							right:'90px'
						},400)
					}
					else
					{
						title02Info.animate({
							opacity:1,
							left:'90px'
						},400)
					}
				})
			},function(){
				if(isRTL)
				{
					title02Info.stop().animate({
						opacity:0,
						right:'70px'
					},400,function(){
						title02Info.fadeOut(0)
					})
				}
				else
				{
					title02Info.stop().animate({
						opacity:0,
						left:'70px'
					},400,function(){
						title02Info.fadeOut(0)
					})
				}
			});


		}else{
			$('.list-title.style-02 .img').remove()
		};



		if ( $('.pic-list.style-02 .list-title.style-01 .bg-full').data('bg') ) {
			var listTitleHtml = $('.list-title .img').html();
			$('body').append('<div class="list-title-bg">'+listTitleHtml+'</div>');
		};

		if ( $('.list-title.style-01.color-invert').length > 0) {
			$('.list-title-bg').addClass('need-bgColor')
		};




		if ( $('header.style-05,header.style-01').length == 0 ) {
			$('body').addClass('site-noFull')
		};

		if ( $('.default-template-page,body.woocommerce-cart,body.woocommerce-account,body.woocommerce-checkout,.woocommerce-form-login,#post-main.post-01').length > 0) {
			$('body').addClass('v-roll')
		};

		if ( $('body.woocommerce-cart').length > 0) {
			$('body').append('<div class="full-icon-bg cart"></div>');
		};
		if ( $('body.woocommerce-account').length > 0) {
			$('body').append('<div class="full-icon-bg account"></div>');
		};
		if ( $('body.woocommerce-checkout').length > 0) {
			$('body').append('<div class="full-icon-bg checkout"></div>');
		};

		if ( $('.woocommerce-form-login').length > 0) {
			$('.woocommerce').addClass('non-side')
		};




		// main menu edit ==================================================================================================================




		$('.m-header li .img').remove();

		if ( $('header .default-menu-list').length > 0 ) {
			$('body > header ul.children,.m-header ul.children').addClass('sub-menu');
		};

		$('header .main-menu .page_item_has_children,header .main-menu .menu-item-has-children').addClass('has-sub');
		$('header .main-menu .current_page_parent').addClass('current-menu-parent');
		$('header .main-menu .current-menu-item,header .main-menu .current_page_item').addClass('current_page_item');
		$('header .main-menu .current_page_ancestor').addClass('current-menu-ancestor');
		

		$('.main-menu > ul > li,.main-menu td').addClass('depth-1');
		$('.main-menu li.depth-1 > .sub-menu > li').addClass('depth-2');
		$('.main-menu li.depth-2 > .sub-menu > li').addClass('depth-3');


		if ( $('header.style-02').length > 0) {

			$('.logo').after('<table class="menu-02 main-menu"><tbody></tbody></table>');
			__ptMenu02Edit($('header .main-menu > ul >li'),3);

		};




		$('header:not(.style-05) .sub-menu').each(function(){

			if ( $('header.style-02').length==0) {

				if ( $('.bg-full',this).length > 0 ) {
					$(this).closest('li').addClass('flex-menu')
				}else{
					$(this).closest('li').addClass('sub-no-img')
				}

			}else{
				$(this).closest('li,td').addClass('flex-menu')
			}

		});
		// $('.flex-menu .sub-menu').wrap('<div class="sub-menu-wrap"><div class="nav-flex-list"></div></div>').after('<div class="close-sub-layer"></div><i class="close-sub-menu btn"></div>');
		$('.flex-menu .sub-menu').wrap('<div class="sub-menu-wrap"><div class="nav-flex-list"></div></div>');
		$('.sub-menu-wrap').after('<div class="close-sub-layer"></div><i class="close-sub-menu btn"></div>');


		$('nav li.flex-menu.has-sub > a,.menu-02 td.flex-menu.has-sub > a').removeAttr('href');


		$('.depth-1.flex-menu').append('<i class="btn close-depth-3"></i><div class="depth-3-layer">');


		$('.flex-menu .sub-menu').each(function(){

			if ( $(this).find('.sub-menu').length > 0 ) {
				$(this).find('.sub-menu').addClass('flex-target');
				$(this).find('li.has-sub').addClass('has-depth-3').append('<div class="nav-call-depth-3"></div>');
			};


			// set list width
			var liNum = $(this).children('li').length;
			$(this).attr('data-length',liNum);


			if ( liNum >= 6 ) {
				var win_w = $(window).width();
				$(this).width( win_w*0.86*0.25*liNum ).addClass('lot');
			};
			if ( liNum < 3 ) {
				var liTransition = '0.8s ';
			}else if ( liNum >= 3 ) {
				var liTransition = '0.6s ';
			};

			// set item show
			$(this).children('li').append('<em class="bg"></em>');
			var sub_i = 0;


			$(this).children('li').each(function(){

				var href = $(this).children('a').attr('href');

				$(this).append('<a class="full" href="'+href+'"></a>');

				$(this).children('em.bg').css({
					'transition':'all '+liTransition + sub_i*0.1 + 's'
				});

				$(this).children('.img').css({
					'transition':'all '+liTransition + sub_i*0.1 + 's'
				});

				sub_i++;

			});

			if ( liNum < 3 ) {
				$(this).attr('data-delay',( sub_i + 11 )*100 )
			}else if ( liNum >= 3 ) {
				$(this).attr('data-delay',( sub_i + 5 )*100 )
			};


		});


		//resize for flexMenu
		$(window).resize(function(){
			$('.flex-menu .sub-menu').each(function(){
				var liNum = $(this).children('li').length;
				if ( liNum >= 6 ) {
					var win_w = $(window).width();
					$(this).width( win_w*0.86*0.25*liNum );
				}
			})
		});


		$('.has-depth-3 .nav-flex-list .close-sub-layer').remove();


		$('.flex-menu > a,.nav-call-depth-3').on('click',function(){

			var $this = $(this).siblings('.sub-menu-wrap');
			var $btn = $(this);

			if ( $this.find('.sub-menu').hasClass('lot') ) {
				$this.append('<div class="pt-mouse-scroll-wide"><div class="list-scroller"><div class="list-scroller-target"></div></div></div>')
			};

			if ( $(this).hasClass('nav-call-depth-3')) {
				$(this).closest('.depth-1').children('.close-depth-3,.depth-3-layer').addClass('show');
			};

			

			$this.addClass('open-target').fadeIn(0,function(){

				if ( $btn.hasClass('nav-call-depth-3')) {
					$btn.closest('.has-depth-3').addClass('open');
					$('i.close-sub-menu').addClass('hide');
				}else{
					$this.siblings('.close-sub-layer').fadeIn(800);
				};

				setTimeout(function(){

					$this.addClass('open');
					$this.children('.nav-flex-list').children('.sub-menu').css('display','flex');

					if ( $this.children('.nav-flex-list').children('.sub-menu').hasClass('lot') ) {
						$this.ptHScroller({
							contentTarget:'.nav-flex-list',
							wrapTarget: '.sub-menu',
							useScroller: true,
							scrollOffset: 0.1,
							scrollSpeed: 700,
							scrollEase: 'quintOut'
						})
					}

				},50)

			});

			
		});


		$('.close-sub-layer,.close-sub-menu,.close-depth-3').on('click',function(){

			var $this; 

			if ( $(this).hasClass('close-depth-3')) {
				$('.close-depth-3,.depth-3-layer').removeClass('show');
				$('i.close-sub-menu').removeClass('hide');
				$this = $('.depth-2.has-depth-3 .sub-menu-wrap.open-target');
				$this.closest('.has-depth-3').removeClass('open');
			}else{
				$this = $('.depth-1 > .sub-menu-wrap.open-target');
				$('.close-sub-layer').fadeOut(300);
			};

			var $this_sub = $this.children('.nav-flex-list').children('.sub-menu');

			if ( $this_sub.hasClass('lot') ) {
				$this.ptHScrollerCall('dispose');
				$this.find('.pt-mouse-scroll-wide').remove();
			};

			$this.removeClass('open open-target');
			var delayTime = $this_sub.data('delay');
			$this.stop().delay(delayTime).fadeOut(100);

		});





		function CloseNav05(){
			$('body > header').removeClass('open');
			$('header .nav-05-outerWrap').stop().delay(delayTime).fadeOut(100,function(){
				$('.call-main-menu').css('pointer-events','auto');

				if ( $('.main-menu ul').hasClass('lot') ) {
					$('.nav-05-outerWrap').ptHScrollerCall('dispose');
					$('header .pt-mouse-scroll-wide').remove();
				};
			}).css('opacity','1');
		};



		if ( $('header.style-02,header.style-03,header.style-04').length > 0 ) {


			if ( $('.post-01,body.woocommerce-cart,.default-template-page').length > 0 ) {

				$('body').append($('#wpadminbar'));


				if ( $(document).scrollTop() > 300 ){
					$('body > header:not(.hide)').addClass('hide');
				}else{
					$('body > header.hide').removeClass('hide');
				};


				$('body > header').append('<div class="header_bg"></div>');
				var bodyBG = $('body').css('background-color');
				$('.header_bg').css('background-color',bodyBG);

				$(document).on("scroll",function(){

					if ( $(document).scrollTop() > 300 ){
						$('body > header:not(.hide)').addClass('hide');
					}else{
						$('body > header.hide').removeClass('hide');
					};

				})

			}

		};




		if ( $('header.style-05').length > 0 ) {

			$('header.style-05 .sub-menu').each(function(){
				var subLen = $('li',this).length;
				if ( subLen <=5 ) {
					$(this).addClass('font-large')
				}else if (subLen > 10) {
					$(this).addClass('font-small')
				}
			});


			var liNum = $('.main-menu li.depth-1').length;
			$('.main-menu').attr('data-length',liNum);

			$('.main-menu').wrap('<div class="nav-05-outerWrap"></div>');


			$('header .wrap').append('<i class="call-main-menu btn"></i><i class="close-main-menu btn"></i>');
			$('.main-menu li.depth-1').append('<em class="bg"></em>');


			if ( liNum < 3 ) {
				var liTransition = '1.2s ';
			}else if ( liNum >= 3 ) {
				var liTransition = '0.6s ';
			};

			if ( liNum >= 6 ) {
				var win_w = $(window).width() - 80;
				$('header .main-menu > ul').width( win_w*0.25*liNum ).addClass('lot');
			};

			$(window).resize(function(){
				if ( liNum >= 6 ) {
					var win_w = $(window).width() - 80;
					$('header .main-menu > ul').width( win_w*0.25*liNum );
				};
			});


			var nav_i = 0;
			$('header .main-menu li.depth-1').each(function(){

				$(this).children('em.bg').css({
					'transition':'all '+liTransition + nav_i*0.1 + 's'
				});

				$(this).children('.img').css({
					'transition':'all '+liTransition + nav_i*0.1 + 's'
				});

				nav_i++;

			});


			if ( liNum < 3 ) {
				var delayTime = ( nav_i + 11 )*100;
			}else if ( liNum >= 3 ) {
				var delayTime = ( nav_i + 5 )*100;
			};


			$('.call-main-menu').on('click',function(){
				$('header .nav-05-outerWrap').fadeIn(0,function(){

					setTimeout(function(){
						$('body > header').addClass('open');
					},10)

					

					if ( $('.main-menu ul').hasClass('lot') ) {

						$('.nav-05-outerWrap').append('<div class="pt-mouse-scroll-wide"><div class="list-scroller"><div class="list-scroller-target"></div></div></div>').ptHScroller({
							contentTarget:'.main-menu',
							wrapTarget: '.main-menu ul.lot',
							useScroller: true,
							scrollOffset: 0.1,
							scrollSpeed: 700,
							scrollEase: 'quintOut'
						})

					};

				$('header.style-05 .sub-menu').each(function(){
					var subH = $(this).outerHeight();
					if(subH%2 !==0){
						$(this).addClass('transform-hack');
					}else{
						$(this).removeClass('transform-hack');
					}
				});

				});
				$(this).css('pointer-events','none');

			});


			$('.close-main-menu').on('click',CloseNav05);

			$(window).on('keyup', function(evt){
				if(evt.keyCode ==27 && $('header.open').length > 0){
					CloseNav05()
				};
			});


		};

		


		

		$('.call-footer').on('click',function(){
			$('footer').addClass('open');
			$('.close-layer').fadeIn(400);
		});

		$('.close-footer,.close-layer').on('click',function(){
			$('footer').removeClass('open');
			$('.close-layer').fadeOut(400);
		});

		$('.call-search').on('click',function(){
			$('.header-search').addClass('open');
		});

		$('.close-search').on('click',function(){
			$('.header-search').removeClass('open');
		});

		$('.header-search .search-btn').hover(function(){
			$('.header-search').addClass('hover')
		},function(){
			$('.header-search').removeClass('hover')
		});





		if ( $('header.style-01').length > 0){
			$('header.style-01 .wrap').append('<div class="menu-bg"></div>');
			if ( $('header.style-01-02').data('color')){
				$('.menu-bg').css('background',$('header.style-01-02').data('color'))
			}
		};


		function main_sub_show(){
			$(this).children('.sub-menu').fadeIn(200)
		};
		function main_sub_hide(){
			$('.sub-menu',this).fadeOut(200)
		};
		$('.main-menu li.depth-1.sub-no-img,.main-menu li.depth-2.has-sub:not(.flex-menu)').hoverIntent({
			timeout:100,
			sensitivity:100,
			over: main_sub_show,
			out: main_sub_hide
		});



		$('header.style-05 .sub-menu').hover(function(){
			$(this).siblings('a').addClass('hide');
		},function(){
			$(this).siblings('a').removeClass('hide');
		});



		


		function onWCFragmentsLoaded(){


			$('header .widget_shopping_cart_content').addClass('traveller');

			$('.flex-control-thumbs:not(.edited)').addClass('edited').wrap('<div class="wc-flex-thumbs-wrap"></div>');

			var isMobile = /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);

			window.cartMove = function(){

				if ( window.innerWidth <1024 || isMobile ) {
					$('body').append($('body > header .traveller'));
				}else{
					if ( $('header .call-hidden-menu').length > 0) {
						$('header .call-hidden-menu').after( $('body > .traveller') )
					}else{
						$('header .header-right').prepend( $('body > .traveller') )
					}
				}

			};

			cartMove();

			var $widget_cart = $('.widget_shopping_cart_content.traveller');
			var $widget_cart_wrap = $('.traveller .pt_widget_cart_wrap');

			$('.woocommerce-message,main#main > .woocommerce-error,.woocommerce-store-notice').on('click',function(){$(this).fadeOut(200)});

			function cart_widget_show(){
				if ( $('.m-mode').length == 0 ) {
					$widget_cart_wrap.css('pointer-events','auto').fadeIn(0,function(){
						$widget_cart_wrap.animate({opacity:1,top:'40px'},300,'easeInOutQuad')
					})
				};
			};
			function cart_widget_hide(){
				if ( $('.m-mode').length == 0 ) {
					$widget_cart_wrap.css('pointer-events','none').animate({opacity:0,top:'60px'},300,'easeInOutQuad',function(){
						$widget_cart_wrap.fadeOut(0)
					})
				}
			};
			$widget_cart.off('mouseenter.hoverIntent, mouseleave.hoverIntent').hoverIntent({
				timeout:100,
				sensitivity:100,
				over: cart_widget_show,
				out: cart_widget_hide
			});

			$('.m-header .call-cart').on('click',function(){
				$('body > .widget_shopping_cart_content .pt_widget_cart_wrap').css({
					'display':'block',
					'opacity':'1'
				});
				$('.close-m-right').fadeIn(50);
			});
			$('.close-m-right').on('click',function(){
				$('body > .widget_shopping_cart_content .pt_widget_cart_wrap').css({
					'display':'none',
					'opacity':'0'
				});
				$(this).fadeOut(50);
			});


			$('.m-header .cart-num').html( $('.widget_shopping_cart_content .cart-num').text());


			if ( $('.woocommerce-variation-description p').length == 0) {
				$('.woocommerce-variation-description').remove()
			};


		};

		onWCFragmentsLoaded();
		$( document.body ).on( 'wc_fragments_loaded wc_fragments_refreshed', onWCFragmentsLoaded );


		$(window).resize(function(){
			cartMove();
		});



		$('form.cart tbody select').change(function(){
			setTimeout(function(){
				if ( $('.woocommerce-variation-description p').length > 0) {
					$('.woocommerce-variation-description').addClass('show')
				}else{
					$('.woocommerce-variation-description').removeClass('show')
				};
			},300)
		});




		



		// hidden-menu
		
		$('.hidden-menu').after('<div class="close-hidden-menu-layer"></div>');

		$('i.call-hidden-menu,.call-m-right').on('click',function(){
			$('.hidden-menu').addClass('open');
			$('.close-hidden-menu-layer,.close-m-right').fadeIn(500);
		});

		$('i.close-hidden-menu,.close-hidden-menu-layer,.close-m-right').on('click',function(){
			$('.hidden-menu').removeClass('open');
			$('.close-hidden-menu-layer,.close-m-right').fadeOut(500);
		});



		// mobile header
		$('.m-main-menu > ul > li').each(function(){
			if ( $(this).find('ul').length > 0) {
				$(this).append('<em class="call-m-sub"></em>');
			}
		})


		$('.call-m-left').on('click',function(){
			$('.m-left').addClass('open');
			$('.close-m-left').fadeIn(600)
		});
		$('.close-m-left').on('click',function(){
			$('.m-left').removeClass('open');
			$(this).fadeOut(300);
		});




		$('.call-m-sub').on('click',function(){
			$(this).siblings('ul').fadeToggle(0);
			$(this).toggleClass('open');
		});


		$('.related.products').attr('data-length',$('.related.products li').length );
		$('.related.products .bg-full,.post-related .bg-full,.related.products .hover-img,.post-extend .bg-full,.sc-mixbox .hover-img,.sc-images .bg-full').PtListBgTrans();



		// post-02

		
		if ( $('.post-02 .post-banner.h-short').length > 0 ) {
			$('.post-banner').before('<div class="post-boxed-wrap"><div class="wrap"></div></div>');
			$('.post-boxed-wrap > .wrap').append( $('.post-banner,.post-info'));
			$('.post-boxed-wrap').append( $('.post-bar') );
			$('.post-boxed-wrap .post-info').append( $('.share') );
		};


		$('.post-info .text-area').after($('.share').clone());


		responsive();








		$('body > header').animate({opacity:1},400,'easeInOutQuad',function(){
			$('body > header').addClass('trans');
		});






		// Motion Text setting
		var $MoText = $('.pic-list.style-01 .item .h,.list-title.style-01 .intro,.list-title.style-01 .h');
		$MoText.wrapInner('<abbr class="mo-text-wrap"><abbr class="mo-text-inner"></abbr></abbr>');
		$('.mo-text-wrap').append('<i class="mo-line"></i>');


		$.fn.extend({
			MotionText:function (obj) {
				this.each(function(){
					var $text = $(this).find('.mo-text-inner');
					var $line = $(this).find('.mo-line');
					$line.delay(100).animate({'width':'100%'},300,'easeInOutQuad',function(){
						$line.addClass('mo-out');
						$text.animate({opacity:1},300)
					})
				});
				return this;
			}
		});


		// hover Text setting
		var $hoverTextTarget = $('header .main-menu a:not(.full),.hidden-menu .menu a,.pic-list.style-01 .h a,.list-date,.blog-list.style-02 a.more');
		$hoverTextTarget.addClass('has-line').append('<i class="hoverLine"></i>').hover(function(){
			$('.hoverLine',this).stop().animate({width:'100%'},400)
		},function(){
			$('.hoverLine',this).stop().animate({width:'0'},400)
		});

		$('header .flex-menu a.full,header .flex-menu .nav-call-depth-3').hover(function(){
			if(isRTL)
			{
				$(this).closest('li').find('.hoverLine').css('right','0').stop().animate({width:'100%'},400);
			}
			else
			{
				$(this).closest('li').find('.hoverLine').css('left','0').stop().animate({width:'100%'},400);
			}
		},function(){
			if(isRTL)
			{
				$(this).closest('li').find('.hoverLine').css('right','auto').stop().animate({width:'0'},400);
			}
			else
			{
				$(this).closest('li').find('.hoverLine').css('left','auto').stop().animate({width:'0'},400);
			}
		});




		

		$('header .bg-full,.list-title-bg .bg-full,.list-title .bg-full').PtBgTrans();





		// search
		$('i.call-search').on('click',function(){
			$('.header-search').addClass('open');
			setTimeout(function (){ $('header input.search').focus()},900);
		});

		$('i.close-search').on('click',function(){
			$('.header-search').removeClass('open');
			$('header input.search').blur();
		});


		// call-list-widgets

		$('.list-widgets-wrap').append('<i class="close-list-widgets btn"></i>');


		$('.call-list-widgets').on('click',function(){
			$('.list-widgets').addClass('open');
			$('.close-list-widgets').fadeIn(300)
		});
		$('.close-list-widgets').on('click',function(){
			$('.list-widgets').removeClass('open');
			$('.close-list-widgets').fadeOut(300)
		});

		$('.widget_nav_menu .menu-main-container > ul >li').each(function(){
			if ( $('ul',this).length > 0 ) {
				$(this).addClass('has-sub').append('<i class="call-widget-sub"></i>')
			}
		});

		$('.call-widget-sub').on('click',function(){
			$(this).toggleClass('open').siblings('ul').slideToggle(200)
		});





		//piclist portfolio ---------------------------
		if ( $('.pic-list.style-02 .pages li').length > 0) {
			$('.pic-list.style-02').addClass('has-pages')
		};

		// list - pages -setting
		function pic_list_W_setting(){


			if ( $('.pic-list .pages li').length > 0) {

				$('.pic-list > .wrap').css('min-width','0');
				$('.pages').stop().animate({opacity:0},200);

				setTimeout(function(){


					var pic_list_outerW;
					var $fr_wrap;
					
					if ( $('.pic-list.style-02').length > 0) {
						$fr_wrap = $('.pic-list .fr-item-container.longest').get(0);
						if ($fr_wrap) {
							pic_list_outerW = $fr_wrap.scrollWidth;
							$('.pic-list > .wrap').css('min-width',pic_list_outerW)
						}
					};
					if ( $('.pic-list.style-01').length > 0) {
						
						$fr_wrap = $('.pic-list > .wrap').get(0);
						if ($fr_wrap) {
							pic_list_outerW = $fr_wrap.scrollWidth;
							$('.pic-list > .wrap').css('min-width',pic_list_outerW)
						}
					};
					$('.pages').stop().animate({opacity:1},200);
					$('.pic-outerWrap').ptHScrollerCall('resize');
					
				},500)
			};

		};

		pic_list_W_setting();

		$(window).resize(pic_list_W_setting);





		if ( $('.pic-list').length > 0 ) {
			$('.pic-list').wrap('<div class="pic-outerWrap"></div>');
			$('.woocommerce-breadcrumb').remove();
		};

		$('.pic-list.style-02 .item,.pic-list.style-01 .item .img,.blog-list .item:not(.text-post) .img,.ptsc .item .img,.post-banner .item,.post-extend .img').append('<abbr class="pic-loader"></abbr>');

		$('.pic-list').append('<div class="pic-list-mouse-area"></div>');



		var picViewport;

		if ( $('.pic-list.style-01').length > 0) {
			picViewport = true;
			$('.pic-list.style-01 .item').each(function(){
				$('.img',this).append($('.mask',this))
			});
			$('.pic-list.style-01 .item:last').addClass('visible-last');
		}else{
			picViewport = false;
		};


		$('.pic-outerWrap').addClass('inited').append('<div class="pt-mouse-scroll-boxed"><div class="list-scroller"><div class="list-scroller-target"></div></div></div>').ptHScroller({

			contentTarget: '.pic-list',
			wrapTarget: '.wrap',
			viewportTarget: '.item',
			viewportAddClass: '.inviewport',
			wheelTarget: '.pic-list-mouse-area,.pic-list',
			useScroller: true,
			scrollOffset: 0.08,
			enableViewport:picViewport,
			ignoreScrollerWidth:hs_bar_flex,
			scrollSpeed: 500,
			scrollEase: 'quintOut',
			onScroll: function(percent){
				if ( $('.pic-list .item:not(.show)').length > 0 ) {
					$('.pic-list .item.inviewport:not(.show) .bg-full').PtListBgTrans();
					$('.pic-list .item.inviewport:not(.show) .hover-img').PtBgTrans();
				};
				$('.pic-list.style-02').ptFlexRowCall('setScrollPercent', percent);
			}

		});



		var pic02Row = 1;
		if ( $('.pic-list.style-02.row-1').length > 0 || $('.pic-list.style-02.row-1-mini').length > 0) {
			pic02Row = 1;
		};
		if ( $('.pic-list.style-02.row-2').length > 0) {
			pic02Row = 2;
		};
		if ( $('.pic-list.style-02.row-3').length > 0) {
			pic02Row = 3;
		};



		$('.pic-list.style-02').ptFlexRow({
			wrapTarget: '.wrap',
			itemTarget: '.item',
			rows: pic02Row,
			alignType: 2,
			useJustified: PtJustified,
			onInit: function(){
				$('.pic-outerWrap').ptHScrollerCall('enableViewport', true);
				$('.fr-item-container:last').addClass('last')
			},
			onResizeAfter: function(changed){
				if(changed){
					$('.pic-outerWrap').ptHScrollerCall('resize');
				}
			}
		});


		$('.pic-outerWrap').ptHScrollerCall('resize');

		if ( $('.pic-list .item:not(.show)').length > 0 ) {
			$('.pic-list .item.inviewport:not(.show) .bg-full').PtListBgTrans();
			$('.pic-list .item.inviewport:not(.show) .hover-img').PtBgTrans();
		};

		$('.pic-list.style-02 > .wrap').append($('.pages'));





		// woocommerce 

		$('.product_title').append($('.wc-main-part .wrap > .onsale'));

		$('.des-text-wrap').ptBar({
			contentTarget: '.content'
		});


		function wc_form_move(){
			if ( $('.m-mode').length == 0 ) {
				$('body.single-product #main:not(.moved) div[id^="product-"]').append( $('#main form.cart') );
				$('#primary #main:not(.moved)').addClass('moved');
			}else{
				$('#primary #main.moved .woocommerce-product-details__short-description').after( $('#main form.cart') );
				$('#primary #main.moved').removeClass('moved');
			}
		};
		
		if( $('body.single-product #main form.cart table').length > 0 ){
			$('#main form.cart').addClass('wide')
			wc_form_move();
			$(window).resize(wc_form_move)
		};
		


		if ( $('body.single-product #primary').length > 0 ) {

			$('#primary').wrap('<div class="wc-outerWrap"></div>');

			
			$('li.comment .star-rating,.woocommerce-product-rating .star-rating').append('<div class="pt-star-wrap"><i></i><i></i><i></i><i></i><i></i></div>');


			$('.star-rating').each(function(){
				var rating_num = parseInt( $('.rating',this).text()) - 1;
				for (var i = rating_num; i >= 0; i--) {
					$('.pt-star-wrap i',this).eq(i).addClass('on');
				};
			});
			


			$('.comment-form-rating:not(.clicked) .stars a').hover(function(){
				$(this).prevAll('a').addClass('on');
				$(this).addClass('on')
			},function(){
				$('.comment-form-rating .stars a').removeClass('on')
			});

			$('.comment-form-rating .stars a').on('click',function(){
				var clickNum = $(this).text();
				$('.comment-form-rating .stars a').off('mouseenter').unbind('mouseleave').addClass('on clicked');
				$(this).nextAll().removeClass('on')
				$('select#rating option').attr("selected", false);
				$('select#rating option[value="'+clickNum+'"]').attr("selected", true);
			})

			

			$('.main-content').append('<div class="pt-mouse-scroll-boxed"><div class="list-scroller"><div class="list-scroller-target"></div></div></div>').ptHScroller({

				contentTarget:'.wc-outerWrap',
				wrapTarget: '#primary',
				wheelTarget: '.wc-outerWrap',
				useScroller: true,
				scrollOffset: 0.08,
				enableViewport:true,
				ignoreScrollerWidth:hs_bar_flex,
				scrollSpeed: 500,
				scrollEase: 'quintOut'

			});


			// temp js
			$('.wc-tabs li').on('click',function(){
				if ( $('body.m-mode').length== 0 && $('#primary').offset().left == 0 ) {
					$('.main-content').ptHScrollerCall('scroll', [900, 1])
				};
				var liIndex = $(this).index();
				$('.wc-tabs li').removeClass('active');
				$(this).addClass('active');
				$('.woocommerce-Tabs-panel').hide();
				$('.woocommerce-Tabs-panel').eq(liIndex).show(0);
				$('.wc-reviews-root').ptAreaRollCall('resize');
			});


			$('.wc-reviews-root').ptAreaRoll({
				moveRatio: 0.6,
				animateTime: 500,
				easing: 'quintOut',
				useWheel: true
			});


		};


		// post detail

		window.post01rollSlide = 0;
		var $bannerOwl = $('.post-banner.raw-proportion > .wrap');
		var post01OwlLoop;


		if ( $('#post-main.post-02').length > 0) {

			$('.post-02 .post-banner:not(.h-short) ~ .post-info').append($('.post-bar'));
			$('.comment-root').wrapInner('<div class="comment-innerWrap"</div>');



			$('.main-content').append('<div class="pt-mouse-scroll-boxed"><div class="list-scroller"><div class="list-scroller-target"></div></div></div>').ptHScroller({

				contentTarget:'.post-detail-wrap',
				wrapTarget: '.post-02',
				wheelTarget: '.post-detail-wrap',
				viewportTarget: '.item',
				viewportAddClass: '.inviewport',
				useScroller: true,
				scrollOffset: 0.08,
				ignoreScrollerWidth:hs_bar_flex,
				enableViewport:true,
				scrollSpeed: 500,
				scrollEase: 'quintOut',
				onScroll: function(percent){
					if ( $('.ptsc:not(.sc-slider):not(.sc-images) .item:not(.bg-loaded)').length > 0  ) {
						 $('.ptsc:not(.sc-slider):not(.sc-images) .item.inviewport:not(.bg-loaded) .bg-full').PtBgTrans();
					};
					if ( $('.ptsc:not(.sc-slider):not(.sc-images) .img:not(.img-loaded)').length > 0  ) {
						 $('.ptsc:not(.sc-slider):not(.sc-images) .item.inviewport .img:not(.img-loaded) img').PtImgLoaded();
					};
					$('.ptsc:not(.sc-slider):not(.sc-images) .item.inviewport .img.img-loaded:not(.show)').addClass('show');
					$('.ptsc:not(.sc-slider):not(.sc-images) .item.inviewport:not(.show)').addClass('show');
				}

			});


			$('.post-02 .post-extend .item.text-01 .text').wrap('<div class="hover-wrap"></div>').before('<i class="btn"></i>');

			$('.post-02 .post-extend .item.text-02 p,.post-02 .post-extend .item.text-03 p,.post-02 .post-info .text-area').wrap('<div class="ptbar-outerWrap"><div class="content"></div></div>');
			$('.post-02 .post-extend .item.text-02 .ptbar-outerWrap,.post-02 .post-extend .item.text-03 .ptbar-outerWrap,.post-02 .post-info .ptbar-outerWrap').ptBar({
				contentTarget: '.content'
			});

			var tagsLen = $('.post-tags a').length;
			if ( tagsLen > 6) {
				$('.post-tags').addClass('horizontal')
			};

			$('.comment-innerWrap > .wrap').addClass('content');

			$('.comment-innerWrap').ptBar({
				contentTarget: '.content'
			});

		};


		

		var share_i = 0;

		$('.share a').each(function(){
			$(this).css({ 'transition':'all 0.5s ' + share_i*0.02 + 's'});
			share_i++;
		});

		if ( $('.share a').length == 0 ) {
			$('#post-main').addClass('no-share')
		};

		if ( $('.post-category a').length > 3) {
			$('.post-category').wrapInner('<div class="over-content"></div>');
			var cate_a = $('.post-category .over-content').html()
			$('.post-category').append('<i class="btn show-all-category"></i><div class="category-wrap"><div class="inner-wrap">'+cate_a+'<i class="btn close-category"></div></div>');
		};

		$('.show-all-category').on('click',function(){
			$('.category-wrap').fadeIn(200);
		});

		$('.close-category').on('click',function(){
			$('.category-wrap').fadeOut(200);
		});




		if ( $('#post-main.v-post .bg-full').data('src')) {
			$('#post-main.v-post .post-banner .item').addClass('has-video')
		};


		$('.post-extend .item').each(function(){
			if ( $('.bg-full',this).data('src')) {
				$(this).addClass('has-video')
			}
		});



		function owlBtnFunc(event){
			var owlGap = (headerGap - 60)*0.5;
			$('.owl-nav > div').css({
				'margin-left':owlGap,
				'margin-right':owlGap
			})
		};

		if ( $('.post-banner.raw-proportion').length > 0 ) {

			// 

			var itemNums;
			var autoW;
			var owl_margin = 0;
			var owl_headerGap = 0;

			if ( $('.post-02 .post-banner.h-short,.post-02 .post-banner.h-full').length == 0 ) {
				owl_headerGap = headerGap*2
			};


			if ( $('.post-banner.show-one').length > 0) {
				itemNums = 1;
				owl_margin = 2;
				if (  $('.post-banner .item').length > 1 ) {
					post01OwlLoop = true;
				}else{
					post01OwlLoop = false;
				}
				
				autoW = false
			}else{
				autoW = true;

				if ( $('.post-banner.raw-proportion').attr('data-listw') > $bannerOwl.width() - owl_headerGap ) {
					itemNums = $bannerOwl.find('.item').length - 1;
					post01OwlLoop = true
				}else{
					itemNums = $bannerOwl.find('.item').length;
					post01OwlLoop = false
				}

			};
			 

			$bannerOwl.addClass('owl-carousel').owlCarousel({
				loop:post01OwlLoop,
				responsiveRefreshRate:50,
				rewind:true,
				smartSpeed:600,
				autoWidth:autoW,
				margin:owl_margin,
				navElement:'div',
				nav:true,
				dots:true,
				rtl:isRTL,
				onInitialized:owlBtnFunc,
				items:itemNums
			});

			$bannerOwl.on('resized.owl.carousel',owlBtnFunc);

		};

		

		$('.img-caption img').imagesLoaded( function(){
			$('.img-caption').addClass('events-none')
		});


		if ( $('#post-main.post-01').length > 0) {

			$('.post-banner.parallax:not(.v-post)').attr({'data-offset':'0.4','data-direct':'0'}).ptParallax({
				target:'.bg-full',
				addClass:'.pt-parallax',
				useAnimate:false
			});

		};



		function post02Short(){

			if ( $('body.m-mode').length > 0 ){

				$('.post-info .title:not(.edited)').after( $('.post-bar') ).addClass('edited');


			}else if( $('.post-info .title.edited').length > 0 ){
				
				// for post01
				$('.post-01 .post-info').before( $('.post-bar') );
				// for post02
				$('.post-02 .post-banner:not(.h-short) ~ .post-info').append( $('.post-bar') );
				// for post02 short 
				$('.post-boxed-wrap').append( $('.post-bar') );

				$('.post-info .title.edited').removeClass('edited');
				
			};

		};

		post02Short();
		$(window).resize(post02Short);




		// blog -list 

		function blog_p_show(){
			$('.intro',this).slideDown(300,'easeOutQuad')
		};
		function blog_p_hide(){
			$('.intro',this).slideUp(300,'easeOutQuad')
		};

		if ( $('.blog-list').length > 0 ) {


			$('.main-content').addClass('inited').append('<div class="pt-mouse-scroll-boxed"><div class="list-scroller"><div class="list-scroller-target"></div></div></div>').ptHScroller({

				contentTarget:'.blog-list',
				wrapTarget: '.wrap',
				viewportTarget: '.item',
				viewportAddClass: '.inviewport',
				wheelTarget: '.main-content',
				useScroller: true,
				ignoreScrollerWidth:hs_bar_flex,
				scrollOffset: 0.08,
				enableViewport:true,
				scrollSpeed: 500,
				scrollEase: 'quintOut',
				onScroll: function(percent){
					if ( $('.blog-list .item:not(.show)').length > 0 ) {
						$('.blog-list .item.inviewport:not(.show) .bg-full').PtListBgTrans();
					};
					$('.category-wrap.show').removeClass('show').fadeOut(200);
				}

			});



			if ( $('.blog-list .item:not(.show)').length > 0 ) {
				$('.blog-list .item.inviewport:not(.show) .bg-full').PtListBgTrans();
			};

			
			$('.blog-list.style-01 .item').hoverIntent({
				timeout:100,
				sensitivity:100,
				over: blog_p_show,
				out: blog_p_hide
			});



			$(window).resize(function(){
				if ( $('.blog-list.inited').length > 0 ) {
					setTimeout(function(){
						$('.blog-list.inited').ptHScrollerCall('resize');
						$('.blog-list').trigger('resize');
					},50)
				}
			})
			

		};//blog end








		$('.bg-color,.sc-slider .t-style-01 .h').each(function(){
			if ( $(this).data('color') ) {
				var colorCode = $(this).data('color');
				$(this).css('background',colorCode) 
			};
		});


		$('.sc-slider .t-style-02 .h').each(function(){
			if ( $(this).data('color') ) {
				var colorCode = $(this).data('color');
				if ( $(this).has('a')) {
					$(this).children('a').css('color',colorCode) 
				}else{
					$(this).css('color',colorCode) 
				}
			};
		});


		$('.bg-color').each(function(){
			if ( !$(this).data('color') ) {
				$(this).remove()
			}
		});

		$('.icon').each(function(){
			if ( $(this).data('color') ) {
				var colorCode = $(this).data('color');
				$(this).css('color',colorCode) 
			}
		});



		// shortCode setting 


		$('.sc-slider .item,.post-banner.multi-item:not(.raw-proportion) .item').append('<div class="s-btn s-btn-prev"></div><div class="s-btn s-btn-next"></div>');


		if ( $('.ptsc').length > 0 ) {

			
			$('.sc-mixbox').each(function(){

				var mixbox_i = 0;
				$('.item',this).each(function(){
					$(this).css({
						'transition':'opacity 0.5s ' + mixbox_i*0.05 + 's'
					});
					mixbox_i++;
				})

			});

			$('.sc-mixbox .item').each(function(){
				if ( $('>.bg-full.has-data-bg',this).length > 0) {
					$(this).addClass('has-bg')
				}
			});


			$('.ptsc .item').each(function(){

				if ( $('.img .bg-full',this).data('src')) {
					$(this).addClass('has-video');
					if ( $('.img .bg-full',this).data('volume') == 0  ) {
						$(this).addClass('video-mute')
					};
				};
				if ( $('>.bg-full',this).data('src')) {
					$(this).addClass('has-video has-bg-video')
				};

			});


			
			$('.sc-slider').each(function(){
				if ( $('.item',this).length == 1 ){
					$(this).addClass('non-needSwipe')
				};
				if( $(this).data('video-autoplay')==1){
					$('.item.has-video',this).addClass('autoplay');
				}
			});


			$('.sc-mixbox .item .intro').ptAreaRoll({
				moveRatio: 0.4,
				animateTime: 500,
				easing: 'easeOutQuart',
				useWheel: true
			});

			if ( $('.post-02').length == 0 ) {


				$('.main-content').addClass('inited').append('<div class="pt-mouse-scroll-boxed"><div class="list-scroller"><div class="list-scroller-target"></div></div></div>').ptHScroller({
					contentTarget: '.ptsc-list',
					wrapTarget: '.wrap',
					viewportTarget: '.item,.item .img',
					viewportAddClass: '.inviewport',
					wheelTarget: '.main-content',
					useScroller: true,
					scrollOffset: 0.08,
					enableViewport:true,
					scrollSpeed: 500,
					scrollEase: 'quintOut',
					ignoreScrollerWidth:hs_bar_flex,
					onScroll: function(percent){
						if ( $('.ptsc:not(.sc-slider):not(.sc-images) .item:not(.bg-loaded)').length > 0  ) {
							 $('.ptsc:not(.sc-slider):not(.sc-images) .item.inviewport:not(.bg-loaded) .bg-full').PtBgTrans();
						};
						if ( $('.ptsc:not(.sc-slider):not(.sc-images) .img:not(.img-loaded)').length > 0  ) {
							 $('.ptsc:not(.sc-slider):not(.sc-images) .item.inviewport .img:not(.img-loaded) img').PtImgLoaded();
						};
						$('.ptsc:not(.sc-slider):not(.sc-images) .item.inviewport .img.img-loaded:not(.show)').addClass('show');
						$('.ptsc:not(.sc-slider):not(.sc-images) .item.inviewport:not(.show)').addClass('show');
					}

				});

			};



			$('.sc-slider').addClass('pt-slider-root').ptSwipe({
				wrapTarget: '.slider-wrap',
				itemTarget: '.item',
				autoplay: false,
				speed: 800,
				tweenOutOffset: 0.86,
				ease: 'cubicInOut',
				onInit: function(){
					// if video is 'autoplay' 
					this.$element.find('.item.current.has-video.autoplay .bg-full').ptMediaPlayerCall('play');
					// if video hadn't been 'autoplay' 
					this.$element.find('.item.has-video:not(.autoplay) .bg-full').ptMediaPlayerCall('pause');
					this.$element.find('.item.has-video:not(.autoplay)').addClass('pause');
				},
				onSwipeBefore: function(){
					this.$element.find('.item.leaving').animate({opacity:0.3},400,'easeInQuad');
					
					// all video to pause
					this.$element.find('.item.has-video .bg-full').ptMediaPlayerCall('pause');
					this.$element.find('.item.has-video:not(.autoplay)').addClass('pause');
				},
				onSwipeAfter: function(){
					this.$element.find('.item').css('opacity','1');
					this.$element.find('.item:not(.current) .h,.item:not(.current) .intro').stop().animate({opacity:0,top:'50px'},200);
					this.$element.find('.item.current .h').stop().animate({opacity:1,top:0},500);
					this.$element.find('.item.current .intro').stop().delay(200).animate({opacity:1,top:0},500);

					// .ptMediaPlayerCall('unMute')
					$('.sc-slider .item:not(.current) .mute-for-click').removeClass('mute-for-click');
					if ( $('.sc-slider .item.current .mute-for-click').length == 0){
						$('.sc-slider .bg-full.has-data-video').ptMediaPlayerCall('unMute');
					}


					// autoplay video
					this.$element.find('.item.current.autoplay.has-video:not(.pause) .bg-full').ptMediaPlayerCall('play');

					if ( this.$element.data('autoplay')==1 ){
						this.$element.ptSwipeCall('resume');
					};

				}
			});



			$('.ptsc:not(.sc-slider) .item.inviewport:not(.show) .bg-full').PtBgTrans();
			$('.ptsc:not(.sc-slider) .item.inviewport .img:not(.img-loaded) img').PtImgLoaded();

			$('.sc-slider .item.current .h').animate({opacity:1,top:0},500);
			$('.sc-slider .item.current .intro').delay(200).animate({opacity:1,top:0},500);


		};





		if ( $('.ptsc .item.has-video').length > 0  ) {

			var $videoTarget = $('.ptsc .item.has-video .bg-full.has-data-video');

			$videoTarget.addClass('vTarget').closest('.item').append('<div class="v-play v-ctrl"></div><div class="v-pause v-ctrl"></div><div class="v-mobile-play v-ctrl"></div>');
			$videoTarget.ptMediaPlayer({
				autoplay: true,
				full:true,
				controls:false,
				ignoreMobile:true,
				removeVimeoControls: true,
				loop: true
			});

			$('.sc-slider .item.has-video:not(.autoplay)').addClass('pause');
			$('.sc-slider .item.has-video:not(.autoplay) .bg-full').ptMediaPlayerCall('pause');

		};



		if ( $('.post-banner .item.has-video').length > 0 ) {

			var bannerV_auto = true;
			var bannerV_mask = true;
			var $bannerV_target = $('.post-banner .item.has-video .bg-full.has-data-video');
			var bannerV_html = '<div class="v-play v-ctrl"></div><div class="v-pause v-ctrl"></div><div class="v-mobile-play v-ctrl"></div><div class="volume-btn"></div>'

			if ( $('.post-banner').data('video-autoplay')==1 ){

				$('.post-banner .item.has-video').append(bannerV_html);
				
			}else{

				bannerV_auto = false;
				if ( $bannerV_target.data('type') == 1 || $bannerV_target.data('type') == 'mp4'  ) {
					$('.post-banner .item.has-video').append(bannerV_html);
					$bannerV_target.closest('.item').addClass('pause');
				}else{
					bannerV_mask = false;
				}

				$bannerV_target.siblings('.volume-btn').remove()

			}

			

			$bannerV_target.addClass('vTarget').ptMediaPlayer({
				autoplay: bannerV_auto,
				full:true,
				mask:bannerV_mask,
				controls:false,
				ignoreMobile:true,
				volumeBtn:'.volume-btn',
				loop: true
			});


		};



		if ( $('.post-extend .item.has-video').length > 0 ) {

			$('.post-extend .item.has-video').append('<div class="v-play v-ctrl"></div><div class="v-pause v-ctrl"></div><div class="v-mobile-play v-ctrl"></div>');

			var $videoTarget = $('.post-extend .item.has-video .bg-full.has-data-video');

			$videoTarget.addClass('vTarget').ptMediaPlayer({
				autoplay: false,
				full:true,
				controls:false,
				ignoreMobile:true,
				loop: true
			});

			$videoTarget.closest('.item').addClass('pause');


		};




		$('.v-pause').on('click',function(){

			var $videoParent = $(this).closest('.item');
			var $video_root = $(this).closest('.item').parent().parent();

			$videoParent.addClass('pause');
			$videoParent.find('.bg-full.has-data-video').ptMediaPlayerCall('pause');

			$('.sc-slider .bg-full.has-data-video').ptMediaPlayerCall('unMute').removeClass('mute-for-click');

			if ( $video_root.hasClass('sc-slider') && $video_root.data('autoplay')==1 ){

				$video_root.ptSwipeCall('resume');
				
			};

		});


		$('.v-play').on('click',function(){

			var $videoParent = $(this).closest('.item');
			var $video_root = $(this).closest('.item').parent().parent();

			if ( !$videoParent.hasClass('video-mute') && $video_root.hasClass('sc-slider') ) {
				// all video to mute
				$('.sc-slider .bg-full.has-data-video').ptMediaPlayerCall('mute');
				// cilck target to unMute
				$videoParent.find('.bg-full.has-data-video').ptMediaPlayerCall('unMute').addClass('mute-for-click');
			}


			// current video to play
			$videoParent.removeClass('pause');
			$videoParent.find('.bg-full.has-data-video').ptMediaPlayerCall('play');


			if ( $video_root.hasClass('sc-slider')){

				if( !$videoParent.hasClass('autoplay') ){
					$video_root.ptSwipeCall('pause');
				};
				
			};

		});




		$('.s-btn-prev').on('click',function(){
			$(this).closest('.pt-slider-root').ptSwipeCall('prev');
			$('body').addClass('forPrev');
			setTimeout(function(){
				$('body').removeClass('forPrev forNext')
			},500)
		});
		$('.s-btn-next').on('click',function(){
			$(this).closest('.pt-slider-root').ptSwipeCall('next');
			$('body').addClass('forNext');
			setTimeout(function(){
				$('body').removeClass('forPrev forNext')
			},500)
		});




		var checkSliderLoad = function(scSlider){


			var $notShowItems;

			if ( $(scSlider).hasClass('post-banner raw-proportion')) {
				$notShowItems = $(scSlider).find('.owl-item:not(.show):not(.cloned)');
			}else{
				$notShowItems = $(scSlider).find('.item:not(.show)');
			};

			

			if($notShowItems.length > 0){
				var arr = [];
				$notShowItems.each(function(){
					arr.push({ 
						target:this, index: parseInt($(this).data('index')) 
					});
				});

				arr.sort(function(obj1, obj2){
					return obj1.index - obj2.index;
				});

				var $item = $(arr[0].target);
				var $bgTarget = $item.find('.bg-full');
				var bgUrl = $bgTarget.data('bg');



				$bgTarget.css("backgroundImage",'url('+ bgUrl +')').imagesLoaded( { background: true }, function(instance) {
				   	var elements = instance.elements;
				   	$item.addClass('show').find('.pic-loader').remove();
				   	$bgTarget.removeAttr('data-bg');
				   	checkSliderLoad(scSlider);
			   });

			}else{

				//loaded main pic 
				if ( $(scSlider).hasClass('post-banner raw-proportion')) {
					$('.post-banner .owl-item.cloned .bg-full').PtListBgTrans();
					$bannerOwl.trigger('play.owl.autoplay');
					$bannerOwl.on('changed.owl.carousel',function(){
						$bannerOwl.trigger('stop.owl.autoplay').trigger('play.owl.autoplay');
					})
				};

				if( $(scSlider).data('autoplay') == 1 ){
					$(scSlider).ptSwipeCall('setAutoplay',true);
				}
			}

		};


		$('.sc-slider').each(function(){
			checkSliderLoad(this);
		});


		$('.post-banner').each(function(){
			checkSliderLoad(this);
		});

		$('.post-banner.multi-item:not(.raw-proportion)').addClass('pt-slider-root').ptSwipe({
			wrapTarget: '> .wrap',
			itemTarget: '.item',
			autoplay: false,
			speed: 700,
			tweenOutOffset: 0.8,
			ease: 'cubicInOut',
			onInit: function(){
			},
			onSwipeBefore: function(){
				this.$element.find('.item.leaving').animate({opacity:0.3},400,'easeInQuad');
			},
			onSwipeAfter: function(){
				this.$element.find('.item').css('opacity','1');
			}
		});





		//single bg-full data-bg-trans
		var $dataBgElem = $('footer .bg-full.has-data-bg,.site-bg.has-data-bg');
		$dataBgElem.each(function(){
			var dataBgUrl = $(this).data('bg');
			$(this).css("backgroundImage",'url('+ dataBgUrl +')').imagesLoaded( { background: true }, function(instance) {
				var elements = instance.elements;
				$(elements).closest('.bg-full').removeAttr('data-bg');
				$(elements).closest('.site-bg.bg-full').addClass('show');
			})
		});



		$('.pic-list:not(.product-list):not(.gallery) .item,.blog-list .item').ptImageViewer({
			itemSelector: "i.btn-photo",
			fadeTime: 600,
			skinStyle: 'dark'
		});

		$('.pic-list.gallery').ptImageViewer({
			itemSelector: "a.full",
			fadeTime: 600,
			loop:true,
			skinStyle: 'dark'
		});



		// v-post play ----------------------------------------------------------


		var vSrc;
		var vType;
		var vVolume;


		$('.v-post.item i.btn-video,.v-mobile-play').on('click',function(){


			if ( $(this).hasClass('v-mobile-play')) {

				var $vTarget = $(this).closest('.item').find('.vTarget');
				vSrc = $vTarget.data('src');
				vType = $vTarget.data('type');
				vVolume = $vTarget.data('volume');

			}else{
				vSrc = $(this).data('src');
				vType = $(this).data('type');
				vVolume = $(this).data('volume');
			};

			var $videoHtml = '<div class="bg-full" data-src="'+vSrc+'"  data-type="'+vType+'"  data-volume="'+vVolume+'">';

			$('body').append('<div class="popup-player"><div class="player-wrap">'+$videoHtml+'</div><span class="close"></span></div>');
			var $v_player = $('.popup-player');


			$v_player.fadeIn(0,function(){

				$('.popup-player .close').addClass('show');
				$v_player.animate({opacity:1},400,function(){
					
					$v_player.find('.bg-full').ptMediaPlayer({
						autoplay: true,
						full:false,
						controls:true,
						loop: false
					})
				})
			});


			$('.popup-player .close').on('click',function(){
				$(this).removeClass('show');
				$v_player.animate({opacity:0},400,function(){
					$v_player.find('.bg-full').ptMediaPlayerCall('dispose',true);
					$v_player.remove();
				})
			})


		});





		// comment-form 
		$(".comment-form input,.comment-form textarea").focus(function(){
			$(this).closest('p').addClass('focus');
		});
		$(".comment-form input,.comment-form textarea").blur(function(){
			$(this).closest('p').removeClass('focus');
		});



		// detailPages video setting 
		$('.text-area iframe,.default-wrap iframe').each(function(){
			var iframe_src = $(this).attr('src');
			if ( iframe_src.indexOf("youtube.com")>=0 || iframe_src.indexOf("vimeo.com")>=0 ) {
				$(this).wrap("<div class='iframe-wrap'></div>");
			};
		});



		// a data-href
		var ptHandleLink = function(evt)
		{

			if($(this).hasClass('ajax_add_to_cart'))
			{
				return
			};

			if(window.ptPreventLink)
			{
				window.ptPreventLink = false;
				return;
			};

			evt = evt || {};

			var href = this.href || $(this).data('href');
			if(!href || href=='#') return;

			var browserActions = ['mailto:', 'tel:', 'sms:', 'intent:'];
			var len = browserActions.length;
			for(var i = 0; i < len; i ++)
			{
				if( href.indexOf(browserActions[i]) === 0 )
				{
					return;
				}
			}

			if( evt.ctrlKey || this.target == '_blank' || $(this).hasClass('zoom') || $(this).hasClass('play') || $(this).closest('.pic-list').hasClass('gallery')){
				if(!this.href)
				{
					window.location.href = href;
				}
				return;
			};

			var pos = href.lastIndexOf('/');
			if(pos > -1)
			{
				var str = href.substring(pos + 1);
				pos = str.indexOf('#');
				if(pos > -1)
				{
					return;
				}
			};

			evt.preventDefault();

			$('.loader-layer').fadeIn(500,'easeOutQuad',function(){
				window.location.href = href;
			});
			setTimeout(function(){
				$('.loader-layer').addClass('show')
			},300)

		};

	

		$('a:not(.comment-reply-link):not(.not-fade-link)').on('click', ptHandleLink);






		if ( $(document).height()> $(window).height()*1.4 && !navigator.userAgent.match(/mobile/i) && $('body.v-roll').length > 0  ) {

			$('body').append('<div class="go-top"></div>');

			$(document).on("scroll",function(){

				// for go-top
				if ( $(document).scrollTop() > win_h*0.6 ){
					$('.go-top:not(.show)').addClass('show');
				}else{
					$('.go-top.show').removeClass('show');
				};

			})//roll end

		};

		$('.go-top').on('click',function(){
			$('html,body').animate({scrollTop: '0px'},800)
		});






		// site-show
		$('.logo img').imagesLoaded( function() {

			$('.loader-layer').addClass('hide').fadeOut(600,'easeOutQuad',function(){
				if ( window.innerWidth >1024 || !navigator.userAgent.match(/mobile/i)  ){
					$('html').addClass('load');
				};
				$('.list-title.style-02').addClass('show');
				$('.blog-list .text').addClass('show');
				$('.loader-layer').removeClass('hide').addClass('big');
			});

			$('.list-title.style-01 .title-text').addClass('show').animate({opacity:1},0,function(){
				$('.list-title.style-01 .intro,.list-title.style-01 .h').MotionText()
			});
		});





		if($('body.pths-bar-full').length>0){
			$('.main-content .list-scroller').on('mousedown',function(){
				$('html').addClass('pt-mousedown')
			});
			$(document).on('mouseup','.pt-mousedown',function(){
				$('html').removeClass('pt-mousedown')
			})
		};




		$('.flex-control-nav > li').on('touchend mouseup', function(evt){
			var $this = $(this);
			var parentW = this.parentNode.scrollWidth;
			var viewW = $this.parent().width();
			var halfW = viewW >> 1;
			var itemW = $this.width();
			var offsetX;
			if(this.offsetLeft !== undefined)
			{
				offsetX = this.offsetLeft;
			}
			else
			{
				offsetX = $this.offset().left - $this.parent().offset().left;
			}

			var x;

			if(isRTL)
			{
				offsetX = viewW - itemW - offsetX;

				x = halfW - (offsetX + itemW / 2);
				if(x > 0) x = 0;
				if(x < viewW - parentW) x = viewW - parentW;
				x = Math.round(x);
				x = -x;
			}
			else
			{
				x = halfW - (offsetX + itemW / 2);
				if(x > 0) x = 0;
				if(x < viewW - parentW) x = viewW - parentW;
				x = Math.round(x);
			}
			
			if(createjs && createjs.Tween)
			{

				if(this.parentNode.__tweenX === undefined)
					this.parentNode.__tweenX = 0;

				createjs.Tween.get(
					this.parentNode,
					{
						override:true,
						onChange: function(evt){
							var tween = evt.target;
							var target = tween.target;
							
							$(target).css({
								'transform': 'translate3d(' + target.__tweenX + 'px, 0, 0)'
							});
						}
					}
				).to(
					{__tweenX:x},
					600,
					createjs.Ease.quartOut
				);
			}
			else
			{
				$this.parent().css({
					'transform': 'translate3d(' + x + 'px, 0, 0)'
				})
			}
		});



		// just for shop
		$('.main-menu li.current-menu-item a[href*="?_preset="],.m-main-menu li.current-menu-item a[href*="?_preset="]').closest('li').addClass('isCurrent').closest('nav').find('li.current-menu-item:not(.isCurrent)').removeClass('current-menu-item current_page_item');





		// filter-setting

		function pic01Edit(){
			$('.pic-list > .wrap').css('min-width','0');
			var $pic_wrap = $('.pic-list > .wrap').get(0);
			var pic_list_outerW = $pic_wrap.scrollWidth;
			$('.pic-list > .wrap').css('min-width',pic_list_outerW)
			$('.pic-outerWrap').ptHScrollerCall('resize');
		};


		// filter
		$('.filter li').on('click',function(){


			$('.filter li').removeClass('current');
			$(this).addClass('current');
			$('.pic-list .item').removeClass('visible-last');


			if ( $(this).hasClass('filter-all')) {

				if ( $('.m-mode').length > 0) {
					$('.pic-list .item').removeAttr('style').removeClass('m-hide');
				}else{
					$('.pic-list .item').show(300,pic01Edit).removeClass('m-hide');
				};
				$('.pic-list.style-01 .item:last').addClass('visible-last');
				$('.pic-list.disperse-off').addClass('disperse').removeClass('disperse-off')

			}else{

				$('.pic-list.disperse').addClass('disperse-off').removeClass('disperse');

				var current_name = $('span',this).data('filter');
				var $items =  $('.pic-list .item');
				var ItemsLen = $items.length;
				

				$('.pic-list .item').each(function(){

					ItemsLen--;
					var cats = $(this).data('filter');
					var arr = cats.split(' ');

					if(arr.indexOf(current_name) > -1){

						if ( $('.m-mode').length > 0) {
							$(this).removeClass('m-hide').removeAttr('style');
						}else{
							$(this).show(400).removeClass('m-hide');
						};

					}else{
						if ( $('.m-mode').length > 0) {
							$(this).addClass('m-hide').removeAttr('style');
						}else{
							$(this).hide(400).addClass('m-hide');
						}
					};

					// filter done
					if ( ItemsLen == 0 ) {

						var Allitems = $items.length;

						for(var i = Allitems - 1; i >= 0; i --){

							if( $items.eq(i).data('filter').split(' ').indexOf(current_name) > -1 ){
								$items.eq(i).addClass('visible-last');
								setTimeout(pic01Edit,500)
								break;
							}

						}
					}
					
				})
			}

		});



		if (!!window.ActiveXObject || "ActiveXObject" in window || navigator.userAgent.indexOf("Edge") > -1 ){
			var ptRootUrl = window.__pt_theme_root_url;
			$('.s-btn.s-btn-prev').css('cursor','url("'+ptRootUrl+'/data/images/left.ico"),pointer');
			$('.s-btn.s-btn-next').css('cursor','url("'+ptRootUrl+'/data/images/right.ico"),pointer');
			$('.gallery .item a.full').css('cursor','url("'+ptRootUrl+'/data/images/zoom.ico"),pointer');
		};



		// list-meta && single-meta if lots
		$('.item .list-category').each(function(){
			if ( $('a',this).length > 3 ) {
				$(this).wrapInner('<div class="over-content"></div>');
				var cate_a = $('.over-content',this).html();
				$(this).append('<em class="btn show-all-category"></em><div class="category-wrap"><div class="inner-wrap">'+cate_a+'<em class="btn close-category"></em></div></div>');
			}
		});


		$('.show-all-category').on('click',function(){
			$(this).siblings('.category-wrap').addClass('show').fadeIn(200);
		});

		$('.close-category').on('click',function(){
			$(this).closest('.category-wrap.show').removeClass('show').fadeOut(200);
		});


		
		$('.m-mode .sc-mixbox .img').addClass('show');

		$('.pricing-table .item').each(function(){
			var color;
			if( $(this).data('color') ){
				color = $(this).data('color');
				$('.sc-btn a',this).hover(function(){
					$(this).css('background-color',color)
				},function(){
					$(this).removeAttr('style')
				});
			}else{
				color = $('body').data('color');
			};
			$('.price-amount,li i',this).css('color',color);
		})
		




	});//main-function-end



	$(window).resize(function(){

		responsive();
		setTimeout(function(){
			$('.ptbar-outerWrap').ptBarCall('resize');
			$('.flex-viewport').height( $('.flex-active-slide a img').height() )
		},400);
		$('.pc-mode .pic-list.style-01 .item.m-hide').hide(0);
		
		$('.m-mode .sc-mixbox .img').addClass('show');


	})//resize end




}(jQuery));




