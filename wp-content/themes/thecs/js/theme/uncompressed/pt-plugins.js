/**
 * PT Image Viewer
 * Created by foreverpinetree@gmail.com on 2017/3/17.
 */
!(function($, win)
{
    "use strict";

    var ImageViewer = function(element, data)
    {
        this._element = element;
        element.__ivInstance = this;
        this.$e = $(element);

        this._itemSelector = data['itemSelector'] || '.item';
        this._loop = data['loop'] == undefined ? true : data['loop'];
        this._showThumb = data['showThumb'] || false;
        this._animateTime = data['animateTime'] == undefined ? 500 : data['animateTime'];
        this._animateInEase = data['animateInEase'] || 'sineOut';
        this._animateOutEase = data['animateOutEase'] || 'sineOut';
        this._cd = data['cd'] == undefined ? 300 : data['cd'];
        this._fadeTime = data['fadeTime'] == undefined ? 500 : data['fadeTime'];
        this._showTitle = data['showTitle'] == undefined ? true : data['showTitle'];
        this._skinStyle = data['skinStyle'] == undefined ? 'dark' : data['skinStyle'];
        this._dragDistance = data['dragDistance'] == undefined ? 30 : data['dragDistance'];
        this._closeHandler = data['closeHandler'] || null;

        if(win['PT_IV_LOOP'] != undefined)
        {
            this._loop = win['PT_IV_LOOP'];
        }

        if(win['PT_IV_SHOW_THUMB'] != undefined)
        {
            this._showThumb = win['PT_IV_SHOW_THUMB'];
        }

        createjs.Ticker.timingMode = createjs.Ticker.RAF;

        this._isIE9 = navigator.userAgent.indexOf("MSIE 9.0") > -1;

        this._stageWidth = 0;

        this._isDevice = /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);
        this._clickEvt = this._isDevice ? 'touchend mouseup' : 'click';

        this._downEvt = 'touchstart mousedown';
        this._moveEvt = 'touchmove mousemove';
        this._upEvt = 'touchend mouseup';

        this._allowCalls = ['open', 'dispose'];

        this._isRTL = ($('body').css('direction') || '').toLowerCase() == 'rtl';

        this._inCD = false;

        this._lastTapTime = 0;

        this._oldMouseX = 0;
        this._startDragX = 0;
        this._currentX = 0;

        this._dragTarget = null;
        this._canDrag = true;

        this._stageItems = [];

        this._isJsonDataMode = false;

        this.$triggerItems = null;
        this.$items = null;
        this._items = [];
        this._total = 0;

        this._currentIndex = 0;

        this._emptyData = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';

        var ivRootClass = this._showThumb ? 'pt-iv-has-thumb pt-iv-show-thumbs ' : '';
        ivRootClass += this._skinStyle;

        if(this._isDevice)
        {
            ivRootClass += ' is-device';
        }

        this.$html = $('<div class="pt-iv-root ' + ivRootClass + '">' +
            '<div class="pt-iv-container"></div>' +
            '<div class="pt-iv-prev pt-iv-btn"><i></i></div>' +
            '<div class="pt-iv-next pt-iv-btn"><i></i></div>' +
            '<div class="pt-iv-close pt-iv-btn"><i></i></div>' +
            '<div class="pt-iv-title"><i></i><p></p></div>' +
            '<div class="pt-iv-number"><i>1</i><i>/</i><i>' + this._total + '</i></div>' +
            '<ul class="pt-iv-thumb-list"></ul>' +
            (this._showThumb ? '<div class="pt-iv-thumbs-toggle"><i></i></div>' : '') +
            '</div>');
        this.$html.fadeOut(0);

        this.$thumbSource = $('<li class="pt-iv-thumb-item"><img src="' + this._emptyData + '" alt=""></li>');
        this.$thumbList = this.$html.find('.pt-iv-thumb-list');

        this.$numContainer = this.$html.find('.pt-iv-number');
        this.$currentNum = this.$numContainer.children('i:nth-child(1)');
        this.$totalNum = this.$numContainer.children('i:nth-child(3)');
        this.$numContainer.hide();
        
        this.$title = this.$html.find('.pt-iv-title>p');

        this.$imgContent = this.$html.find('.pt-iv-container');

        this._imgItems = [];
        this._thumbItems = [];

        var clickEvent = this._clickEvt;

        if(!this._isDevice)
        {
            this.$html.on('click', function (evt) {
                var $target = $(evt.target);
                if($target.hasClass('pt-iv-item') || $target.hasClass('pt-iv-loading'))
                {
                    evt.preventDefault();
                    evt.stopPropagation();
                    this._onClose();
                }
            }.bind(this));
        }

        if(this._isRTL)
        {
            this.$prevBtn = this.$html.find('.pt-iv-next');
            this.$nextBtn = this.$html.find('.pt-iv-prev');
        }
        else
        {
            this.$prevBtn = this.$html.find('.pt-iv-prev');
            this.$nextBtn = this.$html.find('.pt-iv-next');
        }

        this.$prevBtn.on(clickEvent, function (evt) {
            this._onPrev();
        }.bind(this));
        this.$nextBtn.on(clickEvent, function (evt) {
            this._onNext();
        }.bind(this));

        this.$html.find('.pt-iv-close').on(clickEvent, function (evt) {
            evt.preventDefault();
            evt.stopPropagation();
            this._onClose();
        }.bind(this));

        this.$html.on('wheel', function(event) {
            var evt = event.originalEvent;
            evt.deltaY < 0 ? this._onPrev() : evt.deltaY > 0 ? this._onNext() : '';
        }.bind(this));

        if(this._showThumb)
        {
            this.$html.find('.pt-iv-thumbs-toggle').on(clickEvent, function (evt) {
                evt.preventDefault();
                evt.stopPropagation();
                this.$html.toggleClass('pt-iv-show-thumbs');
            }.bind(this));
        }

        this._onOpenHandler = this._onOpen.bind(this);
        this._onKeyUpHandler = this._onKeyUp.bind(this);
        this._onResizeHandler = this._onResize.bind(this);
        this._onDraggingHandler = this._onDragging.bind(this);
        this._onDragEndHandler = this._onDragEnd.bind(this);

        this.updateData();
    }

    var p = ImageViewer.prototype;

    p.updateData = function ()
    {
        var $selectEles = this.$e.find(this._itemSelector);
        var openMode ;
        if($selectEles.length === 1 && $selectEles.data('src-type') === 'json')
        {
            this._isJsonDataMode = true;

            var self = this;
            (function(){
                var data = $selectEles.data('src');
                if(data && data.src && data.src.length > 0)
                {
                    var len = data.src.length, $item, src, title, thumb;
                    var $container = $('<div></div>');
                    for(var i = 0; i < len; i ++)
                    {
                        src = data.src[i] || '';
                        if(!src) continue;

                        title = data.title ? (data.title[i] || '') : '';
                        thumb = data.thumb ? (data.thumb[i] || '') : '';
                        $item = $('<div data-src="' + src + '" data-title="' + title + '" data-thumb="' + thumb + '"></div>');

                        $container.append($item);
                    }

                    self.$items = $container.children();
                }
            })();
        }
        else
        {
            this._isJsonDataMode = false;
            this.$items = $selectEles;
        }

        this.$triggerItems = $selectEles;
        
        this._items.length = 0;
        this.$items.each(function (index, item) {
            this._items.push(item);
        }.bind(this));

        this._total = this._items.length;

        if(this._total > 1)
        {
            this.$numContainer.show();
        }
        else
        {
            this.$numContainer.hide();
        }

        this.$triggerItems.off('click', this._onOpenHandler).on('click', this._onOpenHandler);

        if(this._total < 2)
        {
            this.$prevBtn.addClass('pt-iv-btn-disabled');
            this.$nextBtn.addClass('pt-iv-btn-disabled');
        }

        this.$totalNum.html(this._total);

        if(this._showThumb)
        {
            this.$thumbList.empty();
            this._thumbItems.length = 0;

            var src, item, $thumb, event = this._downEvt;
            for(var i = 0; i < this._total; i ++)
            {
                item = this._items[i];
                src = $(item).data('thumb');
                $thumb = this.$thumbSource.clone();
                $thumb.children('img').attr('src', src);
                $thumb.on(event, this._onClickThumb.bind(this));
                this.$thumbList.append($thumb);

                this._thumbItems.push($thumb);
            }
        }
    }

    p.open = function (index)
    {
        index = index || 0;

        if(index < 0)
        {
            index = 0;
        }
        else if(index > this._total - 1)
        {
            index = this._total - 1;
        }

        this._currentIndex = index;
        this._onOpen(null);
    }

    p._onClickThumb = function (evt)
    {
        evt.preventDefault();
        evt.stopPropagation();

        var $thumb = $(evt.currentTarget);

        var index = $thumb.index();
        if(index == this._currentIndex)
        {
            return;
        }

        this.open(index);
    }

    p._onResize = function (evt)
    {
        this._stageWidth = this.$html.width();

        var $thumb = this._thumbItems[this._currentIndex];
        this._updateThumbListPos($thumb);
    }

    p._onKeyUp = function (evt)
    {
        switch (evt.keyCode)
        {
            case 37: //left
            case 38: //up
                this._onPrev();
                break;
            case 39: //right
            case 40: //down
                this._onNext();
                break;
            case 27:
                this._onClose();
                break;
        }
    }

    p._onPrev = function ()
    {
        if(this._checkInCD()) return;

        this.$html.removeClass('pt-iv-first-open');

        var index = this._currentIndex;
        index --;
        if(index < 0)
        {
            index = this._loop ? this._total - 1 : 0;
        }

        if(this._currentIndex != index)
        {
            this._currentIndex = index;

            this.$currentNum.html(index + 1);

            var item = this._items[index];
            if(item)
            {
                this._showImage($(item).attr('href') || $(item).data('src'), $(item).data('title'), 1, true);
            }
        }
    }

    p._onNext = function ()
    {
        if(this._checkInCD()) return;

        this.$html.removeClass('pt-iv-first-open');

        var index = this._currentIndex;
        index ++;
        if(index > this._total - 1)
        {
            index = this._loop ? 0 : this._total - 1;
        }

        if(this._currentIndex != index)
        {
            this._currentIndex = index;

            this.$currentNum.html(index + 1);

            var item = this._items[index];
            if(item)
            {
                this._showImage($(item).attr('href') || $(item).data('src'), $(item).data('title'), -1, true);
            }
        }
    }

    /**
     *
     * @param src
     * @param title
     * @param direct, -1: right to left, 1: left to right
     * @param tween
     * @private
     */
    p._showImage = function (src, title, direct, tween)
    {
        this._canDrag = false;

        this._stageItems.forEach(function (item) {
            var x = direct == -1 ? - this._stageWidth : this._stageWidth;
            if(this._isRTL) x = -x;

            createjs.Tween.get(
                item,
                {
                    override:true,
                    onChange:this._onUpdate.bind(this)
                }
            ).to(
                {__tweenX:x},
                tween ? this._animateTime : 0,
                createjs.Ease[this._animateOutEase] || createjs.Ease.sineOut
            ).call(this._onTweenOutEnd.bind(this, item));

        }, this);
        this._stageItems.length = 0;

        var item = this._getImageItem(src);
        this.$imgContent.append(item);

        var x = direct == -1 ? this._stageWidth : - this._stageWidth;
        if(this._isRTL) x = -x;

        this._renderX($(item), x);
        item.__tweenX = x;

        createjs.Tween.get(
            item,
            {
                override:true,
                onChange:this._onUpdate.bind(this)
            }
        ).to(
            {__tweenX:0},
            tween ? this._animateTime : 0,
            createjs.Ease[this._animateInEase] || createjs.Ease.sineOut
        ).call(this._onTweenInEnd.bind(this));

        if(this._showTitle)
        {
            this.$title.fadeOut(tween ? (this._animateTime >> 1) : 0, function (title) {
                this.$title.html(title).fadeIn(this._animateTime >> 1);
            }.bind(this, title));
        }

        this._stageItems.push(item);

        this.$thumbList.children('.pt-iv-thumb-item').removeClass('selected');
        var $thumb = this._thumbItems[this._currentIndex];
        $thumb && $thumb.addClass('selected');

        this._updateThumbListPos($thumb);
        this._updateArrow();
    }

    p._updateThumbListPos = function ($thumb)
    {
        var listW = this.$thumbList.width();
        if(listW < this._stageWidth)
        {
            this.$thumbList.css('left', ((this._stageWidth - listW) >> 1) + 'px');
            return;
        }

        var ox = 0;
        if($thumb)
        {
            ox = this._stageWidth / 2 - $thumb.offset().left - $thumb.width() / 2;
        }

        var x = this.$thumbList.position().left + ox;
        if(x > 0)
        {
            x = 0;
        }
        else if(x < this._stageWidth - listW)
        {
            x = this._stageWidth - listW;
        }

        this.$thumbList.css('left', x + 'px');
    }

    p._dragBack = function ()
    {
        createjs.Tween.get(
            this._dragTarget,
            {
                override:true,
                onChange:this._onUpdate.bind(this)
            }
        ).to(
            {__tweenX:0},
            this._animateTime,
            createjs.Ease[this._animateOutEase] || createjs.Ease.sineOut
        ).call(this._onDragBack.bind(this));
    }

    p._onDragBack = function ()
    {
        this._canDrag = true;
    }

    p._onTweenInEnd = function ()
    {
        this._canDrag = true;
    }

    p._onTweenOutEnd = function (item)
    {
        this._imgItems.push(item);
        $(item).find('img').attr('src', this._emptyData);
        $(item).remove();
    }

    p._renderX = function ($target, x)
    {
        if(this._isIE9)
        {
            $target.css('transform', 'translateX(' + x + 'px)');
        }
        else
        {
            $target.css('transform', 'translate3d(' + x + 'px,0,0)');
        }
    }
    
    p._onUpdate = function (evt)
    {
        var tween = evt.target;
        var target = tween.target;
        
        this._renderX($(target), target.__tweenX);
    }

    p._onOpen = function (evt)
    {
        if(evt)
        {
            evt.preventDefault();
            evt.stopPropagation();
        }

        if(this._checkInCD()) return;

        var item;
        if(this._isJsonDataMode)
        {
            item = this._items[this._currentIndex];
            if(!item)
            {
                this._currentIndex = 0;
                item = this._items[this._currentIndex];
            }
        }
        else if(evt)
        {
            item = evt.currentTarget;
            this._currentIndex = this._items.indexOf(item);
        }
        else
        {
            item = this._items[this._currentIndex];
        }

        if(!item)
        {
            return;
        }

        this.$html.addClass('pt-iv-first-open');

        $('body').addClass('pt-iv-modal').append(this.$html);
        this.$html.addClass('pt-iv-fading pt-iv-fading-in').fadeIn(this._fadeTime, function () {
            this.$html.removeClass('pt-iv-fading pt-iv-fading-in');
        }.bind(this));

        this.$currentNum.html(this._currentIndex + 1);

        setTimeout(function () {
            this._showImage($(item).attr('href') || $(item).data('src'), $(item).data('title'));
        }.bind(this), 150);

        $(win).on('keyup', this._onKeyUpHandler);
        $(win).on('resize', this._onResizeHandler);

        this._onResize();
    }

    p._onClose = function (evt)
    {
        this.$html.addClass('pt-iv-fading pt-iv-fading-out').fadeOut(this._fadeTime, function () {
            this.$html.removeClass('pt-iv-fading pt-iv-fading-out').detach();
            this._stageItems.forEach(function (item) {
                $(item).remove();
            }, this);
            this._stageItems.length = 0;

            if(this._closeHandler)
            {
                this._closeHandler && this._closeHandler.call(this, this);
            }
        }.bind(this));

        $('body').removeClass('pt-iv-modal');

        this.$html.removeClass('pt-iv-first-open');

        $(win).off('keyup', this._onKeyUpHandler);
        $(win).off('resize', this._onResizeHandler);
    }

    p._getImageItem = function (src)
    {
        var item, img;
        if(this._imgItems.length > 0)
        {
            item = this._imgItems.pop();
            img = $(item).find('img').get(0);
        }
        else
        {
            item = document.createElement('div');
            item.className = 'pt-iv-item';
            item.innerHTML = '<div class="pt-iv-loading"><abbr class="pic-loader"></abbr></div>';
        }

        if(!img)
        {
            img = new Image();
            img.onload = function (evt) {
                $(this).parent().addClass('pt-iv-img-loaded');
                this.style.opacity = 1;
            };
            img.alt = '';
            item.appendChild(img);
        }

        item.__tweenX = 0;

        var $item = $(item);
        $item.removeClass('pt-iv-img-loaded');
        img.style.opacity = 0;
        img.src = src || this._emptyData;

        if(this._total > 1)
        {
            $item.on(this._downEvt, this._onStartDrag.bind(this));
        }

        return item;
    }

    p._getIsDblTap = function ()
    {
        var t = new Date().getTime();
        if(this._lastTapTime > 0)
        {
            var o = t - this._lastTapTime;
            if(o < 500)
            {
                this._lastTapTime = 0;
                return true;
            }
        }
        this._lastTapTime = t;
        return false;
    }

    p._onStartDrag = function (event)
    {
        var evt = event.originalEvent;
        evt.preventDefault();

        if($(evt.target).hasClass('pt-iv-item'))
        {
            return;
        }

        if(!this._canDrag) return;
        this._canDrag = false;

        var target = evt.currentTarget;
        this._dragTarget = target;

        $(win).off(this._moveEvt, this._onDraggingHandler).on(this._moveEvt, this._onDraggingHandler);
        $(win).off(this._upEvt, this._onDragEndHandler).on(this._upEvt, this._onDragEndHandler);

        this.$imgContent.addClass('pt-iv-press');

        this._oldMouseX = this._getMouseX(evt);
        this._currentX = 0;
        this._startDragX = this._currentX;
    }

    p._onDragging = function (event)
    {
        var evt = event.originalEvent;
        evt.preventDefault();

        var x = this._getMouseX(evt);
        this._currentX += x - this._oldMouseX;
        this._oldMouseX = x;

        this._renderX($(this._dragTarget), this._currentX);
    }

    p._onDragEnd = function (event)
    {
        $(win).off(this._moveEvt, this._onDraggingHandler);
        $(win).off(this._upEvt, this._onDragEndHandler);

        if(this._dragTarget)
        {
            this._dragTarget.__tweenX = this._currentX;
        }

        var distance = this._currentX - this._startDragX;
        if(this._isRTL)
        {
            distance = - distance;
        }

        var isAvailable = false;
        if(distance < - this._dragDistance)
        {
            if(this._isRTL)
            {
                isAvailable = this._currentIndex >= 0 && this._currentIndex < this._total - 1;
            }
            else
            {
                isAvailable = this._currentIndex < this._total - 1;
            }

            this._loop || isAvailable ? this._onNext() : this._dragBack();
        }
        else if(distance > this._dragDistance)
        {
            if(this._isRTL)
            {
                isAvailable = this._currentIndex > 0 && this._currentIndex <= this._total - 1;
            }
            else
            {
                isAvailable = this._currentIndex > 0;
            }

            this._loop || isAvailable ? this._onPrev() : this._dragBack();
        }
        else
        {
            this._dragBack();
        }

        this.$imgContent.removeClass('pt-iv-press');

        this._dragTarget = null;
    }

    p._updateArrow = function ()
    {
        if(this._total < 2 || this._loop) return;

        var $prev = this.$prevBtn, $next = this.$nextBtn, className = 'pt-iv-btn-disabled';

        if(this._currentIndex <= 0)
        {
            $prev.addClass(className);
            $next.removeClass(className);
        }
        else if(this._currentIndex >= this._total - 1)
        {
            $prev.removeClass(className);
            $next.addClass(className);
        }
        else
        {
            $prev.removeClass(className);
            $next.removeClass(className);
        }
    }

    p._getMouseX = function(evt)
    {
        var x;
        if(this._isDevice)
        {
            var touches = evt.changedTouches, touch;
            touch = touches[0];
            if(touch)
            {
                x = touch.pageX;
            }
            else
            {
                x = evt.pageX !== undefined ? evt.pageX : evt.clientX;
            }
        }
        else
        {
            x = evt.pageX !== undefined ? evt.pageX : evt.clientX;
        }
        return x;
    }

    p._checkInCD = function()
    {
        if(this._inCD) return true;

        setTimeout(function () {
            this._inCD = false;
        }.bind(this), this._cd);

        this._inCD = true;
        return false;
    }

    p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(funcParams && !Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) == 'function')
        {
            f.apply(this, funcParams || []);
        }
    };

    p.dispose = function()
    {
        this.$triggerItems.off('click', this._onOpenHandler);

        this.$html.remove();

        var $win = $(win);

        $win.off('keyup', this._onKeyUpHandler);
        $win.off('resize', this._onResizeHandler);
        $win.off(this._moveEvt, this._onDraggingHandler);
        $win.off(this._upEvt, this._onDragEndHandler);

        var ele = this._element;
        if(ele)
        {
            ele.__ivInstance = null;
            this._element = null;
        }

        this._closeHandler = null;
    }

    $.fn.extend({
        ptImageViewer:function (obj) {
            var iv;
            this.each(function(){
                iv = this.__ivInstance;
                if(iv)
                {
                    iv.updateData();
                }
                else
                {
                    new ImageViewer(this, obj);
                }
            });
            return this;
        },
        ptImageViewerCall:function (funcName, funcParams) {
            this.each(function(){
                var iv = this.__ivInstance;
                iv && iv.execute(funcName, funcParams);
            });
            return this;
        }
    });
}(jQuery, window));


/**
 * Created by qiu on 2016/11/16.
 */
!(function($, win)
{
    "use strict";

    var HScroller = function(element, data)
    {
        this._element = element;
        this._element.__ptHScroller = this;

        var $element = $(this._element);
        this.$element = $element;

        var $content = null;
        if(data.contentTarget && typeof(data.contentTarget) === 'object')
        {
            $content = $(data.contentTarget);
        }
        else
        {
            $content = this.$element.find(data.contentTarget || '.list-content');
        }

        this.$content = $content;
        this._content = $content.get(0);

        if(!this._content)
        {
            if(this._element)
            {
                this._element.__ptHScroller = null;
                this._element = null;
                this.$element = null;
            }

            this.$content = null;
            this._disposed = true;
            
            return this;
        }

        $content.addClass('pt-hs-content');

        var $wrap = null;
        if(data.wrapTarget && typeof(data.wrapTarget) === 'object')
        {
            $wrap = $(data.wrapTarget);
        }
        else
        {
            $wrap = this.$element.find(data.wrapTarget || '.wrap');
        }

        this.$wrap = $wrap;
        this._wrap = $wrap.get(0);
        
        var $items = null;
        if(data.viewportTarget && typeof(data.viewportTarget) === 'object')
        {
            $items = $(data.viewportTarget);
        }
        else if(data.viewportTarget)
        {
            $items = this.$wrap.find(data.viewportTarget);
        }

        this.$scroller = this.$element.find('.list-scroller');
        this.$scrollerHolder = this.$scroller.parent();

        this._items = [];

        if($items)
        {
            $items.each(function(i, item){
                this._items.push($(item));
            }.bind(this));
        }

        createjs.Ticker.timingMode = createjs.Ticker.RAF;

        this._disableTarget = data.disableTarget || 'cannot-hscroll';
        if(this._disableTarget.indexOf('.') === 0)
        {
            this._disableTarget = this._disableTarget.substring(1);
        }

        this._scrollOffset = data.scrollOffset === undefined ? 0.1 : data.scrollOffset;
        this._scrollSpeed = data.scrollSpeed === undefined ? 350 : data.scrollSpeed;
        this._scrollEase = data.scrollEase || 'cubicOut';
        this._scrollOverride = data.override === undefined ? true : data.override;
        this._rememberLastScroll = data.rememberLastScroll === undefined ? true : data.rememberLastScroll;
        this._ignoreScrollerWidth = data.ignoreScrollerWidth === undefined ? true : data.ignoreScrollerWidth;
        this._checkWindowSize = data.checkWindowSize === undefined ? true : data.checkWindowSize;
        this._needButton = data.needButton || false;
        this._enabledButtonClass = data.enabledButtonClass || 'enabled';
        this._buttonScrollPercent = data.buttonScrollPercent === undefined ? 0.5 : data.buttonScrollPercent;
        this._widthTolerance = data.widthTolerance === undefined ? 1 : data.widthTolerance;

        if(this._enabledButtonClass.indexOf('.') === 0)
        {
            this._enabledButtonClass = this._enabledButtonClass.substring(1);
        }

        this._scrollable = true;

        this._lastValue = 0;

        if(this._rememberLastScroll)
        {
            this._storeId = 'pt-hs-' + element.nodeName.toLowerCase() + '-' + (element.id ? element.id + '-' : '') + $element.attr('class').replace(/\s/g, '-');
            this._lastValue = parseInt(win.localStorage ? (win.localStorage.getItem(this._storeId) || 0) : 0) || 0;
        }
        
        this._wheelTarget = data.wheelTarget || '';
        this._preventDefaultScroll = data.preventDefaultScroll === undefined ? true : data.preventDefaultScroll;
        
        this._onWheelBeforeHandler = data.onWheelBefore || null;
        this._onWheelAfterHandler = data.onWheelAfter || null;

        this._disposed = false;

        this._scrollDistance = 0;

        this._allowCalls = ['getX', 'scroll', 'scrollTo', 'enableViewport', 'resize', 'dispose'];

        this._x = 0;
        this._scrollX = 0;
        this._scrollerX = 0;

        this._oldMouseX = 0;
        this._currentX = 0;

        this._enableMouseWheel = false;

        this._containerX = 0;
        this._width = 0;
        this._contentWidth = 0;
        this._offsetWidth = 0;
        this._stepOffset = 0;
        this._tweenValue = 0;
        this._notEnoughToScroll = false;

        this._stageWidth = 0;

        this._scrollRectWidth = 0;
    
        this._isRTL = ($('body').css('direction') || '').toLowerCase() == 'rtl';

        if(this._isRTL)
        {
            var definer = $('<div dir="rtl" style="font-size: 14px; width: 4px; height: 1px; position: absolute; top: -1000px; overflow: scroll">hs</div>').appendTo('body')[0];
            this._rtlType = 'reverse'; //IE

            if (definer.scrollLeft > 0)
            {
                this._rtlType = 'default'; //Webkit
            }
            else
            {
                definer.scrollLeft = 1;
                if (definer.scrollLeft === 0)
                {
                    this._rtlType = 'negative'; //Firefox/Opera
                }
            }

            $(definer).remove();
        }

        this._cssXProp = this._isRTL ? 'right' : 'left';
        this._isTweening = false;

        this._inviewClass = data.viewportAddClass || 'init-viewport';
        this._enableCheckViewport = data.enableViewport || false;

        if(this._inviewClass.indexOf('.') === 0)
        {
            this._inviewClass = this._inviewClass.substring(1);
        }

        this._isDragging = false;

        this._tweenMoveScroller = false;

        this._aniValue = 0;

        this._isIE9 = navigator.userAgent.indexOf("MSIE 9.0") > -1;

        this._onMouseWheelHandler = this._onMouseWheel.bind(this);

        if(this._wheelTarget)
        {
            $(this._wheelTarget).on('wheel', this._onMouseWheelHandler);
        }
        else
        {
            this.$element.on('wheel', this._onMouseWheelHandler);
        }

        var isDevice = /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);
        if(isDevice)
        {
            $content.css({
                'overflow-x': 'auto',
                'overflow-y': 'hidden'
            });
        }

        this.$prevBtn = null;
        this.$nextBtn = null;

        if(this._needButton)
        {
            var $prevBtn = $('<i class="pt-hs-roll-btn prev"></i>');
            var $nextBtn = $('<i class="pt-hs-roll-btn next"></i>');
            $element.append($prevBtn);
            $element.append($nextBtn);

            var isSupportTouch = 'ontouchend' in document ? true : false;
            var btnEvt = isSupportTouch ? 'touchend' : 'click';
            $prevBtn.on(btnEvt, function(){
                this.scroll(this._width * this._buttonScrollPercent, 0);
            }.bind(this));
            $nextBtn.on(btnEvt, function(){
                this.scroll(this._width * this._buttonScrollPercent, 1);
            }.bind(this));

            this.$prevBtn = $prevBtn;
            this.$nextBtn = $nextBtn;
        }

        this._onKeyDownHandler = this._onKeyDown.bind(this);
        $(win).on('keydown', this._onKeyDownHandler);

        this._onAniUpdateHandler = this._onAniUpdate.bind(this);
        this._onAniCompleteHandler = this._onAniComplete.bind(this);

        this._onResizeHandler = this._onResize.bind(this);
        $(win).on('resize', this._onResizeHandler);

        this._onStartDragHandler = this._onStartDrag.bind(this);
        this._onDraggingHandler = this._onDragging.bind(this);
        this._onStopDragHandler = this._onStopDrag.bind(this);

        this.$scroller.on('mousedown', this._onStartDragHandler);
        this.$scrollerHolder.on('mousedown', function(evt){
            if(evt.target === this.$scrollerHolder.get(0))
            {
                evt.preventDefault();
                evt.stopPropagation();

                var mX = evt.pageX === undefined ? evt.clientX : evt.pageX;
                var offsetX = mX - this.$scrollerHolder.offset().left;
                var percent = offsetX / this._scrollRectWidth;
                var x = this._offsetWidth * percent * -1;
                this.scrollTo(x, true);
            }
        }.bind(this));

        $content.on('scroll', this._onNativeScroll.bind(this));

        var isFreshed = document.referrer === win.location.href;

        this._onResize(null, this._lastValue === 0 || !isFreshed);

        this._onScrollHandler = data.onScroll || null;// the init resize should not call this handler.
        this._onScrollEndHandler = data.onScrollEnd || null;

        if(isFreshed && this._lastValue !== 0)
        {
            this.scrollTo(this._lastValue, false);
        }
    }

    var p = HScroller.prototype;

    p._onResize = function (evt, needRender)
    {
        if(needRender === undefined)
        {
            needRender = true;
        }

        this._containerX = this.$element.offset().left;
        this._width = this.$element.width();
        this._contentWidth = this.$wrap.get(0) ? this.$wrap.get(0).scrollWidth : 0;
        this._offsetWidth = this._contentWidth - this._width;
        this._stageWidth = $(win).width();

        this._scrollable = this._checkWindowSize ? (this._stageWidth >= $(win).height()) : true;

        this._scrollDistance = Math.floor(this._scrollOffset * this.$content.width());

        this._notEnoughToScroll = this._contentWidth <= this._width + this._widthTolerance;

        if(this._notEnoughToScroll)
        {
            this._enableMouseWheel = false;
            this.$element.addClass('no-scroll');
        }
        else
        {
            this._enableMouseWheel = true;
            this.$element.removeClass('no-scroll');
        }

        this._scrollRectWidth = parseInt(this.$scrollerHolder.css('width')) || this._width;

        if(!this._ignoreScrollerWidth)
        {
            this._scrollRectWidth -= this.$scroller.width();
        }

        if(this._notEnoughToScroll)
        {
            this.$scroller.hide();
        }
        else
        {
            this.$scroller.css('display', 'block');

            if(this._ignoreScrollerWidth)
            {
                var percent = this._scrollerX / this._scrollRectWidth * 100;
                this.$scroller.css(this._cssXProp, percent + '%');
            }
            else
            {
                this.$scroller.css(this._cssXProp, this._scrollerX + 'px');
            }
        }

        if(this._x + this._contentWidth < this._width)
        {
            this._x = this._width - this._contentWidth;
            if(this._x > 0)
            {
                this._x = 0;
            }
        }

        if(needRender || this._notEnoughToScroll)
        {
            this._render(true);
        }

        if(this._needButton)
        {
            this._updateButtonState(this._x);
        }

        $(win).trigger('h-resize', [{
            scrollX: this._content.scrollLeft, 
            containerX: this._containerX,
            contentWidth: this._contentWidth,
            containerWidth: this._width,
            offsetWidth: this._offsetWidth
        }]);
    }

    p._onKeyDown = function(evt)
    {
        if(!this._scrollable)
        {
            return;
        }

        switch(evt.keyCode)
        {
            case 37: // Left
            case 38: // Top
                this._toPrev(this._scrollDistance * 5);
                break;
            case 39: // Right
            case 40: // Bottom
                this._toNext(this._scrollDistance * 5);
                break;
            case 36: // Home
                this.scrollTo(0, true);
                break;
            case 35: // End
                this.scrollTo(- this._offsetWidth, true);
                break;
            case 33: // Page Up
                this.scrollTo(this._x + this._width, true);
                break;
            case 34: // Page Down
                this.scrollTo(this._x - this._width, true);
                break;
        }
    }

    p._onMouseWheel = function (e)
    {
        if(!this._enableMouseWheel) return;
        if(this._isDragging) return;

        if(!this._scrollable)
        {
            return;
        }

        var evt = e.originalEvent;
        
        if(evt.defaultPrevented || evt.ctrlKey) return;

        var delta;
        if(evt.deltaX !== undefined && evt.deltaX !== 0)
        {
            delta = evt.deltaX;
        }
        else
        {
            delta = evt.deltaY;
        }

        if(this._checkUnderScrollable(evt.target, delta < 0))
        {
            return;
        }

        if(this._preventDefaultScroll) evt.preventDefault();
        if(this._onWheelBeforeHandler) this._onWheelBeforeHandler();

        if(delta < 0)
        {
            this._toPrev(evt.deltaMode === 0 ? -delta : -delta * 60);
        }
        else if(delta > 0)
        {
            this._toNext(evt.deltaMode === 0 ? delta : delta * 60);
        }
        
        if(this._onWheelAfterHandler)
        {
            this._onWheelAfterHandler();
        }
    }

    p._checkUnderScrollable = function(target, toPrev)
    {
        var rootElement = document.body;
        var overflow, element = target;

        if(!$.contains(rootElement, target))
        {
            return false;
        }

        while(element && element !== rootElement)
        {
            if($(element).hasClass(this._disableTarget))
            {
                return true;
            }

            overflow = getComputedStyle(element, '').getPropertyValue('overflow-y');
            if(overflow === 'scroll' || overflow === 'auto')
            {
                if(element.clientHeight < element.scrollHeight)
                {
                    if((toPrev && element.scrollTop > 0) || (!toPrev && Math.round(element.scrollTop) < element.scrollHeight - element.clientHeight - 1))
                    {
                        return true;
                    }
                }
            }
            
            element = element.parentNode;
        }
        return false;
    }

    p._onStartDrag = function (evt)
    {
        evt.preventDefault();
        evt.stopPropagation();

        var $win = $(win);
        $win.off('mousemove', this._onDraggingHandler).on('mousemove', this._onDraggingHandler);
        $win.off('mouseup', this._onStopDragHandler).on('mouseup', this._onStopDragHandler);

        this._isDragging = true;

        this.$scroller.addClass('dragging');

        this._oldMouseX = evt.pageX === undefined ? evt.clientX : evt.pageX;

        this._currentX = this._scrollerX;
    }

    p._onDragging = function (evt)
    {
        evt.preventDefault();
        evt.stopPropagation();

        var mX = evt.pageX === undefined ? evt.clientX : evt.pageX;
        if(this._isRTL)
        {
            this._currentX -= mX - this._oldMouseX;
        }
        else
        {
            this._currentX += mX - this._oldMouseX;
        }
        this._oldMouseX = mX;

        if(this._currentX < 0)
        {
            this._currentX = 0;
        }
        else if(this._currentX > this._scrollRectWidth)
        {
            this._currentX = this._scrollRectWidth;
        }

        var percent = this._currentX / this._scrollRectWidth;

        this._scrollerX = this._currentX;

        if(this._ignoreScrollerWidth)
        {
            this.$scroller.css(this._cssXProp, (percent * 100) + '%');
        }
        else
        {
            this.$scroller.css(this._cssXProp, this._scrollerX + 'px');
        }

        var x = this._offsetWidth * percent * -1;
        this._scrollX = x;
        this._tweenX(x, false);
    }

    p._onStopDrag = function (evt)
    {
        evt.preventDefault();
        evt.stopPropagation();

        var $win = $(win);
        $win.off('mousemove', this._onDraggingHandler);
        $win.off('mouseup', this._onStopDragHandler);

        this._isDragging = false;
        this.$scroller.removeClass('dragging');
    }

    p._onNativeScroll = function(evt)
    {
        if(!this._scrollable)
        {
            return;
        }

        if(this._isTweening)
        {
            return;
        }
        
        var x;

        if(this._isRTL)
        {
            if(this._rtlType === 'reverse')
            {
                x = - this._content.scrollLeft;;//ie
            }
            else if(this._rtlType === 'negative')
            {
                x = this._content.scrollLeft;//firefox
            }
            else
            {
                x = this._content.scrollLeft - this._offsetWidth;//webkit
            }
        }
        else
        {
            x = -this._content.scrollLeft;
        }
        this._aniValue = this._x = x;
        this._render(true, true);
    }

    p._render = function (moveScroller, notSetScrollLeft)
    {
        this._checkViewport();

        if(!notSetScrollLeft)
        {
            if(this._isRTL)
            {
                if(this._rtlType === 'reverse')
                {
                    this._content.scrollLeft = - this._x;;//ie
                }
                else if(this._rtlType === 'negative')
                {
                    this._content.scrollLeft = this._x;//firefox
                }
                else
                {
                    this._content.scrollLeft = this._x + this._offsetWidth;//webkit
                }
            }
            else
            {
                this._content.scrollLeft = - this._x;
            }
        }

        var scrollerX = Math.round(-1 * this._x / this._offsetWidth * this._scrollRectWidth);
        var percent = scrollerX / this._scrollRectWidth;

        if(moveScroller)
        {
            this._updateScroller(Math.floor(scrollerX));
        }

        if(this._onScrollHandler)
        {
            this._onScrollHandler(percent);
        }

        $(win).trigger('h-scroll', [{
            scrollX: this._content.scrollLeft, 
            containerX: this._containerX,
            contentWidth: this._contentWidth,
            containerWidth: this._width,
            offsetWidth: this._offsetWidth
        }]);
    }

    p._checkViewport = function()
    {
        var len = this._items.length;
        if(this._enableCheckViewport && len > 0)
        {
            var $item, itemX;
            for(var i = len - 1; i >= 0; i --)
            {
                $item = this._items[i];
                itemX = $item.offset().left - this._containerX;
                if(itemX > -$item.width() && itemX < this._width)
                {
                    $item.addClass(this._inviewClass);
                    this._items.splice(i, 1);
                }
            }
        }
    }

    p._updateScroller = function(scrollerX)
    {
        if(scrollerX === undefined)
        {
            scrollerX = Math.floor(-1 * this._x / this._offsetWidth * this._scrollRectWidth);
        }

        var percent = scrollerX / this._scrollRectWidth;

        if(this._scrollerX !== scrollerX)
        {
            this._scrollerX = scrollerX;
            if(this._ignoreScrollerWidth)
            {
                this.$scroller.css(this._cssXProp, (percent * 100) + '%');
            }
            else
            {
                this.$scroller.css(this._cssXProp, this._scrollerX + 'px');
            }
        }
    }

    p._toPrev = function (scrollDistance)
    {
        var x;
        if(this._isTweening && this._scrollOverride)
        {
            x = this._scrollX + scrollDistance;
        }
        else
        {
            x = this._x + scrollDistance;
        }

        if(x > 0)
        {
            x = 0;
        }

        this._aniValue = this._x;
        this._scrollX = x;
        this._tweenX(x, true);
    }

    p._toNext = function (scrollDistance)
    {
        var x;
        if(this._isTweening && this._scrollOverride)
        {
            x = this._scrollX - scrollDistance;
        }
        else
        {
            x = this._x - scrollDistance;
        }

        if(x < - this._offsetWidth)
        {
            x = - this._offsetWidth;
        }

        this._aniValue = this._x;
        this._scrollX = x;
        this._tweenX(x, true);
    }

    p._tweenX = function (x, moveScroller)
    {
        if(this._x === x)
        {
            return;
        }

        this._tweenMoveScroller = moveScroller;
        this._isTweening = true;

        this._updateButtonState(x);

        createjs.Tween.get(
            this,
            {
                override:true,
                onChange:this._onAniUpdateHandler
            }
        ).to(
            {_aniValue:x},
            this._scrollSpeed,
            createjs.Ease[this._scrollEase] || createjs.Ease.cubicOut
        ).call(this._onAniCompleteHandler);
    }

    p._onAniUpdate = function (evt)
    {
        this._x = this._aniValue;
        this._render(this._tweenMoveScroller);
    }

    p._onAniComplete = function()
    {
        this._isTweening = false;

        if(this._onScrollEndHandler)
        {
            setTimeout(this._onScrollEndHandler, 0);
        }

        if(this._rememberLastScroll && win.localStorage)
        {
            win.localStorage.setItem(this._storeId, this._x.toString());
        }
    }

    p._updateButtonState = function(x)
    {
        if(x === undefined)
        {
            x = this._x;
        }

        var $prevBtn = this.$prevBtn, $nextBtn = this.$nextBtn, enabledButtonClass = this._enabledButtonClass;
        if($prevBtn && $nextBtn)
        {
            if(this._notEnoughToScroll)
            {
                $prevBtn.removeClass(enabledButtonClass);
                $nextBtn.removeClass(enabledButtonClass);
            }
            else if(x >= 0)
            {
                $prevBtn.removeClass(enabledButtonClass);
                $nextBtn.addClass(enabledButtonClass);
            }
            else if(x <= - this._offsetWidth)
            {
                $prevBtn.addClass(enabledButtonClass);
                $nextBtn.removeClass(enabledButtonClass);
            }
            else
            {
                $prevBtn.addClass(enabledButtonClass);
                $nextBtn.addClass(enabledButtonClass);
            }
        }
    }

    p.enableViewport = function(value)
    {
        this._enableCheckViewport = value;
    }

    p.resize = function()
    {
        this._onResize(null, true);
    }

    p.scrollTo = function(x, useTween)
    {
        if(!this._scrollable)
        {
            return;
        }

        if(useTween === undefined)
        {
            useTween = true;
        }

        if(x > 0)
        {
            x = 0;
        }

        if(x < - this._offsetWidth)
        {
            x = - this._offsetWidth;
        }

        this._scrollX = x;

        if(useTween)
        {
            this._tweenX(x, true);
        }
        else
        {
            this._x = x;

            if(this._isRTL)
            {
                if(this._rtlType === 'reverse')
                {
                    this._content.scrollLeft = - x;;//ie
                }
                else if(this._rtlType === 'negative')
                {
                    this._content.scrollLeft = x;//firefox
                }
                else
                {
                    this._content.scrollLeft = x + this._offsetWidth;//webkit
                }
            }
            else
            {
                this._content.scrollLeft = - x;
            }
        }
    }

    p.scroll = function(distance, direction)
    {
        if(!this._scrollable)
        {
            return;
        }

        if(distance === undefined)
        {
            distance = 0;
        }

        if(distance < 0)
        {
            distance = -distance;
        }

        if(direction === undefined)
        {
            direction = 1;
        }

        var x;
        if(direction > 0)
        {
            x = this._x - distance;
        }
        else
        {
            x = this._x + distance;
        }
        this.scrollTo(x);
    }

    p.getX = function()
    {
        return this._x;
    }

    p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(!Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) === 'function')
        {
            return f.apply(this, funcParams);
        }
    }

    p.dispose = function()
    {
        if(this._disposed) return;
        this._disposed = true;

        var $win = $(win);

        createjs.Tween.removeTweens(this);

        if(this._onResizeHandler)
        {
            $win.off('resize', this._onResizeHandler);
            this._onResizeHandler = null;
        }

        if(this._element)
        {
            this.$element.off('mouseenter mouseleave mousemove wheel');
            this._element.__ptHScroller = null;
            this._element = null;
        }

        if(this._needButton)
        {
            if(this.$prevBtn)
            {
                this.$prevBtn.remove();
                this.$prevBtn = null;
            }

            if(this.$nextBtn)
            {
                this.$nextBtn.remove();
                this.$nextBtn = null;
            }
        }
        
        if(this._wheelTarget)
        {
            $(this._wheelTarget).off('wheel', this._onMouseWheelHandler);
        }

        this._enableMouseWheel = false;

        this._onAniUpdateHandler = null;

        this._onMouseWheelHandler = null;
        this._onWheelBeforeHandler = null;
        this._onWheelAfterHandler = null;

        $win.off('keydown', this._onKeyDownHandler);
        this._onKeyDownHandler = null;

        this.$scroller.off('mousedown', this._onStartDragHandler);
        this.$scrollerHolder.off('mousedown');

        $win.off('mousemove', this._onDraggingHandler);
        $win.off('mouseup', this._onStopDragHandler);

        if(this.$content)
        {
            this.$content.off('scroll');
        }

        this._onStartDragHandler = null;
        this._onDraggingHandler = null;
        this._onStopDragHandler = null;

        this._onScrollHandler = null;
        this._onScrollEndHandler = null;
    }

    $.fn.extend({
        ptHScroller:function (obj) {
            this.each(function(){
                var scroller = this.__ptHScroller;
                scroller && scroller.dispose();
                new HScroller(this, obj);
            });
            return this;
        },
        ptHScrollerCall: function(funcName, funcParams){
            var arr = [];
            this.each(function(){
                var scroller = this.__ptHScroller;
                var returnValue = scroller && scroller.execute(funcName, funcParams);
                arr.push({target:this, value:returnValue});
            });
            return arr;
        },
        ptHScrollerDispose:function () {
            this.each(function(){
                var scroller = this.__ptHScroller;
                scroller && scroller.dispose();
            });
            return this;
        }
    });
}(jQuery, window));


/**
 * Created by pinetree on 2017/08/07.
 */
!(function($, win)
{
    "use strict";

    var FlexRow = function(element, options)
    {
        this._element = element;
        element.__ptFlexRow = this;

        this.$element = $(this._element);

        options = options || {};
        this._options = options;

        var $content;
        if(options.wrapTarget && typeof(options.wrapTarget) === 'object')
        {
            $content = $(options.wrapTarget);
        }
        else
        {
            $content = $(element).find(options.wrapClass || '.wrap');
        }

        this.$content = $content;

        var $items;
        if(options.itemTarget && typeof(options.itemTarget) === 'object')
        {
            $items = $(options.itemTarget);
        }
        else if(options.itemTarget)
        {
            $items = this.$content.find(options.itemTarget || '.item');
        }

        this.$items = $items;

        this._onResizeBefore = options.onResizeBefore || null;
        this._onResizeAfter = options.onResizeAfter || null;

        this._rows = options.rows || 2;

        this._needBR = options.needBR === undefined ? true : options.needBR;

        this._isIE9 = navigator.userAgent.indexOf("MSIE 9.0") > -1;

        this._isRTL = ($('body').css('direction') || '').toLowerCase() == 'rtl';

        this._useJustified = options.useJustified === undefined ? true : options.useJustified;

        if(this._isRTL)
        {
            this._useJustified = false;
        }

        this._alignType = options.alignType || 1;

        this._scrollPercent = 0;

        this._totalWidth = 0;

        this._viewWidth = 0;

        this._disposed = false;

        this.$itemContainers = [];
        var $itemContainer;
        for(var i = 0; i < this._rows; i ++)
        {
            $itemContainer = $('<div class="fr-item-container"></div>');
            this.$content.append($itemContainer);
            this.$itemContainers.push($itemContainer);

            if(this._needBR && i !== this._rows - 1)
            {
                $itemContainer.after('<br/>');
            }
        }

        this._itemContainerWidths = [];

        this._allowCalls = ['setScrollPercent', 'resize', 'dispose'];

        this._onResizeHandler = this._resize.bind(this);

        $(win).on('resize', this._onResizeHandler);

        if(options.onInit)
        {
            options.onInit();
        }
        
        this._resize();
    }

    var p = FlexRow.prototype;

    p._resize = function()
    {
    	this._viewWidth = this.$element.width();
    	
        if(this._onResizeBefore)
        {
            this._onResizeBefore();
        }

        var changed = this._align();

        if(this._useJustified)
        {
        	for(var i = 0; i < this._rows; i ++)
	        {
	            this._itemContainerWidths[i] = Math.round(this.$itemContainers[i].get(0).scrollWidth);
	        }

        	var shouldScroll = this.setScrollPercent(this._scrollPercent);

	        if(this._onResizeAfter)
	        {
	            this._onResizeAfter(changed || !shouldScroll);
	        }
        }
        else
        {
        	if(this._onResizeAfter)
	        {
	            this._onResizeAfter(changed);
	        }
        }
    }

    p._align = function(force)
    {
        var widths = [], w, totalW = 0;
        this.$items.each(function(){
            w = Math.floor($(this).width());
            totalW += w;
            widths.push(w);
        });

        if(!force && this._totalWidth === totalW)
        {
            return false;
        }

        this._totalWidth = totalW;

        var averageW = totalW / this._rows;
        var i, j, len = widths.length, rowWidth = 0, rowIndex = 0;
        var minRowWidth, maxRowWidth, rowsWidths = [];

        if(this._alignType === 1)
        {
        	for(i = 0; i < this._rows; i ++)
        	{
        		rowsWidths[i] = - this._rows + i;
        	}

        	for(i = 0; i < len; i ++)
	        {
	        	minRowWidth = Math.min.apply(null, rowsWidths);
	        	rowIndex = rowsWidths.indexOf(minRowWidth);
	        	rowsWidths[rowIndex] += widths[i];

	            this.$itemContainers[rowIndex].append(this.$items.get(i));
	        }
        }
        else
        {
        	var rightItems = [];
        	var sets = [];

        	for(i = 0; i < this._rows; i ++)
        	{
        		rowsWidths[i] = 0;
        		sets[i] = [];
        	}

        	for(i = 0; i < len; i ++)
	        {
	            rowWidth += widths[i];

	            rowsWidths[rowIndex] += widths[i];

	            sets[rowIndex].push(i);

	            if(rowWidth > averageW)
	            {
	                rowIndex ++;
	                if(rowIndex > this._rows - 1)
	                {
	                	rowIndex = this._rows - 1;
	                }
	                else
	                {
	                	rowWidth = 0;
	                }
	            }
	        }

	        if(this._rows > 1)
	        {
	        	var minRowIndex, maxRowIndex, offsetW, lastItemW, lastIndex;
		        for(i = 0; i < this._rows; i ++)
	        	{
	        		minRowWidth = Math.min.apply(null, rowsWidths);
    				minRowIndex = rowsWidths.indexOf(minRowWidth);

	        		maxRowWidth = Math.max.apply(null, rowsWidths);
    				maxRowIndex = rowsWidths.indexOf(maxRowWidth);

	        		offsetW = maxRowWidth - minRowWidth;

	        		lastIndex = sets[maxRowIndex].length - 1;
	        		lastIndex = sets[maxRowIndex][lastIndex];
	        		lastItemW = widths[lastIndex];

	        		if(lastItemW < offsetW)
	        		{
	        			sets[maxRowIndex].pop();
	        			sets[minRowIndex].unshift(lastIndex);

	        			rowsWidths[maxRowIndex] -= lastItemW;
	        			rowsWidths[minRowIndex] += lastItemW;
	        		}
	        		else
	        		{
	        			break;
	        		}
	        	}
	        }

	      	for(i = 0; i < this._rows; i ++)
	      	{
	      		sets[i].forEach(function(index){
	      			this.$itemContainers[i].append(this.$items.get(index));
	      		}, this);
	      	}
        }

        var arr = [];
        for(i = 0; i < this._rows; i ++)
        {
            this.$itemContainers[i].removeClass('longest');
            arr.push(this.$itemContainers[i].get(0).scrollWidth);
        }

        maxRowWidth = Math.max.apply(null, arr);
        maxRowIndex = arr.indexOf(maxRowWidth);

        this.$itemContainers[maxRowIndex].addClass('longest');

        return true;
    }

    p.setScrollPercent = function(percent)
    {
    	if(!this._useJustified)
    	{
    		return false;
    	}

    	this._scrollPercent = percent;

    	var rowsWidths = this._itemContainerWidths;

    	var excludes = [];
    	rowsWidths.forEach(function(w, i){
    		if(w <= this._viewWidth)
    		{
    			excludes.push(i);
    		}
    	}, this);

    	if(this._rows - excludes.length < 2)
    	{
	    	this.$itemContainers.forEach(function($itemContainer){
	    		this._renderX($itemContainer, 0);
	    	}, this);

	    	return false;
    	}

    	var maxRowWidth = Math.max.apply(null, rowsWidths);
    	var rowIndex = rowsWidths.indexOf(maxRowWidth);
    	var offset, addX;

    	for(var i = 0; i < this._rows; i ++)
    	{
    		if(i === rowIndex || excludes.indexOf(i) > -1)
    		{
    			this._renderX(this.$itemContainers[i], 0);
    			continue;
    		}

    		offset = maxRowWidth - rowsWidths[i];
    		addX = offset * percent;

    		this._renderX(this.$itemContainers[i], addX);
    	}

    	return true;
    }

    p._renderX = function($target, xValue)
    {        
    	if(this._isIE9)
        {
            $target.css('-ms-transform', 'translateX(' + xValue + 'px)');
        }
        else
        {
            $target.css('transform', 'translate3d(' + xValue + 'px,0,0)');
        }
    }

    p.resize = function()
    {
        this._align(true);
    }

    p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(!Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) === 'function')
        {
            f.apply(this, funcParams);
        }
    }

    p.dispose = function()
    {
    	if(this._disposed) return;
        this._disposed = true;

        this._onResizeBefore = null;
        this._onResizeAfter = null;

        $(win).off('resize', this._onResizeHandler);

        this._onResizeHandler = null;

        if(this._element)
        {
            this._element.__ptFlexRow = null;
            this._element = null;
        }
    }

    $.fn.extend({
        ptFlexRow:function (options) {
            this.each(function(){
                var fr = this.__ptFlexRow;
                fr && fr.dispose();
                new FlexRow(this, options);
            });
            return this;
        },
        ptFlexRowCall:function (funcName, funcParams) {
            this.each(function(){
                var fr = this.__ptFlexRow;
                fr && fr.execute(funcName, funcParams);
            });
            return this;
        }
    });
}(jQuery, window));


/**
 * Created by pinetree on 2016/11/16.
 */
!(function($, win)
{
    "use strict";

    var ptMediaPlayer = {
        playingPlayers : [],
        onStatusChanged : null
    };

    var onPTVimeoAPIReady = function ()
    {
        ptMediaPlayer.vimeoApiReady = true;

        if(ptMediaPlayer.vimeoWaitingPlayers)
        {
            var i = ptMediaPlayer.vimeoWaitingPlayers.length, player;
            while(i --)
            {
                player = ptMediaPlayer.vimeoWaitingPlayers[i];
                player && player.init();
            };
            ptMediaPlayer.vimeoWaitingPlayers.length = 0;
        }
    }

    // called by YouTube plugin
    win.onYouTubeIframeAPIReady = function ()
    {
        ptMediaPlayer.youtubeApiReady = true;

        if(ptMediaPlayer.youtubeWaitingPlayers)
        {
            var i = ptMediaPlayer.youtubeWaitingPlayers.length, player;
            while(i --)
            {
                player = ptMediaPlayer.youtubeWaitingPlayers[i];
                player && player.init();
            };
            ptMediaPlayer.youtubeWaitingPlayers.length = 0;
        }
    }

    var parseUrlParams = function(url)
    {
        var index = url.indexOf("?");
        if(index == -1) return null;

        var source = url.substring(index + 1);

        index = source.indexOf('#');
        if(index > -1)
        {
            source = source.substring(0, index);
        }

        source = source.replace(/&amp;|&amp%3b/ig, '&');

        var arr = source.split("&"), temp;
        var len = arr.length, obj = {};
        for(var i = 0; i < len; i ++)
        {
            temp = arr[i].split("=");
            if(temp.length > 1)
            {
                obj[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
            }
        }
        return obj;
    }

    var VimeoPlayer = function (container, src, options, callback, playerMgr)
    {
        this.type = 'vimeo';

        this._container = container;
        this._options = options;

        this._callback = callback;
        this._playerMgr = playerMgr;

        this._isInited = false;

        this._needPlay = false;
        this._needPause = false;
        this._needMute = false;

        var wh = this._options.full ? 'width:' + this._options.width + 'px;height:' + this._options.height + 'px;' : 'width:100%;height:100%;';
        var playerElement = '<div class="pt-vimeo-player" style="z-index:' + (options.controls ? 2 : 0) + ';' + wh + 'position:absolute;left:0;top:0;"></div>';
        $(this._container).append(playerElement);

        this._src = src;

        Object.defineProperty(this, 'src',{
            get: this.getSrc
        });
    }

    var vp = VimeoPlayer.prototype;
    
    vp.init = function ()
    {
        var volume = this._options.volume === undefined ? 1 : this._options.volume;

        if(this._options.autoplay && volume > 0)
        {
            volume = 0;
            this._needMute = true;
        }

        var options = {
            url: this._src,
            autopause: false,
            width: this._options.width,
            height: this._options.height,
            autoplay: this._options.autoplay ? true : false,
            background: this._options.controls ? false : true,
            loop: this._options.loop ? true : false,
            muted: volume === 0 ? true : false
        };

        this.player = new Vimeo.Player($(this._container).children('.pt-vimeo-player').get(0), options);
        this.player.setVolume(volume);
        this.player.on('loaded', this.onPlayerReady.bind(this));
        this.player.on('play', this._onPlay.bind(this));
        this.player.on('pause', this._onPause.bind(this));
        this.player.on('ended', this._onPlayEnd.bind(this));

        if(this._options.autoplay)
        {
            this._playerMgr.handlePlayer('play', this);
        }
    }

    vp.onPlayerReady = function()
    {
        $(this._container).animate({opacity:1}, 400);

        if(this._callback)
        {
            this._callback.call(this._playerMgr);
        }

        this._isInited = true;

        this._playerMgr.onStart();

        if(this._needPlay)
        {
            this._needPlay = false;
            this.play();
        }

        if(this._needPause)
        {
            this._needPause = false;
            this.pause()
        }

        if(this._needMute)
        {
            this._needMute = false;
            this.mute()
        }

        var self = this;
        this.player.getPaused().then(function(paused) {
            if(paused)
            {
                self._playerMgr.handlePlayer('pause', self);
            }
        }).catch(function(error) {
            // an error occurred
        });
    }

    vp.play = function ()
    {
        if(!this._isInited)
        {
            this._needPlay = true;
        }
        else
        {
            this.player.play();

            this._playerMgr.handlePlayer('play', this);
        }
    }

    vp.pause = function ()
    {
        if(!this._isInited)
        {
            this._needPause = true;
        }
        else
        {
            this.player.pause();

            this._playerMgr.handlePlayer('pause', this);
        }
    }

    vp.mute = function()
    {
        if(!this._options.volume)
        {
            return;
        }

        if(!this._isInited)
        {
            this._needMute = true;
        }
        else
        {
            this.player.setVolume(0);

            this._playerMgr.handlePlayer('mute', this);
        }
    }

    vp.unMute = function()
    {
        if(!this._options.volume)
        {
            return;
        }

        if(!this._isInited)
        {
            this._needMute = false;
        }
        else
        {
            this.player.setVolume(this._options.volume === undefined ? 1 : this._options.volume);

            this._playerMgr.handlePlayer('unMute', this);
        }
    }

    vp._onPlay = function (evt)
    {
        this._playerMgr.handlePlayer('play', this);
    }

    vp._onPause = function (evt)
    {
        this._playerMgr.handlePlayer('pause', this);
    }

    vp._onPlayEnd = function (evt)
    {
        this._playerMgr.onEnd();

        this._playerMgr.handlePlayer('end', this);
    }

    vp.getSrc = function()
    {
        return this._src;
    }

    vp.dispose = function ()
    {
        this.pause();

        this._callback = null;
        this._playerMgr = null;

        if(this.player)
        {
            this.player.unload();
            this.player = null;
        }
    }

    var YoutubePlayer = function (container, src, options, callback, playerMgr)
    {
        this.type = 'youtube';

        this._container = container;
        this._options = options;
        this.player = null;

        this._callback = callback;
        this._playerMgr = playerMgr;

        this._isInited = false;

        this._needPlay = false;
        this._needPause = false;
        this._needMute = false;

        var wh = this._options.full ? 'width:' + this._options.width + 'px;height:' + this._options.height + 'px;' : 'width:100%;height:100%;';
        var playerElement = '<div class="pt-youtube-player" style="z-index:' + (options.controls ? 2 : 0) + ';' + wh + 'position:absolute;left:0;top:0;"></div>';
        $(this._container).append(playerElement);

        var obj = parseUrlParams(src);
        if(obj)
        {
            if(obj['hd'])
            {
                this._options['hd'] = true;
            }
        }

        var split_c = "v=";
        var split_n = 1;
        if( src.match(/(youtube.com)/) )
        {
            split_c = "v=";
            split_n = 1;
        }
        if( src.match(/(youtu.be)/) )
        {
            split_c = "/";
            split_n = 3;
        }

        var id = src.split(split_c)[split_n];
        id = id.replace(/(&)+(.*)/, "");

        this._src = id;

        Object.defineProperty(this, 'src',{
            get: this.getSrc
        });
    }
    
    var yp = YoutubePlayer.prototype;

    yp.init = function ()
    {
        this.player = new YT.Player($(this._container).children('.pt-youtube-player').get(0), {
            videoId: this._src,
            width: this._options.width,
            height: this._options.height,
            playerVars: {
                'autohide': 1,
                'autoplay': 0,
                'disablekb': 0,
                'controls': this._options.controls ? 1 : 0,
                'enablejsapi': 1,
                'modestbranding': 1,
                'showinfo': 0,
                'rel': 0,
                'wmode': 'opaque',
                'hd': this._options.hd ? 1 : 0
            },
            events: {
                'onReady': this.onPlayerReady.bind(this),
                'onStateChange': this.onPlayerStateChange.bind(this)
            }
        });

        if(this._options.autoplay)
        {
            this._needPlay = true;

            this._playerMgr.handlePlayer('play', this);
        }
    }

    yp.onPlayerStateChange = function (evt)
    {
        switch (evt.data)
        {
            case -1:
                this._playerMgr.handlePlayer('pause', this);
                break;
            case 0:
                if(this._options.loop)
                {
                    this.player.playVideo();
                }
                else
                {
                    this._playerMgr.onEnd();

                    this._playerMgr.handlePlayer('end', this);
                }
                break;
            case 1:
                this._playerMgr.handlePlayer('play', this);
                break;
            case 2:
                this._playerMgr.handlePlayer('pause', this);
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
        }
    }

    yp.onPlayerReady = function (evt)
    {
        var volume = this._options.volume === undefined ? 1 : this._options.volume;

        if(this._options.autoplay)
        {
            this._needMute = true;
        }

        this.player.setVolume(Math.floor(volume * 100));

        $(this._container).animate({opacity:1}, 400);

        if(this._callback)
        {
            this._callback.call(this._playerMgr);
        }

        this._isInited = true;

        this._playerMgr.onStart();

        if(this._options.volume === 0)
        {
            this.player.mute();
        }

        if(this._needPlay)
        {
            this._needPlay = false;
            this.play();
        }

        if(this._needPause)
        {
            this._needPause = false;
            this.pause()
        }

        if(this._needMute)
        {
            this._needMute = false;
            this.mute();
        }
    }

    yp.play = function ()
    {
        if(!this._isInited)
        {
            this._needPlay = true;
        }
        else
        {
            this.player.playVideo();

            this._playerMgr.handlePlayer('play', this);
        }
    }

    yp.pause = function ()
    {
        if(!this._isInited)
        {
            this._needPause = true;
        }
        else
        {
            this.player.pauseVideo();

            this._playerMgr.handlePlayer('pause', this);
        }
    }

    yp.mute = function()
    {
        if(!this._options.volume)
        {
            return;
        }

        if(!this._isInited)
        {
            this._needMute = true;
        }
        else
        {
            this.player.mute();

            this._playerMgr.handlePlayer('mute', this);
        }
    }

    yp.unMute = function()
    {
        if(!this._options.volume)
        {
            return;
        }

        if(!this._isInited)
        {
            this._needMute = false;
        }
        else
        {
            this.player.unMute();

            this._playerMgr.handlePlayer('unMute', this);
        }
    }

    yp.getSrc = function()
    {
        return this._src;
    }

    yp.dispose = function ()
    {
        this.pause();

        this._callback = null;
        this._playerMgr = null;

        if(this.player)
        {
            this.player.destroy();
            this.player = null;
        }
    }

    var MP4Player = function (container, src, options, callback, playerMgr)
    {
        this.type = 'mp4';

        this._container = container;
        this._src = src;
        this._options = options;
        this._callback = callback;
        this._playerMgr = playerMgr;
        this._loop = options.loop;

        this.player = null;

        Object.defineProperty(this, 'src',{
            get: this.getSrc
        });
    }
    
    var mp = MP4Player.prototype;

    mp.init = function ()
    {
        var options = this._options;
        var $container = $(this._container);

        var volume = options.volume != undefined ? options.volume : 1;
        var autoplay = options.autoplay ? 'autoplay' : '';
        var controls = options.controls ? 'controls' : '';
        var w = options.width != undefined ? 'width="' + options.width + '"' : '';
        var h = options.height != undefined ? 'height="' + options.height + '"' : '';
        var loop = options.loop ? 'loop' : '';
        var muted = volume === 0 || autoplay ? 'muted' : '';
        var params = [autoplay, controls, w, h, loop, muted];
        var ele = '<video src="' + this._src + '" ' + params.join(' ') + ' style="z-index:' + (options.controls ? 2 : 0) + ';"></video>';
        $container.append(ele);

        this.player = $container.children('video').get(0);

        if(autoplay)
        {
            this.player.volume = 0;
            this._playerMgr.handlePlayer('mute', this);
        }
        else
        {
            this.player.volume = volume;
        }

        this.player.onplay = this._onPlay.bind(this);
        this.player.onpause = this._onPause.bind(this);
        this.player.onended = this._onPlayEnd.bind(this);

        $container.animate({opacity:1}, 400);

        if(this._callback)
        {
            this._callback.call(this._playerMgr);
        }

        this._playerMgr.onStart();

        if(options.autoplay && !this.player.paused)
        {
            this._playerMgr.handlePlayer('play', this);
        }
    }

    mp.play = function ()
    {
        if(this.player)
        {
            this.player.play();
        }

        this._playerMgr.handlePlayer('play', this);
    }

    mp.pause = function ()
    {
        if(this.player)
        {
            this.player.pause();
        }

        this._playerMgr.handlePlayer('pause', this);
    }

    mp.mute = function()
    {
        if(!this._options.volume)
        {
            return;
        }

        if(this.player)
        {
            this.player.muted = true;
        }

        this._playerMgr.handlePlayer('mute', this);
    }

    mp.unMute = function()
    {
        if(!this._options.volume)
        {
            return;
        }

        if(this.player)
        {
            this.player.muted = false;

            if(this.player.volume === 0)
            {
                this.player.volume = this._options.volume;
            }
        }

        this._playerMgr.handlePlayer('unMute', this);
    }

    mp._onPlay = function (evt)
    {
        this._playerMgr.handlePlayer('play', this);
    }

    mp._onPause = function (evt)
    {
        this._playerMgr.handlePlayer('pause', this);
    }

    mp._onPlayEnd = function (evt)
    {
        if(!this._loop)
        {
            this._playerMgr.onEnd();
        }

        this._playerMgr.handlePlayer('end', this);
    }

    mp.getSrc = function()
    {
        return this._src;
    }

    mp.dispose = function ()
    {
        this.pause();

        if(this.player)
        {
            this.player.onplay = null;
            this.player.onpause = null;
            this.player.onended = null;
            this.player = null;
        }

        this._callback = null;
        this._playerMgr = null;
    }

    /**
     *
     * @param element
     * @param data, {type=>[1:mp4,2:youtube,3:vimeo], src=>string, volume=>0-1, loop=>[0,1], autoplay=>[0,1], controls=>[0,1], mask=>[0,1], width=>px, height=>px}.
     * @constructor
     */
    var MediaPlayer = function(element, data)
    {
        this._element = element;
        element.style.overflow = 'hidden';
        element.__ptMediaPlayer = this;

        var options = {};
        this._options = options;

        for(var key in data)
        {
            if(data.hasOwnProperty(key))
            {
                options[key] = data[key];
            }
        }

        if(options.full === undefined)
        {
            options.full = true;
        }

        if(options.mask === undefined)
        {
            options.mask = true;
        }

        this._src = options.src;
        this._type = options.type;
        this._fullshow = options.full;
        this._onStartHandler = options.onStart || null;
        this._onEndHandler = options.onEnd || null;

        this.$volumeBtn = options.volumeBtn ? $(element).find(options.volumeBtn) : null;
        if(this.$volumeBtn && this.$volumeBtn.length === 0)
        {
            this.$volumeBtn = $(element).siblings(options.volumeBtn);
        }

        this._hasVolumeBtn = this.$volumeBtn && this.$volumeBtn.length > 0;

        this._playingClass = options.playingClass || 'pt-media-playing';

        var container = '<div class="pt-video-container" style="width:100%;height:100%;position:relative;z-index:999;"></div>';
        this._container = $(container).get(0);
        this.$container = $(this._container);
        element.appendChild(this._container);

        var wh = this._fullshow ? '' : 'width:100%;height:100%;';
        var holder = '<div class="pt-video-holder ' + (this._fullshow ? 'full' : 'not-full') + '" style="position:relative;opacity:0;' + wh + '"></div>';
        this._holder = $(holder).get(0);
        this.$holder = $(this._holder);
        this._container.appendChild(this._holder);

        if(options.mask)
        {
            var mask = '<div class="pt-video-mask" style="width:100%;height:100%;z-index:1;overflow:hidden;position:absolute;top:0;left:0;"></div>';
            this.$container.append(mask);
        }

        this._resizeHandler = this._onResize.bind(this);

        var $ele = $(element);
        
        if($ele.data('type') !== undefined)
        {
            this._type = $ele.data('type');
        }

        if($ele.data('src') !== undefined)
        {
            this._src = $ele.data('src');
        }

        if($ele.data('volume') !== undefined)
        {
            options.volume = $ele.data('volume');
        }

        if($ele.data('loop') !== undefined)
        {
            options.loop = $ele.data('loop');
        }

        if($ele.data('autoplay') !== undefined)
        {
            options.autoplay = $ele.data('autoplay');
        }

        if($ele.data('controls') !== undefined)
        {
            options.controls = $ele.data('controls');
        }

        if(options.volume === 0)
        {
            this._hasVolumeBtn = false;

            if(this.$volumeBtn && this.$volumeBtn.length > 0)
            {
                this.$volumeBtn.remove();
            }
        }

        if(this._hasVolumeBtn)
        {
            this.$volumeBtn.on('click', this._onToggleVolume.bind(this));
        }

        var obj = parseUrlParams(this._src);
        if(obj)
        {
            obj.width = obj.width || obj.w || obj.mw;
            obj.height = obj.height || obj.h || obj.mh;

            if(obj.width !== undefined)
            {
                options.width = obj.width;
            }
            if(obj.height !== undefined)
            {
                options.height = obj.height;
            }
        }

        if(options.width === undefined)
        {
            options.width = 640;
        }
        if(options.height === undefined)
        {
            options.height = 360;
        }
        
        options.width = parseInt(options.width);
        options.height = parseInt(options.height);

        this.$container.data('videoWidth', options.width);
        this.$container.data('videoHeight', options.height);

        this.player = null;

        this._vimeoJSId = '__pt_vimeo_api';
        this._youtubeJSId = '__pt_youtube_api';

        this._allowCalls = ['dispose', 'play', 'pause', 'mute', 'unMute'];

        this._isMobile =  /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);
        if(this._isMobile)
        {
            options.mask = false;
        }

        if((options.ignoreMobile || window.mpIgnoreMobile) && this._isMobile)
        {
            //ignore mobile
        }
        else if(this._src)
        {
            this.init();
        }
    }

    var p = MediaPlayer.prototype;

    /**
     * 1:mp4, 2:youtube, 3:vimeo
     */
    p.init = function ()
    {
        var options = this._options;

        switch (this._type)
        {
            case 'mp4':
            case '1':
            case 1:
                this._type = 'mp4';
                this.player = new MP4Player(this._holder, this._src, options, this._onResize, this);
                this.player.init();
                break;
            case 'youtube':
            case '2':
            case 2:
                this._type = 'youtube';
                this.player = new YoutubePlayer(this._holder, this._src, options, this._onResize, this);
                if(ptMediaPlayer.youtubeApiReady)
                {
                    this.player.init();
                }
                else
                {
                    if(!ptMediaPlayer.youtubeWaitingPlayers)
                    {
                        ptMediaPlayer.youtubeWaitingPlayers = [];
                    }
                    ptMediaPlayer.youtubeWaitingPlayers.push(this.player);

                    if(!document.getElementById(this._youtubeJSId))
                    {
                        this._insertJS('https://www.youtube.com/iframe_api', null, this._youtubeJSId);
                    }
                }
                break;
            case 'vimeo':
            case '3':
            case 3:
                this._type = 'vimeo';
                this.player = new VimeoPlayer(this._holder, this._src, options, this._onResize, this);
                if(ptMediaPlayer.vimeoApiReady)
                {
                    this.player.init();
                }
                else
                {
                    if(!ptMediaPlayer.vimeoWaitingPlayers)
                    {
                        ptMediaPlayer.vimeoWaitingPlayers = [];
                    }
                    ptMediaPlayer.vimeoWaitingPlayers.push(this.player);

                    if(!document.getElementById(this._vimeoJSId))
                    {
                        this._insertJS('https://player.vimeo.com/api/player.js', onPTVimeoAPIReady, this._vimeoJSId);
                    }
                }
                break;
            default:
                this._type = 'mp4';
                this.player = new MP4Player(this._holder, this._src, options, this._onResize, this);
                break;
        }

        if(this._fullshow)
        {
            $(win).on('resize', this._resizeHandler);
        }
    }

    p.onStart = function ()
    {
        if(this._onStartHandler)
        {
            this._onStartHandler(this._element, this.player);
        }
    }

    p.onEnd = function ()
    {
        if(this._onEndHandler)
        {
            this._onEndHandler(this._element, this.player);
        }
    }

    p.handlePlayer = function (type, player)
    {
        var players = ptMediaPlayer.playingPlayers;

        switch (type)
        {
            case 'play':
                if(players.indexOf(player) === -1)
                {
                    players.push(player);

                    if(ptMediaPlayer.onStatusChanged)
			        {
			        	ptMediaPlayer.onStatusChanged(true, type);
			        }
                }
                $('body').addClass(this._playingClass);
                break;

            case 'pause':
            case 'end':
                var index = players.indexOf(player);
                if(index > -1)
                {
                    players.splice(index, 1);

                    if(ptMediaPlayer.onStatusChanged)
			        {
			        	ptMediaPlayer.onStatusChanged(players.length > 0, type);
			        }
                }

                if(players.length <= 0)
                {
                    $('body').removeClass(this._playingClass);
                }
                break;

            case 'mute':
                $(this._element).addClass('pt-mediaplayer-muted');
                break;

            case 'unMute':
                $(this._element).removeClass('pt-mediaplayer-muted');
                break;
        }
    }

    p._onToggleVolume = function(evt)
    {
        if($(this._element).hasClass('pt-mediaplayer-muted'))
        {
            this.unMute();
        }
        else
        {
            this.mute();
        }
    }

    p._onResize = function (evt)
    {
        if(!this._fullshow)
        {
            return;
        }

        var $container = this.$container;
        var $holder = this.$holder;
        var vw = $container.data('videoWidth');
        var vh = $container.data('videoHeight');
        var sw = this._container.clientWidth;
        var sh = this._container.clientHeight;
        var scale = Math.max(sw / (vw - 5), sh / (vh - 5));
        var x, y, w, h;

        if(this._type == 'mp4')
        {
            w = vw * scale;
            h = vh * scale;
            x = ((sw - w) >> 1) - 1;
            y = ((sh - h) >> 1) - 1;

            $holder.css({
                left: x + 'px',
                top: y + 'px',
                width: (w + 2) + 'px',
                height: (h + 2) + 'px'
            });
        }
        else
        {
            var $iframe = $holder.find('iframe');

            w = vw * scale + 2;
            h = vh * scale + 2;

            x = ((sw - w) >> 1) - 1;
            y = ((sh - h) >> 1) - 1;

            $iframe.css({
                width: w + 'px',
                height: h + 'px'
            });

            $holder.css({
                left: x + 'px',
                top: y + 'px'
            });
        }
    }

    p._insertJS = function(src, callback, id)
    {
        var tag = document.createElement('script');

        if (callback)
        {
            if (tag.readyState)
            {
                tag.onreadystatechange = function(){
                    if (tag.readyState === "loaded" ||
                        tag.readyState === "complete"){
                        tag.onreadystatechange = null;
                        callback();
                    }
                };
            }
            else
            {
                tag.onload = function() {
                    callback();
                };
            }
        }

        tag.src = src;
        if(id)
        {
            tag.id = id;
        }

        var firstScriptTag = document.getElementsByTagName('script')[0];
        if(firstScriptTag)
        {
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        }
        else
        {
            var headTag = document.getElementsByTagName('head')[0];
            headTag.appendChild(tag);
        }
    };

    p.play = function()
    {
        this.player.play();
    };

    p.pause = function()
    {
        this.player.pause();
    };

    p.mute = function()
    {
        this.player.mute();
    }

    p.unMute = function()
    {
        this.player.unMute();
    }

    p.execute = function(funcName, funcParams)
    {
        if(!this.player || !funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(funcParams && !Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) == 'function')
        {
            f.apply(this, funcParams || []);
        }
    };

    p.dispose = function(needRemoveContainer)
    {
        if(this.player)
        {
            var index;
            if(ptMediaPlayer.vimeoWaitingPlayers)
            {
                index = ptMediaPlayer.vimeoWaitingPlayers.indexOf(this.player);
                if(index > -1)
                {
                    ptMediaPlayer.vimeoWaitingPlayers.splice(index, 1);
                }
            }

            if(ptMediaPlayer.vimeoWaitingPlayers)
            {
                index = ptMediaPlayer.vimeoWaitingPlayers.indexOf(this.player);
                if(index > -1)
                {
                    ptMediaPlayer.vimeoWaitingPlayers.splice(index, 1);
                }
            }

            this.player.dispose();
            this.player = null;
        }

        if(this.$volumeBtn && this.$volumeBtn.length > 0)
        {
            this.$volumeBtn.off('click');
        }

        if(needRemoveContainer)
        {
            this.$container.remove();
        }

        if(this._resizeHandler)
        {
            $(win).off('resize', this._resizeHandler);
            this._resizeHandler = null;
        }

        if(this._element)
        {
            this._element.__ptMediaPlayer = null;
            this._element = null;
        }
    };

    $.fn.extend({
        ptMediaPlayer:function (options) {
            this.each(function(){
                var mp = this.__ptMediaPlayer;
                mp && mp.dispose();
                new MediaPlayer(this, options);
            });
            return this;
        },
        ptMediaPlayerCall:function (funcName, funcParams) {
            this.each(function(){
                var mp = this.__ptMediaPlayer;
                mp && mp.execute(funcName, funcParams);
            });
            return this;
        }
    });
}(jQuery, window));


/**
 * Created by foreverpinetree@gmail.com on 2017/06/25.
 */
!(function($, win)
{
    "use strict";

    var __ars = [];

    var AreaRoll = function(ele, obj)
    {
        __ars.push(this);

        this._element = ele;
        this.$element = $(ele);

        ele.__ptAreaRoll = this;

        createjs.Ticker.timingMode = createjs.Ticker.RAF;

        this._moveRatio = obj.moveRatio == undefined ? 0.8 : obj.moveRatio;
        this._animateTime = obj.animateTime == undefined ? 500 : obj.animateTime;
        this._easing = obj.easing == undefined ? 'quartOut' : obj.easing;
        this._useWheel = obj.useWheel == undefined ? true : obj.useWheel;

        this._winWidth = 0;

        this._targetHeight = 0;
        this._eleHeight = 0;
        this._moveDistance = 0;
        this._y = 0;
        this._tweenValue = 0;

        this._allowCalls = ['dispose', 'resize'];

        this._isDevice = /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);

        if(this._isDevice)
        {
            this._useWheel = false;

            this.$element.addClass('pt-area-roll-device');
        }

        this._clickEvt = this._isDevice ? 'touchend mouseup' : 'click';

        this._onResizeHandler = this._onResize.bind(this);
        this._onWheelHandler = this._onWheel.bind(this);
        this._onTweenUpdateHandler = this._onTweenUpdate.bind(this);

        this.$target = null;
        this.$wrap = null;
        this.$prevBtn = null;
        this.$nextBtn = null;

        this._init();
    }

    var p = AreaRoll.prototype;

    p._init = function()
    {
        var $ele = this.$element;
        $ele.wrapInner('<div class="pt-area-roll-inner"></div>');
        $ele.wrap('<div class="pt-area-roll-wrap"></div>');
        $ele.on('scroll', function(evt){
            var scrollTop = $ele.scrollTop();
            this._y = -scrollTop;
            this._updateButtonState(this._y);
        }.bind(this));

        this.$target = $ele.children('.pt-area-roll-inner');

        var $wrap = $ele.parent();
        this.$wrap = $wrap;

        var html = '<div class="pt-area-roll-up pt-area-roll-btn"></div>';
        html += '<div class="pt-area-roll-down pt-area-roll-btn"></div>';
        $wrap.append(html);

        this.$prevBtn = $wrap.children('.pt-area-roll-up');
        this.$nextBtn = $wrap.children('.pt-area-roll-down');

        this.$prevBtn.on(this._clickEvt, this._onPrev.bind(this));
        this.$nextBtn.on(this._clickEvt, this._onNext.bind(this));

        $(win).on('resize', this._onResizeHandler);
        this._onResize();
    }

    p._onResize = function(evt)
    {
        var sw = $(win).width();
        if(evt)
        {
            if(this._winWidth === sw)
            {
                return;
            }
        }

        this._winWidth = sw;

        this._targetHeight = this.$target.height();
        this._eleHeight = $(this._element).height();
        this._moveDistance = Math.ceil(this._eleHeight * this._moveRatio);

        if(this._useWheel)
        {
            if(this._targetHeight > this._eleHeight)
            {
                this._element.addEventListener('wheel', this._onWheelHandler, true);
            }
            else
            {
                this._element.removeEventListener('wheel', this._onWheelHandler, true);
            }
        }

        if(this._y >= 0)
        {
            this._y = 0;
        }

        if(this._y <= this._eleHeight - this._targetHeight)
        {
            this._y = this._eleHeight - this._targetHeight;
        }

        this._render(false);
    }

    p.resize = function()
    {
        this._onResize();
    }

    p._onWheel = function(evt)
    {
        if(this._eleHeight >= this._targetHeight)
        {
            return;
        }

        if(evt.deltaY < 0)
        {
            if(this._y >= 0)
            {
                return;
            }

            evt.preventDefault();
            this._onPrev();
        }
        else if(evt.deltaY > 0)
        {
            if(this._y <= this._eleHeight - this._targetHeight)
            {
                return;
            }

            evt.preventDefault();
            this._onNext();
        }
    }

    p._onPrev = function(evt)
    {
        if(evt)
        {
            evt.preventDefault();
            evt.stopPropagation();
        }

        if(this._eleHeight >= this._targetHeight)
        {
            return;
        }

        if(this._y >= 0)
        {
            return;
        }

        this._y += this._moveDistance;

        if(this._y >= 0)
        {
            this._y = 0;
        }

        this._render(true);
    }

    p._onNext = function(evt)
    {
        if(evt)
        {
            evt.preventDefault();
            evt.stopPropagation();
        }

        if(this._eleHeight >= this._targetHeight)
        {
            return;
        }

        if(this._y <= this._eleHeight - this._targetHeight)
        {
            return;
        }

        this._y -= this._moveDistance;

        if(this._y <= this._eleHeight - this._targetHeight)
        {
            this._y = this._eleHeight - this._targetHeight;
        }

        this._render(true);
    }

    p._render = function(tween)
    {
        if(this._isDevice)
        {
            if(tween)
            {
                this._tweenValue = this.$element.scrollTop();
                createjs.Tween.get(
                    this,
                    {
                        override:true,
                        onChange:this._onTweenUpdateHandler
                    }
                ).to(
                    {_tweenValue: -this._y },
                    this._animateTime,
                    createjs.Ease[this._easing] || createjs.Ease.cubicOut
                );
            }
            else
            {
                this.$element.scrollTop(-this._y);
            }
        }
        else
        {
            if(tween)
            {
                this._tweenValue = parseInt(this.$target.css('top'));
                createjs.Tween.get(
                    this,
                    {
                        override:true,
                        onChange:this._onTweenUpdateHandler
                    }
                ).to(
                    {_tweenValue: this._y },
                    this._animateTime,
                    createjs.Ease[this._easing] || createjs.Ease.cubicOut
                );
            }
            else
            {
                this.$target.css('top', this._y + 'px');
            }
        }

        this._updateButtonState(this._y);
    }

    p._onTweenUpdate = function(evt)
    {
        if(this._isDevice)
        {
            this.$element.scrollTop(this._tweenValue);
        }
        else
        {
            this.$target.css('top', this._tweenValue + 'px');
        }
    }

    p._updateButtonState = function(y)
    {
        if(this._eleHeight >= this._targetHeight)
        {
            this.$prevBtn.addClass('disabled');
            this.$nextBtn.addClass('disabled');

            this.$wrap.addClass('no-roll');
        }
        else if(y >= 0)
        {
            this.$prevBtn.addClass('disabled');
            this.$nextBtn.removeClass('disabled');

            this.$wrap.removeClass('no-roll');
        }
        else if(y <= this._eleHeight - this._targetHeight)
        {
            this.$prevBtn.removeClass('disabled');
            this.$nextBtn.addClass('disabled');

            this.$wrap.removeClass('no-roll');
        }
        else
        {
            this.$prevBtn.removeClass('disabled');
            this.$nextBtn.removeClass('disabled');
            
            this.$wrap.removeClass('no-roll');
        }
    }

    p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(funcParams && !Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) == 'function')
        {
            f.apply(this, funcParams || []);
        }
    };

    p.dispose = function()
    {
        this.$prevBtn && this.$prevBtn.off(this._clickEvt);
        this.$nextBtn && this.$nextBtn.off(this._clickEvt);
        this.$target && this.$target.stop();

        if(this._element)
        {
            if(this._useWheel)
            {
                this._element.removeEventListener('wheel', this._onWheelHandler, true);
            }

            this._element.__ptAreaRoll = null;
            this._element = null;
        }

        createjs.Tween.removeTweens(this);

        this._onWheelHandler = null;
        this._onTweenUpdateHandler = null;

        var index = __ars.indexOf(this);
        if(index > -1)
        {
            __ars.splice(index, 1);
        }
    }

    $.fn.extend({
        ptAreaRoll:function (options) {
            this.each(function(){
                var ar = this.__ptAreaRoll;
                ar && ar.dispose();
                new AreaRoll(this, options || {});
            });
            return this;
        },
        ptAreaRollCall:function (funcName, funcParams) {
            this.each(function(){
                var ar = this.__ptAreaRoll;
                ar && ar.execute(funcName, funcParams);
            });
            return this;
        }
    });

    win.addEventListener('load', function(evt){
        __ars.forEach(function(ar){
            ar && ar.resize();
        });
    });
}(jQuery, window));


/**
 * Created by foreverpinetree@gmail.com on 2017/08/12.
 * Site: http://www.3theme.com
 */
!(function($, win)
{
    "use strict";

    var Swipe = function(element, options)
    {
        this._element = element;
        element.__ptSwipe = this;

        this.$element = $(this._element);

        options = options || {};
        this._options = options;

        this._ease = options.ease || 'cubicOut';
        this._dragEase = options.dragEase || 'quartOut';
        this._speed = options.speed === undefined ? 350 : options.speed;
        this._tweenOutOffset = options.tweenOutOffset || 0;

        this._autoplay = options.autoplay === undefined ? true : options.autoplay;

        this._isPaused = false;

        this._prevMovingClass = 'prev-moving';
        this._nextMovingClass = 'next-moving';

        this._onSwipeBefore = options.onSwipeBefore || null;
        this._onSwipeAfter = options.onSwipeAfter || null;

        this._duration = this.$element.data('duration') || 5000;

        createjs.Ticker.timingMode = createjs.Ticker.RAF;

        this._isIE9 = navigator.userAgent.indexOf("MSIE 9.0") > -1;

        var $content = null;
        if(options.wrapTarget && typeof(options.wrapTarget) === 'object')
        {
            $content = $(options.wrapTarget);
        }
        else
        {
            $content = this.$element.find(options.wrapTarget || '.wrap');
        }

        this.$content = $content;

        var $items = null;
        if(options.itemTarget && typeof(options.itemTarget) === 'object')
        {
            $items = $(options.itemTarget);
        }
        else if(options.itemTarget)
        {
            $items = this.$content.find(options.itemTarget);
        }

        this._itemClass = typeof(options.itemTarget) === 'string' ? options.itemTarget : '';
        if(this._itemClass.indexOf('.') === 0)
        {
            this._itemClass = this._itemClass.substring(1);
        }

        this.$items = $items;
        $items.css({
            'position': 'absolute',
            'top': '0',
        });

        $items.each(function(i, item){
            $(item).css('z-index', i).attr('data-index', i);
        });

        var prevLabel = this.$element.data('prevLabel') || '';
        var nextLabel = this.$element.data('nextLabel') || '';
        var len = $items.length;
        
        var html = '<div class="pt-swipe-arrow">';
        html += '<span class="pt-swipe-prev">' + prevLabel + '</span><span class="pt-swipe-next">' + nextLabel + '</span></div>';
        html += '<div class="pt-swipe-dots"><ul>';
        for(var i = 0; i < len; i ++)
        {
            html += '<li>' + (i + 1) + '</li>';
        }
        html += '</ul>';

        this.$element.append(html);

        this.$element.find('.pt-swipe-prev').on('click', this._onPrev.bind(this));
        this.$element.find('.pt-swipe-next').on('click', this._onNext.bind(this));

        this.$dotList = this.$element.find('.pt-swipe-dots>ul>li');
        this.$dotList.on('click', this._onClickDot.bind(this));

        this._disposed = false;

        this._isTweening = false;
        this._isDragging = false;

        this._dargScale = 0.3;
        this._dragDistance = 50;

        this._timerId = -1;

        this._allowCalls = ['setAutoplay', 'prev', 'next', 'pause', 'resume', 'dispose'];

        this._currentIndex = 0;
        this._totalCount = len;

        this._onResizeHandler = this._onResize.bind(this);

        $(win).on('resize', this._onResizeHandler);

        this._isDevice = /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);

        this._clickEvt = this._isDevice ? 'touchend mouseup' : 'click';
        this._downEvt = 'touchstart mousedown';
        this._moveEvt = 'touchmove mousemove';
        this._upEvt = 'touchend mouseup';

        this._onDraggingHandler = this._onDragging.bind(this);
        this._onDragEndHandler = this._onDragEnd.bind(this);

        $items.on(this._downEvt, this._onStartDrag.bind(this));

        this._onResize();

        if(this._totalCount > 0)
        {
            this._open(0);
        }

        if(options.onInit && typeof(options.onInit) === 'function')
        {
            options.onInit.apply(this, null);
        }
    }

    var p = Swipe.prototype;

    p._onResize = function(evt)
    {
        this._width = this.$content.width();
    }

    p._onStartDrag = function (event)
    {
        if(!this._isDevice && this._itemClass && !$(event.target).hasClass(this._itemClass))
        {
            return;
        }

        if(this._isTweening || this._isDragging)
        {
            return;
        }

        this._isDragging = true;

        if(this._timerId > -1)
        {
            clearTimeout(this._timerId);
        }

        var evt = event.originalEvent;

        var target = evt.currentTarget;
        this._dragTarget = target;

        $(win).off(this._moveEvt, this._onDraggingHandler).on(this._moveEvt, this._onDraggingHandler);
        $(win).off(this._upEvt, this._onDragEndHandler).on(this._upEvt, this._onDragEndHandler);

        this._oldMouseX = this._getMouseX(evt);
        this._currentX = 0;
        this._startDragX = this._currentX;
    }

    p._onDragging = function (event)
    {
        var evt = event.originalEvent;

        var x = this._getMouseX(evt);
        this._currentX += x - this._oldMouseX;
        this._oldMouseX = x;

        this._renderX($(this._dragTarget), this._currentX * this._dargScale);

        var comingIndex, leavingX;
        if(this._currentX > 0) //prev
        {
            comingIndex = this._currentIndex - 1;
            if(comingIndex < 0)
            {
                comingIndex = this._totalCount - 1;
            }
            leavingX = - this._width + this._currentX * this._dargScale;
        }
        else if(this._currentX < 0) //next
        {
            comingIndex = this._currentIndex + 1;
            if(comingIndex > this._totalCount - 1)
            {
                comingIndex = 0;
            }
            leavingX = this._width + this._currentX * this._dargScale;
        }

        this.$items.removeClass('leaving');
        var comingItem = this.$items.get(comingIndex);
        $(comingItem).addClass('leaving');
        this._renderX($(comingItem), leavingX);
    }

    p._onDragEnd = function (event)
    {
        $(win).off(this._moveEvt, this._onDraggingHandler);
        $(win).off(this._upEvt, this._onDragEndHandler);

        if(this._dragTarget)
        {
            this._dragTarget.__tweenX = this._currentX * this._dargScale;
        }

        var distance = this._currentX - this._startDragX;

        if(distance < - this._dragDistance)
        {
            this._onNext();
            this._isDragging = false;
        }
        else if(distance > this._dragDistance)
        {
            
            this._onPrev();
            this._isDragging = false;
        }
        else
        {
            this._dragBack();
        }

        this._dragTarget = null;
    }

    p._getMouseX = function(evt)
    {
        var x;
        if(this._isDevice)
        {
            var touches = evt.changedTouches, touch;
            touch = touches[0];
            if(touch)
            {
                x = touch.pageX;
            }
            else
            {
                x = evt.pageX !== undefined ? evt.pageX : evt.clientX;
            }
        }
        else
        {
            x = evt.pageX !== undefined ? evt.pageX : evt.clientX;
        }
        return x;
    }

    p._dragBack = function ()
    {
        createjs.Tween.get(
            this._dragTarget,
            {
                override:true,
                onChange:this._onUpdate.bind(this)
            }
        ).to(
            {__tweenX:0},
            300,
            createjs.Ease.sineOut
        ).call(this._onDragBack.bind(this));
    }

    p._onUpdate = function (evt)
    {
        var tween = evt.target;
        var target = tween.target;
        
        this._renderX($(target), target.__tweenX);
    }

    p._onDragBack = function ()
    {
        this._isDragging = false;
        this._checkAutoplay();
    }

    p._open = function(index, direction)
    {
        if(this._isTweening)
        {
            return;
        }

        this.$items.removeClass('current leaving');

        this.$dotList.removeClass('selected');
        $(this.$dotList.get(index)).addClass('selected');

        var currentItem = this.$items.get(index);
        $(currentItem).addClass('current');

        if(index === this._currentIndex)
        {
            var $item = $(currentItem);
            this._bringToTop(currentItem);
            this._renderX($item, 0);

            this._checkAutoplay();

            return;
        }
        var prevIndex = this._currentIndex;
        this._currentIndex = index;

        var prevItem = this.$items.get(prevIndex);
        $(prevItem).addClass('leaving');

        if(direction === undefined)
        {
            direction = index > prevIndex ? 1 : -1;
        }

        this._isTweening = true;

        var movingClass = direction < 0 ? this._prevMovingClass : this._nextMovingClass;
        this.$element.removeClass(this._prevMovingClass).removeClass(this._nextMovingClass).addClass(movingClass);

        if(this._onSwipeBefore)
        {
            this._onSwipeBefore.call(this, null);
        }

        this._tweenOut(prevItem, direction);
        this._tweenIn(currentItem, direction);
    }

    p._tweenIn = function(item, direction)
    {
        var x = direction > 0 ? this._width : - this._width;

        if(this._isDragging)
        {
            x = item.__renderingX || 0;
        }

        this._renderX($(item), x);

        item.__tweenValue = x;

        this._bringToTop(item);

        this._tweenX(item, 0, true);
    }

    p._tweenOut = function(item, direction)
    {
        var x = direction > 0 ? - this._width : this._width;
        var startX = item.__renderingX || 0;

        item.__tweenValue = this._isDragging ? startX : 0;
        
        this._bringToTop(item);

        if(direction > 0)
        {
            x += this._tweenOutOffset * this._width;
        }
        else
        {
            x -= this._tweenOutOffset * this._width;
        }

        this._tweenX(item, this._isDragging ? x + startX : x, false);
    }

    p._tweenX = function (item, x, listenComplete)
    {
        var tween = createjs.Tween.get(
            item,
            {
                override:true,
                onChange:this._onAniUpdate.bind(this)
            }
        );

        tween.to(
            {__tweenValue:x},
            this._speed,
            this._isDragging ? (createjs.Ease[this._dragEase] || createjs.Ease.quartOut) : (createjs.Ease[this._ease] || createjs.Ease.cubicOut)
        );

        if(listenComplete)
        {
            tween.call(this._onAniComplete.bind(this));
        }
    }

    p._onAniUpdate = function (evt)
    {
        var tween = evt.target;
        var target = tween.target;
        this._renderX($(target), target.__tweenValue);
    }

    p._onAniComplete = function()
    {
        this._isTweening = false;
        this.$element.removeClass(this._prevMovingClass).removeClass(this._nextMovingClass);
        this.$items.removeClass('leaving');

        if(this._onSwipeAfter)
        {
            this._onSwipeAfter.call(this, null);
        }

        this._checkAutoplay();
    }

    p._renderX = function($target, xValue)
    {
        if(this._isDragging && $target.get(0))
        {
            $target.get(0).__renderingX = xValue;
        }

        if(this._isIE9)
        {
            $target.css('-ms-transform', 'translateX(' + xValue + 'px)');
        }
        else
        {
            $target.css('transform', 'translate3d(' + xValue + 'px,0,0)');
        }
    }

    p._bringToTop= function(target)
    {
        var arr = [];
        this.$items.each(function(){
            if(this !== target)
            {
                arr.push(this);
            }
        });

        arr.forEach(function(item, i){
            $(item).css('z-index', i);
        });
        $(target).css('z-index', arr.length);
    }

    p._checkAutoplay = function()
    {
        if(!this._autoplay)
        {
            return;
        }

        if(this._isPaused)
        {
            return;
        }

        if(this._timerId > -1)
        {
            clearTimeout(this._timerId);
        }

        this._timerId = setTimeout(function(){
            this._timerId = -1;
            this._onNext();
        }.bind(this), this._duration);
    }

    p._onPrev = function(evt)
    {
        if(this._isTweening)
        {
            return;
        }

        var index = this._currentIndex - 1;
        if(index < 0)
        {
            index = this._totalCount - 1;
        }

        this._open(index, -1);
    }

    p._onNext = function(evt)
    {
        if(this._isTweening)
        {
            return;
        }

        var index = this._currentIndex + 1;
        if(index > this._totalCount - 1)
        {
            index = 0;
        }

        this._open(index, 1);
    }

    p._onClickDot = function(evt)
    {
        if($(evt.currentTarget).hasClass('selected'))
        {
            return;
        }

        var index = $(evt.currentTarget).index();
        this._open(index);
    }

    p.prev = function()
    {
        this._onPrev();
    }

    p.next = function()
    {
        this._onNext();
    }

    p.setAutoplay = function(autoplay)
    {
        this._autoplay = autoplay;

        if(autoplay)
        {
            if(this._timerId < 0)
            {
                this._checkAutoplay();
            }
        }
        else
        {
            if(this._timerId > 0)
            {
                clearTimeout(this._timerId);
                this._timerId = -1;
            }
        }
    }

    p.pause = function()
    {
        this._isPaused = true;

        if(this._timerId > 0)
        {
            clearTimeout(this._timerId);
            this._timerId = -1;
        }
    }

    p.resume = function()
    {
        this._isPaused = false;
        this._checkAutoplay();
    }

    p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(!Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) === 'function')
        {
            f.apply(this, funcParams);
        }
    }

    p.dispose = function()
    {
        if(this._disposed) return;
        this._disposed = true;

        this._onSwipeBefore = null;
        this._onSwipeAfter = null;

        $(win).off('resize', this._onResizeHandler);
        this._onResizeHandler = null;

        this.$content.off(this._upEvt).off(this._moveEvt).off(this._downEvt);

        if(this._element)
        {
            this.$element.find('.pt-swipe-prev').off('click');
            this.$element.find('.pt-swipe-next').off('click');
            this.$dotList.off('click');

            this._element.__ptSwipe = null;
            this._element = null;
        }
    }

    $.fn.extend({
        ptSwipe:function (options) {
            this.each(function(){
                var fr = this.__ptSwipe;
                fr && fr.dispose();
                new Swipe(this, options);
            });
            return this;
        },
        ptSwipeCall:function (funcName, funcParams) {
            this.each(function(){
                var fr = this.__ptSwipe;
                fr && fr.execute(funcName, funcParams);
            });
            return this;
        }
    });
}(jQuery, window));


/**
 * Created by qiu on 2017/10/08.
 */
!(function($, win)
{
    "use strict";

    var __bars = [];

    var Bar = function(element, data)
    {
        __bars.push(this);

        this._element = element;
        this._element.__ptBar = this;

        var $element = $(this._element);
        this.$element = $element;

        this.$content = $element.find(data.contentTarget || '.content');
        this._content = this.$content.get(0);

        var $html = $('<div class="pt-bar-group">' + 
            '<div class="pt-bar"></div>' + 
            '</div>');
        $element.append($html);

        this.$barTarget = $html.find('.pt-bar');

        this.$barTarget.on('mousedown', this._onStartDrag.bind(this));

        this._oldMouseY = 0;
        this._currentY = 0;
        this._containerHeight = 0;
        this._scrollRectHeight = 0;
        this._barHeight = 0;
        this._contentHeight = 0;
        this._offsetHeight = 0;

        this._disposed = false;

        this._isDragging = false;

        this._allowCalls = ['resize'];

        this._onResizeHandler = this._onResize.bind(this);
        this._onDraggingHandler = this._onDragging.bind(this);
        this._onStopDragHandler = this._onStopDrag.bind(this);

        $(win).on('resize', this._onResizeHandler);

        this.$content.on('scroll', this._onScroll.bind(this));

        this._onResize();
    }

    var p = Bar.prototype;

    p._onResize = function (evt)
    {
        this._barHeight = this.$barTarget.height();
        this._containerHeight = this.$element.height();
        this._scrollRectHeight = this._containerHeight - this._barHeight;
        this._contentHeight = this._content.scrollHeight;
        this._offsetHeight = this._contentHeight - this._containerHeight;

        if(this._offsetHeight <= 1)
        {
            this.$element.addClass('no-roll');
        }
        else
        {
            this.$element.removeClass('no-roll');
        }
    }

    p._onStartDrag = function (evt)
    {
        if(this._contentHeight <= this._containerHeight)
        {
            return;
        }

        this._isDragging = true;

        evt.preventDefault();
        evt.stopPropagation();

        var $win = $(win);
        $win.off('mousemove', this._onDraggingHandler).on('mousemove', this._onDraggingHandler);
        $win.off('mouseup', this._onStopDragHandler).on('mouseup', this._onStopDragHandler);

        this._oldMouseY = evt.pageY === undefined ? evt.clientY : evt.pageY;

        var scrollTop = this._content.scrollTop;
        var percent = scrollTop / this._offsetHeight;
        this._currentY = this._scrollRectHeight * percent;
    }

    p._onDragging = function (evt)
    {
        if(this._contentHeight <= this._containerHeight)
        {
            return;
        }

        evt.preventDefault();
        evt.stopPropagation();

        var mY = evt.pageY === undefined ? evt.clientY : evt.pageY;
        this._currentY += mY - this._oldMouseY;
        this._oldMouseY = mY;

        if(this._currentY < 0)
        {
            this._currentY = 0;
        }
        else if(this._currentY > this._scrollRectHeight)
        {
            this._currentY = this._scrollRectHeight;
        }

        var percent = this._currentY / this._scrollRectHeight;

        this.$barTarget.css('transform', 'translate3d(0, ' + this._currentY + 'px, 0)');

        this._content.scrollTop = percent * this._offsetHeight;
    }

    p._onStopDrag = function (evt)
    {
        evt.preventDefault();
        evt.stopPropagation();

        this._isDragging = false;

        var $win = $(win);
        $win.off('mousemove', this._onDraggingHandler);
        $win.off('mouseup', this._onStopDragHandler);
    }

    p._onScroll = function(evt)
    {
        if(this._isDragging) return;

        if(this._contentHeight <= this._containerHeight)
        {
            return;
        }

        var scrollTop = this._content.scrollTop;
        var percent = scrollTop / this._offsetHeight;
        var y = this._scrollRectHeight * percent;
        this.$barTarget.css('transform', 'translate3d(0, ' + y + 'px, 0)');
    }

    p.resize = function()
    {
        if(this._disposed) return;
        
        this._onResize();
    }

    p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return null;
        }

        if(!Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) === 'function')
        {
            return f.apply(this, funcParams);
        }
        return null;
    }

    p.dispose = function()
    {
        if(this._disposed) return;
        this._disposed = true;

        var $win = $(win);

        if(this._onResizeHandler)
        {
            $win.off('resize', this._onResizeHandler);
            this._onResizeHandler = null;
        }

        if(this._element)
        {
            this.$element.off('mouseenter mouseleave mousemove wheel');
            this._element.__ptBar = null;
            this._element = null;
        }

        this.$barTarget.off('mousedown', this._onStartDragHandler);
        $win.off('mousemove', this._onDraggingHandler);
        $win.off('mouseup', this._onStopDragHandler);

        if(this.$content)
        {
            this.$content.off('scroll');
        }

        this._onStartDragHandler = null;
        this._onDraggingHandler = null;
        this._onStopDragHandler = null;

        var index = __bars.indexOf(this);
        if(index > -1)
        {
            __bars.splice(index, 1);
        }
    }

    $.fn.extend({
        ptBar:function (obj) {
            this.each(function(){
                var bar = this.__ptBar;
                bar && bar.dispose();
                new Bar(this, obj);
            });
            return this;
        },
        ptBarCall: function(funcName, funcParams){
            var arr = [];
            this.each(function(){
                var bar = this.__ptBar;
                var returnValue = bar && bar.execute(funcName, funcParams);
                arr.push({target:this, value:returnValue});
            });
            return arr;
        }
    });

    win.addEventListener('load', function(evt){
        __bars.forEach(function(bar){
            bar && bar.resize();
        });
    });
}(jQuery, window));


/**
 * Created by foreverpinetree@gmail.com on 2017/05/23.
 */
!(function($, win)
{
    "use strict";

    var Parallax = function(element, obj)
    {
        this._element = element;
        this._element.__ptParallax = this;

        this.$element = $(element);

        createjs.Ticker.timingMode = createjs.Ticker.RAF;

        this._targetQuery = obj.target || '.item';
        this._addClass = obj.addClass || 'pt-parallax';
        this._addTargetClass = obj.addTargetClass || 'pt-parallax-target';
        this._useAnimate = obj.useAnimate == undefined ? true : obj.useAnimate;
        this._animateTime = obj.animateTime == undefined ? 500 : obj.animateTime;
        this._animateEasing = obj.animateEasing || createjs.Ease.cubicOut;

        this._offset = parseFloat(this.$element.data('offset') || 0) || 0;
        this._direct = this.$element.data('direct') == '1' ? 1 : 0;

        if(this._offset < 0)
        {
            this._offset = 0;
        }

        this._eleWidth = 0;
        this._eleHeight = 0;
        this._targetHeight = 0;
        this._offsetHeight = 0;

        this._forceAnimate = false;

        if(this._addClass.indexOf('.') == 0)
        {
            this._addClass = this._addClass.substring(1);
        }

        if(this._addTargetClass.indexOf('.') == 0)
        {
            this._addTargetClass = this._addTargetClass.substring(1);
        }

        this.$element.addClass(this._addClass);

        this._onResizeHandler = null;
        this._onScrollHandler = null;
        this._onAniUpdateHandler = null;

        this._aniValue = 0;

        this.$target = this.$element.find(this._targetQuery);

        if(this.$target.length > 0)
        {
            this._init();
        }
    }

    var p = Parallax.prototype;

    p._init = function()
    {
        if(this._addTargetClass)
        {
            this.$target.addClass(this._addTargetClass);
        }

        var $win = $(win);

        this._onResizeHandler = this._onResize.bind(this);
        $win.on('resize', this._onResizeHandler);

        this._onScrollHandler = this._onScroll.bind(this);
        $win.on('scroll', this._onScrollHandler);

        this._onAniUpdateHandler = this._onAniUpdate.bind(this);

        this._onResize();
    }

    p._onResize = function(evt)
    {
        var winHeight = $(win).height();

        this._eleWidth = this._element.clientWidth;
        this._eleHeight = this._element.clientHeight;
        this._targetHeight = Math.ceil(this._eleHeight + (this._eleHeight * this._offset) * ((winHeight - this._eleHeight) / winHeight));
        this._offsetHeight = Math.ceil(this._eleHeight * this._offset);

        this.$target.css('height', this._targetHeight + 'px');

        this._onScroll();
    }

    p._onScroll = function (evt) 
    {
        var $ele = this.$element, scrollTop = $(win).scrollTop(), offsetTop = $ele.offset().top, winHeight = $(win).height();
        if(offsetTop + this._eleHeight > scrollTop && offsetTop < scrollTop + winHeight)
        {
            var $target = this.$target, y = 0, top, p;

            if(this._direct == 0)
            {
                top = scrollTop - offsetTop;
                p = top / winHeight;
                y = p * this._offsetHeight;
            }
            else
            {
                top = scrollTop - offsetTop - this._eleHeight;
                p = top / (winHeight + this._eleHeight);
                y = -1 * (p + 1) * this._offsetHeight;
            }

            if(this._useAnimate || this._forceAnimate)
            {
                createjs.Tween.get(
                    this,
                    {
                        override:true,
                        onChange:this._onAniUpdateHandler
                    }
                ).to(
                    {_aniValue:y},
                    this._animateTime,
                    createjs.Ease[this._animateEasing] || createjs.Ease.cubicOut
                );
            }
            else
            {
                this._renderY($target, y);
            }
        }
    }

    p._onAniUpdate = function()
    {
        this._renderY(this.$target, this._aniValue);
    }

    p._renderY = function ($target, y)
    {
        y = Math.round(y);
        $target.css('transform', 'translate3d(0,' + y + 'px,0)');
    }

    p.resize = function()
    {
        this._forceAnimate = true;
        this._onResize();
        this._forceAnimate = false;
    }

    p.execute = function(funcName, funcParams)
    {
        if(funcName.indexOf('_') === 0)
        {
            return;
        }

        var f = this[funcName];
        if(f && typeof(f) == 'function')
        {
            f.apply(this, funcParams || []);
        }
    }

    p.dispose = function()
    {
        var $win = $(win);

        if(this._onScrollHandler)
        {
            $win.off('scroll', this._onScrollHandler);
            this._onScrollHandler = null;
        }

        if(this._onResizeHandler)
        {
            $win.off('resize', this._onResizeHandler);
            this._onResizeHandler = null;
        }

        if(this._element)
        {
            this._element.__ptParallax = null;
            this._element = null;
        }

        this._onAniUpdateHandler = null;
    }

    $.fn.extend({
        ptParallax:function (obj) {
            var isDevice = /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);
            var isIE = (!!window.ActiveXObject || "ActiveXObject" in window);
            if(isDevice || isIE)
            {
                return this;
            }

            this.each(function(){
                var parallax = this.__ptParallax;
                parallax && parallax.dispose();

                new Parallax(this, obj || {});
            });
            return this;
        },
        ptParallaxCall: function(funcName, funcParams){
            this.each(function(){
                var parallax = this.__ptParallax;
                parallax && parallax.execute(funcName, funcParams);
            });
            return this;
        }
    });
})(jQuery, window);