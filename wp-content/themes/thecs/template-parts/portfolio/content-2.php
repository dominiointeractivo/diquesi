
<?php
	$thecs_row = thecs_get_page_setting_property( 'portfolio_row', '1' );
	$thecs_boxed = thecs_get_page_setting_property( 'portfolio_use_boxed', '1' ) === '1' ? 'boxed' : '';
	$thecs_item_gap = thecs_get_page_setting_property( 'portfolio_item_gap', '0' ) === '1' ? 'item-gap' : '';
	$thecs_thumb_size = thecs_get_page_setting_property( 'portfolio_thumb_size', '1' );
	$thecs_fixed_classes = array('', 'fixed fixed-h', 'fixed fixed-v', 'fixed fixed-s');
	$thecs_fixed_class = $thecs_fixed_classes[intval($thecs_thumb_size) - 1];

	$thecs_list_class = 'pic-list style-02 row-'.$thecs_row.' '.$thecs_boxed.' '.$thecs_fixed_class.' '.$thecs_item_gap;
?>
<div class="<?php echo esc_attr( $thecs_list_class ); ?>">
	<div class="wrap">
	<?php
		thecs_output_page_title();

		$thecs_portfolio_num = thecs_get_page_setting_property( 'portfolio_num', '12' );
		$thecs_portfolio_per_page = intval( $thecs_portfolio_num );
		if( $thecs_portfolio_per_page == 0 )
		{
			$thecs_portfolio_per_page = 12;
		}

		if ( get_query_var('paged') ) {
		    $thecs_portfolio_paged = get_query_var('paged');
		} elseif ( get_query_var('page') ) {
		    $thecs_portfolio_paged = get_query_var('page');
		} else {
		    $thecs_portfolio_paged = 1;
		}

		$thecs_portfolio_category = thecs_get_page_setting_property( 'portfolio_category', '' );
		
		$thecs_portfolio_query_data = 'project_cat='.$thecs_portfolio_category.'&posts_per_page='.$thecs_portfolio_per_page.'&paged='.$thecs_portfolio_paged.'&post_type=project&suppress_filters=0';
		$thecs_portfolio_query_result = new WP_Query( $thecs_portfolio_query_data );

		if( $thecs_row == '1' )
		{
			$thecs_max_size = $thecs_boxed === 'boxed' ? 1200 : 1920;
		}
		elseif( $thecs_row == '2' )
		{
			$thecs_max_size = 720;
		}
		elseif( $thecs_row == '3' )
		{
			$thecs_max_size = 720;
		}

		if( $thecs_portfolio_query_result->have_posts() )
		{
			while( $thecs_portfolio_query_result->have_posts() )
			{
				$thecs_portfolio_query_result->the_post();
				
				$thecs_post_id = get_the_ID();
				$thecs_post_data = thecs_get_post_setting( $thecs_post_id );
				$thecs_post_title = get_the_title();

				$thecs_banner_type = thecs_get_property( $thecs_post_data, 'banner_type', '1', 'pt_project_' );

				$thecs_thumburl = '';
				$thecs_imageurl = '';

				$thecs_thumb_width = 0;
				$thecs_thumb_height = 0;
				
				if( has_post_thumbnail() )
				{
					$thecs_img_obj = thecs_get_image(array(
						'rid' => get_post_thumbnail_id(),
						'max_width' => $thecs_max_size,
						'max_height' => $thecs_max_size,
					));

					if( $thecs_img_obj )
					{
						$thecs_thumburl = $thecs_img_obj['thumb'];
						$thecs_imageurl = $thecs_img_obj['image'];

						$thecs_thumb_width = $thecs_img_obj['thumb_width'];
						$thecs_thumb_height = $thecs_img_obj['thumb_height'];
					}
				}

				if( !$thecs_thumburl )
				{
					continue;
				}

				$thecs_permalink = get_permalink( $thecs_post_id );

				$thecs_hover_img_url = '';
				$thecs_hover_img_rid = thecs_get_property( $thecs_post_data, 'hover_img', '', 'pt_project_' );
				if( $thecs_hover_img_rid != '' )
				{
					$thecs_img_obj = thecs_get_image(array(
						'rid' => $thecs_hover_img_rid,
						'width' => $thecs_thumb_width,
						'height' => $thecs_thumb_height
					));

					if( $thecs_img_obj )
					{
						$thecs_hover_img_url = $thecs_img_obj['thumb'];
					}
				}

				$thecs_video_link = thecs_get_property( $thecs_post_data, 'video_link', '', 'pt_project_' );
				$thecs_video_type = thecs_get_property( $thecs_post_data, 'video_type', '1', 'pt_project_' );
				$thecs_video_volume = thecs_get_property( $thecs_post_data, 'video_volume', '1', 'pt_project_' );

				$thecs_box_class = 'item';

				if( $thecs_banner_type == '2' )
				{
					$thecs_box_class .= ' v-post';
				}

				?>
				<div class="<?php echo esc_attr( $thecs_box_class ); ?>" data-w="<?php echo esc_attr( $thecs_thumb_width ); ?>" data-h="<?php echo esc_attr( $thecs_thumb_height ); ?>">
					<div class="img">
						<div class="bg-full" data-bg="<?php echo esc_url( $thecs_thumburl ); ?>"></div>
						<?php
							if( $thecs_hover_img_url )
							{
								?><div class="hover-img" data-bg="<?php echo esc_url( $thecs_hover_img_url ); ?>"></div><?php
							}
						?>
						<a class="full" data-href="<?php echo esc_url( $thecs_permalink ); ?>"></a>
					</div>
					<div class="text">
						<div class="list-category">
						<?php
							$thecs_categories = get_the_terms( $thecs_post_id, 'project_cat' );
							if( $thecs_categories )
							{
								foreach($thecs_categories as $thecs_cat)
								{
									?><a href="<?php echo esc_url( get_term_link( $thecs_cat->term_id ) ); ?>"><?php echo esc_html( $thecs_cat->name ); ?></a><?php
								}
							}
						?>
						</div>
						<?php
							if( $thecs_post_title != '' )
							{
								?><h6><a href="<?php echo esc_url( $thecs_permalink ); ?>"><?php echo esc_html( $thecs_post_title ); ?></a></h6><?php
							}
						?>
					</div>
					<div class="mask"></div>
					<?php
						if( $thecs_banner_type == '2' )
						{
							?>
							<div class="list-icon">
								<i class="btn-video" data-src="<?php echo esc_attr( $thecs_video_link ); ?>" data-type="<?php echo esc_attr( $thecs_video_type ); ?>" data-volume="<?php echo esc_attr( $thecs_video_volume ); ?>"></i>
							</div>
							<?php
						}
						else
						{
							$thecs_post_gallery_data = thecs_parse_image_group_data( thecs_get_property( $thecs_post_data, 'post_gallery', '', 'pt_project_' ) );
							$thecs_post_gallery_items = $thecs_post_gallery_data['item'];
							$thecs_post_gallery_json = array(
								'src' => array(),
								'title' => array(),
							);

							foreach( $thecs_post_gallery_items as $thecs_post_gallery_item )
							{
								$thecs_post_gallery_vars = $thecs_post_gallery_item['variables'];

								$thecs_img_obj = thecs_get_image(array(
									'rid' => $thecs_post_gallery_item['rid'],
									'max_width' => 1920
								));
								
								if(	$thecs_img_obj )
								{
									$thecs_post_gallery_json['src'][] = $thecs_img_obj['thumb'];
								}
								else
								{
									continue;
								}

								$thecs_post_gallery_json['title'][] = $thecs_post_gallery_vars['title'];
							}

							if( count( $thecs_post_gallery_json['src'] ) === 0 && $thecs_imageurl )
							{
								$thecs_post_gallery_json['src'][] = $thecs_imageurl;
								$thecs_post_gallery_json['title'][] = '';
							}

							if( count( $thecs_post_gallery_json['src'] ) > 0 )
							{
								?>
								<div class="list-icon">
									<i class="btn-photo" data-src-type="json" data-src="<?php echo esc_attr( wp_json_encode( $thecs_post_gallery_json ) ); ?>"></i>
								</div>
								<?php
							}
						}
					?>
				</div>
				<?php
			}
		}

		echo thecs_paging_nav( $thecs_portfolio_query_result, $thecs_portfolio_paged );
	?>
	</div>
</div>