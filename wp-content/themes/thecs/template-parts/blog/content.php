<div class="wrap">
	<?php

	thecs_output_page_title();

	$thecs_blog_num = thecs_get_page_setting_property( 'blog_num', '12' );
	$thecs_blog_per_page = intval( $thecs_blog_num );
	if( $thecs_blog_per_page == 0 )
	{
		$thecs_blog_per_page = 12;
	}

	$thecs_blog_category = thecs_get_page_setting_property( 'blog_category', '' );

	if ( get_query_var('paged') ) {
	    $thecs_blog_paged = get_query_var('paged');
	} elseif ( get_query_var('page') ) {
	    $thecs_blog_paged = get_query_var('page');
	} else {
	    $thecs_blog_paged = 1;
	}
	
	$thecs_blog_query_data = 'category_name='.$thecs_blog_category.'&posts_per_page='.$thecs_blog_per_page.'&paged='.$thecs_blog_paged.'&suppress_filters=0';
	$thecs_blog_query_result = new WP_Query( $thecs_blog_query_data );

	if( $thecs_blog_query_result->have_posts() )
	{
		while( $thecs_blog_query_result->have_posts() )
		{
			$thecs_blog_query_result->the_post();
			
			$thecs_post_id = get_the_ID();
			$thecs_post_data = thecs_get_post_setting( $thecs_post_id );
			$thecs_post_title = get_the_title();

			$thecs_banner_type = thecs_get_property( $thecs_post_data, 'banner_type', '1' );

			$thecs_thumburl = '';
			$thecs_imageurl = '';

			$thecs_thumb_width = 640;
			$thecs_thumb_height = 384;
			
			if( has_post_thumbnail() )
			{
				if( !$thecs_raw_proportion )
				{
					$thecs_img_obj = thecs_get_image(array(
						'rid' => get_post_thumbnail_id(),
						'width' => $thecs_thumb_width,
						'height' => $thecs_thumb_height,
						'crop_part_vertical' => thecs_get_property( $thecs_post_data, 'image_crop_part', 3 )
					));
				}
				else
				{
					$thecs_img_obj = thecs_get_image(array(
						'rid' => get_post_thumbnail_id(),
						'max_width' => $thecs_thumb_width,
					));
				}

				if( $thecs_img_obj )
				{
					$thecs_thumburl = $thecs_img_obj['thumb'];
					$thecs_imageurl = $thecs_img_obj['image'];

					$thecs_thumb_width = $thecs_img_obj['thumb_width'];
					$thecs_thumb_height = $thecs_img_obj['thumb_height'];
				}
			}

			$thecs_permalink = get_permalink( $thecs_post_id );

			$thecs_video_link = thecs_get_property( $thecs_post_data, 'video_link', '' );
			$thecs_video_type = thecs_get_property( $thecs_post_data, 'video_type', '1' );
			$thecs_video_volume = thecs_get_property( $thecs_post_data, 'video_volume', '1' );

			$thecs_box_class = 'item';

			if( $thecs_banner_type == '2' )
			{
				$thecs_box_class .= ' v-post';
			}

			if( !$thecs_thumburl )
			{
				$thecs_box_class .= ' text-post';
			}

			if( is_sticky() )
			{
				$thecs_box_class .= ' sticky';
			}

			?>
			<div <?php post_class( $thecs_box_class ); ?> data-w="<?php echo esc_attr( $thecs_thumb_width) ;?>" data-h="<?php echo esc_attr( $thecs_thumb_height) ;?>">
				<?php
					if( $thecs_thumburl )
					{
						?>
						<div class="img">
							<div class="bg-full" data-bg="<?php echo esc_url( $thecs_thumburl ); ?>"></div>
							<a class="full" data-href="<?php echo esc_url( $thecs_permalink ); ?>"></a>
							<div class="mask"></div>
							<?php
								if( $thecs_banner_type == '2' )
								{
									?>
									<div class="list-icon">
										<i class="btn-video" data-src="<?php echo esc_attr( $thecs_video_link ); ?>" data-type="<?php echo esc_attr( $thecs_video_type ); ?>" data-volume="<?php echo esc_attr( $thecs_video_volume ); ?>"></i>
									</div>
									<?php
								}
								else
								{
									$thecs_post_gallery_data = thecs_parse_image_group_data( thecs_get_property( $thecs_post_data, 'post_gallery', '' ) );
									$thecs_post_gallery_items = $thecs_post_gallery_data['item'];
									$thecs_post_gallery_json = array(
										'src' => array(),
										'title' => array(),
									);

									foreach( $thecs_post_gallery_items as $thecs_post_gallery_item )
									{
										$thecs_post_gallery_vars = $thecs_post_gallery_item['variables'];

										$thecs_img_obj = thecs_get_image(array(
											'rid' => $thecs_post_gallery_item['rid'],
											'max_width' => 1920
										));
										
										if(	$thecs_img_obj )
										{
											$thecs_post_gallery_json['src'][] = $thecs_img_obj['thumb'];
										}
										else
										{
											continue;
										}

										$thecs_post_gallery_json['title'][] = $thecs_post_gallery_vars['title'];
									}

									if( count( $thecs_post_gallery_json['src'] ) === 0 && $thecs_imageurl )
									{
										$thecs_post_gallery_json['src'][] = $thecs_imageurl;
										$thecs_post_gallery_json['title'][] = '';
									}

									if( count( $thecs_post_gallery_json['src'] ) > 0 )
									{
										?>
										<div class="list-icon">
											<i class="btn-photo" data-src-type="json" data-src="<?php echo esc_attr( wp_json_encode( $thecs_post_gallery_json ) ); ?>"></i>
										</div>
										<?php
									}
								}
							?>
						</div>
						<?php
					}
				?>
				<div class="text">
					<?php
						$thecs_time_link = get_the_title() != '' ? get_month_link( get_the_time('Y'), get_the_time('m'), get_the_time('d')) : $thecs_permalink;
						?><a class="list-date" href="<?php echo esc_url( $thecs_time_link ); ?>"><?php echo esc_html( get_the_date( get_option('date_format', 'F d Y') ) ); ?></a><?php
					?>
					<?php
						if( $thecs_post_title != '' )
						{
							?><h4><a href="<?php echo esc_url( $thecs_permalink ); ?>"><?php echo esc_html( $thecs_post_title ); ?></a></h4><?php
						}

						$thecs_post_description = wp_trim_words( get_the_excerpt(), 45 );

						if( $thecs_post_description )
						{
							?><div class="intro"><?php echo esc_html( $thecs_post_description ); ?></div><?php
						}
					?>
					<div class="list-category">
					<?php
						$thecs_categories = get_the_category();
						if( $thecs_categories )
						{
							foreach($thecs_categories as $thecs_cat)
							{
								?><a href="<?php echo esc_url( get_category_link( $thecs_cat->term_id ) ); ?>"><?php echo esc_html( $thecs_cat->cat_name ); ?></a><?php
							}
						}
					?>
					</div>
					
				</div>
				<a class="more" href="<?php echo esc_url( $thecs_permalink ); ?>"><?php esc_html_e( 'Read More', 'thecs' ); ?></a>
			</div>
			<?php
		}
	}

	echo thecs_paging_nav( $thecs_blog_query_result, $thecs_blog_paged );
	?>
</div>