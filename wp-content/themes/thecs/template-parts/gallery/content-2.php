<?php
	$thecs_row = thecs_get_page_setting_property( 'gallery_row', '1' );
	$thecs_boxed = thecs_get_page_setting_property( 'gallery_use_boxed', '1' ) === '1' ? 'boxed' : '';
	$thecs_item_gap = thecs_get_page_setting_property( 'gallery_item_gap', '0' ) === '1' ? 'item-gap' : '';
	$thecs_thumb_size = thecs_get_page_setting_property( 'gallery_thumb_size', '1' );
	$thecs_fixed_classes = array('', 'fixed fixed-h', 'fixed fixed-v', 'fixed fixed-s');
	$thecs_fixed_class = $thecs_fixed_classes[intval($thecs_thumb_size) - 1];

	$thecs_list_class = 'pic-list gallery style-02 row-'.$thecs_row.' '.$thecs_boxed.' '.$thecs_fixed_class.' '.$thecs_item_gap;
?>
<div class="<?php echo esc_attr( $thecs_list_class ); ?>">
	<div class="wrap">
	<?php
		thecs_output_page_title();

		$thecs_gallery_num = thecs_get_page_setting_property( 'gallery_num', '-1' );
		$thecs_gallery_per_page = intval( $thecs_gallery_num );
		if( $thecs_gallery_per_page == 0 )
		{
			$thecs_gallery_per_page = 12;
		}

		if ( get_query_var('paged') ) {
		    $thecs_gallery_paged = get_query_var('paged');
		} elseif ( get_query_var('page') ) {
		    $thecs_gallery_paged = get_query_var('page');
		} else {
		    $thecs_gallery_paged = 1;
		}

		$thecs_gallery_data = thecs_parse_image_group_data( thecs_get_page_setting_property( 'gallery_data', '' ) );
		$thecs_gallery_items = $thecs_gallery_data['item'];
		$thecs_gallery_data_count = count( $thecs_gallery_items );

		if( $thecs_gallery_per_page === -1 )
		{
			$thecs_gallery_total_page = 1;
			$thecs_gallery_images_data = $thecs_gallery_items;
		}
		else
		{
			$thecs_gallery_total_page = ceil( $thecs_gallery_data_count / $thecs_gallery_per_page );
			$thecs_gallery_data_start = ( intval( $thecs_gallery_paged ) - 1 ) * $thecs_gallery_per_page;
			if( $thecs_gallery_data_start < $thecs_gallery_data_count)
			{
				$thecs_gallery_images_data = array_slice( $thecs_gallery_items, $thecs_gallery_data_start, $thecs_gallery_per_page );
			}
			else
			{
				$thecs_gallery_images_data = array();
			}
		}

		if( $thecs_row == '1' )
		{
			$thecs_max_size = $thecs_boxed === 'boxed' ? 1200 : 1920;
		}
		elseif( $thecs_row == '2' )
		{
			$thecs_max_size = 720;
		}
		elseif( $thecs_row == '3' )
		{
			$thecs_max_size = 720;
		}

		foreach( $thecs_gallery_images_data as $thecs_gallery_item )
		{
			$thecs_gallery_vars = $thecs_gallery_item['variables'];

			$thecs_img_obj = thecs_get_image(array(
				'rid' => $thecs_gallery_item['rid'],
				'max_width' => $thecs_max_size,
				'max_height' => $thecs_max_size,
			));

			if( $thecs_img_obj )
			{
				$thecs_thumburl = $thecs_img_obj['thumb'];
				$thecs_imageurl = $thecs_img_obj['image'];

				?>
				<div class="item" data-w="<?php echo esc_attr( $thecs_img_obj['thumb_width'] ); ?>" data-h="<?php echo esc_attr( $thecs_img_obj['thumb_height'] ); ?>">
					<div class="img">
						<div class="bg-full" data-bg="<?php echo esc_url( $thecs_thumburl ); ?>"></div>
						<a class="full" data-src="<?php echo esc_url( $thecs_imageurl ); ?>" data-title="<?php echo esc_attr( $thecs_gallery_vars['title'] ); ?>"></a>
					</div>

					<div class="text">
						<h6><?php echo esc_html( $thecs_gallery_vars['title'] ); ?></h6>
					</div>
					<div class="mask"></div>
				</div>
				<?php
			}
		}

		echo thecs_paging_nav( $thecs_gallery_total_page, $thecs_gallery_paged );
	?>	
	</div>
</div>