<?php
	$thecs_list_size = thecs_get_page_setting_property( 'gallery_list_size', '1' ) === '1' ? 'size-normal' : 'size-small';
	$thecs_disperse = thecs_get_page_setting_property( 'gallery_disperse', '0' ) === '1' ? 'disperse' : '';
	$thecs_thumb_size = thecs_get_page_setting_property( 'gallery_thumb_size', '1' );
	$thecs_fixed_classes = array('', 'fixed fixed-h', 'fixed fixed-v', 'fixed fixed-s');
	$thecs_fixed_class = $thecs_fixed_classes[intval($thecs_thumb_size) - 1];

	$thecs_list_class = 'pic-list style-01 gallery '.$thecs_list_size.' '.$thecs_disperse.' '.$thecs_fixed_class;

	$thecs_gallery_data = thecs_parse_image_group_data( thecs_get_page_setting_property( 'gallery_data', '' ) );
?>
<div class="<?php echo esc_attr( $thecs_list_class ); ?>">
	<?php
		$thecs_gallery_cats = $thecs_gallery_data['cat'];

		if( count( $thecs_gallery_cats ) > 1 )
		{
			?>
			<div class="list-widgets">
				<i class="call-list-widgets btn"></i>
				<div class="list-widgets-wrap">
					<div class="content">
						<div class="filter">
							<h4><?php esc_html_e( 'Filter', 'thecs' ); ?></h4>
							<ul>
								<li class="filter-all current"><span><?php esc_html_e( 'Show All', 'thecs' ); ?></span></li>
								<?php
									foreach( $thecs_gallery_cats as $thecs_cat_index => $thecs_cat )
									{
										?><li><span data-filter="filter-cat-<?php echo esc_attr( $thecs_cat_index ); ?>"><?php echo esc_html( $thecs_cat ); ?></span></li><?php
									}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
	?>
	<div class="wrap">
	<?php
		thecs_output_page_title();

		$thecs_gallery_num = thecs_get_page_setting_property( 'gallery_num', '-1' );
		$thecs_gallery_per_page = intval( $thecs_gallery_num );
		if( $thecs_gallery_per_page == 0 )
		{
			$thecs_gallery_per_page = 12;
		}

		if ( get_query_var('paged') ) {
		    $thecs_gallery_paged = get_query_var('paged');
		} elseif ( get_query_var('page') ) {
		    $thecs_gallery_paged = get_query_var('page');
		} else {
		    $thecs_gallery_paged = 1;
		}

		$thecs_gallery_items = $thecs_gallery_data['item'];
		$thecs_gallery_data_count = count( $thecs_gallery_items );

		if( $thecs_gallery_per_page === -1 )
		{
			$thecs_gallery_total_page = 1;
			$thecs_gallery_images_data = $thecs_gallery_items;
		}
		else
		{
			$thecs_gallery_total_page = ceil( $thecs_gallery_data_count / $thecs_gallery_per_page );
			$thecs_gallery_data_start = ( intval( $thecs_gallery_paged ) - 1 ) * $thecs_gallery_per_page;
			if( $thecs_gallery_data_start < $thecs_gallery_data_count)
			{
				$thecs_gallery_images_data = array_slice( $thecs_gallery_items, $thecs_gallery_data_start, $thecs_gallery_per_page );
			}
			else
			{
				$thecs_gallery_images_data = array();
			}
		}
				
		foreach( $thecs_gallery_images_data as $thecs_gallery_item )
		{
			$thecs_gallery_vars = $thecs_gallery_item['variables'];

			$thecs_img_obj = thecs_get_image(array(
				'rid' => $thecs_gallery_item['rid'],
				'max_width' => 800,
				'max_height' => 800,
			));

			if( $thecs_img_obj )
			{
				$thecs_thumburl = $thecs_img_obj['thumb'];
				$thecs_imageurl = $thecs_img_obj['image'];

				$thecs_filter_data = 'filter-cat-'.$thecs_gallery_item['cat_index'];

				?>
				<div class="item" data-w="<?php echo esc_attr( $thecs_img_obj['thumb_width'] ); ?>" data-h="<?php echo esc_attr( $thecs_img_obj['thumb_height'] ); ?>" data-filter="<?php echo esc_attr( $thecs_filter_data ); ?>">
					<div class="img">
						<div class="bg-full" data-bg="<?php echo esc_url( $thecs_thumburl ); ?>"></div>
						<a class="full" data-src="<?php echo esc_url( $thecs_imageurl ); ?>" data-title="<?php echo esc_attr( $thecs_gallery_vars['title'] ); ?>"></a>
					</div>

					<div class="text">
						<h6><?php echo esc_html( $thecs_gallery_vars['title'] ); ?></h6>
					</div>
					<div class="mask"></div>
				</div>
				<?php
			}
		}

		echo thecs_paging_nav( $thecs_gallery_total_page, $thecs_gallery_paged );
	?>	
	</div>
</div>