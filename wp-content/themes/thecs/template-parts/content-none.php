<div class="page-other null">
	<div class="text">
		<?php
			$thecs_not_found_content_msg = thecs_get_theme_option('thecs_not_found_content_msg', '');
			if($thecs_not_found_content_msg == '')
			{
				?><h3><?php esc_html_e('It seems we can&rsquo;t find what you&rsquo;re looking for.', 'thecs')?></h3><?php
			}
			else
			{
				thecs_kses_content( $thecs_not_found_content_msg );
			}

		?>
		<h6><?php esc_html_e('Try search', 'thecs'); ?></h6>
		<?php 
			global $thecs_g_searth_form_type;
			$thecs_g_searth_form_type = '3';
			
			get_search_form();
		?>
		<div class="sc-btn style-01">
			<a href="<?php echo esc_url( home_url('/') ); ?>"><?php esc_html_e('Back to homepage', 'thecs'); ?></a>
		</div>
	</div>
</div>