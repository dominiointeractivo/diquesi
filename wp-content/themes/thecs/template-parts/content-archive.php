<?php
	$thecs_raw_proportion = thecs_get_theme_option( 'thecs_archives_img_raw', '1' ) === '1';
	$thecs_raw_class = $thecs_raw_proportion ? 'raw-proportion' : '';
?>
<div class="blog-list style-02 <?php echo esc_attr( $thecs_raw_class ); ?>">
	<div class="wrap">
		<?php

		if ( have_posts() )
		{			
			while ( have_posts() )
			{
				the_post();
				
				$thecs_post_id = get_the_ID();
				$thecs_post_data = thecs_get_post_setting( $thecs_post_id );
				$thecs_post_title = get_the_title();

				$thecs_post_type = get_post_type();
				$thecs_prop_prefix = $thecs_post_type == 'project' ? 'pt_project_' : 'pt_post_';

				$thecs_banner_type = thecs_get_property( $thecs_post_data, 'banner_type', '1', $thecs_prop_prefix );

				$thecs_thumburl = '';
				$thecs_imageurl = '';

				$thecs_thumb_width = 640;
				$thecs_thumb_height = 384;
				
				if( has_post_thumbnail() )
				{
					if( !$thecs_raw_proportion )
					{
						$thecs_img_obj = thecs_get_image(array(
							'rid' => get_post_thumbnail_id(),
							'width' => $thecs_thumb_width,
							'height' => $thecs_thumb_height,
							'crop_part_vertical' => thecs_get_property( $thecs_post_data, 'image_crop_part', 3, $thecs_prop_prefix )
						));
					}
					else
					{
						$thecs_img_obj = thecs_get_image(array(
							'rid' => get_post_thumbnail_id(),
							'max_width' => $thecs_thumb_width,
						));
					}

					if( $thecs_img_obj )
					{
						$thecs_thumburl = $thecs_img_obj['thumb'];
						$thecs_imageurl = $thecs_img_obj['image'];

						$thecs_thumb_width = $thecs_img_obj['thumb_width'];
						$thecs_thumb_height = $thecs_img_obj['thumb_height'];
					}
				}

				$thecs_permalink = get_permalink( $thecs_post_id );

				$thecs_video_link = thecs_get_property( $thecs_post_data, 'video_link', '', $thecs_prop_prefix );
				$thecs_video_type = thecs_get_property( $thecs_post_data, 'video_type', '1', $thecs_prop_prefix );
				$thecs_video_volume = thecs_get_property( $thecs_post_data, 'video_volume', '1', $thecs_prop_prefix );

				$thecs_box_class = 'item';

				if( $thecs_banner_type == '2' )
				{
					$thecs_box_class .= ' v-post';
				}

				if( !$thecs_thumburl )
				{
					$thecs_box_class .= ' text-post';
				}

				if( is_sticky() )
				{
					$thecs_box_class .= ' sticky';
				}

				?>
				<div <?php post_class( $thecs_box_class ); ?> data-w="<?php echo esc_attr( $thecs_thumb_width) ;?>" data-h="<?php echo esc_attr( $thecs_thumb_height) ;?>">
					<?php
						if( $thecs_thumburl )
						{
							?>
							<div class="img">
								<div class="bg-full" data-bg="<?php echo esc_url( $thecs_thumburl ); ?>"></div>
								<a class="full" data-href="<?php echo esc_url( $thecs_permalink ); ?>"></a>
								<div class="mask"></div>
								<?php
									if( $thecs_banner_type == '2' )
									{
										?>
										<div class="list-icon">
											<i class="btn-video" data-src="<?php echo esc_url( $thecs_video_link ); ?>" data-type="<?php echo esc_attr( $thecs_video_type ); ?>" data-volume="<?php echo esc_attr( $thecs_video_volume ); ?>"></i>
										</div>
										<?php
									}
									else
									{
										$thecs_post_gallery_data = thecs_parse_image_group_data( thecs_get_property( $thecs_post_data, 'post_gallery', '', $thecs_prop_prefix ) );
										$thecs_post_gallery_items = $thecs_post_gallery_data['item'];
										$thecs_post_gallery_json = array(
											'src' => array(),
											'title' => array(),
										);

										foreach( $thecs_post_gallery_items as $thecs_post_gallery_item )
										{
											$thecs_post_gallery_vars = $thecs_post_gallery_item['variables'];

											$thecs_img_obj = thecs_get_image(array(
												'rid' => $thecs_post_gallery_item['rid'],
												'max_width' => 1920
											));
											
											if(	$thecs_img_obj )
											{
												$thecs_post_gallery_json['src'][] = $thecs_img_obj['thumb'];
											}
											else
											{
												continue;
											}

											$thecs_post_gallery_json['title'][] = $thecs_post_gallery_vars['title'];
										}

										if( count( $thecs_post_gallery_json['src'] ) === 0 && $thecs_imageurl )
										{
											$thecs_post_gallery_json['src'][] = $thecs_imageurl;
											$thecs_post_gallery_json['title'][] = '';
										}

										if( count( $thecs_post_gallery_json['src'] ) > 0 )
										{
											?>
											<div class="list-icon">
												<i class="btn-photo" data-src-type="json" data-src="<?php echo esc_attr( wp_json_encode( $thecs_post_gallery_json ) ); ?>"></i>
											</div>
											<?php
										}
									}
								?>
							</div>
							<?php
						}
					?>
					<div class="text">
						<?php
							$thecs_time_link = get_the_title() != '' ? get_month_link( get_the_time('Y'), get_the_time('m'), get_the_time('d')) : $thecs_permalink;
							?><a class="list-date" href="<?php echo esc_url( $thecs_time_link ); ?>"><?php echo esc_html( get_the_date( get_option('date_format', 'F d Y') ) ); ?></a><?php
						?>
						<?php
							if( $thecs_post_title != '' )
							{
								?><h4><a href="<?php echo esc_url( $thecs_permalink ); ?>"><?php echo esc_html( $thecs_post_title ); ?></a></h4><?php
							}

							$thecs_post_description = wp_trim_words( get_the_excerpt(), 45 );

							if( $thecs_post_description )
							{
								?><div class="intro"><?php echo esc_html( $thecs_post_description ); ?></div><?php
							}
						?>
						<div class="list-category">
						<?php 
							if( $thecs_post_type === 'project' )
							{
								$thecs_categories = get_the_terms( $thecs_post_id, 'project_cat' );
								if( $thecs_categories )
								{
									foreach($thecs_categories as $thecs_cat)
									{
										?><a href="<?php echo esc_url( get_term_link( $thecs_cat->term_id ) ); ?>"><?php echo esc_html( $thecs_cat->name ); ?></a><?php
									}
								}
							}
							else
							{
								$thecs_categories = get_the_category();
								if( $thecs_categories )
								{
									foreach($thecs_categories as $thecs_cat)
									{
										?><a href="<?php echo esc_url( get_category_link( $thecs_cat->term_id ) ); ?>"><?php echo esc_html( $thecs_cat->cat_name ); ?></a><?php
									}
								}
							}
						?>
						</div>
						
					</div>
					<a class="more" href="<?php echo esc_url( $thecs_permalink ); ?>"><?php esc_html_e( 'Read More', 'thecs' ); ?></a>
				</div>
				<?php
			}
		}

		echo thecs_paging_nav();
	?>
	</div>	
</div>
