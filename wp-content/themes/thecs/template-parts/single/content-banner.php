<?php

	$thecs_banner_class = 'post-banner';

	$thecs_rawsize = thecs_get_property( $thecs_post_data, 'rawsize', '1', $thecs_prop_prefix );
	$thecs_banner_class .= ' '.( $thecs_rawsize === '1' ? 'raw-proportion' : '' );

	if( $thecs_rawsize === '1' )
	{
		$thecs_slides_to_show_type = thecs_get_property( $thecs_post_data, 'slides_to_show', '1', $thecs_prop_prefix );
		$thecs_banner_class .= ' '.( $thecs_slides_to_show_type === '2' ? 'show-one' : '' );
	}
	
	$thecs_parallax = thecs_get_property( $thecs_post_data, 'parallax', '0', $thecs_prop_prefix );

	$thecs_banner_h_class = thecs_get_property( $thecs_post_data, 'height_type', 'h-normal', $thecs_prop_prefix );
	$thecs_banner_class .= ' '.$thecs_banner_h_class;

	$thecs_banner_bg_color = thecs_get_property( $thecs_post_data, 'bg_color', '', $thecs_prop_prefix );

	$thecs_use_original_img = thecs_get_property( $thecs_post_data, 'use_original_img', '0', $thecs_prop_prefix );

	$thecs_banner_caption = thecs_get_property( $thecs_post_data, 'caption_img', '', $thecs_prop_prefix );

	$thecs_image_list = thecs_get_property( $thecs_post_data, 'image_list', '', $thecs_prop_prefix );
	$thecs_img_list_arr = array();

	if( $thecs_image_list )
	{
		$thecs_image_list_data = explode(',', $thecs_image_list);
		foreach( $thecs_image_list_data as $thecs_image_item_id )
		{
			if( $thecs_image_item_id === '' ) continue;

			if( intval( $thecs_use_original_img ) === 1 )
			{
				$thecs_img_obj = thecs_get_image(array(
					'rid' => $thecs_image_item_id,
				));
			}
			else
			{
				$thecs_img_obj = thecs_get_image(array(
					'rid' => $thecs_image_item_id,
					'max_width' => 1920
				));
			}
			
			if( $thecs_img_obj )
			{
				$thecs_img_list_arr[] = array('url' => $thecs_img_obj['thumb'], 'w' => $thecs_img_obj['thumb_width'], 'h' => $thecs_img_obj['thumb_height']);
			}
		}

		if( count($thecs_img_list_arr) > 1 )
		{
			$thecs_banner_class .= ' multi-item';
		}
	}
	else
	{
		if( has_post_thumbnail() )
		{
			if( intval( $thecs_use_original_img ) === 1 )
			{
				$thecs_img_obj = thecs_get_image(array(
					'rid' => get_post_thumbnail_id()
				));
			}
			else
			{
				$thecs_img_obj = thecs_get_image(array(
					'rid' => get_post_thumbnail_id(),
					'max_width' => 1920
				));
			}

			if( $thecs_img_obj )
			{
				$thecs_img_list_arr[] = array('url' => $thecs_img_obj['thumb'], 'w' => $thecs_img_obj['thumb_width'], 'h' => $thecs_img_obj['thumb_height']);
			}
		}
	}

	if( $thecs_parallax == '1' )
	{
		$thecs_banner_class .= ' parallax';
	}

	if( count($thecs_img_list_arr) === 0 )
	{
		$thecs_banner_class .= ' only-text';
	}
	
?>
<div class="<?php echo esc_attr($thecs_banner_class); ?>" data-autoplay="0" data-duration="5000">
	<?php
		if( $thecs_banner_bg_color )
		{
			?><div class="bg-color" data-color="<?php echo esc_attr( $thecs_banner_bg_color ); ?>"></div><?php
		}

		if( $thecs_banner_caption )
		{
			$thecs_img_obj = thecs_get_image(array(
				'rid' => $thecs_banner_caption,
				'max_width' => 1920
			));

			if( $thecs_img_obj )
			{
				?><div class="img-caption"><img src="<?php echo esc_url( $thecs_img_obj['thumb'] ); ?>" alt="<?php esc_attr_e('Image Caption', 'thecs'); ?>" /></div><?php
			}
		}
	?>
	<div class="wrap">
		<?php
			foreach( $thecs_img_list_arr as $thecs_img_obj )
			{
				?>
				<div class="item">
					<div class="bg-full" data-bg="<?php echo esc_url( $thecs_img_obj['url'] ); ?>" data-w="<?php echo esc_attr( $thecs_img_obj['w'] ); ?>" data-h="<?php echo esc_attr( $thecs_img_obj['h'] ); ?>"></div>
				</div>
				<?php
			}
		?>
	</div>
</div>