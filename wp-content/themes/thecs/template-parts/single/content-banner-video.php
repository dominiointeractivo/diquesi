<?php

	$thecs_banner_class = 'post-banner';

	$thecs_rawsize = thecs_get_property( $thecs_post_data, 'rawsize', '1', $thecs_prop_prefix );
	$thecs_banner_class .= ' '.( $thecs_rawsize === '1' ? 'raw-proportion' : '' );

	$thecs_parallax = thecs_get_property( $thecs_post_data, 'parallax', '0', $thecs_prop_prefix );

	$thecs_banner_h_class = thecs_get_property( $thecs_post_data, 'height_type', 'h-normal', $thecs_prop_prefix );
	$thecs_banner_class .= ' '.$thecs_banner_h_class;

	$thecs_banner_bg_color = thecs_get_property( $thecs_post_data, 'bg_color', '', $thecs_prop_prefix );

	$thecs_video_link = thecs_get_property( $thecs_post_data, 'video_link', '', $thecs_prop_prefix );
	$thecs_video_type = thecs_get_property( $thecs_post_data, 'video_type', '1', $thecs_prop_prefix );
	$thecs_video_volume = thecs_get_property( $thecs_post_data, 'video_volume', '1', $thecs_prop_prefix );
	$thecs_video_autoplay = thecs_get_property( $thecs_post_data, 'video_autoplay', '0', $thecs_prop_prefix );

	$thecs_use_original_img = thecs_get_property( $thecs_post_data, 'use_original_img', '0', $thecs_prop_prefix );

	$thecs_banner_caption = thecs_get_property( $thecs_post_data, 'caption_img', '', $thecs_prop_prefix );

	$thecs_video_bg_img = thecs_get_property( $thecs_post_data, 'video_bg_img', '', $thecs_prop_prefix );
	$thecs_video_bg_img_data = array( 'url' => '', 'w' => 1280, 'h' => 720 );

	if( $thecs_video_bg_img )
	{
		if( intval( $thecs_use_original_img ) === 1 )
		{
			$thecs_img_obj = thecs_get_image(array(
				'rid' => $thecs_video_bg_img,
			));
		}
		else
		{
			$thecs_img_obj = thecs_get_image(array(
				'rid' => $thecs_video_bg_img,
				'max_width' => 1920
			));
		}
		
		if( $thecs_img_obj )
		{
			$thecs_video_bg_img_data['url'] = $thecs_img_obj['thumb'];
			$thecs_video_bg_img_data['w'] = $thecs_img_obj['thumb_width'];
			$thecs_video_bg_img_data['h'] = $thecs_img_obj['thumb_height'];
		}
	}
	
	if( !$thecs_video_bg_img_data['url'] )
	{
		if( has_post_thumbnail() )
		{
			if( intval( $thecs_use_original_img ) === 1 )
			{
				$thecs_img_obj = thecs_get_image(array(
					'rid' => get_post_thumbnail_id()
				));
			}
			else
			{
				$thecs_img_obj = thecs_get_image(array(
					'rid' => get_post_thumbnail_id(),
					'max_width' => 1920
				));
			}

			if( $thecs_img_obj )
			{
				$thecs_video_bg_img_data['url'] = $thecs_img_obj['thumb'];
				$thecs_video_bg_img_data['w'] = $thecs_img_obj['thumb_width'];
				$thecs_video_bg_img_data['h'] = $thecs_img_obj['thumb_height'];
			}
		}
	}

	if( $thecs_parallax == '1' )
	{
		$thecs_banner_class .= ' parallax';
	}

	if( !$thecs_video_bg_img_data['url'] )
	{
		$thecs_banner_class .= ' only-text';
	}
	
?>
<div class="<?php echo esc_attr($thecs_banner_class); ?>" data-video-autoplay="<?php echo esc_attr( $thecs_video_autoplay ); ?>">
	<?php
		if( $thecs_banner_bg_color )
		{
			?><div class="bg-color" data-color="<?php echo esc_attr( $thecs_banner_bg_color ); ?>"></div><?php
		}

		if( $thecs_banner_caption )
		{
			$thecs_img_obj = thecs_get_image(array(
				'rid' => $thecs_banner_caption,
				'max_width' => 1920
			));

			if( $thecs_img_obj )
			{
				?><div class="img-caption"><img src="<?php echo esc_url( $thecs_img_obj['thumb'] ); ?>" alt="<?php esc_attr_e('Image Caption', 'thecs'); ?>" /></div><?php
			}
		}
	?>
	<div class="wrap">
		<div class="item">
			<div class="bg-full" data-bg="<?php echo esc_url( $thecs_video_bg_img_data['url'] ); ?>" data-w="<?php echo esc_attr( $thecs_video_bg_img_data['w'] ); ?>" data-h="<?php echo esc_attr( $thecs_video_bg_img_data['h'] ); ?>" data-src="<?php echo esc_attr( $thecs_video_link); ?>" data-type="<?php echo esc_attr( $thecs_video_type); ?>" data-volume="<?php echo esc_attr( $thecs_video_volume); ?>"></div>
		</div>
	</div>
</div>