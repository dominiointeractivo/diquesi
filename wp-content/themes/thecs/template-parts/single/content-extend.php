<div class="post-extend">
<?php
	$thecs_post_gallery_data = thecs_parse_image_group_data( thecs_get_property( $thecs_post_data, 'post_gallery', '', $thecs_prop_prefix ) );
	$thecs_post_gallery_items = $thecs_post_gallery_data['item'];

	foreach( $thecs_post_gallery_items as $thecs_post_gallery_item )
	{
		$thecs_extend_item_class = 'item';
		$thecs_post_gallery_vars = $thecs_post_gallery_item['variables'];

		if( intval( $thecs_post_gallery_vars['gap'] ) === 1 )
		{
			$thecs_extend_item_class .= ' gap';
		}

		$thecs_extend_item_class .= ' text-0'.$thecs_post_gallery_vars['text_type'];
		$thecs_extend_item_class .= ' '.$thecs_post_gallery_vars['display'];

		$thecs_img_obj = thecs_get_image(array(
			'rid' => $thecs_post_gallery_item['rid'],
			'max_width' => 1920
		));
		
		$thecs_post_extend_thumb = '';
		$thecs_post_extend_img = '';
		if(	$thecs_img_obj )
		{
			$thecs_post_extend_thumb = $thecs_img_obj['thumb'];
			$thecs_post_extend_img = $thecs_img_obj['image'];
		}
		else
		{
			continue;
		}

		$thecs_use_video = $thecs_post_gallery_vars['video_type'] != '0';

		?>
		<div class="<?php echo esc_attr( $thecs_extend_item_class ); ?>" data-w="<?php echo esc_attr( $thecs_img_obj['thumb_width'] ); ?>" data-h="<?php echo esc_attr( $thecs_img_obj['thumb_height'] ); ?>">
			<div class="img">
			<?php
				if( $thecs_use_video )
				{
					?><div class="bg-full" data-bg="<?php echo esc_url( $thecs_post_extend_thumb ); ?>" data-src="<?php echo esc_url( $thecs_post_gallery_vars['video_link'] ); ?>" data-type="<?php echo esc_attr( $thecs_post_gallery_vars['video_type'] ); ?>" data-volume="<?php echo esc_attr( $thecs_post_gallery_vars['video_volume'] ); ?>"></div><?php
				}
				else
				{
					?><div class="bg-full" data-bg="<?php echo esc_url( $thecs_post_extend_thumb ); ?>"></div><?php
				}
			?>
			</div>
			<?php
				if( $thecs_post_gallery_vars['title'] != '' || $thecs_post_gallery_vars['des'] != '' )
				{
					?>
					<div class="text">
						<?php
							if( $thecs_post_gallery_vars['title'] != '' )
							{
								?><h2><?php echo esc_html( $thecs_post_gallery_vars['title'] ); ?></h2><?php
							}

							if( $thecs_post_gallery_vars['des'] != '' )
							{
								?><p><?php echo esc_html( $thecs_post_gallery_vars['des'] ); ?></p><?php
							}
						?>
					</div>
					<?php
				}
			?>
		</div>
		<?php
	}
?>
</div>