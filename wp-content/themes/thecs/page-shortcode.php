<?php
/*
 * Template Name: Shortcode
 */
	require THECS_THEME_DIR . '/inc/theme/parse-page-setting.php';

	global $thecs_g_site_bg;
	$thecs_g_site_bg = thecs_get_page_setting_property( 'shortcode_site_bg', '' );

	get_header();

	?>
	<div class="ptsc-list">
		<div class="wrap">
		<?php
			thecs_output_page_title();
			thecs_output_page_shortcode();
		?>
		</div>
	</div>
	<?php
	
	get_footer(); 
?>