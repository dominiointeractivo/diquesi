<?php
/**
 * Template for displaying search forms in thecs
 */
	global $thecs_g_searth_form_type;
	if( !isset($thecs_g_searth_form_type) )
	{
		$thecs_g_searth_form_type = '';
	}

	if( $thecs_g_searth_form_type == '' )
	{
		?>
		<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url('/') ); ?>">
			<label>
				<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'thecs' ); ?>" value="<?php echo esc_attr(get_search_query()); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'thecs' ); ?>" />
			</label>
			<button type="submit" class="search-submit"><span class="screen-reader-text"><?php echo esc_html_x( 'Search', 'submit button', 'thecs' ); ?></span></button>
		</form>
		<?php
	}
	elseif( $thecs_g_searth_form_type == '1' )
	{	
		?>
		<div class="header-search">
			<i class="call-search btn"></i>
			<form role="search" method="get" action="<?php echo esc_url( home_url('/') ); ?>">
				<input type="search" class="search" placeholder="<?php esc_attr_e('Enter a key word here', 'thecs'); ?>" name="s">
				<input type="submit" class="search-btn" value="" />
				<i class="close-search btn"></i>
			</form>
		</div>
		<?php
	}
	elseif ( $thecs_g_searth_form_type == '2' )
	{
		?>
		<form role="search" method="get" action="<?php echo esc_url( home_url('/') ); ?>">
			<input class="m-search" type="search" placeholder="<?php esc_attr_e('Enter a key word here', 'thecs'); ?>" name="s">
			<input type="submit" class="search-btn" value="" />
		</form>
		<?php
	}
	elseif ( $thecs_g_searth_form_type == '3' )
	{
		?>
		<form role="search" method="get" action="<?php echo esc_url( home_url('/') ); ?>">
			<input type="search" class="search-field" placeholder="<?php esc_attr_e('Search &hellip;', 'thecs'); ?>" name="s">
			<button type="submit" class="search-submit"><span class="screen-reader-text"><?php esc_attr_e('Search', 'thecs'); ?></span></button>
		</form>
		<?php
	}

	$thecs_g_searth_form_type = '';//clear global value
?>