	</div>
	<?php
		if( thecs_get_theme_option('thecs_enable_footer', '1') === '1' )
		{
			?>
			<footer>
				<i class="call-footer btn"></i>
				<i class="close-footer btn"></i>

				<div class="wrap">
					<?php
						$thecs_footer_img = thecs_get_theme_option('thecs_footer_img', '');
						$thecs_img_obj = thecs_get_image(array(
							'rid' => $thecs_footer_img,
							'max_width' => 640,
						));
						if( $thecs_img_obj )
						{
							$thecs_img_w = $thecs_img_obj['thumb_width'];
							$thecs_img_h = $thecs_img_obj['thumb_height'];

							?>
							<div class="img">
								<div class="bg-full" data-bg="<?php echo esc_url( $thecs_img_obj['thumb'] ); ?>" data-w="<?php echo esc_attr($thecs_img_w); ?>" data-h="<?php echo esc_attr($thecs_img_h); ?>"></div>
							</div>
							<?php
						}
					?>
					<div class="text">
						<div class="footer_title">
							<h5 class="line"><?php echo esc_html( thecs_get_theme_option('thecs_footer_title', '') ); ?></h5>
							<h5 class="back"><?php echo esc_html( thecs_get_theme_option('thecs_footer_title', '') ); ?></h5>
						</div>
						<div class="text-area">
							<?php thecs_kses_content( thecs_get_theme_option('thecs_footer_content', ''), true, null, true ) ?>
							<div class="pt-social">
								<ul>
									<?php
										$thecs_socials_names = array( 'facebook', 'twitter', 'pinterest', 'flickr', 'instagram', 'linkedin', 'whatsapp', 'behance', 'tumblr', 'googleplus', 'vimeo', 'youtube', 'email' );
										$thecs_social_texts = array(
											'facebook' => '<i class="fa fa-facebook-square"></i>',
											'twitter' => '<i class="fa fa-twitter-square"></i>',
											'pinterest' => '<i class="fa fa-pinterest-square"></i>',
											'flickr' => '<i class="fa fa-flickr"></i>',
											'instagram' => '<i class="fa fa-instagram"></i>',
											'linkedin' => '<i class="fa fa-linkedin-square"></i>',
											'whatsapp' => '<i class="fa fa-whatsapp"></i>',
											'behance' => '<i class="fa fa-behance-square"></i>',
											'tumblr' => '<i class="fa fa-tumblr-square"></i>',
											'googleplus' => '<i class="fa fa-google-plus-square"></i>',
											'vimeo' => '<i class="fa fa-vimeo-square"></i>',
											'youtube' => '<i class="fa fa-youtube-square"></i>',
											'email' => '<i class="fa fa-envelope-square"></i>',
										);

										foreach($thecs_socials_names as $thecs_social_item_name)
										{
											$thecs_social_url = thecs_get_theme_option('thecs_social_' . $thecs_social_item_name, '');
											if($thecs_social_url != '')
											{
												if( $thecs_social_item_name !== 'email' )
												{
													echo '<li><a href="'.esc_attr( $thecs_social_url ).'" target="_blank">'.$thecs_social_texts[ $thecs_social_item_name ].'</a></li>';
												}
												else
												{
													echo '<li><a href="'.esc_attr( $thecs_social_url ).'" class="not-fade-link">'.$thecs_social_texts[ $thecs_social_item_name ].'</a></li>';
												}
											}
										}
									?>
								</ul>
							</div>
						</div>
					</div>

					<div class="copyright"><?php echo esc_html( thecs_get_theme_option('thecs_footer_copyright', esc_html__('&copy;2018 FOREVERPINETREE', 'thecs')) ); ?></div>
					
				</div>
			</footer>
			<?php
		}

		if( thecs_get_theme_option('thecs_enable_hidden_menu', '1') === '1' )
		{
			?>
			<nav class="hidden-menu">
				<i class="close-hidden-menu btn"></i>
				<div class="wrap">
					<div class="pt-widget-list">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</nav>
			<?php
		}

		wp_footer(); 
	?>
</body>
</html>