<?php
	$thecs_list_size = thecs_get_page_setting_property( 'portfolio_list_size', '1' ) === '1' ? 'size-normal' : 'size-small';
	$thecs_disperse = thecs_get_page_setting_property( 'portfolio_disperse', '0' ) === '1' ? 'disperse' : '';
	$thecs_thumb_size = thecs_get_page_setting_property( 'portfolio_thumb_size', '1' );
	$thecs_fixed_classes = array('', 'fixed fixed-h', 'fixed fixed-v', 'fixed fixed-s');
	$thecs_fixed_class = $thecs_fixed_classes[intval($thecs_thumb_size) - 1];
	
	$thecs_list_class = 'pic-list style-01 '.$thecs_list_size.' '.$thecs_disperse.' '.$thecs_fixed_class;

	$thecs_portfolio_category = thecs_get_page_setting_property( 'portfolio_category', '' );
?>
<div class="<?php echo esc_attr( $thecs_list_class ); ?>">
	<?php
		$thecs_portfolio_categories = get_categories(array(
			'taxonomy' => 'project_cat',
		));

		if( $thecs_portfolio_category == '' )
		{
			$thecs_portfolio_filter_enabled = true;
		}
		else
		{
			$thecs_portfolio_filter_comma = strpos( $thecs_portfolio_category, ',' );
			$thecs_portfolio_filter_enabled = $thecs_portfolio_filter_comma !== false && $thecs_portfolio_filter_comma > 0;
		}

		if( $thecs_portfolio_filter_enabled && count( $thecs_portfolio_categories ) > 1 )
		{
			?>
			<div class="list-widgets">
				<i class="call-list-widgets btn"></i>
				<div class="list-widgets-wrap">
					<div class="content">
						<div class="filter">
							<h4><?php esc_html_e( 'Filter', 'thecs' ); ?></h4>
							<ul>
								<li class="filter-all current"><span><?php esc_html_e( 'Show All', 'thecs' ); ?></span></li>
								<?php
									if( $thecs_portfolio_category == '' )
									{
										foreach( $thecs_portfolio_categories as $thecs_cat )
										{
											?><li><span data-filter="filter-cat-<?php echo esc_attr( $thecs_cat->term_id ); ?>"><?php echo esc_html( $thecs_cat->name ); ?></span></li><?php
										}
									}
									else
									{
										$thecs_cats = explode( ',', $thecs_portfolio_category );
										foreach( $thecs_portfolio_categories as $thecs_cat )
										{
											if( in_array($thecs_cat->name, $thecs_cats) )
											{
												?><li><span data-filter="filter-cat-<?php echo esc_attr( $thecs_cat->term_id ); ?>"><?php echo esc_html( $thecs_cat->name ); ?></span></li><?php
											}
										}
									}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
	?>
	<div class="wrap">
	<?php
		thecs_output_page_title();

		$thecs_portfolio_num = thecs_get_page_setting_property( 'portfolio_num', '12' );
		$thecs_portfolio_per_page = intval( $thecs_portfolio_num );
		if( $thecs_portfolio_per_page == 0 )
		{
			$thecs_portfolio_per_page = 12;
		}

		if ( get_query_var('paged') ) {
		    $thecs_portfolio_paged = get_query_var('paged');
		} elseif ( get_query_var('page') ) {
		    $thecs_portfolio_paged = get_query_var('page');
		} else {
		    $thecs_portfolio_paged = 1;
		}
		
		$thecs_portfolio_query_data = 'project_cat='.$thecs_portfolio_category.'&posts_per_page='.$thecs_portfolio_per_page.'&paged='.$thecs_portfolio_paged.'&post_type=project&suppress_filters=0';
		$thecs_portfolio_query_result = new WP_Query( $thecs_portfolio_query_data );

		$numProyect = 1;

		if( $thecs_portfolio_query_result->have_posts() )
		{

			

			while( $thecs_portfolio_query_result->have_posts() )
			{
				$thecs_portfolio_query_result->the_post();
				
				$thecs_post_id = get_the_ID();
				$thecs_post_data = thecs_get_post_setting( $thecs_post_id );
				$thecs_post_title = get_the_title();

				$thecs_banner_type = thecs_get_property( $thecs_post_data, 'banner_type', '1', 'pt_project_' );

				$thecs_thumburl = '';
				$thecs_imageurl = '';

				$thecs_thumb_width = 0;
				$thecs_thumb_height = 0;

				
				
				if( has_post_thumbnail() )
				{
					$thecs_img_obj = thecs_get_image(array(
						'rid' => get_post_thumbnail_id(),
						'max_width' => 800,
						'max_height' => 800,
					));

					if( $thecs_img_obj )
					{
						$thecs_thumburl = $thecs_img_obj['thumb'];
						$thecs_imageurl = $thecs_img_obj['image'];

						$thecs_thumb_width = $thecs_img_obj['thumb_width'];
						$thecs_thumb_height = $thecs_img_obj['thumb_height'];
					}
				}

				if( !$thecs_thumburl )
				{
					continue;
				}

				$thecs_permalink = get_permalink( $thecs_post_id );

				$thecs_hover_img_url = '';
				$thecs_hover_img_rid = thecs_get_property( $thecs_post_data, 'hover_img', '', 'pt_project_' );
				if( $thecs_hover_img_rid != '' )
				{
					$thecs_img_obj = thecs_get_image(array(
						'rid' => $thecs_hover_img_rid,
						'width' => $thecs_thumb_width,
						'height' => $thecs_thumb_height
					));

					if( $thecs_img_obj )
					{
						$thecs_hover_img_url = $thecs_img_obj['thumb'];
					}
				}

				$thecs_video_link = thecs_get_property( $thecs_post_data, 'video_link', '', 'pt_project_' );
				$thecs_video_type = thecs_get_property( $thecs_post_data, 'video_type', '1', 'pt_project_' );
				$thecs_video_volume = thecs_get_property( $thecs_post_data, 'video_volume', '1', 'pt_project_' );

				$thecs_box_class = 'item';

				if( $thecs_banner_type == '2' )
				{
					$thecs_box_class .= ' v-post';
				}

				$thecs_categories = get_the_terms( $thecs_post_id, 'project_cat' );
				$thecs_filter_keys = array();
				if( $thecs_categories )
				{
					foreach( $thecs_categories as $thecs_cat )
					{
						$thecs_filter_keys[] = 'filter-cat-'.$thecs_cat->term_id;
					}
				}

				if( count($thecs_filter_keys) > 0 )
				{
					$thecs_filter_data = implode(' ', $thecs_filter_keys);
				}
				else
				{
					$thecs_filter_data = '';
				}

				?>
				<div class="<?php echo esc_attr( $thecs_box_class ); ?>" data-w="<?php echo esc_attr( $thecs_thumb_width ); ?>" data-h="<?php echo esc_attr( $thecs_thumb_height ); ?>" data-filter="<?php echo esc_attr( $thecs_filter_data ); ?>">
					

					<a class="project_link" href="<?php echo esc_url( $thecs_permalink ); ?>"></a>
					
					<div class="img">
						<!--<div class="bg-full" data-bg="<?php /* echo esc_url( $thecs_thumburl ); */ ?>"></div>-->

						
						    <?php if (is_mobile()) { ?>
					       		
							<?php } else if(is_tablet()) { ?>
					            <div class="bg-full" data-bg="<?php echo get_field( "imagen_para_el_destacado"); ?>"></div>
					  		<?php }else { ?>
					  			<div class="bg-full" data-bg="<?php echo get_field( "imagen_para_el_destacado"); ?>"></div>
							<?php } ?>

						

						<?php
							if( $thecs_hover_img_url )
							{
								?><div class="hover-img" data-bg="<?php echo esc_url( $thecs_hover_img_url ); ?>"></div><?php
							}
						?>
						<a class="full" data-href="<?php echo esc_url( $thecs_permalink ); ?>"></a>
						<?php
							if( $thecs_banner_type == '2' )
							{
								?>
								<div class="list-icon">
									<i class="btn-video" data-src="<?php echo esc_attr( $thecs_video_link ); ?>" data-type="<?php echo esc_attr( $thecs_video_type ); ?>" data-volume="<?php echo esc_attr( $thecs_video_volume ); ?>"></i>
								</div>
								<?php
							}
							else
							{
								$thecs_post_gallery_data = thecs_parse_image_group_data( thecs_get_property( $thecs_post_data, 'post_gallery', '', 'pt_project_' ) );
								$thecs_post_gallery_items = $thecs_post_gallery_data['item'];
								$thecs_post_gallery_json = array(
									'src' => array(),
									'title' => array(),
								);

								foreach( $thecs_post_gallery_items as $thecs_post_gallery_item )
								{
									$thecs_post_gallery_vars = $thecs_post_gallery_item['variables'];

									$thecs_img_obj = thecs_get_image(array(
										'rid' => $thecs_post_gallery_item['rid'],
										'max_width' => 1920
									));
									
									if(	$thecs_img_obj )
									{
										$thecs_post_gallery_json['src'][] = $thecs_img_obj['thumb'];
									}
									else
									{
										continue;
									}

									$thecs_post_gallery_json['title'][] = $thecs_post_gallery_vars['title'];
								}

								if( count( $thecs_post_gallery_json['src'] ) === 0 && $thecs_imageurl )
								{
									$thecs_post_gallery_json['src'][] = $thecs_imageurl;
									$thecs_post_gallery_json['title'][] = '';
								}

								if( count( $thecs_post_gallery_json['src'] ) > 0 )
								{
									?>
									<div class="list-icon">
										<i class="btn-photo" data-src-type="json" data-src="<?php echo esc_attr( wp_json_encode( $thecs_post_gallery_json ) ); ?>"></i>
									</div>
									<?php
								}
							}
						?>

					</div>
					<div class="text">
						<div class="list-category">
							
						

						<?php

							$miCliente = get_field( "cliente" );

							if( $miCliente ) {
							    echo $miCliente;
							}

						?>

						

						</div>

						<div class="data_project">

							<p class="num_project">0<?php echo $numProyect ++ ?>”</p>

							<a href="<?php echo esc_url( $thecs_permalink ); ?>"><h6><?php echo get_field( "copy_para_el_destacado" ); ?></h6></a>
							

							<?php /*
								if( $thecs_post_title != '' )
								{
									?><h6><a href="<?php echo esc_url( $thecs_permalink ); ?>"><?php echo esc_html( $thecs_post_title ); ?></a></h6><?php
								} */
							?>

						</div>

					</div>

					<?php
						$color_top = get_field( "degradado_top" );
						$color_bottom = get_field( "degradado_bottom" );
					?>

					<div class="back" style="background: <?php echo $color_top ?>;
					background: -moz-linear-gradient(top, <?php echo $color_top ?> 0%, <?php echo $color_bottom ?> 100%);
					background: -webkit-linear-gradient(top, <?php echo $color_top ?> 0%, <?php echo $color_bottom ?> 100%);
					background: linear-gradient(to bottom, <?php echo $color_top ?> 0%, <?php echo $color_bottom ?> 100%)
					;filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $color_top ?>;', endColorstr='<?php echo $color_bottom ?>;',GradientType=0 );">

						<?php
							if( $thecs_categories )
							{
								foreach($thecs_categories as $thecs_cat)
								{
									?>
									<!--
									<a href="<?php /* echo esc_url( get_term_link( $thecs_cat->term_id ) ); */ ?>"><?php /* echo esc_html( $thecs_cat->name ); */ ?></a>
									-->

									<h6 class="di_categoria"><?php  echo esc_html( $thecs_cat->name ); ?></h6>
									<?php
								}
							}
						?>



					</div>
					<div class="mask"></div>
				
				</div>
				<?php
			}
		}

		echo thecs_paging_nav( $thecs_portfolio_query_result, $thecs_portfolio_paged );
	?>
	</div>
</div>