<div class="di_row proy-cabecera"  style="background-color:<?php echo $proyGroupField['color_del_proyecto']; ?>" >
    <div class="di_container">
        <div class="proy-cabecera-banner">
            <?php
                if( $proyGroupField['banner'] ): ?>
            		<img src="<?php $proyGroupField['banner']; ?>" />
            	<?php endif; ?>
                
            </div>        
         	<div class="proy-cabecera-copy">
            <?php
                echo  $proyGroupField['copy_cabecera'];
            ?>
        </div>
    </div>
</div>