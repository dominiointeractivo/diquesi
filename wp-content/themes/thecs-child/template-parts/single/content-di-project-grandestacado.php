<div class="di_row proy-gran-destacado" >
    <div class="di_container">
        <div class="proy-gran-destacado-copy">
        	<?php echo $proyGroupField['copy_gran_destacado'];?>
        </div> 
        <div class="proy-gran-destacado-imagen">
            <?php 
            $imagen = $proyGroupField['imagen_destacada'];
            if( $imagen ) {
                echo wp_get_attachment_image( $imagen, "full" );
        	}
        	?>
        </div>
    </div>    
</div>