<div class="di_row proy-datos" >

    <div class="di_container_full">


        <div class="proy-datos-marca">
        	<?php echo $proyGroupField['marca'];?>
        </div> 
        <div class="proy-datos-objetivos">
        	<?php echo $proyGroupField['objetivos'];?>
        </div>
        <div class="proy-datos-cliente">
        	<?php echo $proyGroupField['cliente'];?>
        </div>
        <div class="proy-datos-categoria">
        	<?php echo $proyGroupField['categoria'];?>
        </div>
        <a class="proy-datos-link_al_proyecto" href="<?php echo $proyGroupField['proy-datos-link_al_proyecto'];?>">proy-datos-link_al_proyecto
        	
        </a>
        <div class="proy-datos-imagen_destacada">
            <?php 
            $imagenDestacada = $proyGroupField['imagen_destacada'];
        	$size = 'full'; 
        	if( $imagenDestacada ) {
        	    echo wp_get_attachment_image( $imagenDestacada, $size );
        	}
        	?>
        </div>

        <ul> 
        	<?php $degradado = $proyGroupField['degradado'];?>
        	<li>degradado_left<?php echo $degradado['degradado_left']; ?></li>
        	<li>degradado_right<?php echo $degradado['degradado_right']; ?> </li>
    	</ul>



    </div>
          
</div>