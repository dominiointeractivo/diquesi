<?php
    $color_top = get_field( "degradado_top" );
    $color_bottom = get_field( "degradado_bottom" );
?>

    <style>
        .di_row.row_after:after {
            background: <?php echo the_field('color_de_fondo');
            ?> !important;
        }
        
        .di_row.row_before:before {
            background: <?php echo the_field('color_de_fondo');
            ?> !important;
        }

    </style>


    <div class="proy-projectdetail" style="background-color:<?php echo the_field('color_de_fondo'); ?>">

        <div class="di_row proy-data">

            <div class="di_container_full">

                <div class="proy-projectdetail-information">

                    <div class="di_container_inner proy-projectdetail-information_column proy-projectdetail-information_column_description column_left">
                        <?php 
                	   $proyGroupField = get_field('informacion_del_proyecto');
                     ?>

                            <div class="proy-projectdetail-information_description">
                                <h1 class="di_description_text_line" style="color:<?php echo $color_top ?>;">
                            <?php echo  $proyGroupField['descripcion_del_proyecto']; ?>  
                        </h1>
                                <h1 class="di_description_text_back">
                            <?php echo  $proyGroupField['descripcion_del_proyecto']; ?>
                        </h1>
                            </div>

                            <div class="proy-projectdetail-information-objetivos">
                                <p class="data_item_label" style="color:<?php echo $color_top ?>;">
                                    Objetivos
                                </p>
                                <div>
                                    <?php echo  $proyGroupField['objetivos'];?>
                                </div>
                            </div>
                    </div>


                    <div class="di_container_inner proy-projectdetail-information_column proy-projectdetail-information_column_data column_right" style="background: <?php echo $color_top ?>;
                    background: -moz-linear-gradient(left, <?php echo $color_top ?> 0%, <?php echo $color_bottom ?> 100%);
                    background: -webkit-linear-gradient(left, <?php echo $color_top ?> 0%, <?php echo $color_bottom ?> 100%);
                    background: linear-gradient(to right, <?php echo $color_top ?> 0%, <?php echo $color_bottom ?> 100%)
                    ;filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $color_top ?>;', endColorstr='<?php echo $color_bottom ?>;',GradientType=0 );">

                        <div class="proy-projectdetail-information_data">

                            <p class="data_item_label">Cliente</p>
                            <p class="data_item_value">
                                <?php
                                $miCliente = get_field( "cliente" );
                                if( $miCliente ) {
                                    echo $miCliente;
                                }
                            ?>
                            </p>
                            <p class="data_item_label">Proyecto</p>
                            <p class="data_item_value">
                                <?php
                                if( $thecs_categories ){
                                    foreach($thecs_categories as $thecs_cat){
                                        echo esc_html( $thecs_cat->name ); 
                                    }
                                }
                            ?>
                            </p>
                            <?php
                            $link = $proyGroupField['enlace_del_proyecto'];     
                            if( $link ): 
                            	$link_url = $link['url'];
                            	$link_title = $link['title'];
                            	$link_target = $link['target'] ? $link['target'] : '_self';
                        	?>
                                <a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                                    <?php echo esc_html($link_title); ?>
                                </a>
                                <?php endif; ?>
                        </div>

                        <div class="proy-projectdetail-information_image">

                            <?php if (has_post_thumbnail( $post->ID ) ): ?>
                                <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                                    <img src="<?php echo $image[0]; ?>" alt="<?php echo $proyGroupField['descripcion_del_proyecto']; ?>" title="<?php echo $proyGroupField['descripcion_del_proyecto']; ?>">
                                    <?php endif; ?>

                        </div>
                    </div>



                </div>


            </div>
        </div>

        <?php if( have_rows('row') ): ?>
            <div class="proy-projectdetail-rows">
                <?php while( have_rows('row') ): the_row(); 
        		$projectDetailRowOptions = get_sub_field('opciones_de_fila');
        		
        		$projectDetailRowOptions_BackGroundType = $projectDetailRowOptions["fondo_de_fila"];        		
        		$projectDetailRowStyle ="";
        		if($projectDetailRowOptions_BackGroundType=="color")
        		{
        		    $projectDetailRowStyle .="background-color:".$projectDetailRowOptions["color_de_fondo_de_la_fila"].";";
        		}else if($projectDetailRowOptions_BackGroundType=="imagen")
        		{
                    
        		    $backgroundImg = $projectDetailRowOptions["imagen_de_fondo_de_la_fila"];
        		    $projectDetailRowStyle .="background-image:url('".$backgroundImg."')";
        		}else if($projectDetailRowOptions_BackGroundType=="degradado")
                {
                    $backgroundImg = $projectDetailRowOptions["direccion_de_degradado_de_la_fila"];
                    $projectDetailRowStyle .="background: -moz-linear-gradient(".$projectDetailRowOptions["direccion_de_degradado_de_la_fila"].", ".$color_top." 0%," .$color_bottom." 100%); background: -webkit-linear-gradient(".$projectDetailRowOptions["direccion_de_degradado_de_la_fila"].", ".$color_top." 0%," .$color_bottom." 100%);";  
                }
        		
        		$projectDetailRowOptions_CustomId = $projectDetailRowOptions["id_personalizada_de_fila"];
        		$rowId="";
        		if($projectDetailRowOptions_CustomId)
        		    $rowId .= ' id="'.$projectDetailRowOptions_CustomId.'" ';
    		    
        		$projectDetailRowOptions_CustomClass = $projectDetailRowOptions["class_personalizada_de_fila"];
    		    $rowCss="";
    		    if($projectDetailRowOptions_CustomClass)
    		        $rowCss .= $projectDetailRowOptions_CustomClass;

                $projectDetailRowOptions_CustomStyles = $projectDetailRowOptions["css_personalizado_de_fila"];
                $rowStyles="";
                if($projectDetailRowOptions_CustomStyles)
                    $rowStyles .= ' css="'.$projectDetailRowOptions_CustomStyles.'" ';
            ?>
                    <div class="proy-projectdetail-row di_row <?php echo $rowCss; ?>" style="<?php echo $projectDetailRowStyle;?> <?php echo $projectDetailRowOptions_CustomStyles;?> ">

                        <div class="di_container">
                            <div class="di_container_inner">
                                <?php $projectDetailRowType = get_sub_field('tipo_de_fila');
            			/*echo "<h1>".$projectDetailRowType."</h1>";*/
            			switch ($projectDetailRowType) {
                            case "Solo texto":
                                $groupContent = get_sub_field('fila_solo_texto');
                                ?>
                                    <div class="proy-projectdetail-row-text">
                                        <?php 
                                    echo $groupContent['texto_de_la_fila'];
                                ?>
                                    </div>
                                    <?php 
                                break;
                            
                            case "Solo imagen":
                                $groupContent = get_sub_field('fila_solo_imagen');
                                $images = $groupContent['imagenes_fila_solo_imagen'];
                                if( $images ):								?>
                                        <?php
    										foreach($images as $img){
                                                $groupContentImg = null;
                                                $groupContentImg_alt = null;
                                                $groupContentImg_src = null;
                                                    $imgCount = count($img);
                                                    foreach($img as $subimg){							
        												
        												$imgContent = $subimg;
        												$groupContentImg=$subimg["imagen"];
        												$groupContentImg_alt = ($imgContent['alt_para_la_imagen']!=null ? $imgContent['alt_para_la_imagen'] : "");
                                                        $groupContentImg_src = ($imgContent['link_para_la_imagen']!=null ? $imgContent['link_para_la_imagen'] : "");
     
                                                            if( $groupContentImg_src): 
                                                                $groupContentImg_src_url = $groupContentImg_src['url'];
                                                                $groupContentImg_src_title = $groupContentImg_src['title'];
                                                                $groupContentImg_src_target = $groupContentImg_src['target'] ? $groupContentImg_src['target'] : '_self';
                                                            endif; 
                                                        ?>
                                            <div class="image_box" style="width: <?php echo 100/$imgCount ?>%;">
                                                <?php if($groupContentImg_src != null){ ?>
                                                    <a href="<?php echo $groupContentImg_src_url ?>" target="$groupContentImg_src_target" title="<?php echo $groupContentImg_src_title; ?>">
                                                        <?php } ?>

                                                            <img src="<?php echo $groupContentImg; ?>" alt="<?php echo $groupContentImg_alt; ?>" />

                                                            <?php if($groupContentImg_src != null){ ?>
                                                    </a>
                                                    <?php } ?>
                                            </div>
                                            <?php } ?>
                                                <?php } ?>
                                                    <?php 
                                endif;
                                break;                           
                            case "Texto e imagen":
                                $groupContent = get_sub_field('fila_texto_e_imagen');
                                ?>
                                                        <div class="text_wrap">
                                                            <div class="proy-projectdetail-row-text">
                                                                <style>
                                                                    .text_wrap .proy-projectdetail-row-text h2,
                                                                    .text_wrap .proy-projectdetail-row-text h2.h {
                                                                        color: <?php echo $color_top ?> !important;
                                                                    }

                                                                </style>
                                                                <?php
                                            echo $groupContent['texto'];
                                        ?>

                                                            </div>
                                                        </div>
                                                        <?php 
                                                            
                                $images = $groupContent['imagenes_fila'];
                                if( $images ):                              ?>
                                                            <?php
                                            foreach($images as $img){
                                                $groupContentImg = null;
                                                $groupContentImg_alt = null;
                                                $groupContentImg_src = null;
                                                    $imgCount = count($img);
                                                    foreach($img as $subimg){                           
                                                        
                                                        $imgContent = $subimg;
                                                        $groupContentImg=$subimg["imagen"];
                                                        $groupContentImg_alt = ($imgContent['alt_para_la_imagen']!=null ? $imgContent['alt_para_la_imagen'] : "");
                                                        $groupContentImg_src = ($imgContent['link_para_la_imagen']!=null ? $imgContent['link_para_la_imagen'] : "");
     
                                                            if( $groupContentImg_src): 
                                                                $groupContentImg_src_url = $groupContentImg_src['url'];
                                                                $groupContentImg_src_title = $groupContentImg_src['title'];
                                                                $groupContentImg_src_target = $groupContentImg_src['target'] ? $groupContentImg_src['target'] : '_self';
                                                            endif; 
                                                        ?>
                                                                <div class="image_box" style="width: <?php echo (100 - 33.33)/$imgCount ?>%;">
                                                                    <?php if($groupContentImg_src != null){ ?>
                                                                        <a href="<?php echo $groupContentImg_src_url ?>" target="$groupContentImg_src_target" title="<?php echo $groupContentImg_src_title; ?>">
                                                                            <?php } ?>

                                                                                <img src="<?php echo $groupContentImg; ?>" alt="<?php echo $groupContentImg_alt; ?>" />

                                                                                <?php if($groupContentImg_src != null){ ?>
                                                                        </a>
                                                                        <?php } ?>
                                                                </div>
                                                                <?php } ?>
                                                                    <?php } ?>
                                                                        <?php 
                                endif;                           
                                
                                break;
                           
                            case "Imagen y texto":
                                $groupContent = get_sub_field('fila_imagen_y_texto');
                                $images = $groupContent['imagenes_fila_copiar'];
                                if( $images ):                              ?>
                                                                            <?php
                                            foreach($images as $img){
                                                $groupContentImg = null;
                                                $groupContentImg_alt = null;
                                                $groupContentImg_src = null;
                                                    $imgCount = count($img);
                                                    foreach($img as $subimg){                           
                                                        
                                                        $imgContent = $subimg;
                                                        $groupContentImg=$subimg["imagen"];
                                                        $groupContentImg_alt = ($imgContent['alt_para_la_imagen']!=null ? $imgContent['alt_para_la_imagen'] : "");
                                                        $groupContentImg_src = ($imgContent['link_para_la_imagen']!=null ? $imgContent['link_para_la_imagen'] : "");
     
                                                            if( $groupContentImg_src): 
                                                                $groupContentImg_src_url = $groupContentImg_src['url'];
                                                                $groupContentImg_src_title = $groupContentImg_src['title'];
                                                                $groupContentImg_src_target = $groupContentImg_src['target'] ? $groupContentImg_src['target'] : '_self';
                                                            endif; 
                                                        ?>
                                                                                <div class="image_box" style="width: <?php echo (100 - 33.33)/$imgCount ?>%;">
                                                                                    <?php if($groupContentImg_src != null){ ?>
                                                                                        <a href="<?php echo $groupContentImg_src_url ?>" target="$groupContentImg_src_target" title="<?php echo $groupContentImg_src_title; ?>">
                                                                                            <?php } ?>

                                                                                                <img src="<?php echo $groupContentImg; ?>" alt="<?php echo $groupContentImg_alt; ?>" />

                                                                                                <?php if($groupContentImg_src != null){ ?>
                                                                                        </a>
                                                                                        <?php } ?>
                                                                                </div>
                                                                                <?php } ?>
                                                                                    <?php } ?>
                                                                                        <?php 
                                endif;                           
                                
                                ?>
                                                                                            <div class="text_wrap">
                                                                                                <div class="proy-projectdetail-row-text">
                                                                                                    <style>
                                                                                                        .text_wrap .proy-projectdetail-row-text h2,
                                                                                                        .text_wrap .proy-projectdetail-row-text h2.h {
                                                                                                            color: <?php echo $color_top ?> !important;
                                                                                                        }

                                                                                                    </style>

                                                                                                    <?php
                                        echo $groupContent['texto_copiar'];
                                    ?>
                                                                                                </div>
                                                                                            </div>
                                                                                            <?php    
                            break;
                        } 
                        ?>

                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
            </div>
            <?php endif; ?>
    </div>
