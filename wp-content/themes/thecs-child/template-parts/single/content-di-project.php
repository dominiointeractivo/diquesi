<?php
$cabecera = get_field("cabecera");
if( $cabecera ): 
?>
<div class="di_row proy">

    <div class="di_container">
        <div class="proy-cabecera"  style="background-color:<?php echo $cabecera['color_del_proyecto']; ?>" >
            <div class="proy-cabecera-banner">
            <?php
                echo $cabecera['banner'];
            ?>
            </div>        
         	<div class="proy-cabecera-copy">
            <?php
                echo  $cabecera['copy_cabecera'];
            ?>
            </div>
        </div>
    </div>
    
</div>
<?php endif; ?>