<div class="proy-siguiente" style="background-color:<?php echo $proyGroupField['color_siguiente']; ?>">
    <div class="proy-siguiente-copy">
    	<?php echo $proyGroupField['copy_siguiente'];?>
    </div> 
    
    <div class="proy-siguiente-imagen">
        <?php 
        $imagen = $proyGroupField['fondo_siguiente'];
        if( $imagen ) {
            echo wp_get_attachment_image( $imagen, "full" );
    	}
    	?>
    </div> 
    <div class="proy-siguiente-detalle">
        <?php
        // revisar ACF si realmente debe ser un tipo imagen        
        $imagen = $proyGroupField['detalle_siguiente'];
        if( $imagen ) {
            echo wp_get_attachment_image( $imagen, "full" );
    	}
    	?>
    </div>       
</div>