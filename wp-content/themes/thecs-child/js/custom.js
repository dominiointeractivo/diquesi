jQuery(function($){

	console.log('Custom load');

	
	// REMPLACE SVG ******************************************************************************************
    // (replace <img class=".svg"... for <svg...)
	
	window.replaceForSvgInline = function(){

	    // Replace all SVG images with inline SVG
	   
	    var counter = 0;
	   
	    $('img.svg').each(function(){

	    	counter ++

	        var $img = $(this);
	        var imgID = $img.attr('id');
	        var imgClass = $img.attr('class');
	        var imgURL = $img.attr('src');

	        $.get(imgURL, function(data) {
	            // Get the SVG tag, ignore the rest
	            var $svg = $(data).find('svg');

	            // Add replaced image's ID to the new SVG
	            if(typeof imgID !== 'undefined') {
	                $svg = $svg.attr('id', imgID);
	            }
	            // Add replaced image's classes to the new SVG
	            if(typeof imgClass !== 'undefined') {
	                $svg = $svg.attr('class', imgClass+' replaced-svg');
	            }

	            // Remove any invalid XML tags as per http://validator.w3.org
	            $svg = $svg.removeAttr('xmlns:a');

	            // Replace image with new SVG
	            $img.replaceWith($svg);

	            //console.log('Remplaza svg ' + counter);

	        }, 'xml');

	    });


	}



	window.setColorLogo = function(){

		if($('.single-project').length > 0){
			
			$darkColor = $('#darkColor').text();
			$lightColor = $('#lightColor').text();

			var ruleLogoDark = "#Logo_bubble {fill: " + $darkColor + " ;}";
			var ruleLogoLight = ".di_logo_text_line {color: " + $lightColor + " ;}";
            
            var ruleMenuDark = "header.style-05 .wrap>i.btn {color: " + $darkColor + " ;}";
			var ruleMenuLight = "header.style-05 .wrap>i.btn:hover:before {color: " + $lightColor + " !important;}";
			
			$("<div />", { html: '<style>' + ruleLogoDark + ruleLogoLight + ruleMenuDark + ruleMenuLight +'</style>'}).appendTo("body");

		}
	}

	window.onload = function() {
		replaceForSvgInline();
		setColorLogo();
	}


	/*SMOOTH SCROLL FOR CONTACT LINKS*/

	if($( window ).width()>1023){

		$('.page-id-507 .sc-btn a').bind('click',function(event){
	        var $anchor = $(this);
	        
	        $('.ptsc-list').stop().animate({
	            scrollLeft: $($anchor.attr('href')).offset().left
	        }, 1500);
	        event.preventDefault();
	    });
	    
	}else{

		$('.page-id-507 .sc-btn a').bind('click',function(event){
			var $anchor = $(this);

			$('html, body').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top
			}, 1500);
			event.preventDefault();
		});
	}



});
