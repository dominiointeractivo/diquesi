<?php
	get_header();

	while( have_posts() )
	{
		the_post();
		
		$thecs_post_id = get_the_ID();
		$thecs_post_data = thecs_get_post_setting( $thecs_post_id );
		$thecs_post_type = get_post_type();
		$thecs_prop_prefix = $thecs_post_type == 'project' ? 'pt_project_' : 'pt_post_';
		$thecs_single_type = thecs_get_property( $thecs_post_data, 'type', '1', $thecs_prop_prefix );
		$thecs_banner_type = thecs_get_property( $thecs_post_data, 'banner_type', '1', $thecs_prop_prefix );
		$thecs_text_typeset = thecs_get_property( $thecs_post_data, 'text_typeset', '2', $thecs_prop_prefix );		
		   
		?>

		<span id="darkColor" style="display:none;"><?php echo the_field('degradado_top'); ?></span>
		<span id="lightColor" style="display:none;"><?php echo the_field('degradado_bottom'); ?></span>

		<script>

			jQuery(document).ready(function( $ ){

			});

		</script>

		<div class="post-detail-wrap" > <!--style="background-color:<?php echo get_field("color_de_fondo"); ?>"-->                 
			
			<main id="post-main" class="post-0<?php echo esc_attr( $thecs_single_type ); ?> <?php echo esc_attr( $thecs_banner_type === '2' ? 'v-post' : '' ); ?>">
			<?php

				if( $thecs_banner_type === '2' )
				{
					require thecs_parse_part_path( '/template-parts/single/content-banner-video' );
				}
				else
				{
					require thecs_parse_part_path( '/template-parts/single/content-banner' );
				}
				
				?>
				<!--
				<div class="post-bar">
					<div class="wrap">
						<?php
							$thecs_post_socials_str = thecs_post_socials( $thecs_post_id );
							if( $thecs_post_socials_str != '' )
							{
								?>
								<div class="share">
									<i class="btn"><?php esc_html_e('Share', 'thecs'); ?></i>
									<div class="wrap">
										<?php thecs_kses_content( $thecs_post_socials_str ); ?>
									</div>
								</div>
								<?php
							}
						?>
						
						<div class="post-meta">
							<div class="wrap">
								<div class="time">
									<i><?php esc_html_e('Date', 'thecs'); ?></i>
									<a href="<?php echo esc_url(get_month_link( get_the_time('Y'), get_the_time('m'), get_the_time('d'))); ?>"><?php the_date(get_option('date_format', 'd M,Y')); ?> </a>
								</div>
								<div class="author">
									<i><?php esc_html_e('Author', 'thecs'); ?></i>
									<a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><?php the_author(); ?></a>
								</div>
								<?php
									$thecs_categories = $thecs_post_type == 'project' ? get_the_terms( $thecs_post_id, 'project_cat' ) : get_the_category();
									if( $thecs_categories && !is_wp_error( $thecs_categories ) && count( $thecs_categories ) > 0 )
									{
										?>
										<div class="post-category">
											<i><?php esc_html_e('Categories', 'thecs'); ?></i>
											<?php 
												if( $thecs_post_type == 'project' )
												{
													foreach( $thecs_categories as $thecs_cat )
													{
														?><a href="<?php echo esc_url( get_category_link( $thecs_cat->term_id ) ); ?>"><?php echo esc_html( $thecs_cat->name ); ?></a><?php
													}
												}
												else
												{
													foreach( $thecs_categories as $thecs_cat )
													{
														?><a href="<?php echo esc_url( get_category_link( $thecs_cat->term_id ) ); ?>"><?php echo esc_html( $thecs_cat->cat_name ); ?></a><?php
													}
												}
											?>
										</div>
										<?php
									}
								?>
							</div>
						</div>
						
					</div>
					<div class="elem-line"></div>
				</div>
				-->
				<!-- bar-end -->

				<div class="post-content">

					<div class="post-wrap">

						<?php

							$proyGroupField = get_field("informacion_del_proyecto");	   
							if( $proyGroupField ):	   
								require thecs_parse_part_path( '/template-parts/single/content-di-projectdetail' );	   
								$proyGroupField=null;	   
							endif;	

							/*
							$proyGroupField = get_field("cabecera");		
							if( $proyGroupField ): 		  
								require thecs_parse_part_path( '/template-parts/single/content-di-project-cabecera' );		 
							endif;	 

							$proyGroupField = get_field("datos_de_proyecto");	   
							if( $proyGroupField ):		 
								require thecs_parse_part_path( '/template-parts/single/content-di-project-datos' );		 
								$proyGroupField=null;	   
							endif; 	   	   

							$proyGroupField = get_field("detalle_proyecto_1");	   
							if( $proyGroupField ):	       
								require thecs_parse_part_path( '/template-parts/single/content-di-project-detalle' );	       
								$proyGroupField=null;	  
							endif;	   	   

							$proyGroupField = get_field("detalle_proyecto_2");	   
							if( $proyGroupField ):	       
								require thecs_parse_part_path( '/template-parts/single/content-di-project-detalle' );	       
								$proyGroupField=null;	   
							endif;	   	   

							$proyGroupField = get_field("gran_destacado");	   
							if( $proyGroupField ):	       
								require thecs_parse_part_path( '/template-parts/single/content-di-project-grandestacado' );	       
								$proyGroupField=null;	   
							endif;	   	   

							$proyGroupField = get_field("siguiente");	   
							if( $proyGroupField ):	       
								require thecs_parse_part_path( '/template-parts/single/content-di-project-siguiente' );	       
								$proyGroupField=null;	   
							endif;	   	   
							*/
							

						?>

					</div>

				</div>
			<!--
				<div class="post-info text-style-0<?php echo esc_attr( $thecs_text_typeset ); ?>">
					<div class="wrap">
						<div class="title"><h1><?php the_title(); ?></h1></div>
						<div class="text-area"><?php the_content(); ?></div>
						<?php
							wp_link_pages('before=<div class="page-links">&after=</div>&next_or_number=next&previouspagelink='.esc_html__('Prev Page', 'thecs').'&nextpagelink='.esc_html__('Next Page', 'thecs'));
						?>
					</div>
				</div>
			-->
				<!-- detail text -->
				<?php
					require thecs_parse_part_path( '/template-parts/single/content-extend' );

					$thecs_shortcode_text = thecs_get_property( $thecs_post_data, 'shortCodeText', '', 'pt_sc_' );
					if( $thecs_shortcode_text && $thecs_single_type === '2' )
					{
						?>
						<div class="ptsc-list">
							<div class="wrap">
							<?php
								
								echo do_shortcode( shortcode_unautop( wp_specialchars_decode( stripslashes( $thecs_shortcode_text ), ENT_QUOTES ) ) );
							?>
							</div>
						</div>
						<?php
					}

					$thecs_show_comment = $thecs_post_type == 'project' ? thecs_get_theme_option('thecs_show_project_comment', '0') : thecs_get_theme_option('thecs_show_comment', '1');
					if ( $thecs_show_comment != '0' && $thecs_show_comment != '' && ( comments_open() || get_comments_number() ) )
					{
						comments_template();
					}

					$thecs_prev_post = get_previous_post();
					$thecs_next_post = get_next_post();
				?>
				<div class="post-footer">
					<div class="wrap">
						<?php
							if( $thecs_post_type == 'project' )
							{
								$thecs_post_tags = get_the_terms( $thecs_post_id, 'project_tag' );
							}
							else
							{
								$thecs_post_tags = get_the_tags();
							}

							if( $thecs_post_tags && count( $thecs_post_tags ) > 0 )
							{
								?>
								<div class="post-tags">
									<span><?php esc_html_e('TAGS', 'thecs'); ?></span>
									<?php
										foreach( $thecs_post_tags as $thecs_post_tag )
										{
											?><a href="<?php echo esc_url( get_tag_link( $thecs_post_tag->term_id ) ); ?>"><?php echo esc_html( $thecs_post_tag->name ); ?></a><?php
										}
									?>
								</div>
								<?php
							}
						?>
						<div class="post-nav">
							<?php
								if( $thecs_prev_post )
								{
									$thecs_pn_permalink = get_permalink( $thecs_prev_post->ID );
									?>
									<div class="post-prev ctrl">
										<div class="text">
											<span><?php esc_html_e('Anterior', 'thecs'); ?></span>
											<h5><?php echo esc_html( $thecs_prev_post->post_title ); ?></h5>
										</div>
										<a class="full" href="<?php echo esc_url( $thecs_pn_permalink ); ?>"></a>
									</div>
									<?php
								}

								if( $thecs_next_post )
								{
									$thecs_pn_permalink = get_permalink( $thecs_next_post->ID );
									?>
									<div class="post-next ctrl">
										<div class="text">
											<span><?php esc_html_e('Siguiente', 'thecs'); ?></span>
											<h5><?php echo esc_html( $thecs_next_post->post_title ); ?></h5>
										</div>
										<a class="full" href="<?php echo esc_url( $thecs_pn_permalink ); ?>"></a>
									</div>
									<?php
								}
							?>
						</div>
						<div class="post-related">
							<h2 class="title"><?php esc_html_e('Relacionados', 'thecs'); ?></h2>
							<div class="related-list">
								<?php
									$thecs_related_show_num = 3;

									$thecs_related_posts = thecs_get_related_posts_data( $thecs_post_id, $thecs_post_type, $thecs_related_show_num, $thecs_single_type );
									if( count($thecs_related_posts) == $thecs_related_show_num )
									{
										foreach( $thecs_related_posts as $thecs_related_post )
										{
											$thecs_related_imageurl = $thecs_related_post['imageurl'];
											if( $thecs_related_imageurl == '' )
											{
												continue;
											}
											?>
											<div class="item" data-w="<?php echo esc_attr( $thecs_related_post['w'] ); ?>" data-h="<?php echo esc_attr( $thecs_related_post['h'] ); ?>">
												<div class="img">
													<div class="mask_related">
														<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mascara_relacionados.png">
													</div>
													<div class="bg-full" data-bg="<?php echo esc_url( $thecs_related_imageurl ); ?>"></div>
												</div>
												<a class="full" href="<?php echo esc_url( $thecs_related_post['link'] ); ?>"></a>
												<div class="text">
													<div class="category">
														<?php thecs_kses_content( implode( '', $thecs_related_post['tags'] ) ); ?>
													</div>
													<h5><?php echo esc_html( $thecs_related_post['title'] ); ?></h5>
												</div>
											</div>
											<?php
										}
									}
								?>
							</div>
						</div>
					</div>
				</div>
			</main>
		</div>
		<?php
	};
	
	get_footer(); 
?>