<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dominiointeractivo_com_db2019');

/** MySQL database username */
define('DB_USER', 'di_dbadm2019');

/** MySQL database password */
define('DB_PASSWORD', 'bdWMZWu1EyuDqUIJ');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'BqQhIN?/s_E_ DVa2kQv~k%qfh*H`*UDtgPu|sdB!5Z2$V)r:|:Qrji*AI( ,;LY');
define('SECURE_AUTH_KEY',  ' XFqgwO:%5!)3mR~jcO8nU3TD[8|(Yk*m0&{j;3,=`/*x:Fw=-<zQY9tRNI<ztI*');
define('LOGGED_IN_KEY',    'H]35gd+$GU+2<IIiWfx$ZK8aFT_<*^W+{-soJzpmGhm-_}{^~BVpUe=^Q4_eFk%W');
define('NONCE_KEY',        '(I_Ogi<}sZg6wTQ~`JW?VUHKMg6>Pftf`)fUET%#H2&8+/-NcJ1x6{t5fFk2>J&0');
define('AUTH_SALT',        'Yd8|<Ih,L9S}aB+1?(36fbcT7g5Ofjb q&4hO||-=h(VRA!jz.xW.SV2 hICAB;l');
define('SECURE_AUTH_SALT', 'N>$f)~<jjj#VlMj,yzX8DFB9sD[]`G3HKoFnjt06q.#hoo1gp !9Kq]f1*FjpSd;');
define('LOGGED_IN_SALT',   '+@27!vlE//DI0$_#GD5]RX72.]tq7}7(4]fwsQ!63Flm{ip%we~QBs>xa.O=j.C!');
define('NONCE_SALT',       'x.=J[vd+>RTe`XZ>X5K<F-_n~[ycE 8q4jgn`q{7!<3xVXhsBF+}vlh-8Zn/mI8?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_di_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
